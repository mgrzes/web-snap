-- This function takes names of all state features of the snap environment and adds one column to the t_join_states table where each of these columns corresponds to one state feature. In this way, t_joint_states will allow having the joint state space of the environment.
CREATE OR REPLACE FUNCTION create_joint_states(tablename TEXT) RETURNS integer AS $$
DECLARE
	variables RECORD;
	colname TEXT;
BEGIN
	-- iterate over all distinct values of var_name, i.e., over all different snap environment features
	FOR variables IN SELECT DISTINCT var_name FROM t_env_variables_values ORDER BY var_name LOOP
		RAISE NOTICE 'processing snap state variable: %', variables.var_name;

		-- each snap variable name becomes the column in the table which stors the joint state space

		-- altering table tabname
		colname := 'var_' || variables.var_name; -- each variable name gets prefix 'var_' before the actual name of the state variable
		RAISE NOTICE 'adding column % to table %', colname, tablename;
		-- the added conlumn has the constraint check in order to preserve integrity with the t_env_variables_values table
		EXECUTE 'ALTER TABLE ' || tablename || ' ADD ' || colname || ' VARCHAR(256) CHECK (check_env_values(cast (''' || variables.var_name || ''' as varchar), cast (' || colname || ' as varchar))=TRUE)';
	END LOOP;

	RETURN 0;

END;
$$ LANGUAGE plpgsql;

---------------------

-- this function cheks if the value of the state feature added to the table which represents sets of states is either null or has the value which exists in the table t_env_variables_values
CREATE OR REPLACE FUNCTION check_env_values(varname varchar, newvalue varchar) RETURNS bool AS $$
DECLARE
	tmp RECORD;
BEGIN
	IF newvalue IS NULL THEN
		-- the new value of the state feature can be either null or has to match values from the t_env_variables_values table
		RETURN TRUE;
	END IF;
	SELECT var_value INTO tmp FROM t_env_variables_values WHERE var_name=varname AND var_value=newvalue;
	IF FOUND THEN
		RAISE NOTICE 'value % of variable % exists in table t_env_variables_values - OK', newvalue, varname;
		RETURN TRUE;
	ELSE
		RAISE NOTICE 'value % of variable % does not exist in table t_env_variables_values - ERROR', newvalue, varname;
	END IF;	
	RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

----------------------

-- this function removes all columns from a given table whose names start with 'var_'
CREATE OR REPLACE FUNCTION remove_joint_states(tablename TEXT) RETURNS integer AS $$
DECLARE r RECORD;
DECLARE colname TEXT; -- the full name of the column with starting 'var_'
DECLARE pos integer;
BEGIN 
FOR r IN SELECT column_name FROM information_schema.columns WHERE table_name = tablename
LOOP 
	RAISE NOTICE 'Checking column: %', r;
	colname := r;
	colname := trim(leading '(' from colname);
	colname := trim(trailing ')' from colname);
	SELECT strpos into pos from strpos(colname, 'var_');
	RAISE NOTICE 'var_ found at the position % in the column name %', pos, colname;
	IF pos = 1 THEN
		RAISE NOTICE 'Removing column % from table %', colname, tablename;
		EXECUTE 'ALTER TABLE ' || tablename || ' DROP ' || colname ;
	END IF;
END LOOP;
	return 1;
END;
$$ LANGUAGE plpgsql;

---------------------

CREATE OR REPLACE FUNCTION state_values_check() RETURNS trigger AS $$
DECLARE r record;
BEGIN
	raise notice 'updating table: %', TG_TABLE_NAME;

	raise notice 'new record is %', new;

	--FOR r IN SELECT (each(hstore(NEW))).*
	--LOOP 
	--	RAISE NOTICE 'key:%, value: %', r.key, r.value; 
	--END LOOP; 
	RETURN new; 
END
$$ LANGUAGE plpgsql;


