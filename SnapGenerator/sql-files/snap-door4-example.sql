-- this is an example - door 4 - domain for the snap database

INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'open', 0.01);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'half_open', 0.01);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'closed', 0.98);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('light', 'on', 0.01);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('light', 'off', 0.99);

INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('open_door_slightly', 10);
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('open_door', 10);
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('close_door', 10);
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('switchlighton', 10);
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('switchlightoff', 10);

INSERT INTO t_abilities(abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) VALUES ('Rn_door_handle', 'recognition', 0.5, 0.2, 0.95, 0.01, 0.0, 1.0);
INSERT INTO t_abilities(abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) VALUES ('Rn_switch', 'recognition', 0.5, 0.2, 0.95, 0.01, 0.0, 1.0);

INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'open');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'half_open');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'closed');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('light_sensor', 'on');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('light_sensor', 'off');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('opening', 'yes');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('opening', 'no');

INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'open', 0.70);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'open', 0.10);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'open', 0.20);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'closed', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'closed', 0.70);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'closed', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'half_open', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'half_open', 0.7);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'half_open', 0.15);

INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'open_door', 0.85);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'open_door', 0.15);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'close_door', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'close_door', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'open_door_slightly', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'open_door_slightly', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'switchlighton', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'switchlighton', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'switchlightoff', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'switchlightoff', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'other', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'other', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'yes', 'nothing', 0.5);
INSERT INTO t_behaviour_sensor_model(obs_name, obs_value, beh_name, probability) VALUES ('opening', 'no', 'nothing', 0.5);

INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('light_sensor', 'on', 'light', 'on', 0.80);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('light_sensor', 'off', 'light', 'on', 0.20);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('light_sensor', 'on', 'light', 'off', 0.10);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('light_sensor', 'off', 'light', 'off', 0.90);

-- effects
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (1, 'door', 'open', 'open_door_slightly');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (2, 'door', 'half_open', 'open_door_slightly');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (3, 'door', 'closed', 'close_door');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (4, 'door', 'closed', 'close_door');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (5, 'door', 'open', 'open_door');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (6, 'door', 'open', 'open_door');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (7, 'light', 'on', 'switchlighton');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (8, 'light', 'off', 'switchlightoff');

-- preconditions for effects
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (1, 'door', 'half_open');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (2, 'door', 'closed');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (3, 'door', 'open');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (4, 'door', 'half_open');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (5, 'door', 'closed');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (6, 'door', 'half_open');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (7, 'light', 'off');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (8, 'light', 'on');

-- sets of states when behaviour is impossible
INSERT INTO t_when_is_behaviour_impossible(state_set_id, beh_name, var_name, var_value) VALUES (1, 'open_door_slightly', 'door', 'open');

-- insert data into the IU table
INSERT INTO t_iu_row(row_id) VALUES (1);
INSERT INTO t_iu_row(row_id) VALUES (2);
INSERT INTO t_iu_row(row_id) VALUES (3);
INSERT INTO t_iu_row(row_id) VALUES (4);

INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (1, 'door', 'open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (1, 'light', 'on');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (2, 'door', 'open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (2, 'light', 'on');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (3, 'door', 'open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (3, 'light', 'on');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (4, 'door', 'open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (4, 'light', 'on');

INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (1, 'door', 'closed');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (2, 'door', 'closed');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (3, 'door', 'half_open');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (4, 'light', 'off');

INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (1, 'Rn_door_handle');
INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (2, 'Rn_door_handle');
INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (3, 'Rn_door_handle');
INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (4, 'Rn_switch');

INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (1, 'open_door');
INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (2, 'open_door_slightly');
INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (3, 'open_door_slightly');
INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (4, 'switchlighton');

INSERT INTO t_rewards(state_set_id, reward_value) VALUES (1, 15);
INSERT INTO t_rewards_desc(state_set_id, var_name, var_value) VALUES (1, 'door', 'open');
INSERT INTO t_rewards_desc(state_set_id, var_name, var_value) VALUES (1, 'light', 'on');


