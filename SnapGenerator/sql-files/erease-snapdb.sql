-- (1) for abilities we assume only two possible values: 'yes' or 'no'

-- ### Create tables ###

-- be careful, this will erase all data from deleted tables !!!
DROP TABLE t_iu_behaviours;
DROP TABLE t_iu_abilities;
DROP TABLE t_iu_task_states;
DROP TABLE t_iu_goals;
DROP TABLE t_iu_row;
DROP TABLE t_abilities4behaviours;
DROP TABLE t_sensor_model;
DROP TABLE t_observations_values;
DROP TABLE t_default_probabilities4abilities;
DROP TABLE t_types_of_dementia;
DROP TABLE t_abilities;
DROP TABLE t_types_of_abilities;
DROP TABLE t_rewards_desc;
DROP TABLE t_rewards;
DROP TABLE t_preconditions4effects_of_behaviours;
DROP TABLE t_effects_of_behaviours;
DROP TABLE t_when_is_behaviour_impossible;
DROP TABLE t_when_is_behaviour_relevant;
DROP TABLE t_behaviours;
DROP TABLE t_env_variables_values;

