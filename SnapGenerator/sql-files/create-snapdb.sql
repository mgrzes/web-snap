-- (1) for abilities we assume only two possible values: 'yes' or 'no'

-- ### Create tables ###

-- be careful, this will erase all data from deleted tables !!!
DROP TABLE t_iu_behaviours;
DROP TABLE t_iu_abilities;
DROP TABLE t_iu_task_states;
DROP TABLE t_iu_goals;
DROP TABLE t_iu_row;
DROP TABLE t_abilities4behaviours;
DROP TABLE t_behaviour_sensor_model;
DROP TABLE t_sensor_model;
DROP TABLE t_observations_values;
DROP TABLE t_default_probabilities4abilities;
DROP TABLE t_types_of_dementia;
DROP TABLE t_abilities;
DROP TABLE t_types_of_abilities;
DROP TABLE t_rewards_desc;
DROP TABLE t_rewards;
DROP TABLE t_preconditions4effects_of_behaviours;
DROP TABLE t_effects_of_behaviours;
DROP TABLE t_when_is_behaviour_impossible;
DROP TABLE t_when_is_behaviour_relevant;
DROP TABLE t_behaviours;
DROP TABLE t_env_variables_values;

-- variables and their values for environment variables
CREATE TABLE t_env_variables_values(
	var_name VARCHAR(256) CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (var_name ~* '^[a-z_0-9]+$' ),
	var_value VARCHAR(256) CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (var_value ~* '^[a-z_0-9]+$' ),
	var_value_initial_prob REAL NOT NULL CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (var_value_initial_prob >= 0 AND var_value_initial_prob <= 1),
	PRIMARY KEY (var_name, var_value)
);

-- this table is for behaviours: behaviours do not need values because each behaviour name identifies the behaviour
CREATE TABLE t_behaviours(
	beh_name VARCHAR(256) CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (beh_name ~* '^[a-z_0-9]+$' ),
	-- timeout for behaviour B is the time we wait while the person is doing nothing in a situation where we expect them to do behaviour B
	beh_timeout REAL DEFAULT 0.0 CONSTRAINT "C5: non-negative value required" CHECK (beh_timeout >= 0),
	PRIMARY KEY (beh_name)
);

-- these two behaviours are always present in the database
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('other', 1000000);
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('nothing', 1000000);

-- THIS TABLE IS NOT USED ANY MORE BACAUSE THE IU TABLE IS USE FOR GENERATING THE SPUDD FILE
-- relevant states for behaviours: when beh_name can achieve something in the joint state (where the joint state can be the set of states), but by achieving something we meant that there will be some progress towards completing the plan, so there, there can be some plan related information encoded in this table
-- we need state_set_id here because given behaviours may be relevant in two disjoint sets of states
--CREATE TABLE t_when_is_behaviour_relevant(
--	state_set_id SERIAL,
--	beh_name VARCHAR(256) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE CONSTRAINT "C27: other is never relevant" CHECK ( NOT beh_name='other' ),
--	var_name VARCHAR(256),
--	var_value VARCHAR(256),
	--relev_probabil REAL DEFAULT 1.0 CONSTRAINT "C13: the probability in the range [0-1] required" CHECK (relevance_probability >= 0 AND relevance_probability <= 1 ),
--	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
--	PRIMARY KEY (state_set_id, beh_name, var_name)
--);

CREATE TABLE t_when_is_behaviour_impossible(
	state_set_id SERIAL,
	beh_name VARCHAR(256) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	PRIMARY KEY (state_set_id, beh_name, var_name)
);

-- beh_effect_id is required when we assume that given effect of behaviour may happen in two disjoint sets of states
CREATE TABLE t_effects_of_behaviours(
	beh_effect_id SERIAL PRIMARY KEY,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	beh_name VARCHAR(256) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE
);

CREATE TABLE t_preconditions4effects_of_behaviours (
	beh_effect_id SERIAL REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	PRIMARY KEY (beh_effect_id, var_name)
);

-- we need reward_id here, because any two discjoint sets of states may have the same value of reward: we can implement this by having given reward with different reward_ids
CREATE TABLE t_rewards (
	state_set_id SERIAL PRIMARY KEY,
	reward_value REAL NOT NULL CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK (reward_value >= 0)
);
CREATE TABLE t_rewards_desc (
	state_set_id SERIAL,
	FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	PRIMARY KEY (state_set_id, var_name)
);

-- INFORMATION ABOUT IMPOSSIBLE STATES WILL BE READ FROM TABLE t_effects_of_behaviours AND t_preconditions4effects_of_behaviours. BASICALLY POSSIBLE STATES ARE THOSE WHICH SATISFY PRECONDITIONS FOR AT LEAST ONE EFFECT OF A GIVEN BEHAVIOUR. BUT HERE WE DO NOT CARE IF THE BEHAVIOUR WILL DO SOMETIHNG GOOD FOR THE PLAN, OR UNDO SOMETHING WHICH WAS DONE BEFORE. HERE WE FOCUS ON THE LOCAL PROPERITES OF ACTIONS. PLAN-RELATED STATE RELEVANCE IS IN A SEPARATE TABLE t_when_is_behaviour_relevant.

CREATE TABLE t_types_of_abilities(
	abil_type VARCHAR(256) CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (abil_type ~* '^[a-z_0-9]+$' ),
	PRIMARY KEY (abil_type)
);

INSERT INTO t_types_of_abilities(abil_type) VALUES ('recognition');
INSERT INTO t_types_of_abilities(abil_type) VALUES ('affordance');
INSERT INTO t_types_of_abilities(abil_type) VALUES ('recall_step');

-- this table is for abilities
CREATE TABLE t_abilities(
	abil_name VARCHAR(256) PRIMARY KEY CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (abil_name ~* '^[a-z_0-9]+$' ),
	abil_type VARCHAR(256) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE,
	gain_prob REAL NOT NULL CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (gain_prob >= 0 AND gain_prob <= 1),
	lose_prob REAL NOT NULL CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (lose_prob >= 0 AND lose_prob <= 1),
	gain_prompt_prob REAL NOT NULL CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (gain_prompt_prob >= 0 AND gain_prompt_prob <= 1),
	lose_prompt_prob REAL NOT NULL CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (lose_prompt_prob >= 0 AND lose_prompt_prob <= 1),
	-- inital_prob is the probability that abil_name=yes in the initial state
	abil_initial_prob REAL NOT NULL CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (abil_initial_prob >= 0 AND abil_initial_prob <= 1),
	-- the cost of prompting to regain this ability
	abil_prompt_cost REAL NOT NULL CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK (abil_prompt_cost >= 0),
	parent_abil_name VARCHAR(256) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE CONSTRAINT "C30: ability cannot depend on itself" CHECK (parent_abil_name != abil_name)
);

CREATE TABLE t_types_of_dementia(
	dementia_type VARCHAR(256) CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (dementia_type ~* '^[a-z_0-9]+$' ),
	PRIMARY KEY (dementia_type)
);

INSERT INTO t_types_of_dementia(dementia_type) VALUES ('light');
INSERT INTO t_types_of_dementia(dementia_type) VALUES ('medium');
INSERT INTO t_types_of_dementia(dementia_type) VALUES ('severe');

CREATE TABLE t_default_probabilities4abilities(
	dementia_type VARCHAR(256) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE,
	abil_type VARCHAR(256) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE,
	gain_prob REAL NOT NULL CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (gain_prob >= 0 AND gain_prob <= 1),
	lose_prob REAL NOT NULL CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (lose_prob >= 0 AND lose_prob <= 1),
	gain_prompt_prob REAL NOT NULL CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (gain_prompt_prob >= 0 AND gain_prompt_prob <= 1),
	lose_prompt_prob REAL NOT NULL CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (lose_prompt_prob >= 0 AND lose_prompt_prob <= 1),
	PRIMARY KEY (dementia_type, abil_type)
);

INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('light', 'recognition', 0.8, 0.2, 0.99, 0.01);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('light', 'affordance', 0.8, 0.2, 0.99, 0.01);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('light', 'recall_step', 0.8, 0.2, 0.99, 0.01);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('medium', 'recognition', 0.7, 0.3, 0.99, 0.01);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('medium', 'affordance', 0.7, 0.3, 0.99, 0.01);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('medium', 'recall_step', 0.6, 0.4, 0.99, 0.01);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('severe', 'recognition', 0.6, 0.4, 0.7, 0.3);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('severe', 'affordance', 0.6, 0.4, 0.7, 0.3);
INSERT INTO t_default_probabilities4abilities(dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) VALUES ('severe', 'recall_step', 0.5, 0.5, 0.7, 0.3);


-- this table is for observations and their values
CREATE TABLE t_observations_values(
	obs_name VARCHAR(256) CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (obs_name ~* '^[a-z_0-9]+$' ),
	obs_value VARCHAR(256) CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (obs_value ~* '^[a-z_0-9]+$' ),
	PRIMARY KEY (obs_name, obs_value)
);

-- this table is for the sensor model of all sensors
CREATE TABLE t_sensor_model(
	obs_name VARCHAR(256),
	obs_value VARCHAR(256),
	FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	probability REAL NOT NULL CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (probability >= 0 AND probability <= 1),
	PRIMARY KEY (obs_name, obs_value, var_name, var_value)
);

-- this table is for the sensor model of behaviour (behariour is like one variable with many values which correspond to different behaviours)
CREATE TABLE t_behaviour_sensor_model(
	obs_name VARCHAR(256),
	obs_value VARCHAR(256),
	FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE,
	beh_name VARCHAR(256),
	FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE,
	probability REAL NOT NULL CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (probability >= 0 AND probability <= 1),
	PRIMARY KEY (obs_name, obs_value, beh_name)
);

-- THIS TABLE IS NOT USED ANYMORE, THIS DATA IS READ DIRECTLY FROM THE IU TABLE
-- abilities required by behariours: a tuple here means that ability abil_name is required by behaviour beh_name
--CREATE TABLE t_abilities4behaviours(
--	abil_name VARCHAR(256) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE,
--	beh_name VARCHAR(256) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE,
--	PRIMARY KEY (abil_name, beh_name)
--);

-- ### IU tables ###

CREATE TABLE t_iu_row(
	row_id INT PRIMARY KEY,
	data_row BOOLEAN DEFAULT TRUE NOT NULL,
	row_probability REAL DEFAULT 1.0 CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (row_probability >= 0 AND row_probability <= 1 ),
	row_confirmed BOOLEAN DEFAULT TRUE NOT NULL
);

CREATE TABLE t_iu_goals(
	row_id INT REFERENCES t_IU_row(row_id) ON UPDATE CASCADE,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	PRIMARY KEY (row_id, var_name, var_value)
);

CREATE TABLE t_iu_task_states(
	row_id INT REFERENCES t_IU_row(row_id) ON UPDATE CASCADE,
	var_name VARCHAR(256),
	var_value VARCHAR(256),
	FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE,
	PRIMARY KEY (row_id, var_name, var_value)
);

CREATE TABLE t_iu_abilities(
	row_id INT REFERENCES t_IU_row(row_id) ON UPDATE CASCADE,
	abil_name VARCHAR(256) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE,
	PRIMARY KEY (row_id, abil_name)
);

CREATE TABLE t_iu_behaviours(
	row_id INT REFERENCES t_IU_row(row_id) ON UPDATE CASCADE,
	beh_name VARCHAR(256) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE,
	PRIMARY KEY (row_id, beh_name)
);

-- ### Load functions ###

-- create the language before loading functions
-- CREATE LANGUAGE plpgsql;

-- this will call the script which defines the function to create the joint state space
-- \i 'snap-plpgsql-functions.sql'

-- ### Create triggers using functions loaded above ###

-- CREATE TRIGGER relation_check BEFORE INSERT OR UPDATE ON t_state_reward FOR EACH ROW EXECUTE PROCEDURE state_values_check();


