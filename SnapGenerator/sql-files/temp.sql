SELECT
FK.TABLE_NAME as K_Table,
CU.COLUMN_NAME as FK_Column,
PK.TABLE_NAME as PK_Table,
PT.COLUMN_NAME as PK_Column,
C.CONSTRAINT_NAME as Constraint_Name
FROM       INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C
INNER JOIN  INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME
INNER JOIN      INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME
INNER JOIN      INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME
INNER JOIN  (
       SELECT      i1.TABLE_NAME, i2.COLUMN_NAME
       FROM        INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1
           INNER JOIN      INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME
               WHERE       i1.CONSTRAINT_TYPE = 'PRIMARY KEY'
       ) PT ON PT.TABLE_NAME = PK.TABLE_NAME
-- optional:
--ORDER BY
--       1,2,3,4
WHERE      PK.TABLE_NAME='t_env_variables_values';

-- how to make this running?

postgres=# SELECT * FROM foo;
 a  | b  |   c    
----+----+--------
 10 | 20 | Pavel
 30 | 40 | Zdenek
(2 rows)

postgres=# select (each(hstore(foo))).* from foo;
 key | value  
-----+--------
 a   | 10
 b   | 20
 c   | Pavel
 a   | 30
 b   | 40
 c   | Zdenek
(6 rows)

CREATE OR REPLACE FUNCTION trgfce() 
RETURNS trigger AS $$
DECLARE r record; 
BEGIN 
  FOR r IN SELECT (each(hstore(NEW))).* 
  LOOP 
    RAISE NOTICE 'key:%, value: %', r.key, r.value; 
  END LOOP; 
  RETURN new; 
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER trg BEFORE INSERT ON foo FOR EACH ROW EXECUTE PROCEDURE trgfce();

postgres=# INSERT INTO foo VALUES(80,90,'Zbynek');
NOTICE:  key:a, value: 80
NOTICE:  key:b, value: 90
NOTICE:  key:c, value: Zbynek
INSERT 0 1


-- in order to use this function I firstly have to make tgfce working with hstore.
CREATE OR REPLACE FUNCTION trgfce() 
RETURNS trigger AS $$
DECLARE r record; 
BEGIN 
  FOR r IN SELECT (each(hstore(NEW))).* 
  LOOP 
    RAISE NOTICE 'key:%, value: %', r.key, r.value; 
  END LOOP; 
  RETURN new; 
END
$$ LANGUAGE plpgsql;

--

CREATE OR REPLACE FUNCTION state_values_check() 
RETURNS trigger AS $$
DECLARE r record; 
BEGIN 
  FOR r IN SELECT (each(hstore(NEW))).*
  LOOP 
    RAISE NOTICE 'key:%, value: %', r.key, r.value; 
  END LOOP; 
  RETURN new; 
END
$$ LANGUAGE plpgsql;

--

CREATE OR REPLACE FUNCTION state_values_check1() RETURNS TRIGGER LANGUAGE plpgsql AS $$
DECLARE
	varname TEXT;
	tmp	RECORD;
BEGIN

-- check if the added value is in the columns whose name starts with 'var_'

IF TG_RELNAME='t_state_reward' THEN
	varname := trim(leading 'var_' from TG_NAME);
	SELECT var_value INTO tmp FROM t_env_variables_values WHERE var_name=varname AND var_value=NEW.TG_NAME;
	IF FOUND THEN
		-- product type is valid for this server
		RETURN NEW;
	ELSE
		RAISE EXCEPTION ' ups ';
		RETURN NULL;
	END IF;
END IF;
  
RETURN NEW;
END;
$$;

--

CREATE OR REPLACE FUNCTION state_values_check3() RETURNS trigger AS $$
DECLARE r RECORD;
DECLARE tmp RECORD;
DECLARE varname TEXT;
DECLARE i integer;
DECLARE j integer;
DECLARE varvalue TEXT;
DECLARE tmpc TEXT;
DECLARE pos integer;
BEGIN 
i := 0;
FOR r IN SELECT column_name FROM information_schema.columns WHERE table_name ='t_state_reward'
LOOP 
	RAISE NOTICE 'Checking column: %', r;
	varname := r;
	varname := trim(leading '(' from varname);
	varname := trim(trailing ')' from varname);
	SELECT strpos into pos from strpos(varname, 'var_');
	RAISE NOTICE 'var_ found at the position % in the column name %', pos, varname;
	IF pos = 1 THEN
		varname := trim(leading 'var_' from varname);
		RAISE NOTICE 'var_name:% ', varname;
		j := i;
		--FOR tmpc in each(NEW)
		--LOOP
			--IF j = 0 THEN
			--	BREAK;
			--END IF;
		--	varvalue := tmpc;
			--j : = j - 1;
		--END LOOP
		SELECT var_value INTO tmp FROM t_env_variables_values WHERE var_name=varname AND var_value='openaa';
		IF FOUND THEN
			-- the value of the state variable exists in the t_env_variables_values table
			RETURN NEW;
		ELSE
			RAISE EXCEPTION ' ups ';
			RETURN NULL;
		END IF;
	END IF;
	i := i + 1;
END LOOP; 
RETURN new; 
END
$$ LANGUAGE plpgsql;

