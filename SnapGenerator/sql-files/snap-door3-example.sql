-- this is an example - door 3 - domain for the snap database

INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'open', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'half_open', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'closed', 1.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('window', 'up', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('window', 'down', 1.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('stove', 'on', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('stove', 'off', 1.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('light', 'on', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('light', 'off', 1.0);

INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('open_door_slightly', 10);
INSERT INTO t_behaviours(beh_name, beh_timeout) VALUES ('close_door', 10);

INSERT INTO t_abilities(abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) VALUES ('Rn_door_handle', 'recognition', 0.5, 0.2, 0.95, 0.01, 0.0, 1.0);

INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'open');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'half_open');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'closed');

INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'open', 0.70);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'open', 0.10);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'open', 0.20);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'closed', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'closed', 0.70);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'closed', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'half_open', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'half_open', 0.7);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'half_open', 0.15);

-- effects
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (1, 'door', 'open', 'open_door_slightly');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (2, 'door', 'half_open', 'open_door_slightly');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (3, 'door', 'closed', 'close_door');
INSERT INTO t_effects_of_behaviours(beh_effect_id, var_name, var_value, beh_name) VALUES (4, 'door', 'closed', 'close_door');

-- preconditions for effects
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (1, 'door', 'half_open');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (2, 'door', 'closed');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (3, 'door', 'open');
INSERT INTO t_preconditions4effects_of_behaviours(beh_effect_id, var_name, var_value) VALUES (4, 'door', 'half_open');

-- if there is a tuple in this table, it means that the behaviour will accomplish something in a given state
INSERT INTO t_when_is_behaviour_relevant(state_set_id, beh_name, var_name, var_value) VALUES (1, 'close_door', 'door', 'open');
INSERT INTO t_when_is_behaviour_relevant(state_set_id, beh_name, var_name, var_value) VALUES (2, 'close_door', 'door', 'half_open');
INSERT INTO t_when_is_behaviour_relevant(state_set_id, beh_name, var_name, var_value) VALUES (3, 'open_door_slightly', 'door', 'closed');
INSERT INTO t_when_is_behaviour_relevant(state_set_id, beh_name, var_name, var_value) VALUES (4, 'open_door_slightly', 'door', 'half_open');

-- sets of states when behaviour is impossible
INSERT INTO t_when_is_behaviour_impossible(state_set_id, beh_name, var_name, var_value) VALUES (1, 'open_door_slightly', 'door', 'open');

INSERT INTO t_abilities4behaviours(abil_name, beh_name) VALUES ('Rn_door_handle', 'close_door');
INSERT INTO t_abilities4behaviours(abil_name, beh_name) VALUES ('Rn_door_handle', 'open_door_slightly');

-- insert data into the IU table
INSERT INTO t_iu_row(row_id) VALUES (1);
INSERT INTO t_iu_row(row_id) VALUES (2);
INSERT INTO t_iu_row(row_id, data_row) VALUES (3, false);
INSERT INTO t_iu_row(row_id) VALUES (4);

INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (1, 'door', 'half_open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (1, 'door', 'open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (2, 'door', 'half_open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (2, 'door', 'open');

INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (1, 'door', 'closed');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (1, 'stove', 'off');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (2, 'window', 'up');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (2, 'light', 'on');

INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (1, 'Rn_door_handle');
INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (2, 'Rn_door_handle');

INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (1, 'open_door_slightly');
INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (2, 'close_door');

INSERT INTO t_rewards(state_set_id, reward_value) VALUES (1, 25);
INSERT INTO t_rewards(state_set_id, reward_value) VALUES (2, 15);
INSERT INTO t_rewards_desc(state_set_id, var_name, var_value) VALUES (1, 'door', 'open');
INSERT INTO t_rewards_desc(state_set_id, var_name, var_value) VALUES (2, 'door', 'half_open');


