-- sql code for updating existing databases

CREATE TABLE t_behaviour_sensor_model(
	obs_name VARCHAR(256),
	obs_value VARCHAR(256),
	FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE,
	beh_name VARCHAR(256),
	FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE,
	probability REAL NOT NULL CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (probability >= 0 AND probability <= 1),
	PRIMARY KEY (obs_name, obs_value, beh_name)
);

