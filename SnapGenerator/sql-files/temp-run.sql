\echo ------------------------------------------------
CREATE OR REPLACE FUNCTION validate_doc(char(3))
RETURNS bool AS $$
DECLARE
    doc ALIAS FOR $1;
    doc_number int;
    doc_digit int;
BEGIN
    IF char_length(doc) < 3 THEN
    RAISE EXCEPTION 'The DOC number must have 3 digits';
    END IF;
    doc_number := to_number(substr(doc,1,2),'99');
    doc_digit := to_number(substr(doc,3,1),'9');
    IF mod(doc_number,2) = doc_digit THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END;
$$ LANGUAGE plpgsql;
\echo ------------------------------------------------
CREATE DOMAIN doc AS char(3) CHECK (validate_doc(VALUE) = TRUE);
\echo ------------------------------------------------
CREATE TABLE document (
    number doc
);
\echo ------------------------------------------------
INSERT INTO document VALUES ('100'); -- DOC ok
INSERT INTO document VALUES ('020'); -- DOC ok
INSERT INTO document VALUES ('071'); -- DOC ok
INSERT INTO document VALUES ('101'); -- DOC invalid
INSERT INTO document VALUES ('202'); -- DOC invalid
INSERT INTO document VALUES ('20'); -- DOC number too small
INSERT INTO document VALUES ('2030'); -- DOC number too long
\echo ------------------------------------------------
SELECT * FROM document;
