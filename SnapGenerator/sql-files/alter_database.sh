#!/bin/bash

#allsnapdbs='SELECT d.datname as "Name" FROM pg_catalog.pg_database d JOIN pg_catalog.pg_roles r ON d.datdba = r.oid JOIN pg_catalog.pg_tablespace t on d.dattablespace = t.oid WHERE r.rolname='\''x721demouser'\'' AND d.datname LIKE '\''snap%'\'' ORDER BY 1;'
#echo $allsnapdbs

#for line in  `echo $allsnapdbs | psql -d snap-config -U x721demouser -W` ; do
#  if [[ "$line" =~ "^snap.*$" ]]; then
#    echo $line
#  else
#    echo "skipping ["$line"]"
#  fi
#done


dbname=$1

cmd='ALTER TABLE t_abilities ADD COLUMN parent_abil_name VARCHAR(256) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE CONSTRAINT "C30: ability cannot depend on itself" CHECK (parent_abil_name != abil_name)'

echo $cmd

echo $cmd | psql -d $dbname -U x721demouser -W

