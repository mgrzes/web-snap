-- this is an example - door 2 - domain for the snap database

INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'open', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'half_open', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('door', 'closed', 1.0);

INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('window', 'up', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('window', 'down', 1.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('stove', 'on', 0.0);
INSERT INTO t_env_variables_values(var_name, var_value, var_value_initial_prob) VALUES ('stove', 'off', 1.0);

INSERT INTO t_behaviours(beh_name) VALUES ('nothing');
INSERT INTO t_behaviours(beh_name) VALUES ('other');
INSERT INTO t_behaviours(beh_name) VALUES ('open_door_slightly');
INSERT INTO t_behaviours(beh_name) VALUES ('close_door');

INSERT INTO t_abilities(abil_name, gain_recall_prob, loose_recall_prob, gain_recall_prompt_prob, loose_recall_prompt_prob, abil_initial_prob, abil_prompt_cost) VALUES ('Rn_door_handle', 0.5, 0.2, 0.95, 0.01, 0.0, -1); -- depends on is left not specified here

INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'open');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'half_open');
INSERT INTO t_observations_values(obs_name, obs_value) VALUES ('door_sensor', 'closed');

INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'open', 0.70);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'open', 0.10);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'open', 0.20);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'closed', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'closed', 0.70);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'closed', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'open', 'door', 'half_open', 0.15);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'half_open', 'door', 'half_open', 0.7);
INSERT INTO t_sensor_model(obs_name, obs_value, var_name, var_value, probability) VALUES ('door_sensor', 'closed', 'door', 'half_open', 0.15);


-- call this function in order to add new columnts to the joint states tables
SELECT create_joint_states('t_state_timeout');
SELECT create_joint_states('t_state_reward');
--SELECT create_joint_states('t_state_impossible4behaviour');
SELECT create_joint_states('t_state_relevant4behaviour');
SELECT create_joint_states('t_state_ideal_behaviours');

-- conditional effects
INSERT INTO t_state_ideal_behaviours(beh_name, var_name, new_var_value, var_door) VALUES ('open_door_slightly', 'door', 'open', 'half_open');
INSERT INTO t_state_ideal_behaviours(beh_name, var_name, new_var_value, var_door) VALUES ('open_door_slightly', 'door', 'half_open', 'closed');
INSERT INTO t_state_ideal_behaviours(beh_name, var_name, new_var_value, var_door) VALUES ('close_door', 'door', 'closed', 'open');
INSERT INTO t_state_ideal_behaviours(beh_name, var_name, new_var_value, var_door) VALUES ('close_door', 'door', 'closed', 'half_open');

-- just to test the remove_joint_state function: remove and add again joint state columns
SELECT remove_joint_states('t_state_timeout');
SELECT create_joint_states('t_state_timeout');

-- when the door is open the reward is high
INSERT INTO t_state_timeout(state_timeout, var_door) VALUES (10.0, 'open');
INSERT INTO t_state_timeout(state_timeout, var_door) VALUES (20.0, 'closed');
INSERT INTO t_state_timeout(state_timeout, var_door) VALUES (20.0, 'half_open');

INSERT INTO t_state_reward(state_reward, var_door) VALUES (10.0, 'open');
INSERT INTO t_state_reward(state_reward, var_door) VALUES (-20.0, 'closed');
INSERT INTO t_state_reward(state_reward, var_door) VALUES (-20.0, 'half_open');

-- if something is here, it means that the behaviour is imossible in a given set of states, if there is no entry for a given state, it means that the behaviour is possible in that state
--INSERT INTO t_state_impossible4behaviour(beh_name, var_door) VALUES ('close_door', 'closed');
--INSERT INTO t_state_impossible4behaviour(beh_name, var_door) VALUES ('open_door_slightly', 'open');

-- if there is a tuple in this table, it means that the behaviour will accomplish something in a given state
INSERT INTO t_state_relevant4behaviour(beh_name, var_door) VALUES ('close_door', 'open');
INSERT INTO t_state_relevant4behaviour(beh_name, var_door) VALUES ('close_door', 'half_open');
INSERT INTO t_state_relevant4behaviour(beh_name, var_door) VALUES ('open_door_slightly', 'closed');
INSERT INTO t_state_relevant4behaviour(beh_name, var_door) VALUES ('open_door_slightly', 'half_open');

INSERT INTO t_abilities4behaviours(abil_name, beh_name) VALUES ('Rn_door_handle', 'close_door');
INSERT INTO t_abilities4behaviours(abil_name, beh_name) VALUES ('Rn_door_handle', 'open_door_slightly');

-- insert data into the IU table
INSERT INTO t_iu_row(row_id) VALUES (1);
INSERT INTO t_iu_row(row_id) VALUES (2);
INSERT INTO t_iu_row(row_id, data_row) VALUES (3, false);

INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (1, 'door', 'half_open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (1, 'door', 'open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (2, 'door', 'half_open');
INSERT INTO t_iu_goals(row_id, var_name, var_value) VALUES (2, 'door', 'open');

INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (1, 'door', 'closed');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (1, 'window', 'up');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (2, 'door', 'closed');
INSERT INTO t_iu_task_states(row_id, var_name, var_value) VALUES (2, 'window', 'up');

INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (1, 'Rn_door_handle');
INSERT INTO t_iu_abilities(row_id, abil_name) VALUES (2, 'Rn_door_handle');

INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (1, 'open_door_slightly');
INSERT INTO t_iu_behaviours(row_id, beh_name) VALUES (2, 'open_door_slightly');



