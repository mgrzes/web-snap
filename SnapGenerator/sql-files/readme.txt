This directory contains all sql files for the snap web interface.

create-snapdb.sql - this file ereases and then creates all tables
which are required for the snap database

snap-database-forms.odb - this files allows connecting to the
database from openoffice; this is useful to see database
tables in the graphical view

snap-door4-example.sql - the current version of the example
door database.

erease-snapdb.sql - removes tables from the snap database, used by web snap

All remaining SQL files are not used at the moment.

