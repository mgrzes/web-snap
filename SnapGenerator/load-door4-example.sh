#/bin/sh

echo The first argument to this script is the name of the database where this example should be loaded!

#cd sql-files

psql -d $1 -U x721demouser -W < sql-files/create-snapdb.sql

psql -d $1 -U x721demouser -W < sql-files/snap-door4-example.sql

#cd ..

#java -jar snap-ubuntu.jar -o /tmp/snap.txt -u x721demouser -d $1 -s localhost -n


