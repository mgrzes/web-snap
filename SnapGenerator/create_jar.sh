# this is to create the jar file without the manifest file
# jar cfe snap.jar snap/SnapGenerator  snap/* jargs/*

cd snap
# set class path to jargs.gnu package
javac -cp .:../ *.java
cd ..

cd jargs/gnu
javac *.java
cd ../..

jar cfm snap-ubuntu.jar manifest.txt snap/* jargs/gnu/* sql-files/*.sql sql-files/*.odb

# Print what is in the jar file:
jar tf snap-ubuntu.jar

# test the jar file if it runns OK
java -jar snap-ubuntu.jar -h

