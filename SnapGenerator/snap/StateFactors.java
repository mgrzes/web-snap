package snap;

/**
 * Author: Marek Grzes
 */

import java.util.Vector;

public class StateFactors {
	public Vector<String> _variables;
	public Vector<String> _values;
	public StateFactors(){
		_variables = new Vector<String>();
		_values = new Vector<String>();
	}
	public void add(String var, String val) {
		_variables.add(var);
		_values.add(val);
	}
	public String getVal(String var) {
		for (int i=0; i < _variables.size(); i++ ) {
			if ( var.compareTo(_variables.elementAt(i))==0 ) {
				return _values.elementAt(i);
			}
		}
		return null;
	}
	public void print() {
		System.out.print("[");
		for (int i=0; i < _variables.size(); i++ ) {
			if ( i > 0) {
				System.out.print(" ");
			}
			System.out.print(_variables.elementAt(i) + "=" + _values.elementAt(i));
		}
		System.out.print("]");
	}
}
