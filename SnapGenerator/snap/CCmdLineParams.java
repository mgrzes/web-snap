package snap;

/**
 * Author: Marek Grzes
 */

import jargs.gnu.CmdLineParser;

public class CCmdLineParams {
	final static String _default_user = "x721demouser";
	final static String _default_passfile = System.getProperty("user.home") + System.getProperty("file.separator") + ".snappgpasswd";
	
    static CmdLineParser _parser;
    static CmdLineParser.Option _option_dbname;
    static CmdLineParser.Option _option_out_file;
    static CmdLineParser.Option _option_noninteractive;
    static CmdLineParser.Option _option_help;
    static CmdLineParser.Option _option_dbserver;
    static CmdLineParser.Option _option_dbuser;
    static CmdLineParser.Option _option_dbpassfile;
    static CmdLineParser.Option _option_dbpassword;
    static CmdLineParser.Option _option_hicontrol;
    static CmdLineParser.Option _option_call_caregiver;
    
    // This is STUPID to use these variables here but the CCmdLineParser does not allow accessing
    // the same parameter twice, strange behaviour or a bug! For this reason I have to store the value
    // of each parameter in these variables below after reading them for the first time from the CmdLineParser.
    String _dbname;
    String _out_file;
    int _non_interactive;
    String _dbserver;
    String _dbuser;
    String _dbpassfile;
    String _dbpassword;
    int _hicontrol;
    int _call_caregiver;
 
	public static CCmdLineParams getInstance() {
		return CCmdLineParamsHolder.instance;
	}

	public void print_usage() {
		System.out.println("This program can be used with the following parameters:");
	    System.out.printf("Database name:\t\t -%s --%s. Required in the non-interactive mode.\n" ,"d", "dbname");
	    System.out.printf("Database server:\t\t -%s --%s. ('localhost' when not specified).\n" ,"s", "dbserver");
	    System.out.printf("Database user:\t\t -%s --%s. ('%s' when not specified).\n" ,"u", "dbuser", _default_user);
	    System.out.printf("Database password file:\t\t -%s --%s. ('%s' when not specified).\n" ,"p", "dbpasswordfile", _default_passfile);
	    System.out.printf("Database password:\t\t -%s --%s.\n" ,"w", "dbpassword");
	    System.out.printf("Output file:\t\t -%s --%s. Always required.\n", "o", "out-file");
	    System.out.printf("Non-interactive mode:\t\t -%s --%s. If this parameter is specified, the database is used instead of input from stdin.\n", "n", "non-interactive");
	    System.out.printf("Add content required for hierarchical control:\t\t -%s --%s. This flag will add to the generated file things such as parent control variable.\n", "i", "hicontrol");
	    System.out.printf("Call care giver:\t\t -%s --%s. This flag will add an action call_caregiver and variable caregiver_present.\n", "c", "call-caregiver");
	    System.out.printf("Help:\t\t -%s --%s\n", "h","help");
	}
	
	private CCmdLineParams() {
	    _dbname = null;
	    _out_file = null;
	    _non_interactive = -1;
	    _dbserver = null;
	    _dbuser = null;
	    _dbpassfile = null;
	    _hicontrol = -1;
	    _call_caregiver = -1;
		_parser = new CmdLineParser();
		_option_dbname = _parser.addStringOption('d', "dbname");
		_option_out_file = _parser.addStringOption('o', "out-file");
		_option_noninteractive = _parser.addBooleanOption('n', "non-interactive");
        _option_help = _parser.addBooleanOption('h',"help");
		_option_dbserver = _parser.addStringOption('s', "dbserver");
		_option_dbuser = _parser.addStringOption('u', "dbuser");
		_option_dbpassfile = _parser.addStringOption('p', "dbpasswordfile");
		_option_dbpassword = _parser.addStringOption('w', "dbpassword");
		_option_hicontrol = _parser.addBooleanOption('i', "hicontrol");
		_option_call_caregiver = _parser.addBooleanOption('c', "call-caregiver");
	}

	public void parse_args(String[] args) {
        try {
            _parser.parse(args);
            Boolean value = (Boolean)_parser.getOptionValue(_option_help);
            if ( value != null ) {
            	print_usage();
            	System.exit(0);
            }
        }
        catch ( CmdLineParser.OptionException e ) {
            System.err.println(e.getMessage());
            print_usage();
            System.exit(1);
        }		
	}
	
	public String out_file_name() {
		if (_out_file != null) {
			return _out_file;
		} else {
	        String value = (String)_parser.getOptionValue(_option_out_file);
	        if ( value != null) {
	        	_out_file = value;
	        	return _out_file;
	        }
	        System.err.println("the out-file has to be specified! It can be this: '-o snap.txt'\n");
	        print_usage();
			System.exit(1);
			return "";
		}
	}

	public String dbserver() {
		if (_dbserver != null) {
			return _dbserver;
		} else {
			String value = (String)_parser.getOptionValue(_option_dbserver);
	        if ( value != null) {
	        	_dbserver = value;
	        } else {
	        	_dbserver = "localhost";
	        }
        	return _dbserver;
		}
	}

	public String dbuser() {
		if (_dbuser != null) {
			return _dbuser;
		} else {
			String value = (String)_parser.getOptionValue(_option_dbuser);
	        if ( value != null) {
	        	_dbuser = value;
	        } else {
	        	_dbuser = _default_user;
	        }
        	return _dbuser;
		}
	}

	public String dbpassordfile() {
		if (_dbpassfile != null) {
			return _dbpassfile;
		} else {
			String value = (String)_parser.getOptionValue(_option_dbpassfile);
	        if ( value != null) {
	        	_dbpassfile = value;
	        } else {
	        	_dbpassfile = _default_passfile;
	        }
        	return _dbpassfile;
		}
	}

	public String dbpassord() {
		if (_dbpassword != null) {
			return _dbpassword;
		} else {
			String value = (String)_parser.getOptionValue(_option_dbpassword);
	        if ( value != null) {
	        	_dbpassword = value;
	        } else {
	        	_dbpassword = null;
	        }
        	return _dbpassword;
		}
	}
	
	public String dbname() {
		if (_dbname != null) {
			return _dbname;
		} else {
			String value = (String)_parser.getOptionValue(_option_dbname);
	        if ( value != null) {
	        	_dbname = value;
	        	return _dbname;
	        }
	        System.err.println("the dbname not specified!");
	        print_usage();
			System.exit(1);
			return "";
		}
	}
	
	public boolean non_interactive() {
		if ( _non_interactive == -1) {
	        Boolean value = (Boolean)_parser.getOptionValue(_option_noninteractive);
	        if ( value != null) {
	        	if ( value == false) {
	        		_non_interactive = 0;
	        	} else {
	        		_non_interactive = 1;
	        	}
	        } else {
	        	_non_interactive = 0;
	        }
		}
		if (_non_interactive == 0) return false;
		if (_non_interactive == 1) return true;
		return false;
	}

	public boolean is_hicontrol_required() {
		if ( _hicontrol == -1) {
	        Boolean value = (Boolean)_parser.getOptionValue(_option_hicontrol);
	        if ( value != null) {
	        	if ( value == false) {
	        		_hicontrol = 0;
	        	} else {
	        		_hicontrol = 1;
	        	}
	        } else {
	        	_hicontrol = 0;
	        }
		}
		if (_hicontrol == 0) return false;
		if (_hicontrol == 1) return true;
		return false;
	}

	public boolean is_call_caregiver_required() {
		if ( _call_caregiver == -1) {
	        Boolean value = (Boolean)_parser.getOptionValue(_option_call_caregiver);
	        if ( value != null) {
	        	if ( value == false) {
	        		_call_caregiver = 0;
	        	} else {
	        		_call_caregiver = 1;
	        	}
	        } else {
	        	_call_caregiver = 0;	// false by default
	        }
		}
		if (_call_caregiver == 0) return false;
		if (_call_caregiver == 1) return true;
		return false;
	}
	
	private static final class CCmdLineParamsHolder {
		static final CCmdLineParams instance = new CCmdLineParams();    
	}
}
