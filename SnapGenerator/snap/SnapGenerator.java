package snap;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.*;
import java.util.Date;

// TODO: some SQL code could be moved to functions and then reused!

class IURow{
	public String _behaviour;
	public Vector<String> _abilities;
	public double _probability;
	public int _row_id;
}

public class SnapGenerator {

	public static String get_row_behaviour(Connection db_connection, int row_id) {
		String query = "";
		try {
			Statement st = db_connection.createStatement();
			query = "SELECT beh_name FROM t_iu_behaviours WHERE row_id='" + row_id + "' ORDER BY 1 ASC";
			ResultSet rs = st.executeQuery(query);
			String res=null;
			while (rs.next()) {
				if ( res == null) {
					res = rs.getString(1);
				} else {
				    System.out.println("DB: there is a bug and it is your fault, only one behaviour can be specified in one row in the IU table, check row " + row_id);
				    System.exit(1);
				}
			}
			rs.close();
			st.close();
			return res;
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		}
		return "not found";
	}

	public static Vector<String> get_row_abilities(Connection db_connection, int row_id) {
		Vector<String> res = new Vector<String>();
		String query = "";
		try {
			Statement st = db_connection.createStatement();
			query = "SELECT abil_name FROM t_iu_abilities WHERE row_id='" + row_id + "' ORDER BY 1 ASC";
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				res.add(rs.getString(1));
			}
			rs.close();
			st.close();
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		}
		return res;
	}
	
	public static Vector<IURow> get_all_iu_data_rows(Connection db_connection, Set<String> allIUBehs) {
		Vector<IURow> res = new Vector<IURow>();
		String query="empty";
		try {
			Statement st = db_connection.createStatement();
			query = "SELECT row_id, data_row, row_probability, row_confirmed FROM t_iu_row ORDER BY row_id ASC";
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				// check if this is a data row
				if ( rs.getString(2).compareTo("t") == 0 ) {
					// TODO: this check can be added later on
					//if ( rs.getString(4).compareTo("f") == 0) {
					//    System.out.println("DB: all rows in the IU table have to be confirmed before generating the SPUDD file!");
					//    System.exit(1);
					//}
					IURow temp = new IURow();
					temp._row_id = Integer.parseInt(rs.getString(1));
					temp._probability = Double.parseDouble(rs.getString(3));
					temp._behaviour = get_row_behaviour(db_connection, temp._row_id);
					if ( allIUBehs != null ) {
						allIUBehs.add(temp._behaviour); // store all IU behaviours
					}
					temp._abilities = get_row_abilities(db_connection, temp._row_id);
					res.add(temp);
				}
			}
			rs.close();
			st.close();
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		}
		return res;
	}
	
	public static Vector<Pair<String, String>> get_two_columns_from_db(Connection db_connection, String tablename, String column1, String column2) {
		Vector<Pair<String, String>> res = new Vector<Pair<String, String>>();
		String query = "";
		try {
		    System.out.println("DB: \t\t reading " + column1 + " and " + column2 + " from " + tablename);
			
			Statement st = db_connection.createStatement();
			query = "SELECT " + column1 + "," + column2 + " FROM " + tablename + "  ORDER BY 1 ASC, 2 ASC";
			ResultSet rs = st.executeQuery(query);
			while (rs.next()) {
				res.add(new Pair<String,String>(rs.getString(1),rs.getString(2)));
			}
			rs.close();
			st.close();
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		}
		return res;
	}
	
    public static void testpbd(PrintStream ps) {
	Variable behaviourv = new Variable("behaviour");
	for (int i=1; i<=3; i++) {
	    behaviourv.addValue("bb"+i);
	    behaviourv.addValue("bg"+i);
	}
	Vector<Variable> relevant = new Vector<Variable>();
	Variable tmprv = new Variable("rlstep6");
	for (int i=1; i<=3; i++) {
	    tmprv.addValue("bb"+i);
	    tmprv.addValue("bg"+i);
	}
	relevant.add(tmprv);
	tmprv = new Variable("rlstep4");
	tmprv.addValue("bb3");
	tmprv.addValue("bg3");
	relevant.add(tmprv);
	tmprv = new Variable("rlstep5");
	for (int i=1; i<=2; i++) {
	    tmprv.addValue("bb"+i);
	    tmprv.addValue("bg"+i);
	}
	relevant.add(tmprv);
	Iterator i = behaviourv.vals.iterator();
	while (i.hasNext()) {
	    String n = (String) i.next();
	    tmprv = new Variable("rn"+n);
	    tmprv.addValue(n);
	    relevant.add(tmprv);
	}
	printBehDyn(ps,relevant,behaviourv);
    }
    public static void printBehDyn(PrintStream ps, Vector<Variable> relevant, Variable behaviourv) {
	Vector<String> nonnullbehs = new Vector<String>();
	Iterator i = behaviourv.vals.iterator();
	while (i.hasNext()) {
	    String n = (String) i.next();
	    if (!(n.equalsIgnoreCase("nothing") ||
		  n.equalsIgnoreCase("other"))) {
		nonnullbehs.add(n);
	    }
	}
	printBehDynRec(ps,0,relevant,nonnullbehs);
    }
    // returns true if v has some element that is also in b  (v and b intersect)
    public static boolean hasMemberIn(Vector<String> v, Vector<String> b) {
	int k=0;
	while (k<v.size() && !(b.contains(v.elementAt(k)))) 
	    k++;
	return (k<v.size());
    }
    public static void printBehDynRec(PrintStream ps, int index, Vector<Variable> relevant, Vector<String> behList) {
	// assumes th rl_step abilities all come first, and that relevant is in the same order as abilVars.
	// relevant[i] is a Variable with the name of the ability, and values giving the behaviours that this ability is 
	// relevant to
	//ps.println("index is "+index+" and behList is ");
	//for (int i=0; i<behList.size(); i++) {
	//ps.println(behList.elementAt(i));
	//}
	if (relevant.isEmpty()) {
	    return;
	} else if (behList.isEmpty()) {
	    ps.print("(behaviournothing)");
	} else if (relevant.elementAt(index).name.startsWith("rl_step") || relevant.elementAt(index).name.startsWith("rlstep")) {
	    ps.print("("+relevant.elementAt(index).name+"  (yes   ");
	    printBehDynRec(ps,index+1,relevant,behList); 
	    ps.println(")");
	    ps.print("(no  ");
	    
	    Vector<String> newbehList = new Vector<String>(behList);
	    newbehList.removeAll(relevant.elementAt(index).vals);
	    printBehDynRec(ps,index+1,relevant,newbehList);  // still to be implemented:  removeAll - does this work? 
	    ps.println("))");
	} else {
	    // only behaviour_recall abilities left in the list, so we can go through them one by one
	    ps.println("[+");
	    for (int k=index; k<relevant.size(); k++) {		
		if (hasMemberIn(relevant.elementAt(k).vals,behList)) {  
		    ps.print("("+relevant.elementAt(k).name+"'   (yes  [+");
		    for (int j=0; j<relevant.elementAt(k).vals.size(); j++) {
			if (behList.contains(relevant.elementAt(k).vals.elementAt(j)))    
			    ps.print("(genericBehDyn"+relevant.elementAt(k).vals.elementAt(j)+")  ");
		    }
		    ps.println("])");
		    ps.println("(no  (0.0)))");
		}
	    }
	    ps.println("(beh_none_relevant)");
	    ps.println("]");
	}	    
    }
    public static class Variable {
	public String name;
	public Vector<String> vals;
	public Variable(String nm) {
	    name = nm;
	    vals = new Vector<String>();
	}
	public void addValue(String val) {
	    vals.add(val);
	}
	public void printOut(PrintStream ps) {
	    ps.print("("+name+" ");
	    for (int i=0; i<vals.size(); i++) {
		ps.print(" "+vals.elementAt(i)+" ");
	    }
	    ps.println(")");
	}
	public int findVal(String findme)
	{
	    int i=0;
	    boolean found=false;
	    while (i<vals.size() && !found) {
		found = findme.equalsIgnoreCase(vals.elementAt(i));
		i++;
	    }
	    if (found && i<=vals.size())
		return i-1;
	    else
		return -1;
	    
	}
    }
    public static void printVars(Vector<Variable> theVars, PrintStream ps, String vartype, Vector<Variable> extravars) {
	
	ps.println("("+vartype);
	// print out variable names
	for (int i=0; i<theVars.size(); i++) {
		ps.print("\t");
	    theVars.elementAt(i).printOut(ps);
	}
	if (extravars != null) {
		// print out extra variable names
		for (int i=0; i<extravars.size(); i++) {
			ps.print("\t");
			extravars.elementAt(i).printOut(ps);
		}
	}
	ps.println(")");
	    
    }
    
    /// This methods gets environment variables and their values from the database.
    /// @param db_connect - connection to the data base
    /// @table - table name in the database
    /// @var_column - the name of the column in the table which stores names of variables
    /// @val_column - column which stores values of variables
    public static Vector<Variable> gatherVarVal_db(Connection db_connection, String table, String var_column, String val_column, String description) 
	throws java.io.IOException
    {
    	System.out.println("DB: Reading all " + description + " from the database:");
		Vector<Variable> theVars = new Vector<Variable>();
		try {
			Statement st = db_connection.createStatement();
			// (*) get all distinct variables
			ResultSet rs = st.executeQuery("SELECT distinct "+var_column+" FROM "+table +" ORDER BY 1 ASC");
			while (rs.next()) {
				String var_name = rs.getString(1);
				Variable tempv = new Variable(var_name);
				if ( description.startsWith("ability") ) {
					// (*) if this is ability then just add two values by default
					tempv.addValue("yes");
					System.out.println("DB: \tvariable="+var_name+" with value=yes added by default");
					tempv.addValue("no");
					System.out.println("DB: \tvariable="+var_name+" with value=no added by default");
					theVars.add(tempv);
				} else {
					Statement st2 = db_connection.createStatement();
					// (*) get all values of the current variable var_name
					ResultSet rs2 = st2.executeQuery("SELECT "+var_column+","+val_column+" FROM "+table+" where "+var_column+"='"+var_name+"'  ORDER BY 1 ASC, 2 ASC");
					while (rs2.next()) {
						String var_value = rs2.getString(2);
						System.out.println("DB: \tvariable="+var_name+" with value="+var_value+" read from the database");
						tempv.addValue(var_value);
					}
					theVars.add(tempv);
					st2.close();
					rs2.close();
				}
			}
			rs.close();
			st.close();
			return theVars;
		} catch(SQLException e) {
		    System.out.println("DB: Connection Failed! Check output console");
		    e.printStackTrace();
		    System.exit(1);
		    return null;
		}		
	}
    
    /// Reads in values of the variable which has dedicated table: e.g. behaviour.
    public static Variable gatherVar_db(Connection db_connection, String var_name, String table, String var_column, String description) 
	throws java.io.IOException
    {
    	System.out.println("DB: Reading " + description + " from the database:");
		Variable theVar = new Variable(var_name);
		try {
			Statement st = db_connection.createStatement();
			// (*) get all variables
			ResultSet rs = st.executeQuery("SELECT "+var_column+" FROM "+table + " ORDER BY 1 ASC");
			while (rs.next()) {
				String var_value = rs.getString(1);
				theVar.addValue(var_value);
				System.out.println("DB: \tvariable=behaviour with value="+var_value+" read from the database");
			}
			rs.close();
			st.close();
			return theVar;
		} catch(SQLException e) {
		    System.out.println("DB: Connection Failed! Check output console");
		    e.printStackTrace();
		    System.exit(1);
		    return null;
		}		
	}
  
    public static Variable gatherVar(String vartype, BufferedReader in, String varname) 
	throws java.io.IOException
    {
	String tmp;
	Variable tmpv = new Variable(varname);
	if (!varname.equalsIgnoreCase("")) {
	    if (vartype.startsWith("behaviour")) {
		tmpv.addValue("nothing");
		tmpv.addValue("other");
	    }
	    do {
		System.out.print("enter value name for "+vartype+" "+tmpv.name+" (or enter if done):");
		tmp = in.readLine().trim();
		if (!tmp.equalsIgnoreCase(""))
		    tmpv.addValue(tmp);
	    } while (!tmp.equalsIgnoreCase(""));
	} 
	return tmpv;
    }
    
    public static Vector<Variable> gatherVars(String vartype, BufferedReader in) 
	throws java.io.IOException
    {
	String tmp;
	Variable tmpv;
	Vector<Variable> theVars = new Vector<Variable>(); 
	do {
	    System.out.print("enter "+vartype+"  name (or enter if done):");
	    tmp = in.readLine();
	    tmp.trim();
	    //System.out.println("you entered "+tmp);
	    if (!tmp.equalsIgnoreCase("")) {
		if (vartype.startsWith("ability")) {
		    // special case - always yes/no
		    tmpv = new Variable(tmp);
		    tmpv.addValue("yes");
		    tmpv.addValue("no");
		} else {
		    tmpv = gatherVar(vartype, in, tmp);
		}
		theVars.add(tmpv);
	    }
	} while (!tmp.equalsIgnoreCase(""));
	return theVars;
    }
    public static int findVar(Vector<Variable> theVars, String findme)
    {
	int i=0;
	boolean found=false;
	while (i<theVars.size() && !found) {
	    found = findme.equalsIgnoreCase(theVars.elementAt(i).name);
	    i++;
	}
	if (found && i<=theVars.size())
	    return i-1;
	else
	    return -1;
	    
    }
    
    
    public static double [] enumerateStates(Vector<Variable> theVars, String getitem,  BufferedReader in, PrintStream ps, String dflt)
	throws java.io.IOException
    {
	int [] currentstate = new int[theVars.size()];
	for (int j=0; j<currentstate.length; j++) {
	    currentstate[j] = 0;
	}

	// compute total number of states
	int numstates=1;
	for (int j=0; j<theVars.size(); j++) {
	    numstates *= theVars.elementAt(j).vals.size();
	}
	double [] thevals = new double[numstates];
	boolean done = false;
	boolean finished = false;
	String querys;
	String tmp="";
	int k=0,i=0,j=0;
	while (!done) {
	    querys = "";
	    for (i=0; i<currentstate.length; i++)  {
		// actually need the variables in reverse
		j=currentstate.length-i-1;
		querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
	    }
	    querys = querys+"\t enter value of "+getitem+": ";
	    
	    tmp = readIn(in, querys, dflt);
	    thevals[k++] = Double.parseDouble(tmp);

	    // update currenstate
	    finished = false;
	    i=0;
	    while (!finished) {
		currentstate[i]++;
		j=currentstate.length-i-1;
		if (currentstate[i] == theVars.elementAt(j).vals.size()) {
		    currentstate[i]=0;
		    i++;
		} else {
		    finished = true;
		}
		if (i==theVars.size()) {
		    finished = true;
		    done = true;
		}
	    }
	}
	return thevals;
	
    }

    /// The default value of the reward comes from the DB, this is for the reward_id in t_rewards where there is nothing in t_rewards_desc
    public static double [] enumerateStates_db_rewards(Connection connection, Vector<Variable> theVars, String getitem,  BufferedReader in, PrintStream ps, String dflt)
	throws java.io.IOException
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		double [] thevals = new double[numstates];
		boolean done = false;
		boolean finished = false;
		String querys;
		String tmp="";
		int k=0,i=0,j=0;
		while (!done) {
		    querys = "";
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
				querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
		    }
		    
			if (CCmdLineParams.getInstance().non_interactive() == true) {
			    querys = "DB: for state " + querys + " read the value of " + getitem + ": ";
				System.out.print(querys);
				String query = "";
				try {
					Statement strewardids = connection.createStatement();
					
					query = "SELECT state_set_id, reward_value FROM t_rewards ORDER BY 1 ASC, 2 ASC";

					ResultSet rsrewardsids = strewardids.executeQuery(query);
					
					// behaviour may be relevant in many sets of states so we have to iterate over beh_relevanc_id which identifies the set of states
					// in which this behaviour is relevant; we have to iterate over them to see which of them satisfies our state
					double state_reward = Double.parseDouble(dflt);
					while (rsrewardsids.next()) { // loop over sets of state
						double reward = rsrewardsids.getDouble(2);
					
						Statement st = connection.createStatement();
						query = "SELECT var_name, var_value FROM t_rewards_desc";
						query = query + " WHERE state_set_id='" + rsrewardsids.getString(1) + "' ORDER BY 1 ASC, 2 ASC";
						boolean OK = true;
						ResultSet rs = st.executeQuery(query);
						while (rs.next()) {
							String var_name = rs.getString(1);
							String var_value = rs.getString(2);
						    for (i=0; i<currentstate.length; i++)  {
								// actually need the variables in reverse
								j=currentstate.length-i-1;
								if ( var_name.compareTo(theVars.elementAt(j).name) == 0 ) {
									if (var_value.compareTo(theVars.elementAt(j).vals.elementAt(currentstate[i])) != 0) {
										OK = false;
									}
								}
						    }
						}
						if ( OK == true ) {
							state_reward += reward;
						}
					}
					tmp = Double.toString(state_reward);
					System.out.println(tmp + " (from the DB)");
					
					rsrewardsids.close();
					strewardids.close();			
				} catch(SQLException e) {
				    System.out.println("DB: this query failed: '" + query + "'");
				    e.printStackTrace();
				    System.exit(1);
				    return thevals;
				}
			} else {
				// (*) get the value from STDIN
			    querys = querys+"\t enter value of "+getitem+": ";
				tmp = readIn(in, querys, dflt);
			}
		    thevals[k++] = Double.parseDouble(tmp);
	
		    // update currenstate
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }

    /**
     * @param beh_name
     */
    public static double [] enumerateStates_db_satisfied4behaviour(Connection connection, Vector<Variable> theVars, String beh_name)
	throws Exception
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		double [] thevals = new double[numstates];
		boolean done = false;
		boolean finished = false;
		int k=0,i=0,j=0;
		while (!done) {
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
		    }
		    
		    boolean atLeastOne = false;
			if (CCmdLineParams.getInstance().non_interactive() == true) {

				// at least one precondition should satisfied the state
				String query = "";
				try {
					Statement stids = connection.createStatement();
					
					query = "SELECT beh_effect_id FROM t_effects_of_behaviours";
					query = query + " WHERE beh_name='" + beh_name + "' ORDER BY 1 ASC";

					ResultSet rsids = stids.executeQuery(query);
					
					while (rsids.next()) { // loop over effects of behaviours
						String stateid = rsids.getString(1);
					
						Statement st = connection.createStatement();
						query = "SELECT var_name, var_value FROM t_preconditions4effects_of_behaviours";
						query = query + " WHERE beh_effect_id=" + stateid + " ORDER BY 1 ASC, 2 ASC"; 
						ResultSet rs = st.executeQuery(query);
						
						if (rs.next()) { // when no preconditions defined, so assume that behaviour is always satisfied
							rs = st.executeQuery(query); // need to query again here
							boolean allValuesOK = true; // satisfied by default before search starts
							boolean stateOK = true;
							while (rs.next()) { // loop over specification the current set of states
								// get the current (var,value) pair
								String satisfied_var_name = rs.getString(1);
								String satisfied_var_value = rs.getString(2);
								// check if this pair does not contradict the current state k which is being processed in this iteration of the main loop while.
							    for (i=0; i<currentstate.length; i++)  {
									// actually need the variables in reverse
									j=currentstate.length-i-1;
									if ( satisfied_var_name.compareTo(theVars.elementAt(j).name) == 0) {
										if ( satisfied_var_value.compareTo(theVars.elementAt(j).vals.elementAt(currentstate[i])) != 0) {
											allValuesOK = false;
										}
									} else {
										stateOK = false;
									}
							    }
							}
							if ( stateOK && allValuesOK ) {
								atLeastOne = true;
								break;
							}
						} else {
							atLeastOne = true;
							break;
						}
					}
					rsids.close();
					stids.close();			
				} catch(SQLException e) {
				    System.out.println("DB: this query failed: '" + query + "'");
				    e.printStackTrace();
				    System.exit(1);
				    return thevals;
				}
			} else {
				// (*) get the value from STDIN
				throw new Exception("ERROR: reading from stdin not implemented");
			}
			if ( atLeastOne ) {
				thevals[k++] = 1;
			} else {
				thevals[k++] = 0;
			}
	
		    // update currenstate
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }    
    
    /// @param beh_name - we are reading impossible states for behaviour beh_name
    /// @param tablename - the name of the table from which we are going to read data for joint states
    public static double [] enumerateStates_db_impossible4behaviour(Connection connection, String tablename, Vector<Variable> theVars, String getitem, String beh_name,  BufferedReader in, PrintStream ps, String dflt)
	throws java.io.IOException
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		double [] thevals = new double[numstates];
		boolean done = false;
		boolean finished = false;
		String querys;
		String tmp="";
		int k=0,i=0,j=0;
		while (!done) {
		    querys = "";
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
				querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
		    }
		    
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				// (*) get the value from the database from the t_joint_state table
			    querys = "DB: for state " + querys + " read if behaviour " + beh_name + " is possible 0(yes), 1(no): ";
				System.out.print(querys);
					
				String query = "";
				try {
					Statement stids = connection.createStatement();
					
					query = "SELECT state_set_id FROM " + tablename;
					query = query + " WHERE beh_name='" + beh_name + "' ORDER BY 1 ASC";

					ResultSet rsids = stids.executeQuery(query);
					
					// behaviour may be relevant in many sets of states so we have to iterate over beh_relevanc_id which identifies the set of states
					// in which this behaviour is relevant; we have to iterate over them to see which of them satisfies our state
					tmp = null;
					while (rsids.next()) { // loop over sets of state
						String stateid = rsids.getString(1);
					
						Statement st = connection.createStatement();
						query = "SELECT var_name, var_value FROM " + tablename;
						query = query + " WHERE state_set_id=" + stateid + " AND beh_name='" + beh_name + "' ORDER BY 1 ASC, 2 ASC"; 
						ResultSet rs = st.executeQuery(query);
						boolean OK = true;
						if ( tmp == null) {
							tmp = "0"; // possible by default before search starts
						}
						while (rs.next()) { // loop over specification the current set of states
							// get the current (var,value) pair
							String impossible_var_name = rs.getString(1);
							String impossible_var_value = rs.getString(2);
							// check if this pair does not contradict the current state k which is being processed in this iteration of the main loop while.
						    for (i=0; i<currentstate.length; i++)  {
								// actually need the variables in reverse
								j=currentstate.length-i-1;
								if ( impossible_var_name.compareTo(theVars.elementAt(j).name) == 0) {
									if ( impossible_var_value.compareTo(theVars.elementAt(j).vals.elementAt(currentstate[i])) != 0) {
										OK = false;
									}
								}
						    }
						}
						if ( OK == true ) {
							tmp = "1";
							System.out.print(" IMPOSSIBLE ");
							break;
						}
					}
					if ( tmp==null ) {
						tmp = "0";
						System.out.print(" POSSIBLE ");
					}
					if (tmp.compareTo("0") == 0) {
						System.out.print(" POSSIBLE ");
					}
					
					System.out.println(tmp + " (from the DB)");
					rsids.close();
					stids.close();			
				} catch(SQLException e) {
				    System.out.println("DB: this query failed: '" + query + "'");
				    e.printStackTrace();
				    System.exit(1);
				    return thevals;
				}
			} else {
				// (*) get the value from STDIN
			    querys = querys+"\t enter value of "+getitem+": ";
				tmp = readIn(in, querys, dflt);
			}
		    thevals[k++] = Double.parseDouble(tmp);
	
		    // update currenstate
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }

    /// @return true when state currentstate is a goal state; in the current implementation the goal state is the state
    /// which has the highest reward in t_rewards
    public static boolean is_goal_state(Connection connection, int[] currentstate, Vector<Variable> theVars)
	throws java.io.IOException
	{
    	String query="";
    	// SELECT var_name, var_value, reward_value, t_rewards.state_set_id FROM t_rewards_desc INNER JOIN t_rewards ON t_rewards_desc.state_set_id=t_rewards.state_set_id WHERE reward_value=(SELECT MAX(reward_value) FROM t_rewards) ORDER BY 4;
		try {
			Statement st = connection.createStatement();			
			query = "SELECT var_name, var_value, reward_value, t_rewards.state_set_id FROM t_rewards_desc INNER JOIN t_rewards ON t_rewards_desc.state_set_id=t_rewards.state_set_id WHERE reward_value=(SELECT MAX(reward_value) FROM t_rewards) ORDER BY 4";
			ResultSet rs = st.executeQuery(query);
			boolean initialised = false;
			boolean OK = true;
			int prev_state_set_id = -1;
			while (rs.next()) {
				int curr_state_set_id = Integer.parseInt(rs.getString(4));
				// This works on a condition that there is ORDER BY 4 at the end of the query above
				if ( !initialised ) {
					initialised = true;
				} else {
					// we are starting the next state set
					if (prev_state_set_id != curr_state_set_id) {
						// if the previous set was OK then the current state is covered by this set
						if ( OK ) {
							return true;
						} else {
							// reset OK and maybe the next set of states will satisfy the state
							OK = true;
						}
					}
				}
				// get the goal (var,value) pair
				String goal_var_name = rs.getString(1);
				String goal_var_value = rs.getString(2);
				// check if this pair does not contradict the current state
			    for (int i=0; i<currentstate.length; i++)  {
					// actually need the variables in reverse
					int j=currentstate.length-i-1;
					if ( goal_var_name.compareTo(theVars.elementAt(j).name) == 0) {
						if ( goal_var_value.compareTo(theVars.elementAt(j).vals.elementAt(currentstate[i])) != 0) {
							// the current set does not cover the current state
							OK = false;
						}
					}
			    }
			    prev_state_set_id = curr_state_set_id;
			}
			rs.close();
			st.close();
			return OK;
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		}
    	return false;
    }

    public static String [] enumerateStates_is_goal(Connection connection, Vector<Variable> theVars, String goal_value, String nongoal_value)
	throws java.io.IOException
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		String [] thevals = new String[numstates];
		boolean done = false;
		boolean finished = false;
		String querys;
		String tmp="";
		int k=0,i=0,j=0;
		while (!done) {
		    querys = "";
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
				querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
		    }
		    
		    querys = "DB: for state " + querys + " read if this is a goal state: ";
			System.out.print(querys);
			if ( is_goal_state(connection, currentstate, theVars) == true) {
				tmp = goal_value;
				System.out.print(" GOAL with value ");
			} else {
				tmp = nongoal_value;
				System.out.print("NOT GOAL with value ");
			}
			System.out.println(tmp + " (from the DB)");
		    thevals[k++] = tmp;
	
		    // update currenstate
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }    
    
    /// @param beh_name - we are reading relevant states for behaviour beh_name
    public static double [] enumerateStates_relevant4nothing(Connection connection, Vector<Variable> theVars, String beh_name,  BufferedReader in, PrintStream ps, String dflt)
	throws java.io.IOException
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		double [] thevals = new double[numstates];
		boolean done = false;
		boolean finished = false;
		String querys;
		String tmp="";
		int k=0,i=0,j=0;
		while (!done) {
		    querys = "";
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
				querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
		    }
		    
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				
		    querys = "DB: for state " + querys + " read if behaviour " + beh_name + " can achieve something (is relevant) 1(yes), 0(no): ";
			System.out.print(querys);
				// if this is behaviour nothing, then it is relevant in the goal state, so we need to check
				// here if the current state falls into the set of goal states
				if ( beh_name.compareTo("nothing") == 0 && is_goal_state(connection, currentstate, theVars) == true) {
					tmp = "1";
					System.out.print(" RELEVANT ");
				} else {
					tmp = "0";
					System.out.print("NOT RELEVANT ");
				}
				System.out.println(tmp + " (from the DB)");
			} else {
				// (*) get the value from STDIN
			    querys = querys+"\t enter value of "+querys+": ";
				tmp = readIn(in, querys, dflt);
			}
		    thevals[k++] = Double.parseDouble(tmp);
	
		    // update currenstate
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }
    
    /// @param getitem - not used in the DB mode
    /// @param beh_name - we are reading relevant states for behaviour beh_name
    public static String [] enumerateStates_db_relevant4behaviour(Connection connection,
    																Vector<Variable> theVars,
    																String getitem, IURow iurow,
    																BufferedReader in, PrintStream ps, String dflt)
	throws java.io.IOException
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		String [] thevals = new String[numstates];
		boolean done = false;
		boolean finished = false;
		String querys;
		String tmp="";
		int k=0,i=0,j=0;
		while (!done) {
		    querys = "";
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
				querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
		    }
		    
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				
			    querys = "DB: for state " + querys + " read if row " + iurow._row_id + " is relevant in this state 1(yes), 0(no): ";
				System.out.print(querys);
				String query = "";
				try {
					Statement stbehrelevids = connection.createStatement();
					
					Statement st = connection.createStatement();
					query = "SELECT var_name, var_value FROM t_iu_task_states WHERE row_id='" + iurow._row_id + "' ORDER BY 1 ASC, 2 ASC";
					ResultSet rs = st.executeQuery(query);
					boolean OK = true;
					while (rs.next()) { // loop over specification the current set of states
						// get the current (var,value) pair
						String relevant_var_name = rs.getString(1);
						String relevant_var_value = rs.getString(2);
						// check if this pair does not contradict the current state k which is being processed in this iteration of the main loop while.
					    for (i=0; i<currentstate.length; i++)  {
							// actually need the variables in reverse
							j=currentstate.length-i-1;
							if ( relevant_var_name.compareTo(theVars.elementAt(j).name) == 0) {
								if ( relevant_var_value.compareTo(theVars.elementAt(j).vals.elementAt(currentstate[i])) != 0) {
									OK = false;
									System.out.print("\nThis state feature does not match for relevance:");
									System.out.print(relevant_var_name + "=" + relevant_var_value + " against ");
									System.out.print(theVars.elementAt(j).name + "="+ theVars.elementAt(j).vals.elementAt(currentstate[i]) + "\n");
								}
							}
					    }
					}
					if ( OK == true ) {
						// to kiedy behaviour zamiast 1.0
						//tmp = "behaviour" + iurow._behaviour; // instead of 1.0, behaviour from this row is placed in the leaf node
						tmp = "1.0";
						System.out.print(" RELEVANT ");
					} else {
						tmp = "0.0";
						System.out.print(" NOT RELEVANT ");
					}
					System.out.println(tmp + " (from the DB)");
					stbehrelevids.close();
				} catch(SQLException e) {
				    System.out.println("DB: Connection Failed! Check output console");
				    System.out.println("DB: this query failed: '" + query + "'");
				    e.printStackTrace();
				    System.exit(1);
				    return thevals;
				}
			} else {
				// (*) get the value from STDIN
			    querys = querys+"\t enter value of "+getitem+": ";
				tmp = readIn(in, querys, dflt);
			}
		    thevals[k++] = tmp;
	
		    // update currenstate
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }    

    /// @return the effect of beh_name on variable var_name when this behaviour is executed in state 'state', the default value of this effect is default_value
    public static String get_effect_of_behaviour_in_state(
    		Connection connection,
    		String var_name,
    		String beh_name,
    		StateFactors state,
    		String default_value
    ) {
    	String dflt = null;

    	
    	String query = "";
		try {
			Statement st = connection.createStatement();
			
			// If there is no precondition defined then the outcome always happens (the state does not matter)
			query = "SELECT var_name, var_value FROM t_effects_of_behaviours";
			query = query + " WHERE t_effects_of_behaviours.beh_name='" + beh_name + "' AND t_effects_of_behaviours.var_name='" + var_name + "'";
			query = query + " AND t_effects_of_behaviours.beh_effect_id NOT IN (SELECT beh_effect_id FROM t_preconditions4effects_of_behaviours)";
			query = query + " ORDER BY 1 ASC, 2 ASC";

			logquery(query);
			
			ResultSet noprecoditionset = st.executeQuery(query);
			if ( noprecoditionset.next() ) {
				dflt = noprecoditionset.getString(2);
				System.out.println(dflt + " (from the DB without any preconditions)");
				st.close();			
				return dflt;
			}
			st.close();
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		    return null;
		}    	
    	
    	query = "";
		try {
			Statement stbeheffids = connection.createStatement();
			
			query = "SELECT beh_effect_id FROM t_effects_of_behaviours";
			query = query + " WHERE t_effects_of_behaviours.beh_name='" + beh_name + "' AND t_effects_of_behaviours.var_name='" + var_name + "'";
			query = query + " ORDER BY 1 ASC";

			ResultSet rsbeheffids = stbeheffids.executeQuery(query);
			
			// this effect may be a result of different sets of states, and each set of states will have different beheffid, we have
			// to iterate over them to see which of them satisfies our state 'state'
			while (rsbeheffids.next()) {
				String beheffid = rsbeheffids.getString(1);
				
				query = "SELECT t_preconditions4effects_of_behaviours.var_name, t_preconditions4effects_of_behaviours.var_value, t_effects_of_behaviours.var_value FROM t_effects_of_behaviours INNER JOIN t_preconditions4effects_of_behaviours";
				query = query + " ON t_effects_of_behaviours.beh_effect_id=t_preconditions4effects_of_behaviours.beh_effect_id";
				query = query + " WHERE t_effects_of_behaviours.beh_name='" + beh_name + "' AND t_effects_of_behaviours.var_name='" + var_name + "'";
				query = query + " AND t_effects_of_behaviours.beh_effect_id=" + beheffid;
				query = query + " ORDER BY 1 ASC, 2 ASC, 3 ASC";

				Statement st = connection.createStatement();
				
				ResultSet rs = st.executeQuery(query);
				boolean OK = true;
				String effect_new_var_value = null;
				while (rs.next()) {
					if (effect_new_var_value == null) {
						effect_new_var_value = rs.getString(3);
					}
					// rs contains all preconditions which have to be satisfied in the current state
					String precond_var = rs.getString(1);
					if (rs.wasNull()) {
						continue;
					}
					String precond_value = rs.getString(2);
					if (rs.wasNull()) {
						continue;
					}
					// check the current state with the current precondition
					String state_value = state.getVal(precond_var);
					if(precond_value.compareTo( state_value ) != 0 ) {
						// if variable theVars.elementAt(j).name is found in rs, then
						OK = false;
					}
				}
				if (OK == true) {
					if (dflt == null) {
						dflt = effect_new_var_value;
					} else {
						if (dflt.compareTo(effect_new_var_value) != 0) {
							System.out.print("DB: from this state: ");
							state.print();
							System.out.println(" behaviour " + beh_name + " has two different outcomes " + dflt + " and " + effect_new_var_value + " on variable " + var_name);
							System.out.println("DB: this is wrong there should be only one effect possible in this situation");
							System.exit(1);
						} else {
							dflt = effect_new_var_value;
						}
					}
				}
			}
			if (dflt == null) {
				dflt = default_value;
			}
			System.out.println(dflt + " (from the DB)");
			rsbeheffids.close();
			stbeheffids.close();			
		} catch(SQLException e) {
		    System.out.println("DB: this query failed: '" + query + "'");
		    e.printStackTrace();
		    System.exit(1);
		    return null;
		}    	
    	
    	return dflt;
    }
    
    /// This method reads actions effects: P(effect|current state, behaviour).
    /// @param connection - connection to the database
    /// @param var_name - name of the variable whose effects we are looking for in one call to this funcion
    /// @param beh_name - we are reading effects of behaviour beh_name on variable var_name
    public static String [] enumerateStates_db_effects(	Connection connection,
    													Vector<Variable> theVars,
    													String var_name,
    													String beh_name,
    													BufferedReader in,
    													PrintStream ps)
	throws java.io.IOException
    {
		int [] currentstate = new int[theVars.size()];
		for (int j=0; j<currentstate.length; j++) {
		    currentstate[j] = 0;
		}
	
		// compute total number of states
		int numstates=1;
		for (int j=0; j<theVars.size(); j++) {
		    numstates *= theVars.elementAt(j).vals.size();
		}
		String [] thevals = new String[numstates];
		boolean done = false;
		boolean finished = false;
		String querys;
		String tmp="";
		String dflt="###";
		int k=0,i=0,j=0;
		while (!done) {
		    querys = "";
		    StateFactors state = new StateFactors();
		    for (i=0; i<currentstate.length; i++)  {
				// actually need the variables in reverse
				j=currentstate.length-i-1;
				querys = querys+"\t "+theVars.elementAt(j).name+"="+theVars.elementAt(j).vals.elementAt(currentstate[i]);
				
				// store the current state in object state
				state.add(theVars.elementAt(j).name, theVars.elementAt(j).vals.elementAt(currentstate[i]) );
				
				// Set the default, dflt, value here. The default value of variable var_name is the value of this variable which is in the current state for
				// which the effect is being specified in this iteration of the main while loop.
				if ( var_name.compareTo(theVars.elementAt(j).name) == 0 ) {
					dflt = theVars.elementAt(j).vals.elementAt(currentstate[i]);
				}
		    }
		    
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				// (*) get the value from the database from the t_joint_state table
			    querys = "DB: for state " + querys + " read the effect of behaviour " + beh_name + " on variable " + var_name + ": ";
				System.out.print(querys);
				tmp = get_effect_of_behaviour_in_state(connection, var_name, beh_name, state, dflt);
			} else {
				// (*) get the value from STDIN
			    querys = querys+"\t enter the effect of behaviour " + beh_name + " on variable " + var_name + ": ";
				tmp = readIn(in, querys, dflt);
			}
		    thevals[k++] = var_name+tmp;
	
		    // update the current state
		    finished = false;
		    i=0;
		    while (!finished) {
			currentstate[i]++;
			j=currentstate.length-i-1;
			if (currentstate[i] == theVars.elementAt(j).vals.size()) {
			    currentstate[i]=0;
			    i++;
			} else {
			    finished = true;
			}
			if (i==theVars.size()) {
			    finished = true;
			    done = true;
			}
		    }
		}
		return thevals;
    }
    
    /// This method is for printing state values which are of type String.
    public static int printtree(Vector<Variable> theVars, String [] statevals, StringWriter ps) 
	throws java.io.IOException
    {
	return printtree(theVars,0,statevals,0,ps);
    }
    public static int printtree(Vector<Variable> theVars, int var, String [] statevals, int val, StringWriter ps)
	throws java.io.IOException 
    {
	// if at end
	if (var == theVars.size()) {
	    // write out the value corresponding to this value
	    // not sure if this is right syntax
	    ps.write("("+statevals[val]+")");
	    return ++val;
	}
	Variable thevar = theVars.elementAt(var);
	// I'm using theVars[i].name to mean the variable name here -
	ps.write("("+thevar.name+"  ");
	
	int newval;
	for (int i=0; i<thevar.vals.size(); i++) {
	    ps.write("("+thevar.vals.elementAt(i)+"  ");
	    newval = printtree(theVars,var+1,statevals,val,ps);
	    ps.write(")");
	    val = newval;
		if ( var == 0 && i < thevar.vals.size() - 1 ) {
			ps.write("\n");
			for (int k=0; k<=var; k++) {
				ps.write("\t");
			}
		}
	}
	ps.write(")");
	return val;
    }
    
    public static int printtree(Vector<Variable> theVars, double [] statevals, PrintStream ps) 
	throws java.io.IOException 
    {
	return printtree(theVars,0,statevals,0,ps);
    }
    public static int printtree(Vector<Variable> theVars, int var, double [] statevals, int val, PrintStream ps)
	throws java.io.IOException 
    {
	// if at end
	if (var == theVars.size()) {
	    // write out the value corresponding to this value
	    // not sure if this is right syntax
	    ps.print("("+statevals[val]+")");
	    return ++val;
	}
	Variable thevar = theVars.elementAt(var);
	// I'm using theVars[i].name to mean the variable name here -
	ps.print("("+thevar.name+"  ");
	
	int newval;
	for (int i=0; i<thevar.vals.size(); i++) {
	    ps.print("("+thevar.vals.elementAt(i)+"  ");
	    newval = printtree(theVars,var+1,statevals,val,ps);
	    ps.print(")");
	    val = newval;
		if ( var == 0 && i < thevar.vals.size() - 1 ) {
			ps.print("\n");
			for (int k=0; k<=var; k++) {
				ps.print("\t");
			}
		}
	}
	ps.print(")");
	return val;
    }
    
    public static String readIn(BufferedReader in, String query)
	throws java.io.IOException
    {
	String tmp = "";
	while (tmp.equalsIgnoreCase("")) {
	    System.out.print(query);
	    tmp = in.readLine().trim();
	}
	return tmp;
    }
    public static String readIn(BufferedReader in, String query, String deflt)
	throws java.io.IOException
    {
	System.out.print(query);
	System.out.print("\t ["+deflt+"] ");
	String tmp = in.readLine().trim();
	if (tmp.equalsIgnoreCase("")) 
	    return deflt;
	return tmp;
    }
    public static void userComment(String comment)
    {
	userComment(System.out,comment);
    }
    public static void userComment(PrintStream ps, String comment)
    {
	ps.println("*********************************************************************************************");
	ps.println(comment);
	ps.println("*********************************************************************************************");
    }

    /// This method opens the connection to the postgresql database. The database driver is required only
    /// in this function, so it won't be required in the interactive mode without the use of the 
    /// data base.
    /// @return the connection object is returned.
    static Connection open_db_connection() {
		System.out.println("DB: -------- PostgreSQL JDBC Connection Testing ------------");
		  
		try {
		    Class.forName("org.postgresql.Driver");
	 
		} catch (ClassNotFoundException e) {
		    System.out.println("DB: Where is your PostgreSQL JDBC Driver? Include in your library path!");
		    System.out.println("DB: In ubuntu 'sudo apt-get install libpg-java' may be required first!");
		    System.out.println("DB: after that the direver is here /usr/share/java/postgresql.jar");
		    e.printStackTrace();
		    System.out.println("DB: \ntry this when running this program from the command line:");
		    System.out.println("DB: java -classpath .:/usr/share/java/postgresql.jar snap.SnapGenerator -n\n");
		    System.out.println("DB: If you are running the snap generator from the jar file, the JDBC postgres drives has to be here: /usr/share/java/postgresql.jar\n");
		    System.exit(1);
		    return null;
		}
	 
		System.out.println("DB: PostgreSQL JDBC Driver Registered!");
		
		String url = "jdbc:postgresql://" + CCmdLineParams.getInstance().dbserver() + "/" + CCmdLineParams.getInstance().dbname();
		Properties props = new Properties();
		props.setProperty("user", CCmdLineParams.getInstance().dbuser());
		String password = CCmdLineParams.getInstance().dbpassord();
		if ( password != null ) {
			props.setProperty("password", password );
		} else {
			props.setProperty("password", readPassAsString(CCmdLineParams.getInstance().dbpassordfile()) );
		}
		// In order to use ssl for connecting to postgres, the database has to be configured for ssl first.
		// props.setProperty("ssl","true");
		props.setProperty("port", "5432");
		try {
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("DB: Connection to "+url+" openned successfully");
			return conn;
		} catch(SQLException e) {
		    System.out.println("DB: Connection Failed! Check output console");
		    e.printStackTrace();
		    System.exit(1);
		    return null;
		}    	
    }
    
    public static void main(String[] args) throws Exception
    {
    // Parse the command line parameters and store them in the singleton object.
	CCmdLineParams.getInstance().parse_args(args);

	Connection db_connection=null;
	// The DB connection is required in the non-interactive mode.
	if (CCmdLineParams.getInstance().non_interactive() == true) {
		db_connection = open_db_connection();
	}
	String querys;
	String tmp="";
	int tmpi;
	Vector<Variable> taskVars;
	Vector<Variable> obsVars;
	Vector<Variable> abilVars;
	Vector<Variable> stateVars;
	Vector<Variable> behaviour = new Vector<Variable>();
	Variable behaviourv;

	Vector<Variable> relevant = new Vector<Variable>();

	Variable tmpv, tmpo;
	double tmpd;
	FileOutputStream out; 
	PrintStream outp; 
	double [] vals;
	String [] strvals;
	        
	//outp = System.out;
	//testpbd(System.out);
	try {
		// The out file name has to be given as a command line parameter, e.g, '-o snap.txt'.
	    out = new FileOutputStream(CCmdLineParams.getInstance().out_file_name());
	    outp = new PrintStream( out );

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		outp.println("// POMDP generated using WEB-SNAP on " + format.format(new Date()));
		outp.println("// http://www.cs.uwaterloo.ca/~mgrzes/code/web-snap");
		outp.println("// Marek Grzes, 2013\n");

	    try {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		userComment("Welcome to SNAP Generator (quick and dirty version)\nYou will be asked an interminable series of questions for a single IU group by the program.\nEach section of questions will be preceded by a small description of what you are about to do.\nDefault answers for questions are shown in square brackets [0.0].\nIMPORTANT: If you make a mistake, you'll have to start all over again from the start, so don't screw up!.\nIf you do screw up (remember, *don't* screw up), best to make a note of where you made the mistake, and then go back in later and fix by hand.\n\nWhen you are done, your POMDP will be in a file called 'snap.txt'. Don't forget to rename this file.\n\n When you are ready to start, hit any key.");
		if (CCmdLineParams.getInstance().non_interactive() == false) {		
			in.readLine();
		}
		
		userComment("First, specify task variables. These are the elements of the IU 'goals' and the 'environment state'.");
		if (CCmdLineParams.getInstance().non_interactive() == true) {
			taskVars = gatherVarVal_db(db_connection, "t_env_variables_values", "var_name", "var_value", "environmet variables and values");
		} else {
			taskVars = gatherVars("task variable", in);
		}
		userComment("Now, add one 'behaviour' for each element in the IU 'action' column\nYou do not need to add 'doing nothing', or 'doing something totally off task");
		// behaviours nothing and other get added automatically
		if (CCmdLineParams.getInstance().non_interactive() == true) {
			behaviour.add(gatherVar_db(db_connection,  "behaviour", "t_behaviours", "beh_name", "behaviours"));
		} else {
			behaviour.add(gatherVar("behaviour",in,"behaviour"));
		}
		userComment("Now, add one 'ability' for each element in the IU 'Rn/Rl/Af' column. Only add each ability once.");
		if (CCmdLineParams.getInstance().non_interactive() == true) {
			abilVars = gatherVarVal_db(db_connection, "t_abilities", "abil_name", "-- NOT USED --", "ability elements");
		} else {
			abilVars = gatherVars("ability variable",in);
		}
		userComment("Now, add one observation for each sensor that plays a role in this IU group.");
		if (CCmdLineParams.getInstance().non_interactive() == true) {
			obsVars = gatherVarVal_db(db_connection, "t_observations_values", "obs_name", "obs_value", "observations");
		} else {
			obsVars = gatherVars("observation", in);
		}
		
		// only one behaviour variable, so we make a shortcut to it
		behaviourv = behaviour.elementAt(0);

		stateVars = new Vector<Variable>();
		stateVars.addAll(taskVars);
		stateVars.addAll(behaviour);
		stateVars.addAll(abilVars);

		outp.println("// VARIABLES");
		Vector<Variable> extravars = new Vector<Variable>();
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			Variable pc = new Variable("parent_control");
			pc.addValue("no");
			pc.addValue("yes");
			extravars.add(pc);
		}
		if (CCmdLineParams.getInstance().is_call_caregiver_required() == true) {
			Variable pc = new Variable("caregiver_present");
			pc.addValue("no");
			pc.addValue("yes");
			extravars.add(pc);
		}
		printVars(stateVars,outp,"variables",extravars);
		outp.println("\n// OBSERVATIONS");
		extravars.clear();
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			Variable cobs = new Variable("control_obs");
			cobs.addValue("no");
			cobs.addValue("yes");
			extravars.add(cobs);
		}
		printVars(obsVars,outp,"observations",extravars);
		outp.println("unnormalised");
		System.out.println();
		// sensor model
		userComment("Now, add one sensor model for each sensor observation giving the reliability levels for that sensor\n Each sensor will be associated with one state variable (task or behaviour or ability)");
		outp.println("\n// SENSOR MODELS");
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.print("" + 
			"dd control_obsOF\n" +
			"    (parent_control' (yes    (control_obsyes))\n" +
			"                     (no     (control_obsno)))\n" +
			"enddd\n");
		}
		for (int i=0; i<obsVars.size(); i++) {
		    tmpo = obsVars.elementAt(i);
		    
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				// (*) reading the model of the current sensor from the database
			    System.out.println("DB: reading from the database the sensor model for "+tmpo.name);
			    tmpi = -1;
				try {
					boolean variable_sensor = false;
					boolean behaviour_sensor = false;
					Statement st = db_connection.createStatement();
					// Firstly check if the sensor depends on one variable only
					ResultSet rs = st.executeQuery("SELECT DISTINCT obs_name, var_name FROM t_sensor_model WHERE obs_name='"+tmpo.name+"' ORDER BY 1 ASC, 2 ASC");
					int rowCount = 0;
					while ( rs.next() ) rowCount++;
					if (rowCount == 1 ) {
						variable_sensor = true;
					}
					if ( rowCount > 1) {
						System.err.println("DB: Exactly one variable can be associated with sensor "+tmpo.name+"! Check t_sensor_model WHERE obs_name="+tmpo.name);
						System.exit(1);
					}
					rs = st.executeQuery("SELECT DISTINCT obs_name FROM t_behaviour_sensor_model WHERE obs_name='"+tmpo.name+"' ORDER BY 1 ASC");
					rowCount = 0;
					while ( rs.next() ) rowCount++;
					if (rowCount == 1) {
						behaviour_sensor = true;
					}
					if ( variable_sensor && behaviour_sensor ) {
						System.err.println("DB: Sensor "+tmpo.name+" can be specified only in one of these two tables: t_sensor_model or t_behaviour_sensor_model! Check these tables WHERE obs_name="+tmpo.name);
						System.exit(1);
					}
				
					if (variable_sensor) {
						// Have to execute the same query again because the result set is not scrollable so cannot scroll it back.
						rs = st.executeQuery("SELECT DISTINCT obs_name, var_name FROM t_sensor_model where obs_name='"+tmpo.name+"' ORDER BY 1 ASC, 2 ASC");
						rs.next(); // this moves to the first element in the result set
						tmpi = findVar(stateVars,rs.getString(2));
						if (tmpi == -1) {
							System.err.println("DB: there is some inconsistency in the database involving variable "+rs.getString(2)+" and sensor "+tmpo.name);
							System.exit(1);
						}
						tmpv = stateVars.elementAt(tmpi);
						outp.println("dd "+tmpo.name+"OF");
						outp.print("\t("+tmpv.name+"'  ");
					    for (int j=0; j<tmpv.vals.size(); j++) {
					    	if (j>0) {
					    		outp.print("\t\t");
					    	}
							outp.print("("+tmpv.vals.elementAt(j)+"  ("+tmpo.name+"'  ");
							for (int k=0; k<tmpo.vals.size(); k++) {
								rs = st.executeQuery("SELECT probability FROM t_sensor_model WHERE obs_name='"+tmpo.name+"' and obs_value='"+tmpo.vals.elementAt(k)+"' and var_name='"+tmpv.name+"' and var_value='"+tmpv.vals.elementAt(j)+"'");
								if ( !rs.next() ) { // scroll to the first element in the result set
									System.out.println("DB:\tERROR P("+tmpo.name+"="+tmpo.vals.elementAt(k)+"|'"+tmpv.name+"'="+tmpv.vals.elementAt(j)+") has to be specified in t_sensor_model. <= THIS ERROR IS DUE TO WRONG MODEL IN THE DATABASE, PLEASE READ THE MESSAGE AND CORRECT YOUR MODEL.");
									System.exit(1);
								}
								tmpd = Double.parseDouble(rs.getString(1));
								System.out.println("DB:\tP("+tmpo.name+"="+tmpo.vals.elementAt(k)+"|"+tmpv.name+"="+tmpv.vals.elementAt(j)+")="+tmpd+" read from the database.");
								if (k>0) {
									outp.print("\t\t\t\t\t");
								}
							    outp.print("("+tmpo.vals.elementAt(k)+"  ("+tmpd+"))");
							    if (k<tmpo.vals.size()-1) {
							    	outp.println();
							    }
							}
							outp.print("))");
							if ( j < tmpv.vals.size() - 1 ) {
								outp.println();
							}
						}
						outp.println(")");
						outp.println("enddd");
					}
					
					if (behaviour_sensor) {
						outp.println("dd "+tmpo.name+"OF");
						outp.print("\t(behaviour'  ");
					    for (int j=0; j<behaviourv.vals.size(); j++) {
					    	if (j>0) {
					    		outp.print("\t\t");
					    	}
							outp.print("("+behaviourv.vals.elementAt(j)+"  ("+tmpo.name+"'  ");
							for (int k=0; k<tmpo.vals.size(); k++) {
								rs = st.executeQuery("SELECT probability FROM t_behaviour_sensor_model WHERE obs_name='"+tmpo.name+"' and obs_value='"+tmpo.vals.elementAt(k)+"' and beh_name='"+behaviourv.vals.elementAt(j)+"'");
								// TODO: we could do something in order to skip those behaviours which are not defined in t_behaviour_sensor_model, but then we would need some
								// kind of default impact of those behaviours on each behaviour sensor.
								if ( !rs.next() ) { // scroll to the first element in the result set
									System.out.println("DB:\tP("+tmpo.name+"="+tmpo.vals.elementAt(k)+"|behaviour="+behaviourv.vals.elementAt(j)+") has to be specified in t_behaviour_sensor_model.");
									System.exit(1);
								}
							    tmpd = Double.parseDouble(rs.getString(1));
								System.out.println("DB:\tP("+tmpo.name+"="+tmpo.vals.elementAt(k)+"|behaviour="+behaviourv.vals.elementAt(j)+")="+tmpd+" read from the database.");
								if (k>0) {
									outp.print("\t\t\t\t\t");
								}
							    outp.print("("+tmpo.vals.elementAt(k)+"  ("+tmpd+"))");
							    if (k<tmpo.vals.size()-1) {
							    	outp.println();
							    }
							}
							outp.print("))");
							if ( j < behaviourv.vals.size() - 1 ) {
								outp.println();
							}
						}
						outp.println(")");
						outp.println("enddd");
					}
					
					rs.close();
					st.close();
				} catch(SQLException e) {
				    System.out.println("DB: Connection Failed! Check output console");
				    e.printStackTrace();
				    System.exit(1);
				}		
			}else{
				// (*) original interactive code
			    System.out.println("enter sensor model for "+tmpo.name);
			    tmpi=-1;
			    while (tmpi == -1) {
				System.out.print("what variable is associated with sensor "+tmpo.name+": ");
				tmp = in.readLine().trim();
				tmpi = findVar(stateVars,tmp);
			    }
			    tmpv = stateVars.elementAt(tmpi);
			    outp.println("dd "+tmpo.name+"OF");
			    outp.print("("+tmpv.name+"'  ");
			    for (int j=0; j<tmpv.vals.size(); j++) {
				outp.print("("+tmpv.vals.elementAt(j)+"  ("+tmpo.name+"'  ");
				for (int k=0; k<tmpo.vals.size(); k++) {
				    tmp = readIn(in,"rate in % for sensor "+tmpo.name+"="+tmpo.vals.elementAt(k)+" when "+tmpv.name+"="+tmpv.vals.elementAt(j)+": ","50.0");
				    tmpd = Double.parseDouble(tmp);
				    outp.println("("+tmpo.vals.elementAt(k)+"  ("+tmpd+"))");
				}
				outp.println("))");
			    }
			    outp.println(")");
			    outp.println("enddd");
			}
		}

		// (*) --------------------------- TIMEOUTS FOR BEHAVIOURS -----------------------------
		System.out.println();
		//timeouts model
		userComment("Each behaviour will have a timeout associated with it - timeout for behaviour B is the time we wait while the person is doing nothing in a situation where we expect them to do behaviour B");
		Vector<Pair<String, String>> vpss = get_two_columns_from_db(db_connection, "t_behaviours", "beh_name", "beh_timeout");
		outp.println("\n// TIMEOUTS FOR BEHAVIOURS");
		outp.println("adjunct timeoutdd");
		outp.print("(behaviour ");
		for ( int i = 0; i < vpss.size(); i++ ) {
			outp.print("(" + vpss.elementAt(i).getKey() + " (" + vpss.elementAt(i).getValue() + "))");
			System.out.println("DB:\t\t" + vpss.elementAt(i).getKey() + " has timeout " + vpss.elementAt(i).getValue());
		}
		outp.println(" )");
		
		// dynamics model
		System.out.println();

		// (*) --------------------------- EFFECTS AND PRECONDITIONS OF BEHAVIOURS -----------------------------
		userComment("Now enter dynamics model.  This is giving the effects of user behaviours on the environment state.\n Give the default values that would happen in an ideal world (for a cognitively able person, or a perfect robot)");
		outp.println("\n// EFFECTS AND PRECONDITIONS OF BEHAVIOURS");
		if (CCmdLineParams.getInstance().non_interactive() == true) {
			System.out.println("DB: reading in default behaviours from the database (behaviours nothing and other have default values).");
		}
		for (int i=0; i<taskVars.size(); i++) {
		    outp.println("dd "+taskVars.elementAt(i).name+"_dynamics");
		    outp.println("(behaviour'  ");
		    System.out.println();
		    for (int j=0; j<behaviourv.vals.size(); j++) {
				tmpi = -1;
				while (tmpi < 0) {
				    // default values for nothing and other
				    if (behaviourv.vals.elementAt(j).startsWith("nothing")) {
				    	tmp = "SAME";
				    } else if (behaviourv.vals.elementAt(j).startsWith("other")) {
				    	tmp = "other";
				    } else {
						querys = "new state for "+taskVars.elementAt(i).name+" after behaviour "+behaviourv.vals.elementAt(j)+" (SAME/ANY/";
						for (int k=0; k<taskVars.elementAt(i).vals.size(); k++) 
						    querys = querys+taskVars.elementAt(i).vals.elementAt(k)+"/";
		
						querys = querys+"): ";
					
						if (CCmdLineParams.getInstance().non_interactive() == true) {
							// (*) read from the database
							
							try {
								Statement st = db_connection.createStatement();
								ResultSet rs = st.executeQuery("SELECT var_value FROM t_effects_of_behaviours WHERE beh_name='"+behaviourv.vals.elementAt(j)+"' AND var_name='"+taskVars.elementAt(i).name+"' ORDER BY 1 ASC");
								if (rs.next()) {
									// This function here takes the variable 'taskVars.elementAt(i).name' and behaviour 'behaviourv.vals.elementAt(j)' and
									// has to assign a new value of this variable to 'strvals' for each state. Index of strvals is the index of the
									// enumerated state space. It means that each state can have only one possible effect on this variable (deterministic actions).
									strvals = enumerateStates_db_effects(db_connection, taskVars, taskVars.elementAt(i).name, behaviourv.vals.elementAt(j), in, System.out);
									StringWriter swtmp = new StringWriter();
									printtree(taskVars,strvals, swtmp); // append the whole three to a string
									tmp = swtmp.toString();
								} else {
									tmp = "SAME"; // default value when there is not entry in the DB
									System.out.println("DB:\tAfter behaviour="+behaviourv.vals.elementAt(j)+" a new value of var_name="+taskVars.elementAt(i).name+" is "+tmp+" (default value, not from DB)");
								}
								rs.close();
								st.close();			
							} catch(SQLException e) {
							    System.out.println("DB: Query Failed! Check output console");
							    e.printStackTrace();
							    System.exit(1);
							}						
						}else {
							// (*) reading in from STDIN
							strvals = enumerateStates_db_effects(db_connection, taskVars, taskVars.elementAt(i).name, behaviourv.vals.elementAt(j), in, System.out);
							StringWriter swtmp = new StringWriter();
							printtree(taskVars, strvals, swtmp); // append the whole three to a string
							tmp = swtmp.toString();
						}
				    }
				    tmpi = 0;
				    if (tmp.equalsIgnoreCase("SAME")) {
				    	tmp = "(SAME"+taskVars.elementAt(i).name+")";
				    } else if (tmp.equalsIgnoreCase("ANY")) {
				    	tmp = "(1.0)";
				    } else if (tmp.equalsIgnoreCase("other")) {
				    	tmp = "[+  (SAME"+taskVars.elementAt(i).name+")  (0.05)]";
				    } else if ( taskVars.elementAt(i).findVal(tmp) >= 0 ) {
				    	tmp = "("+taskVars.elementAt(i).name+tmp+")";
				    } else if (tmp.startsWith("(")) {
				    	// tmp contains the tree so just print it to the SPUDD file below
					} else {
				    	tmpi = -1;
				    	System.out.println("incorrect entry - try again");
				    }
				}
				outp.print("("+behaviourv.vals.elementAt(j)+" ");
				outp.println(tmp+")");
			
		    }
		    outp.println(")");
		    outp.println("enddd");
		}

		// (*) --------------------------- IN WHICH STATES GIVEN BEHAVIOUR WILL ACCOMPLISH SOMETHING  -----------------------------
		// behaviour relevance
		System.out.println();
		userComment("Specify in which states each behaviour is 'relevant' in the sense that in these states,\n" + 
				    "the user behavior will accomplish something. There should be one such relevance function for" +
				    "each user behavior (except 'other', which is never relevant)\nThese can also be read from the" +
				    "IU spreadsheet by reading across each row and relating the state to the action.\n " +
				    "The behaviour='nothing' is relevant in the final goal state for this IU group.");
        outp.println("\n// BEHAVIOUR RELEVANCE FUNCTIONS (FOR BEHAVIOURS OTHER AND NOTHING ONLY)\n");
        for (int i=0; i<behaviourv.vals.size(); i++) {
            if (behaviourv.vals.elementAt(i).equalsIgnoreCase("other")) {
                outp.println("// Behaviour other is almost never relevant\n");
            	outp.println("dd "+behaviourv.vals.elementAt(i)+"_relevant");
                outp.println("(0.001)"); // this means that other is never relevant
                outp.println("enddd\n");
                outp.println("dd not_"+behaviourv.vals.elementAt(i)+"_relevant");
                outp.println("[+ (1.0) [* (-1.0)   ("+behaviourv.vals.elementAt(i)+"_relevant)]]");
                outp.println("enddd\n");
            } else if (behaviourv.vals.elementAt(i).equalsIgnoreCase("nothing")) {
                outp.println("// Behaviour nothing is relevant in the goal state only\n");
            	outp.println("dd "+behaviourv.vals.elementAt(i)+"_relevant");
                vals = enumerateStates_relevant4nothing(db_connection, taskVars, behaviourv.vals.elementAt(i), in, System.out, "0");
                printtree(taskVars,vals,outp);
                outp.println("\nenddd\n");
                outp.println("dd not_"+behaviourv.vals.elementAt(i)+"_relevant");
                outp.println("[+ (1.0) [* (-1.0)   ("+behaviourv.vals.elementAt(i)+"_relevant)]]");
                outp.println("enddd\n");
            }
        }	

        outp.println("\n// BEHAVIOUR CAN HAPPEN FUNCTIONS FOR BEHAVIOURS THAT ARE NOT IN THE IU TABLE\n");
		Set<String> allIUBehaviurs = new HashSet<String>();
		Vector<IURow> iurows = get_all_iu_data_rows(db_connection, allIUBehaviurs);
        for (int i=0; i<behaviourv.vals.size(); i++) {
			if (behaviourv.vals.elementAt(i).equalsIgnoreCase("other")) {
				continue;
			}
			if (behaviourv.vals.elementAt(i).equalsIgnoreCase("nothing")) {
				continue;
			}
			if ( !allIUBehaviurs.contains(behaviourv.vals.elementAt(i))) {
				outp.println("dd "+behaviourv.vals.elementAt(i)+"_can_happen");
				// given the behaviour, get impossible states for the behaviour
				double[] impossible = enumerateStates_db_impossible4behaviour(db_connection, "t_when_is_behaviour_impossible", taskVars, "", behaviourv.vals.elementAt(i), in, System.out,"0");
				// given the behaviour, check if it is satisfied
				double[] satisfied = enumerateStates_db_satisfied4behaviour(db_connection, taskVars, behaviourv.vals.elementAt(i));
				// not impossible and satisfied
				if ( impossible.length != satisfied.length ) {
					throw new Exception("ERROR: arrays should have the same lenght here");
				}
				for ( int v = 0; v < satisfied.length; v++ ) {
					if ( satisfied[v] == 1 && impossible[v] == 0 ) {
						continue;
					} else {
						satisfied[v] = 0;
					}
				}
				printtree(taskVars,satisfied,outp);
			    outp.println("\nenddd");
			    outp.println("dd not_"+behaviourv.vals.elementAt(i)+"_can_happen");
			    outp.println("[+ (1.0) [* (-1.0)   ("+behaviourv.vals.elementAt(i)+"_can_happen)]]");
			    outp.println("enddd");
			}
		}        

        // for behaviours that are in the IU table we do not need behaviour relevance, row relevance from the IU table is used instead
        
		// (*) --------------------------- IN WHICH STATES GIVEN ROW WILL ACCOMPLISH SOMETHING  -----------------------------
		userComment("Reading row relevance from the database.");
		outp.println("\n// ROW RELEVANCE FUNCTIONS");
		for (int i=0; i<iurows.size(); i++) {
			outp.println("dd row"+iurows.get(i)._row_id+"_relevant");
			System.out.println();
			System.out.println("Specify relevance of row "+iurows.get(i)._row_id);
			tmp = "..row="+iurows.get(i)._row_id+" relevant (1=yes, 0=no)?";
			String [] stringvals = enumerateStates_db_relevant4behaviour(db_connection, taskVars, tmp, iurows.get(i), in, System.out, "0");
			StringWriter strwriter = new StringWriter();
			printtree(taskVars, stringvals, strwriter);
			outp.print(strwriter.toString());
			outp.println();
		    outp.println("enddd\n");
		    outp.println("dd not_row"+iurows.get(i)._row_id+"_relevant");
		    outp.println("\t[+  (1.0)  [*  (-1.0)  (row"+iurows.get(i)._row_id+"_relevant)]]");
		    outp.println("enddd\n");
		    outp.println("dd row"+iurows.get(i)._row_id+"_relevant_b");
		    outp.println("\t[*  (row"+iurows.get(i)._row_id+"_relevant)  (behaviour"+ iurows.get(i)._behaviour +")]");
		    outp.println("enddd\n");
		}

		userComment("Reading row ability relevance from the database.");
		outp.println("\n// ROW ABILITY RELEVANCE FUNCTIONS");
		for (int i=0; i<iurows.size(); i++) {
			outp.println("dd row"+iurows.get(i)._row_id+"_ability_relevant");
			outp.print("\t[*\t");
			for ( int a=0; a<iurows.get(i)._abilities.size(); a++ ) {
				outp.print("("+iurows.get(i)._abilities.get(a)+"yes)");
			}
			outp.println("\t]");
			outp.println("enddd\n");
			outp.println("dd not_row"+iurows.get(i)._row_id+"_ability_relevant");
			outp.println("\t[+	(1.0)	[*	(-1.0)	(row"+iurows.get(i)._row_id+"_ability_relevant)]]");
			outp.println("enddd\n");		
		}		
		
		/*
		// (*) --------------------------- USE ROWS PROBABILITIES  -----------------------------
		outp.println("\n// USER ROWS PROBABILITIES AND PRINT bb_relevant_p");
		outp.print(	"dd bb_relevant_p\n" +
					"[+\n");
		for (int i=0; i<iurows.size(); i++) {
			if (iurows.get(i)._probability != 1) {
				outp.println("[*  (row" + iurows.get(i)._row_id + "_relevant_b)  (" + iurows.get(i)._probability + ")]");
			} else {
				outp.println("(row" + iurows.get(i)._row_id +"_relevant_b)");
			}
		}
		outp.print(	"]\n" +
					"enddd\n");
		*/
		
		/* THIS IS NOT REQUIRED ANY MORE BECAUSE NOW ABILITIES ARE READ FROM THE IU TABLE
		// (*) --------------------------- WHICH ABILITY IS REQUIRED FOR WHICH BEHAVIOUR  -----------------------------
		// cognitive ability relevance: which ability is required for which behaviour
		System.out.println();
		userComment("Specify for which behaviours each cognitive ability is relevant.  Again, these can be read off the IU spreadsheet.\nFor each ability, pick out the rows in the spreadsheet on which it appears, and then look at which 'action' is on the same row:\nthis cognitive ability is relevant to that behaviour.\n Any abilities which serve to push goals only are relevant to all behaviours except nothing and other\n Note: Usually, no abilities will be relevant to behaviours 'nothing' and 'other', so leave these as default (0)");
		outp.println("\n// COGNITIVE ABILITY RELELVANCE FUNCTIONS - WHICH ABILITY IS REQUIRED BY WHICH BEHAVIOUR");
		Variable tmprv;
		vals = new double[behaviourv.vals.size()];
		for (int j=0; j<abilVars.size(); j++) {
		    System.out.println();
		    System.out.println("Specify relevance of ability "+abilVars.elementAt(j).name);
		    tmp = "...'"+abilVars.elementAt(j).name+"' (1=yes, 0=no)?";
		    
		    for (int k=0; k<behaviourv.vals.size(); k++) {
		    	String tmp2 = null;
				if (CCmdLineParams.getInstance().non_interactive() == true) {
					// (*) read from the DB
					String query = "";
					try {
					    System.out.print("DB: \t\tDoes behaviour: '" + behaviourv.vals.elementAt(k) + "' depend on abiliby " + abilVars.elementAt(j).name + " (1=yes, 0=no): ");
						
						Statement st = db_connection.createStatement();
						query = "SELECT * FROM t_abilities4behaviours WHERE abil_name='" + abilVars.elementAt(j).name + "' AND beh_name='" + behaviourv.vals.elementAt(k) + "'";
						ResultSet rs = st.executeQuery(query);
						if (rs.next()) {
							tmp2 = "1";
							System.out.print(" yes ");
							if (rs.next()) {
								System.err.println("DB: integrity error in the t_abilities4behaviours table");
								System.err.println("DB: query '" + query + "' should return exactly one value, make sure that your relational specification of values in this table satisfies this requirement. ");
								System.exit(1);
							}
						} else {
							tmp2 = "0";
							System.out.print(" no ");
						}
						System.out.println(tmp2 + " (read from the DB)");
						rs.close();
						st.close();			
					} catch(SQLException e) {
					    System.out.println("DB: Connection Failed! Check output console");
					    System.out.println("DB: this query failed: '" + query + "'");
					    e.printStackTrace();
					    System.exit(1);
					}					
				} else {
					// (*) read from STDIN
					String q = "\t\tDoes behaviour: '" + behaviourv.vals.elementAt(k) + "' depend on abiliby " + tmp;
					tmp2 = readIn(in, q, "0");
				}
				tmpd = Double.parseDouble(tmp2);
				vals[k] = tmpd;
		    }
		    
		    outp.println("dd "+abilVars.elementAt(j).name+"_relevant");
		    outp.println("[+  (0.0) ");
		    tmprv = new Variable(abilVars.elementAt(j).name);
		    for (int i=0; i<vals.length; i++) {
			if (vals[i]>0) {
			    // this behaviour is relevant
			    outp.println("("+behaviourv.vals.elementAt(i)+"_relevant)");
			    tmprv.addValue(behaviourv.vals.elementAt(i));
			}
		    }
		    relevant.add(tmprv);
		    outp.println("]");
		    outp.println("enddd");
		}
		System.out.println();
		*/
		
		// (*) --------------------------- GAIN/lose ABILITY PROBABILITIES -----------------------------
		outp.println("\n// PROBABILITIES OF GAINING/LOOSING ABILITIES AFTER PROMPTS AND SPONTANOUSLY");
		userComment("gain/loss effects. These give the probability that a person will gain/lose an ability given that they currently have/don't have it. For each ability, there are four constants:\n gain_recall: prob. the ability will become yes if it is no spontaneously (no prompt)\nlose_recall: prob. the ability will become no if it is yes spontaneously (no prompt)\n\n gain_prompt: prob. the ability will become yes if it is no after the prompt for that ability\nlose_prompt: prob. the ability will become no if it is yes after the prompt for that ability\n");
		System.out.println();
		for (int j=0; j<abilVars.size(); j++) {
			double[] tmptable = new double[4];
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				// (*) real all probabilities from the database using one query
				try {
					Statement st = db_connection.createStatement();
					System.out.println("DB: reading probabilities of gaining/loosing ability "+abilVars.elementAt(j).name);
					ResultSet rs = st.executeQuery("SELECT gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob FROM t_abilities WHERE abil_name='"+abilVars.elementAt(j).name+"'");
					if (rs.next()) {
						System.out.println("DB:\tthe following probabilities read from the DB:");
						for( int i = 1; i <= rs.getMetaData().getColumnCount(); i++ ) {
						  System.out.println( "DB:\t\t" + rs.getMetaData().getColumnName(i) + " = " + rs.getString(i) );
						}
						tmptable[0] = Double.parseDouble(rs.getString(1));
						tmptable[1] = Double.parseDouble(rs.getString(2));
						tmptable[2] = Double.parseDouble(rs.getString(3));
						tmptable[3] = Double.parseDouble(rs.getString(4));
					} else {
						System.err.println("DB: no entries for ability "+abilVars.elementAt(j).name+" in the t_abilities table");
						System.exit(1);
					}
					rs.close();
					st.close();			
				} catch(SQLException e) {
				    System.out.println("DB: Connection Failed! Check output console");
				    e.printStackTrace();
				    System.exit(1);
				}				
			}
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				tmpd = tmptable[0];
			} else {			
			    querys = "Probability that client will gain "+abilVars.elementAt(j).name+" spontaneously (gain_recall)? ";
			    tmp = readIn(in,querys,"0.5");
			    tmpd = Double.parseDouble(tmp);
			}
		    
		    outp.println("dd gain_recall_"+abilVars.elementAt(j).name);
		    outp.println("("+tmpd+")");
		    outp.println("enddd");		    

			if (CCmdLineParams.getInstance().non_interactive() == true) {
				tmpd = tmptable[1];
			} else {
			    querys = "Probability that client will lose "+abilVars.elementAt(j).name+" spontaneously (lose_recall)? ";
			    tmp = readIn(in,querys,"0.5");
			    tmpd = Double.parseDouble(tmp);
			}
		    
		    outp.println("dd lose_recall_"+abilVars.elementAt(j).name);
		    outp.println("("+tmpd+")");
		    outp.println("enddd");		    

			if (CCmdLineParams.getInstance().non_interactive() == true) {
				tmpd = tmptable[2];
			} else {
			    querys = "Probability that client will gain "+abilVars.elementAt(j).name+" after a prompt for "+abilVars.elementAt(j).name+"? ";
			    tmp = readIn(in,querys,"0.5");
			    tmpd = Double.parseDouble(tmp);
			}
		    
		    outp.println("dd gain_recall_"+abilVars.elementAt(j).name+"_prompt");
		    outp.println("("+tmpd+")");
		    outp.println("enddd");		    

			if (CCmdLineParams.getInstance().non_interactive() == true) {
				tmpd = tmptable[3];
			} else {
			    querys = "Probability that client will lose "+abilVars.elementAt(j).name+" after a prompt for "+abilVars.elementAt(j).name+"? ";
			    tmp = readIn(in,querys,"0.5");
			    tmpd = Double.parseDouble(tmp);
			}
		    
		    outp.println("dd lose_recall_"+abilVars.elementAt(j).name+"_prompt");
		    outp.println("("+tmpd+")");
		    outp.println("enddd");		    

		}
		System.out.println();
		// print out the negatives of these
		for (int j=0; j<abilVars.size(); j++) {
		    
		    outp.println("dd stay_recall_"+abilVars.elementAt(j).name);
		    outp.println("[+ (1.0)  [* (-1.0) (gain_recall_"+abilVars.elementAt(j).name+")]]");
		    outp.println("enddd");		    

		    outp.println("dd keep_recall_"+abilVars.elementAt(j).name);
		    outp.println("[+ (1.0)  [* (-1.0) (lose_recall_"+abilVars.elementAt(j).name+")]]");
		    outp.println("enddd");		    

		    outp.println("dd stay_recall_"+abilVars.elementAt(j).name+"_prompt");
		    outp.println("[+ (1.0)  [* (-1.0) (gain_recall_"+abilVars.elementAt(j).name+"_prompt)]]");
		    outp.println("enddd");		    

		    outp.println("dd keep_recall_"+abilVars.elementAt(j).name+"_prompt");
		    outp.println("[+ (1.0)  [* (-1.0) (lose_recall_"+abilVars.elementAt(j).name+"_prompt)]]");
		    outp.println("enddd");		    
		}
		
		/*
		// (*) --------------------------- DEPENDENCIES BETWEEN ABILITIES -----------------------------
		outp.println("\n// DEPENDENCIES BETWEEN ABILITIES");
		// prompt effect dependence functions
		userComment("Specify the dependence between the effects of prompts on cognitive deficit functions\nthis shows when the cognitive deficit will change given the appropriate prompt\nrecalling the step has an effect in any situation.\nBasically, we assume that each ability can depend on at least one other ability,\nso giving a prompt for the dependent ability when the parent (depended on) is missing, will have less effect\nAs a rule of thumb, recall step should have no dependencies, recognition will depend on recall, and affordance on recognition");
		System.out.println("List of abilities:");
		for (int j=0; j<abilVars.size(); j++) 
		    System.out.println(abilVars.elementAt(j).name);
		System.out.println();
		int [] effectdep = new int[abilVars.size()];
		for (int j=0; j<abilVars.size(); j++) {
		    tmp= abilVars.elementAt(j).name;
		    outp.println("dd "+tmp+"_effect_dep");

		    // in general we would query over all combinations of ability variables
		    // but this might take a while
		    // vals = enumerateStates(abilVars, "ability", in, System.out);
		    //printtree(abilVars,vals,outp);

		    // instead, each ability can only be 'vetoed' by one other ability
		    // this is saying that the abilities are structured in a tree, and we
		    // are querying the structure of the tree here
		    tmpi=-1;
		    while (tmpi == -1) {
				if (CCmdLineParams.getInstance().non_interactive() == true) {
					// (*) read dependency from the DB
					try {
						System.out.println("DB: reading from the DB what other ability does " + tmp + " depend on");
						Statement st = db_connection.createStatement();
						// firstly check if depends_on is null for this ability
						ResultSet rs = st.executeQuery("SELECT depends_on FROM t_abilities WHERE abil_name='"+tmp+"' AND depends_on IS NULL");
						if (rs.next()) {
							tmp = "none";
							System.out.println("DB:\tability " + abilVars.elementAt(j).name + " depends on ability '" + tmp + "'");
						} else {
							Statement st2 = db_connection.createStatement();
							// if depends_on is not null, then read its value
							ResultSet rs2 = st2.executeQuery("SELECT depends_on FROM t_abilities WHERE abil_name='"+tmp+"'");
							if (rs2.next()) {
								tmp = rs2.getString(1);
								System.out.println("DB:\tability " + abilVars.elementAt(j).name + " depends on ability " + tmp);
								if (rs2.next()) {
									System.err.println("DB: the ability can depend on one other ability at the moment (it was like this in the interactive version as well!)");
									System.exit(1);
								}
							}
							rs2.close();
							st2.close();
						}
						rs.close();
						st.close();			
					} catch(SQLException e) {
					    System.out.println("DB: Connection Failed! Check output console");
					    e.printStackTrace();
					    System.exit(1);
					}					
				} else {
					// (*) read this dependency from STDIN
					querys = "what other ability does "+tmp+" depend on ?";
					tmp = readIn(in,querys,"none");
				}
				
				if (tmp.equalsIgnoreCase("none")) {
				    outp.println("(1.0)");
				    tmpi = 0;
				} else {
				    tmpi = findVar(abilVars,tmp);
				    if (tmpi >= 0) {
					outp.println("("+tmp+"' (yes (1.0))  (no (0.0)))");
					effectdep[j]=tmpi;
				    } else {
					System.out.println("incorrect ability  - try again");
				    }
				}
		    }


		    outp.println("enddd");		  
  
		    tmp= abilVars.elementAt(j).name;
		    outp.println("dd not_"+tmp+"_effect_dep");
		    outp.println("[+	(1.0) [*	(-1.0)	("+tmp+"_effect_dep)]]");
		    outp.println("enddd");
		}
		*/
		
		// (*) --------------------------- INITIAL STATES -----------------------------
		outp.println("\n// INITIAL STATES");
		System.out.println();
		userComment("Specify the initial states for each state variable except  behaviour which doesn't matter");
		for (int j=0; j<taskVars.size(); j++) {
		    System.out.println();
		    System.out.println("Enter initial state for "+taskVars.elementAt(j).name);
		    outp.println("dd "+taskVars.elementAt(j).name+"_initial");
		    outp.print("("+taskVars.elementAt(j).name+" ");
		    for (int i=0; i<taskVars.elementAt(j).vals.size(); i++) {
				if (CCmdLineParams.getInstance().non_interactive() == true) {
					// (*) read from the DB
					try {
						System.out.print("DB: reading initial probability of "+taskVars.elementAt(j).name+"="+taskVars.elementAt(j).vals.elementAt(i)+": ");
						Statement st = db_connection.createStatement();
						ResultSet rs = st.executeQuery("SELECT var_value_initial_prob FROM t_env_variables_values WHERE var_name='"+taskVars.elementAt(j).name+"' AND var_value='"+taskVars.elementAt(j).vals.elementAt(i)+"'");
						if (rs.next()) {
							tmp = rs.getString(1);
							System.out.println(tmp);
						} else {
							System.err.println("DB: data inconsistency while reading initial probability of "+taskVars.elementAt(j).name+"="+taskVars.elementAt(j).vals.elementAt(i));
							System.exit(1);
						}
						rs.close();
						st.close();			
					} catch(SQLException e) {
					    System.out.println("DB: Connection Failed! Check output console");
					    e.printStackTrace();
					    System.exit(1);
					}					
				} else {
					// (*) read from STDIN
					querys = "initial probability of "+taskVars.elementAt(j).name+"="+taskVars.elementAt(j).vals.elementAt(i)+": ";
					tmp = readIn(in,querys,"0.0");
				}
				tmpd = Double.parseDouble(tmp);
				outp.print("("+taskVars.elementAt(j).vals.elementAt(i)+" ("+tmpd+")) ");
		    }
		    outp.println(")");
		    outp.println("enddd");
		}
		if (CCmdLineParams.getInstance().is_call_caregiver_required() == true) {
			outp.print("" +
			"dd caregiver_present_initial\n" +
			"   (0.0)\n" +
			"enddd\n");
		}
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.print("" +
			"dd parent_control_initial\n" +
			"   (parent_control  (yes  (0.9))  (no  (0.9)))\n" +
			"enddd\n");
		}
		// default for behaviour is nothing - doesn't really matter
		outp.println("dd behaviour_initial");
		outp.println("(1.0)");
		outp.println("enddd");

		// (*) --------------------------- INITIAL ABILITIES -----------------------------
		// initial states for abilities - just query one value
		outp.println("\n// INITIAL ABILITIES");
		for (int i=0; i<abilVars.size(); i++) {
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				// (*) read from the DB
				try {
					System.out.print("DB: reading initial probability of "+abilVars.elementAt(i).name+"=yes: ");
					Statement st = db_connection.createStatement();
					ResultSet rs = st.executeQuery("SELECT abil_initial_prob FROM t_abilities WHERE abil_name='"+abilVars.elementAt(i).name+"'");
					if (rs.next()) {
						tmp = rs.getString(1);
						System.out.println(tmp);
					} else {
						System.err.println("DB: data inconsistency while reading initial probability of "+abilVars.elementAt(i)+"=yes");
						System.exit(1);
					}
					rs.close();
					st.close();			
				} catch(SQLException e) {
				    System.out.println("DB: Connection Failed! Check output console");
				    e.printStackTrace();
				    System.exit(1);
				}					
			} else {
				// (*) read from the STDIN
				tmp = readIn(in,"Enter initial state for "+abilVars.elementAt(i).name+"=yes: ","0.95");
			}
		    tmpd = Double.parseDouble(tmp);
		    outp.println("dd "+abilVars.elementAt(i).name+"_initial");
		    outp.println("("+abilVars.elementAt(i).name+"  (yes  ("+tmpd+"))  (no  ("+(1-tmpd)+")))");
		    outp.println("enddd");
		}
		
		// (*) --------------------------- ACTION COSTS (PROMPTING) -----------------------------
		outp.println("\n// ACTION COSTS (PROMPTING)");
		userComment("Specify costs for each action.  Rule of thumb: Recallstep is 7.0, other recalls and recognition is 1.0, and affordances are 0.5.\nThis means affordance prompts will come before recognition (because they are cheaper), and those before recall.\nTo have this the other way around ,reverse these numbers ( so recallstep 0.5, recognition 1.0 and affordances 7.0).");
		System.out.println();
		if (CCmdLineParams.getInstance().is_call_caregiver_required() == true) {
			outp.println("dd call_caregiver_cost	(1.0)	enddd");
		}
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.println("dd donothing_cost  (parent_control	(yes	(0.5))  (no  (0.0)))   enddd");
		} else{
			outp.println("dd donothing_cost  (0.0)   enddd");
		}
		for (int i=0; i<abilVars.size(); i++) {
			if (CCmdLineParams.getInstance().non_interactive() == true) {
				// (*) read from the DB
				try {
					System.out.print("DB: reading the cost of prompting for "+abilVars.elementAt(i).name+": ");
					Statement st = db_connection.createStatement();
					ResultSet rs = st.executeQuery("SELECT abil_prompt_cost FROM t_abilities WHERE abil_name='"+abilVars.elementAt(i).name+"'");
					if (rs.next()) {
						tmp = rs.getString(1);
						System.out.println(tmp);
					} else {
						System.err.println("DB: data inconsistency while reading initial probability of "+abilVars.elementAt(i)+"=yes");
						System.exit(1);
					}
					rs.close();
					st.close();			
				} catch(SQLException e) {
				    System.out.println("DB: Connection Failed! Check output console");
				    e.printStackTrace();
				    System.exit(1);
				}					
			} else {
				// (*) read from STDIN
			    tmp = readIn(in,"enter cost for action prompt_"+abilVars.elementAt(i).name+": ");
			}
		    tmpd = Double.parseDouble(tmp);
		    outp.println("dd prompt_"+abilVars.elementAt(i).name+"_cost");
		    outp.println("("+tmpd+")");
		    outp.println("enddd");
		}

		// (*) --------------------------- REWARDS FOR JOINT STATES -----------------------------
		System.out.println();
		userComment("The reward function. Give 15.0 for the goal state, 0.0 for the initial state, and values in between for each other state. Try to keep all values other than the goal state below 7.0");
		vals = enumerateStates_db_rewards(db_connection, taskVars, "state reward", in, System.out, "0");
		outp.println("\n// REWARD");
		outp.println("reward ");
		printtree(taskVars,vals,outp);
		outp.println();
		outp.println("discount	0.95");


		// NEW STYLE - not necessary
		// cognitive deficit function structuresenumerateStates_db(
		/*
		for (int i=0; i<abilVars.size(); i++) {
		    outp.println("dd "+abilVars.elementAt(i).name+"_cdf_effect");
		    outp.println("[*  ("+abilVars.elementAt(i).name+"_relevant)");
		    outp.println("(behaviour'");
		    for (int k=0; k<behaviourv.vals.size(); k++) {
			outp.println("("+behaviourv.vals.elementAt(k)+"   ("+abilVars.elementAt(i).name+"_"+behaviourv.vals.elementAt(k)+"_effect))");
		    }
		    outp.println(")]");
		    outp.println("enddd");
		}

		for (int j=0; j<abilVars.size(); j++) {
		    outp.println("dd "+abilVars.elementAt(j).name+"_relax_final");
		    outp.println("(0.01)");
		    outp.println("enddd");		    
		}
		
		
		for (int j=0; j<abilVars.size(); j++) {
		    tmp = abilVars.elementAt(j).name;
		    outp.println("dd initial_"+tmp);
		    outp.println("("+tmp+"'   ");
		    outp.println("(yes     ("+tmp+"_relax_final))");
		    outp.println("(no     [+  (1.0)  [*   (-1.0)   ("+tmp+"_relax_final)]]))");
		    outp.println("enddd");		    		    
		}
		
		for (int j=0; j<abilVars.size(); j++) {
		    tmp = abilVars.elementAt(j).name;
		    outp.println("dd "+tmp+"_relax");
		    outp.println("[+	[*     [+     (1.0)   [* (-1.0)     ("+tmp+"_relaxation_rate)]]  (SAME"+tmp+")]");
		    outp.println("[*   ("+tmp+"_relaxation_rate)   (initial_"+tmp+")]]");
		    outp.println("enddd");		    		    
		}		

		*/
		
		//initial state
		outp.println("\n// INITIAL STATE");
		outp.println("init  [*");
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.println("(parent_control_initial)");
		}
		for (int i=0; i<stateVars.size(); i++) {
		    outp.println("("+stateVars.elementAt(i).name+"_initial)");
		}
		outp.println("]");
		
		outp.println("\n// ADJUNCT GOAL");
		userComment("Print the adjunct goaldd. The goal state is the state which has the highest reward in the database.");
		// (*) 1.0 for goal states and '0.0' for non-goal states
		strvals = enumerateStates_is_goal(db_connection, taskVars, "1.0", "0.0");
		StringWriter swtmp = new StringWriter();
        printtree(taskVars,strvals,swtmp);
        outp.println("adjunct goaldd");
        outp.println(swtmp.toString());
		
		// (*) --------------------------- SPECIFY JOINT STATES FOR WHICH BAHAVIOURS ARE NOT POSSIBLE -----------------------------
		outp.println("\n// JOINT STATES WHERE BEHAVIOURS ARE IMPOSSIBLE");
		userComment("Specify in which states each behaviour is possible. The defaults are that all behaviours are possible in all states,\nbut this is not always true - for example, it is impossible to open the fridge if it is open.");
		outp.println("// 1 means that behaviour is not possible in a given state and 0 means that behaviour is possible in a given state");
		outp.println("dd impossibleBeh");
		//outp.println("(0.0)");
		outp.println("(behaviour'  ");
		for (int k=0; k<behaviourv.vals.size(); k++) {
		    outp.println("("+behaviourv.vals.elementAt(k)+"    ");
		    if (behaviourv.vals.elementAt(k).equalsIgnoreCase("nothing") || 
			behaviourv.vals.elementAt(k).equalsIgnoreCase("other")) {
			outp.println("(0.0)");
		    } else {
			System.out.println();
			System.out.println("Specify impossibile states for behaviour "+behaviourv.vals.elementAt(k));
			// When reading from the t_when_behaviour_is_impossible the last parameter should be "0" becasue states in the table are not possible.
			// and 0 means that the behaviour is not possible in this state
			vals = enumerateStates_db_impossible4behaviour(db_connection, "t_when_is_behaviour_impossible", taskVars, "behaviour="+behaviourv.vals.elementAt(k)+" impossible? (yes=1 [yes means beh is impossible in this state], no=0[no means beh is possible in this state])", behaviourv.vals.elementAt(k), in, System.out,"0");
			printtree(taskVars,vals,outp);
		    }
		    outp.println(")");
		}
		outp.println(")");
		outp.println("enddd");		    		    

		outp.println("\n// ADDITIONAL RELATIONSHIPS FOR BEHAVIOURS");

		// modifying the 0.01 here makes a significant difference
		// if smaller, then the dynamics are much more precise (its much less likely that the user does something random)
		// if larger, then there is a larger probability that the user acts randomly - 
		outp.println("dd randomBehConst   (0.01)  enddd");

		
		outp.println("dd genericBehDyn");
		outp.println("\t[+   ");
        for (int i=0; i<behaviourv.vals.size(); i++) {
			if (behaviourv.vals.elementAt(i).equalsIgnoreCase("other")) {
				continue;
			}
			if (behaviourv.vals.elementAt(i).equalsIgnoreCase("nothing")) {
				continue;
			}
			if ( !allIUBehaviurs.contains(behaviourv.vals.elementAt(i))) {
				outp.println("\t\t[*   (behaviour" + behaviourv.vals.elementAt(i) + ")    (" + behaviourv.vals.elementAt(i) + "_can_happen)]");
			}
        }
		outp.println("\t\t[*   (behaviournothing)    (nothing_relevant)]");
		outp.println("\t\t[*   (behaviourother)    (other_relevant)]");
		outp.println("\t\t[*   (randomBehConst) [+  (1.0)  [*   (-1.0)  (impossibleBeh)]]]");
		outp.println("\t\t(SAMEbehaviour)\n\t]");
		outp.println("enddd");

		outp.println("dd notgenericBehDyn   [+	(1.0)	[*	(-1.0)	(genericBehDyn)]]  enddd");
		
		/* REMOVED WHEN PROBABILITIES FROM THE IU TABLE WERE ADDED
		for (int k=0; k<behaviourv.vals.size(); k++) {
		    outp.println("dd genericBehDyn"+behaviourv.vals.elementAt(k));
		    outp.println("[*   (behaviour"+behaviourv.vals.elementAt(k)+")  ("+behaviourv.vals.elementAt(k)+"_relevant)]");
		    outp.println("enddd");		    		    
		}
		*/

		/*
		outp.println("dd none_relevant");
		outp.println("[*");
		for (int k=0; k<iurows.size(); k++) {
		    outp.println("(not_row"+iurows.get(k)._row_id+"_relevant_b)");
		}
		outp.println("]");
		outp.println("enddd");		    		    

		outp.println("dd beh_none_relevant");
		outp.println("[* (behaviournothing)	(none_relevant)]");
		outp.println("enddd");		    		    
		*/

		if (CCmdLineParams.getInstance().is_call_caregiver_required() == true) {
			outp.println("dd fullBehDynClient");
		} else {
			outp.println("dd fullBehDyn");
		}
		outp.println("[+\n\t(genericBehDyn)");
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.println("\t(parent_control'\n\t\t(yes");
		}
		outp.println("\t\t\t[+");
		for (int k=0; k<iurows.size(); k++) {
			outp.print("\t\t\t\t[*\t");
			outp.print("(row"+iurows.get(k)._row_id+"_ability_relevant)\t");
			if ( iurows.get(k)._probability != 1 ) {
				outp.println("\t[*  (row" + iurows.get(k)._row_id + "_relevant_b)  (" + iurows.get(k)._probability + ")\t]\t]");
			} else {
				outp.println("\t(row" + iurows.get(k)._row_id +"_relevant_b)\t]");
			}
			outp.print("\t\t\t\t[*\t");
			outp.print("(not_row"+iurows.get(k)._row_id+"_ability_relevant)\t");
		    outp.println("\t(row" + iurows.get(k)._row_id + "_relevant)\t(behaviournothing)\t]");
		}
		outp.print("\t\t\t\t[*");
		for (int k=0; k<iurows.size(); k++) {
			outp.print("\t(not_row"+iurows.get(k)._row_id+"_relevant)");
		}
		outp.println("\t(behaviournothing)]");		
		
		// now, ability i depends on ability effectdep[i]  (effectdep[i] is i's parent in the ability tree)
		// printBehDyn(outp,relevant,behaviourv);
		outp.print("\t\t\t]\n");
		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.println(")\n\t\t(no	(behaviournothing)))");
		}
		outp.println("]\nenddd");

		if (CCmdLineParams.getInstance().is_call_caregiver_required() == true) {
			outp.println("dd fullBehDynCG");
			outp.println("[+");
			for (int k=0; k<iurows.size(); k++) {
				outp.println("	(row"+iurows.get(k)._row_id+"_relevant_b)");
			}
			outp.println("	[*");
			for (int k=0; k<iurows.size(); k++) {
				outp.println("		(not_row"+iurows.get(k)._row_id+"_relevant)");
			}
			outp.println("		(behaviournothing)");
			outp.println("	]");
			outp.println("]");
			outp.println("enddd");
			
			outp.println("");
			
			outp.println("dd fullBehDyn");
			outp.println("	(caregiver_present'	(yes	(fullBehDynCG))	(no	(fullBehDynClient)))");
			outp.println("enddd");
			
			outp.println("");
			
			outp.println("dd caregiver_stay_constant");
			outp.println("	(0.2)");
			outp.println("enddd");
			
			outp.println("// if caregiver is not called");
			outp.println("dd caregiver_present_dynamics");
			outp.println("	(caregiver_present	(yes	(caregiver_present'	(yes	(caregiver_stay_constant))	(no	[~ (caregiver_stay_constant)])))");
			outp.println("		(no	(caregiver_presentno)))");
			outp.println("enddd");
		}
		
		/*
		for (int i=0; i<abilVars.size(); i++) {
		    tmp = abilVars.elementAt(i).name;
		    outp.println("dd "+tmp+"_prompt_dynamics");
		    outp.println("("+tmp+"(yes  ("+tmp+"yes))");
		    outp.println("(no  ("+tmp+"'  (no  [+  (1.0) [*  (-1.0) (prompt_"+tmp+"_increase)]])");
		    outp.println("(yes  (prompt_"+tmp+"_increase)))))");
		    outp.println("enddd");
		    
		    outp.println("dd "+tmp+"_dynamics");
		    outp.println("("+tmp+"(yes  ("+tmp+"yes))");
		    outp.println("(no  ("+tmp+"'  (no  [+  (1.0) [*  (-1.0) ("+tmp+"_increase)]])");
		    outp.println("(yes  ("+tmp+"_increase)))))");
		    outp.println("enddd");
		    
		    outp.println("dd "+tmp+"_prompt_dynamics_eff");
		    outp.println("[+ [* ("+tmp+"_prompt_dynamics) ("+tmp+"_effect_dep)]");
		    outp.println("[+ [* (not_"+tmp+"_effect_dep) (SAME"+tmp+")]]]");
		    outp.println("enddd");
		    
		    outp.println("dd "+tmp+"_dynamics_eff");
		    outp.println("[+ [* ("+tmp+"_dynamics) ("+tmp+"_effect_dep)]");
		    outp.println("[+ [* (not_"+tmp+"_effect_dep) ("+tmp+"_relax)]]]");
		    outp.println("enddd");
		}
		*/
		
		outp.println("\n// NEW STYLE FOR ABILITY DYNAMICS");
		// NEW STYLE for ability dynamics
		for (int i=0; i<abilVars.size(); i++) {
		    boolean no_dependance = true;
		    String parent_abil = "";
			try {
				System.out.println("DB: checking if ability "+abilVars.elementAt(i).name+" depends on other abilities (well now, it can depend on max one ability)");
				Statement st = db_connection.createStatement();
				ResultSet rs = st.executeQuery("SELECT parent_abil_name FROM t_abilities WHERE abil_name='"+abilVars.elementAt(i).name+"' ORDER BY 1 ASC");
				if (rs.next()) {
					parent_abil = rs.getString(1);
					if ( parent_abil != null) {
						System.out.println("DB: OK fine, now we know that "+abilVars.elementAt(i).name+" depends on "+parent_abil);
						no_dependance = false;
					}
				}
				rs.close();
				st.close();			
			} catch(SQLException e) {
			    System.out.println("DB: it is not so bad if we are here, this may mean that parent_abil_name is not specified in table t_abilities in the current database.");
			    System.out.println("DB: we continue from here and the program is not terminated.");
			    e.printStackTrace();
			}
		    tmp = abilVars.elementAt(i).name;
			if ( no_dependance == true ) {
			    outp.println("dd "+tmp+"_dynamics");
			    outp.println("("+tmp+"(yes  ("+tmp+"'  (yes   (keep_recall_"+tmp+"))  (no   (lose_recall_"+tmp+"))))");
			    outp.println("        (no   ("+tmp+"'  (yes   (gain_recall_"+tmp+"))  (no   (stay_recall_"+tmp+")))))");
			    outp.println("enddd");
	
			    outp.println("dd "+tmp+"_prompt_dynamics");
			    outp.println("("+tmp+"(yes  ("+tmp+"'  (yes   (keep_recall_"+tmp+"_prompt))  (no   (lose_recall_"+tmp+"_prompt))))");
			    outp.println("        (no   ("+tmp+"'  (yes   (gain_recall_"+tmp+"_prompt))  (no   (stay_recall_"+tmp+"_prompt)))))");
			    outp.println("enddd");
		    } else {
			    outp.println("dd "+tmp+"_dynamics");
			    outp.println("("+parent_abil+" (yes ("+tmp+"(yes  ("+tmp+"'  (yes   (keep_recall_"+tmp+"))  (no   (lose_recall_"+tmp+"))))");
			    outp.println("                              (no   ("+tmp+"'  (yes   (gain_recall_"+tmp+"))  (no   (stay_recall_"+tmp+"))))))");
			    outp.println("                 (no ("+tmp+"' (yes  (0.0))  (no (1.0)) ) ) )");
			    outp.println("enddd");
	
			    outp.println("dd "+tmp+"_prompt_dynamics");
			    outp.println("("+parent_abil+" (yes ("+tmp+"(yes  ("+tmp+"'  (yes   (keep_recall_"+tmp+"_prompt))  (no   (lose_recall_"+tmp+"_prompt))))");
			    outp.println("                              (no   ("+tmp+"'  (yes   (gain_recall_"+tmp+"_prompt))  (no   (stay_recall_"+tmp+"_prompt))))))");
			    outp.println("                 (no ("+tmp+"' (yes  (0.0))  (no (1.0)) ) ) )");
			    outp.println("enddd");
		    }
		}

		if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
			outp.println("\n// PARENT CONTROL DYNAMICS");
			userComment("Print the parent control dynamics dd. The goal state is used - the state which has the highest reward in the database.");
			outp.print("" +
			"dd parent_control_prob\n" +
			"	(parent_control'	(yes	(0.95))	(no	(0.05)))\n" +
			"enddd\n" +
			"dd parent_control_dyn_sub\n" +
			"	(parent_control'	(no	(0.95))	(yes	(0.05)))\n" +
			"enddd\n" +
			"dd parent_control_goal\n");
			//	(planstep	(g1	(parent_control_prob))
			//	(a1	(parent_control_prob))
			//	(b1	(parent_control_prob))
			//	(e1	(0.5)))
			// (*) 0.5 for goal states and 'parent_control_prob' for non-goal states
			strvals = enumerateStates_is_goal(db_connection, taskVars, "0.5", "parent_control_prob");
			swtmp = new StringWriter();
		    printtree(taskVars,strvals,swtmp);
		    outp.print(swtmp.toString());
			outp.print("\n" +
			"enddd\n" +
			"dd parent_control_dynamics\n" +
			"(parent_control	(no	(parent_control_dyn_sub))\n" +
			"		(yes	(parent_control_goal)))\n" +
			"enddd\n");
		}
		
		// action definitions
		outp.println("\n// ACTION DEFINITIONS\n");
		for (int i=-2; i<abilVars.size(); i++) {
			if (i==-2) {
				tmp = "call_caregiver";
				if (CCmdLineParams.getInstance().is_call_caregiver_required() == false) {
					continue;
				}
			} else if (i==-1) { 
				tmp = "donothing";
			} else {
		    	tmp = "prompt_"+abilVars.elementAt(i).name;
			}
		    outp.println("action "+tmp);
		    for (int j=0; j<taskVars.size(); j++) 
			outp.println("\t"+taskVars.elementAt(j).name+"    ("+taskVars.elementAt(j).name+"_dynamics)");
		    outp.println("\tbehaviour	(fullBehDyn)");
		    for (int j=0; j<abilVars.size(); j++) {
			outp.print("\t"+abilVars.elementAt(j).name+"    ("+abilVars.elementAt(j).name);
			if (i==j) 
			    outp.print("_prompt");
			//outp.println("_dynamics_eff)");
			//NEW STYLE
			outp.println("_dynamics)");
		    }
			if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
		    	outp.println("\tparent_control	(parent_control_dynamics)");
			}
			if (CCmdLineParams.getInstance().is_call_caregiver_required() == true) {
				if ( tmp.compareTo("call_caregiver") == 0)
					outp.println("\tcaregiver_present	(caregiver_presentyes)");
				else
					outp.println("\tcaregiver_present	(caregiver_present_dynamics)");
			}
			outp.println("\tobserve");
		    for (int j=0; j<obsVars.size(); j++) 
		    	outp.println("\t\t"+obsVars.elementAt(j).name+"    ("+obsVars.elementAt(j).name+"OF)");
			if (CCmdLineParams.getInstance().is_hicontrol_required() == true) {
		    	outp.println("\t\tcontrol_obs (control_obsOF)");
			}
		    outp.println("\tendobserve");

		    outp.println("\tcost  ("+tmp+"_cost)");
		    outp.println("endaction\n");
		}

		outp.println("\n// TOLERANCE");
		outp.println("tolerance 0.001\n");
		outp.println("\n");
		outp.println("\n");
		outp.println("\n");
		outp.flush();
		outp.close();

		userComment("You are done!  Now, rename your file '" + CCmdLineParams.getInstance().out_file_name() + "' to something else, and have a great day!");

		System.exit(0);

	    }  catch (IOException e) {
	    }
		    
	} catch (java.io.FileNotFoundException e) {
	}

	if (db_connection != null) {
		try {		
			db_connection.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
   }
    
    private static String readPassAsString(String filePath) {
        byte[] buffer = new byte[(int) new File(filePath).length()];
        try {
        FileInputStream f = new FileInputStream(filePath);
        f.read(buffer);
        } catch(IOException e) {
        	System.err.println("Error while reading: " + filePath);
        	e.printStackTrace();
        	System.exit(1);
        }
        String str = new String(buffer);
        str = str.trim();
        return str;
    }
    
    private static void logquery(String query) {
    	try{
    		FileWriter fstream = new FileWriter("/tmp/.web_snap_last_query.log");
    		BufferedWriter out = new BufferedWriter(fstream);
    		out.write(query+"\n");
    		out.close();
 	    }catch (Exception e){
 	    	System.out.println("Error: " + e.getMessage());
 	    }    	
    }
}
