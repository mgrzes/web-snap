<?php
	// Author: Marek Grzes, University of Waterloo, 2010.
?>

<a href="snap-db-select.php">Main</a>
<?php
include_once("model/dbparams.php");
include_once 'model/common.php';
$alltables = get_all_tables();

for ( $i = 0; $i < count($alltables); $i++ ) {
	echo '&nbsp;<a href="edit-table.php?tablename='.$alltables[$i].'">'.$alltables[$i].'</a>'."\n";	
}
?>
<!-- <a href="edit-table.php?tablename=t_env_variables_values">Environment Variables</a>, <a href="edit-table.php?tablename=t_abilities">Abilities</a>,
<a href="edit-table.php?tablename=t_behaviours">Behaviours</a>, <a href="edit-table.php?tablename=t_observations_values">Observations</a> -->
&nbsp;<a href="iu-table.php">IU Table</a>&nbsp;&nbsp;<a href="snap-generator.php">SNAP generator</a>&nbsp;&nbsp;<a href="beh_dynamics.php">Behaviour Dynamics</a>

<br>
<hr>
<br>
