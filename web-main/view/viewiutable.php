<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once('model/common.php');
include_once('viewheader.php');
include_once 'viewtopmenu.php';

if ( count(IUTableModel::$_front_message) > 0 ) {
	echo '<p>';
	for ( $i = 0; $i < count(IUTableModel::$_front_message); $i++ ) {
		echo IUTableModel::$_front_message[$i]."<br>\n";
	}
	echo '</p>';
}

?>

<h3>The IU table</h3>
<p>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<table border="1">
	<tr><th></th><th>IU</th><th>Goals</th><th>Task States</th><th>Abilities</th><th>Behaviours</th><th>Probability</th><th>Status</th></tr>
	<?php
		for ($i=0; $i<$model->getnumrows(); $i++ ) {
			echo '<tr><td><input type="radio" name="row2edit" value="' . $model->getrow($i)->_row_id . '"></td>';
			if ( $model->getrow($i)->_row_type == 0 ) {
				// (*) print separator row only
				echo '<td>'.$model->getrow($i)->_row_id.'</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td><td>---</td></tr>'."\n";
			} else {
				if ($model->_edit_row_id == $model->getrow($i)->_row_id) {
					// (*) display this row in the edit mode
					echo '<input type="hidden" name="row_being_edited" value="'.$model->getrow($i)->_row_id.'">'."\n";
					// (1) row id
					echo '<td style="background-color: #BBCCBB;">'.$model->getrow($i)->_row_id.'</td>';
					// (2) row goals
					echo '<td style="background-color: #BBCCBB;">';
					// ---------------
					echo '<table border="1">'."\n".'<tr><th></th><th>Variable</th><th>Value</th></tr>'."\n";
					// iterate over var_name-var_value pairs
					for ( $j = 0; $j < count($model->getrow($i)->_goals); $j++) {
						// process each var-name-var_value pair
						echo '<tr>'."\n";
						foreach ($model->getrow($i)->_goals[$j] as $key=>$value) {
							$id=$key."=".$value;
							echo '<td><input type="radio" name="goalsvarvalues" value="' .$id. '"></td>';
							echo '<td>'.$key.'</td><td>'.$value.'</td>';							
						}
						echo '</tr>'."\n";
					}
					if ( count($model->getrow($i)->_goals) > 0 ) {
						// print edit buttons only when there is something to edit
						echo '<tr><td colspan=3><input type="submit" name="delete_goal" value="Delete selected goal" onClick="confirmDelete()"></td></tr>';
					}
					echo '<tr><td>&nbsp;&nbsp;&nbsp;</td>';
					echo '<td>';
						// load_column_to_checkbox("var_name", "t_env_variables_values", "goal_var_name", array(), "", false);
						$where_condition = array();
						if ( isset($_POST['goal_var_value']) && $_POST['goal_var_value'] != "" ) {
							array_push($where_condition, "var_value='".$_POST['goal_var_value']."'" );
						}
						load_column_to_checkbox("var_name", "t_env_variables_values", "goal_var_name", $where_condition, $_POST['goal_var_name'], true);
					echo '</td>';
					echo '<td>';
						// load_column_to_checkbox("var_value", "t_env_variables_values", "goal_var_value", array(), "", false);
						$where_condition = array();
						if ( isset($_POST['goal_var_name']) && $_POST['goal_var_name'] != "" ) {
							array_push($where_condition, "var_name='".$_POST['goal_var_name']."'" );
						}
						load_column_to_checkbox("var_value", "t_env_variables_values", "goal_var_value", $where_condition, $_POST['goal_var_value'], true);
					echo '</td>';
					echo '</tr>';
					echo '<tr><td colspan=3><input type="submit" name="add_goal" value="Add new goal"></td></tr>';	
					echo '</table>'."\n";
					// ----------------
					echo '</td style="background-color: #BBCCBB;">';
					// (3) row task states
					echo '<td style="background-color: #BBCCBB;">';
					// ---------------
					echo '<table border="1">'."\n".'<tr><th></th><th>Variable</th><th>Value</th></tr>'."\n";
					// iterate over var_name-var_value pairs
					foreach ($model->getrow($i)->_task_states as $key=>$value) {
						// process each var-name-var_value pair
						echo '<tr>'."\n";
						$id=$key."=".$value;
						echo '<td><input type="radio" name="statesvarvalues" value="' .$id. '"></td>';
						echo '<td>'.$key.'</td><td>'.$value.'</td>';							
						echo '</tr>'."\n";
					}
					if ( count($model->getrow($i)->_task_states) > 0 ) {
						// print edit buttons only when there is something to edit
						echo '<tr><td colspan=3><input type="submit" name="delete_state" value="Delete selected state" onClick="confirmDelete()"></td></tr>';
					}
					echo '<tr><td>&nbsp;&nbsp;&nbsp;</td>';
					echo '<td>';
						// load_column_to_checkbox("var_name", "t_env_variables_values", "state_var_name", array(), "", false);
						$where_condition = array();
						if ( isset($_POST['state_var_value']) && $_POST['state_var_value'] != "" ) {
							array_push($where_condition, "var_value='".$_POST['state_var_value']."'" );
						}
						load_column_to_checkbox("var_name", "t_env_variables_values", "state_var_name", $where_condition, $_POST['state_var_name'], true);
					echo '</td>';
					echo '<td>';
						// load_column_to_checkbox("var_value", "t_env_variables_values",  "state_var_value", array(), "", false);
						$where_condition = array();
						if ( isset($_POST['state_var_name']) && $_POST['state_var_name'] != "" ) {
							array_push($where_condition, "var_name='".$_POST['state_var_name']."'" );
						}
						load_column_to_checkbox("var_value", "t_env_variables_values", "state_var_value", $where_condition, $_POST['state_var_value'], true);
					echo '</td>';
					echo '</tr>';
					echo '<tr><td colspan=3><input type="submit" name="add_state" value="Add new state"></td></tr>';	
					echo '</table>'."\n";
					// ----------------
					echo '</td>';					
					// (4) row abilities
					echo '<td style="background-color: #BBCCBB;">';
					// ---------------
					echo '<table border="1">'."\n".'<tr><th></th><th>Ability</th></tr>'."\n";
					// iterate over abilities
					for ( $j = 0; $j < count($model->getrow($i)->_abilities); $j++) {
						echo '<tr>'."\n";
						echo '<td><input type="radio" name="ability" value="'.$model->getrow($i)->_abilities[$j].'"></td>';
						echo '<td>'.$model->getrow($i)->_abilities[$j].'</td>';
						echo '</tr>'."\n";
					}
					if ( count($model->getrow($i)->_abilities) > 0 ) {
						// print edit buttons only when there is something to edit
						echo '<tr><td colspan=2><input type="submit" name="delete_ability" value="Delete selected ability" onClick="confirmDelete()"></td></tr>';
					}
					echo '<tr><td>&nbsp;&nbsp;&nbsp;</td>';
					echo '<td>';
					load_column_to_checkbox("abil_name", "t_abilities", "abil_name", array(), "", false);
					echo '</td>';
					echo '</tr>';
					echo '<tr><td colspan=2><input type="submit" name="add_ability" value="Add new ability"></td></tr>';	
					echo '</table>'."\n";
					// ----------------
					echo '</td>';
					// (5) row behaviours
					echo '<td style="background-color: #BBCCBB;">';
					// ---------------
					echo '<table border="1">'."\n".'<tr><th></th><th>Behaviour</th></tr>'."\n";
					// iterate over abilities
					for ( $j = 0; $j < count($model->getrow($i)->_behaviours); $j++) {
						echo '<tr>'."\n";
						echo '<td><input type="radio" name="behaviour" value="'.$model->getrow($i)->_behaviours[$j].'"></td>';
						echo '<td>'.$model->getrow($i)->_behaviours[$j].'</td>';
						echo '</tr>'."\n";
					}
					if ( count($model->getrow($i)->_behaviours) > 0 ) {
						// print edit buttons only when there is something to edit
						echo '<tr><td colspan=2><input type="submit" name="delete_behaviour" value="Delete selected behaviour" onClick="confirmDelete()"></td></tr>';
					}
					echo '<tr><td>&nbsp;&nbsp;&nbsp;</td>';
					echo '<td>';
					load_column_to_checkbox("beh_name", "t_behaviours", "beh_name", array(), "", false);
					echo '</td>';
					echo '</tr>';
					echo '<tr><td colspan=2><input type="submit" name="add_behaviour" value="Add new behaviour"></td></tr>';	
					echo '</table>'."\n";
					// ----------------
					echo '</td>';
					// (6) row probability
					echo '<td style="background-color: #BBCCBB;">';
					// ---------------
					echo '<input size="6" type="text" name="row_probability" value="'.$model->getrow($i)->_row_probability.'"><br>';
					echo 'You can type a new value of the probability and hit the button below to save the new value.<br>';
					echo '<input type="submit" name="update_row_probability" value="Save Probability">';
					// ----------------
					echo '</td>';
					// (7) row status
					echo '<td style="background-color: #BBCCBB;">';
					// ---------------
					if ( $model->getrow($i)->_row_confirmed == true ) {
						echo 'confirmed';
					} else {
						echo '<font color="red">This row has not been confirmed. Please, check the value of its probability, check the check box below and hit the confirm button.</font><br>';
						echo 'Probability verified: <input type="checkbox" name="row_confirm_checked" value="checked"><br>';
						echo '<input type="submit" name="confirm_row" value="Confirm">';
					}
					// ----------------
					echo '</td>';
				} else {
					// (*) display a data row
					// (1) row id
					echo '<td><a href="iu-table.php?editrow='.$model->getrow($i)->_row_id.'">'.$model->getrow($i)->_row_id.'</a></td>';
					// (2) row goals
					echo '<td>';
					$tmpk=0;
					// iterate over var_name-var_value pairs
					for ( $j = 0; $j < count($model->getrow($i)->_goals); $j++) {
						// process each var-name-var_value pair
						foreach ($model->getrow($i)->_goals[$j] as $key=>$value) {
							if ($tmpk > 0) {
								echo ', ';
							}
							echo $key."=".$value;
							$tmpk += 1;
						}
					}
					if (count($model->getrow($i)->_goals) == 0) {
						echo '(not specified)';
					}
					echo '</td>';
					// (3) row task states
					echo '<td>';
					$tmpk=0;
					// process each var-name-var_value pair
					foreach ($model->getrow($i)->_task_states as $key=>$value) {
						if ($tmpk > 0) {
							echo ', ';
						}
						echo $key."=".$value;
						$tmpk += 1;
					}
					if (count($model->getrow($i)->_task_states) == 0) {
						echo '(not specified)';
					}
					echo '</td>';
					// (4) row abilities
					echo '<td>';
					$tmpk=0;
					// iterate over var_name-var_value pairs
					for ( $j = 0; $j < count($model->getrow($i)->_abilities); $j++) {
						if ($tmpk > 0) {
							echo ', ';
						}
						echo $model->getrow($i)->_abilities[$j];
						$tmpk += 1;
					}
					if (count($model->getrow($i)->_abilities) == 0) {
						echo '(not specified)';
					}
					echo '</td>';
					// (5) row behaviours
					echo '<td>';
					$tmpk=0;
					// iterate over var_name-var_value pairs
					for ( $j = 0; $j < count($model->getrow($i)->_behaviours); $j++) {
						if ($tmpk > 0) {
							echo ', ';
						}
						echo $model->getrow($i)->_behaviours[$j];
						$tmpk += 1;
					}
					if (count($model->getrow($i)->_behaviours) == 0) {
						echo '(not specified)';
					}
					echo '</td>';
					// (6) row probability
					echo '<td>'.$model->getrow($i)->_row_probability.'</td>';
					// (7) row status
					if ( $model->getrow($i)->_row_confirmed == true ) {
						echo '<td>confirmed</td>';
					} else {
						echo '<td><font color="red">not confirmed</font></td>';
					}
					echo '</tr>'."\n";
				}
			}
		}
	?>
</table>
<br>
<input type="submit" name="add_new_separation_row" value="Add new separtion row">
<input type="submit" name="add_new_data_row" value="Add new data row">
<input type="submit" name="edit_selected_row" value="Edit selected row">
<input type="submit" name="delete_selected_row" value="Delete selected row" onClick="return confirmDelete()">
<input type="submit" name="up_selected_row" value="Move selected row up">
<input type="submit" name="down_selected_row" value="Move selected row down">
<input type="submit" name="check_state_subsumption" value="Check Subsumption of States">
<input type="submit" name="explode_states" value="Check Disjunctive Behaviours">
<input type="submit" name="reset_probs" value="Reset Probabilities" onClick="return confirmQuestion('Are you sure you want to continue? This will reset all probabilities to uniform values.')">
</form>
</p>

<?php
	if (IUTableModel::$_last_message != "") {
		echo '<p>Last database operation: <font color="red">'.IUTableModel::$_last_message.'</font></p>'."\n";
	}

include 'viewfooter.php';
?>
