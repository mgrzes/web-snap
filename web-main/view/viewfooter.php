<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<br>
<hr>
<p>
<?php global $dbname; echo "You are currently working with the snap instance: "; if ($dbname=="") echo '(not specified)'; else echo $dbname; ?>
</p>
<hr>
<p>
If you need help or if you have suggestions please contact me at: <a href="mailto:mgrzes@uwaterloo.ca">mgrzes@uwaterloo.ca</a>.
The current todo list is here <a href="TODO.txt">TODO.txt</a>, and the changelog file is here <a href="CHANGELOG.txt">CHANGELOG.txt</a>. 
</p>

<p><a href="http://www.mozilla.com/en-US/firefox/all.html"><img src="mozilla-firefox.jpg" align="left"></a>
   The current version of this web interface was tested mostly in Firefox.</p>
</body>
</html>
