<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once('model/common.php');
include_once('viewheader.php');
include_once 'viewtopmenu.php';

?>

<h3>Behaviour Dynamics</h3>
<p>
<pre>
	<?php
		for ($i=0; $i<$model->getNumBehs(); $i++ ) {
			$beh = $model->getBeh($i);
			print "\nBehaviour: ".$beh->_behName."\n";
			for ( $j=0; $j<$beh->getNumEffPrec(); $j++) {
				$effPrec = $beh->getEffPrec($j);
				print "\tEffect ".$effPrec->_id.": ";
				for ( $e=0; $e<$effPrec->getNumEff(); $e++ ) {
					$eff = $effPrec->getEff($e);
					$str =  $eff->toString();
					print $str." ";
					if ($e < $effPrec->getNumEff() - 1 ) {
						print "AND ";
					} 
				}
				print "\n";
				print "\tPrecondition ".$effPrec->_id.": ";
				for ( $p=0; $p<$effPrec->getNumPrec(); $p++ ) {
					$prec = $effPrec->getPrec($p);
					$str =  $prec->toString();
					print $str." ";
					if ($p < $effPrec->getNumPrec() - 1 ) {
						print "AND ";
					} 
				}
				print "\n\n";
			}
			for ( $j=0; $j<$beh->getNumBlockedSets(); $j++) {
				$blockedSet = $beh->getBlockedSet($j);
				print "\tImpossible set ".$blockedSet->_id.": ";
				for ( $p=0; $p<$blockedSet->getNum(); $p++ ) {
					$prec = $blockedSet->get($p);
					$str =  $prec->toString();
					print $str." ";
					if ($p < $blockedSet->get() - 1 ) {
						print "AND ";
					} 
				}
				print "\n\n";
			}
		}
		
		// TODO: print blocked data
	?>
</pre>
</p>

<?php

include 'viewfooter.php';

?>
