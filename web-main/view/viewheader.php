<?php
	// Author: Marek Grzes, University of Waterloo, 2010.
?>

<html>
<title>WEB-SNAP</title>

<link rel="stylesheet" type="text/css" href="view/style.css" />

<script LANGUAGE="JavaScript">
<!--

function toggle_scf(what){
	if(what.checked){document.runGeneratorForm.speech_config_file.disabled=0}
	else{document.runGeneratorForm.speech_config_file.disabled=1}
}

function toggle_battery_rate(what){
	if(what.checked){document.runGeneratorForm.battery_discharge_rate.disabled=0}
	else{document.runGeneratorForm.battery_discharge_rate.disabled=1}
}

function confirmDelete() {
var agree=confirm("Are you sure you wish to continue? This will remove the selected item permamently!");
if (agree)
	return true ;
else
	return false ;
}

function confirmQuestion(arg1) {
	var agree=confirm(arg1);
	if (agree)
		return true ;
	else
		return false ;
}

function stopEnterKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && ((node.type=="text") || (node.type="radio") || (node.type="password") || (node.type="checkbox")) )  {return false;}
}

document.onkeypress = stopEnterKey;

// -->
</script>

<body>

