<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once('model/common.php');
include_once('viewheader.php');
include_once 'viewtopmenu.php';
?>

<h3>Snap Generator</h3>

<p> In order to generate the SPUDD file for the planner the following steps are required. If the name of the table starts with
't_iu_', then users **should not** modify this table.
<ol>
  <li>Before the IU table, the following tables have to be populated:
  	<ol>
  	  <li><strong>t_env_variables_values</strong> - environment features and their values</li>
  	  <li><strong>t_behaviours</strong> - behaviours of the person</li>
  	  <li><strong>t_abilities</strong> - abilities of the person; bare in mind that suggestions for names of abilities will appear
  	   after you add behaviours to the t_behaviours table. In this table, each ability has an associated cost of prompting (abil_prompt_cost).
  	   Specify cost for each ability. Rule of thumb: Recallstep is 7.0, other recalls and recognition is 1.0, and affordances
  	   are 0.5. This means affordance prompts will come before recognition (because they are cheaper), and those before recall.
  	   To have this the other way around, reverse these numbers (so recallstep 0.5, recognition 1.0 and affordances 7.0).</li>
  	</ol>
  </li>
  <li>Other tables which should be populated at some point, but not necessary before the IU table:
  	<ol>
  	 <li><strong>t_observations_values</strong> - sensors and possible readings for each sensor (populated by people who provide sensors).
  	 Note, that names of sensors (i.e., obs_name in table t_observations_values) should be different than names
  	 of all variables (i.e., var_name in t_env_variables_values). For example, if you have variable door in your domain, and you want to have
  	 a sensor which provides observations about this variable, the name of the sensor (obs_name) has to be different than any existing
  	 variable in the domain. For the door variable, a good name of the sensour could be door_sensor.
  	 This constraint does not apply to values of variables, which means
  	 that variable door can have value open, and sensor door_sensor can have value open as well.
  	 </li>
  	  <li><strong>t_sensor_model</strong> - probabilities for sensor readings conditioned on state features (populated by people who provide sensors)</li>
    </ol>
  </li>
  <li>Define the world model, that is, preconditions and effects of actions - this is general knowledge of how persons'
      behaviours change the state of the world:
    <ol>
      <li><strong>t_effects_of_behaviours</strong> - here, define which state features will have which value when specific behaviour will take place. If two or
      more features change their values at the same time (but this happens every time), then all rows in this table which define this group should have
      the same numerical value in beh_effect_id. In this way, one specific outcome is defined.</li>
      <li><strong>t_preconditions4effects_of_behaviours</strong> - define preconditions for behaviours here. By preconditions we mean: in which
      state the behaviour will have specific effect. Every precondition is defined as a set of states
      and components of this set in a given precondition are identified by beh_effect_id.
      This means that in this table all rows which have the same beh_effect_id
      define one set of states, i.e., one precondition. Disjunction is not allowed, which means that for a given beh_effect_id each variable has
      to have **only one value** specified. Also, the same variable-value pair can be repeated with different beh_effect_id
      when it defines another precondition. Note, that if given behaviour has only one outcome, which happens regardless of the current state, then
      nothing is required in this table for such a behaviour.</li>
    </ol>
    It is recommended to populate these tables before going to the IU table, but it is not required.
  </li>
  <li>Define the UI table using link 'IU table' in the manu above. In this step, put all the information which you think will define
  the comptete IU table.
    <ol>
      <li>When you think, that the IU table is complete, press 'Check Subsumption of States' button and fix all errors which may appear.</li>
      <li>Now, <strong>do the backup of the full database</strong>. The next step may destroy everting. It should not, but it will shift
      the data significantly and if the process stops midway you will have a mess in the IU table.</li>
      <li>Press, the 'Check Disjunctive Behaviours' (exploding the IU table) button and see if there are any rows which are 'not confirmed'.
      This is an indication for you that
      the probability has an initial uniform value, and a custom value can be used instead. Once you change the probability you can change the status
      of the row to confirmed in the edit mode of the row. You do not have to change probabilities, default uniform values are fine, but
      if you know that certain behaviour is more likely in the given state, than you can reflect this fact in corresponding probabilities.</li>
      <li>After expanding the table you can press the button 'Reset Probabilities' which will rested probabilities to their default values, and also
      will print which rows make a group.</li>
    </ol>
  </li>
  <li>Define rewards:
    <ol>
      <li><strong>t_rewards</strong> - in this table add a separate row for each different value of the reward; each
      of these values has unique state_set_id. Some guidance on how to choose values of rewards: give 15.0 for
      the goal state. Normally, it should be sufficient to specify the reward for the goal state, because all other
      state have the reward of 0 by default. But, in case
      you want to specify rewards for some other states, try to keep all values other than the goal state below 7.0.</li>
      <li>After defining possible values of rewards in the previous step, go to table <strong>t_rewards_desc</strong>
      and define sets of states which correspond to given values of the reward in table t_rewards. For example, if
      there is reward_value=25 in t_rewards which has state_set_id=1, and you want to specify that this reward happens
      only when var_name=hands has value var_value=soapy, then you have to add this variable-value pair to the 
      t_rewards_desc table with state_set_id=1.
      </li>
    </ol>
      Sets of states which define different values of rewards can be overlapping. This means that if a particular state belongs
      to more than one set of states which determine reward, then the sum of all corresponding rewards makes the overall reward
      of the state.
  </li>
  <!-- 
  <li>Determine which ability is required for which behaviour. This information is automatically extracted by the system
      from the IU table. Press the button to do this, that is, to populate table <strong>t_abilities4behaviours</strong>
      according to the existing content of the IU table. Remember that existing dependencies in t_abilities4behaviours
      will be erased first from table t_abilities4behaviours, and populated again using information in the IU table.
    <form method="post">
	  <input type="submit" name="generate_abilities4behaviours" value="Determine abilities for behaviours" onClick="return confirmQuestion('Are you sure you wish to continue? This will remove existing dependencies and read them again from the IU table!')" />
    </form>
  </li>
  <li>Determine when each behaviour is relevent. This information is automatically extracted by the system
      from the IU table. Press the button to do this, but remember that existing information will be erased first
      from table t_when_is_behaviour_relevant. <em>Read the next step if you would like to learn about the difference
      between: impossible/possible states and relevant states.</em>
    <form method="post">
	  <input type="submit" name="generate_behaviourrelevance" value="Determine when each behaviour is relevant" onClick="return confirmQuestion('Are you sure you wish to continue? This will remove existing dependencies and read them again from the IU table!')" />
    </form>
      This table is actually redundanct and the Java SnapGenerator could read this information directly from the IU table.
      However, I keep the IU table separate from the Java SnapGenerator, and I applied this also to the infomration which
      is copied to this table. This allows also for explict representation of state relevance information.
  </li>
  -->  
  <li>Define when each behaviour is impossible. For this, use table <strong>t_when_is_behaviour_impossible</strong>.
      Here, again you are defining sets of states, and in this table each set of states defines when the given behaviour
      is not possible. One set is defined by all rows in this table which have the same value of state_set_id.
      <em>Impossible states for behaviours are those states where we know for sure that the behaviour cannot take place.
      However, the fact that the bahaviour is possible does not imply that behaviour is relevant in this state (set of states). State
      relevance determines whether it 'makes logical sense' to do this behaviour, i.e., whether it will do something good
      towards achieving the final goal. If we just know that the behaviour is possible, it does not mean that it will do
      something good for the task, furthermore, it may even undo something which was already done.</em>
  </li>
  <li>If all the steps above are done, then press this button to generate the spudd file. If you srew up then do not worry, you
      can fix the problem and try again.
      <form method="get" name="runGeneratorForm">
		<input type="checkbox" name="hicontrol" value="yes"/>Check this for hierarchical simulation, this adds to the SPUDD file things such as the parent control variable.<br/>
		<input type="checkbox" name="call_caregiver" value="yes"/>Check this if we want to have action call care giver in your POMDP.<br/>
		<input type="checkbox" name="speech_actions" value="yes" onclick="toggle_scf(this)"/>Generate speech actions (uses translator with option -d). If you are Jonathan, you should probably use this option.<br/>
		<input type="file" name="speech_config_file" disabled> If you are using the generate speech actions option, you can provide speech config file here. If you do not provide the file, the <a href="../SnapGenerator/speechconfig.txt">default file</a> from the server will be used instead.<br/>
		<input type="checkbox" name="sensor_query_actions" value="yes"/>Generate sensor query actions (uses translator with option -q).<br/>
		<input type="checkbox" name="action_specificities" value="yes"/>Generate actions with different specificities (uses translator with option -s).<br/>
		<input type="checkbox" name="add_battery_discharge" value="yes" onclick="toggle_battery_rate(this)"/>Add a battery that discharges at a rate (uses translator with option -b (rate)).<br/>
		<input type="text" name="battery_discharge_rate" value="0.0174" onLoad="toggle_battery_rate(document.runGeneratorForm.add_battery_discharge);"> If you are adding a battery, you have to provide the discharge rate here (only values in the range 0-1 allowed). The default value is given.<br/>
	    <input type="submit" name="generate_spudd" value="Generate" onClick="toggle_battery_rate(document.runGeneratorForm.add_battery_discharge);" /><br>
	    Use this button to see the output of the java Snap Generator: <input type="submit" name="generate_spudd" value="Debugging" /><br>
	    Use this button to see the command line options of the java Snap Generator: <input type="submit" name="generate_spudd" value="Options" /><br>
      </form>
  </li>
</ol> 
</p>

<h3>Simulating POMDP</h3>
<p>
  <ol>
    <li>You need to complete everyting in the previous section, generate the SPUDD file and download it to your
    	local disk space. In what follows, we assume that your SPUDD file is named snap-spudd-file.txt and is
    	located in the current directory.
    </li>
    <li>If you do not have symbolic Perseus on your computer, download it using this
        <a href="http://jhoey02.cs.uwaterloo.ca/symbolicPerseusJava.tar.gz">link</a>, and then upack the archive
        using:<br> <tt>tar -xzf symbolicPerseusJava.tar.gz</tt>
    </li>
    <li>Run the planner and generate the policy:<br>
    	<tt>java -cp /your_full_path/symbolicPerseusJava Solver snap-spudd-file.txt -g -b 1500 -r 2</tt><br>
    	<strong>I would advise you to use in /your_full_path/symbolicPerseusJava the full absolute path in your system. In Linux or Mac
    	this can be /users/Alex/symbolicPerseusJava or /home/Alex/symbolicPerseusJava, and in Windows c:\Users\Alex\symbolicPerseusJava.</strong><br>
    	Depending on the size of your POMDP, planning may take even several hours. When it ends, you will
    	find the policy file in your current directory and its name is: snap-spudd-file.pomdp, assuming that
    	the SPUDD file was snap-spudd-file.txt.
    </li>
    <li>After generating the policy you can run the simulation:<br>
    	<tt>java -cp /your_path/symbolicPerseusJava Solver snap-spudd-file.txt -i snap-spudd-file.pomdp -s 1</tt>
    </li>
    <li>If you want to simulate your POMDP before generating a policy, you can still check out how your model behaves by doing:<br>
        <tt>java -cp /your_path/symbolicPerseusJava Solver snap-spudd-file.txt -s 100</tt>
    </li>
  </ol>
</p>

<h3>FAQ</h3>
<p>
  <ol>
    <li>What is a set of states?</li>
    <li>I'm a little confused about the t_effects_of_behaviours table and how multiple outcomes
        from one behaviour are input.<br>
        If you have many outcomes in one behaviour, you specify different beh_effect_id for each
        outcome/effect. However, if it is the case that several variables change their values within
        one and the same outcome, then all these variables should be listed under the same beh_effect_id.
        So, if put_brush_in_mouth always causes the change of those tree variables at the same time
        (with beh_effect_id 8, 9 or 10) then you should have the same beh_effect_id in all these rows,
        e.g., beh_effect_id=8. If only one variable can change its value at the time, then you shoul
        have them with separate beh_effect_id as it is now.
    </li>        
  </ol>
</p>

<h3>Technical Assumptions (for experts only)</h3>
<p>
  <ol>
    <li>All behaviours of the person are deterministic in this model, because behaviours are defined
        as if they were performed by a cognitively able person. However, one behaviour can have
        many sets of outcomes, but each of these outcomes has a unique set of preconditions and happens
        with probability 1 when the corresponding precondition is satisfied. Preconditions of behaviours
        do not have anything to do with impossible and relevant states for behaviours. Preconditions are
        required in order to deal with multiple outcomes of behaviours which depend on the state in which
        the behaviour happens.
    </li>
    <li>State relevance is important in hierarchical task decomposition. If S1 has to complete before S2
        starts, then it is not relevant to do anything in S2 until S1 completes.
    </li>
    <li>Impossible behaviour in SPUDD files: 1 means that the behaviour is impossible and 0 means possible.
    </li>
    <li>Rewards are additive. If states s belongs to two different sets of states, where each set defines different
        reward, then the reward of state s is the sum all sets this state belongs to.
    </li>
    <li>If the state does not have any reward defined in the database, then its reward has a default value of 0.
    	This implies that the corresponding POMDP has to have the discount factor gamma&lt;1.
    </li>
    <li>Behaviour 'other' is never relevant.</li>
    <li>Behaviour 'nothing' is relevant only in the goal state.</li>
    <li>The goal state is the state which has the highest reward, this can be a set of state of course.</li>
  </ol>
</p>

<?php

	if (SnapGeneratorModel::$_last_message != "") {
		echo '<p>Last operation: <font color="red">'.SnapGeneratorModel::$_last_message.'</font></p>'."\n";
	}

include 'viewfooter.php';
?>
