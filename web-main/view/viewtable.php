<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once('model/common.php');
include_once('viewheader.php');
include_once 'viewtopmenu.php';

$form_name="generic_table_edit_form";
?>

<h3>Edit/View table: <?php echo $tablename; ?></h3>
<p>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="<?php echo $form_name; ?>">
<table border="1">
	<?php
		echo '<input type="hidden" name="tablename" value="'.$tablename.'">'."\n";
		// (*) print headers
		echo '<tr><th></th>';
		for($i = 0; $i < $model->getnumcolumns(); $i++) {
			echo '<th>'.$model->getcolumnname($i).'</th>';
		}
		echo '</tr>'."\n";
		for ($i=0; $i<$model->getnumrows(); $i++ ) {
			
			if ( $model->_edit_row_primary_key == "" ) {
				echo '<tr><td><input type="radio" name="row2edit" value="'.$model->getrowsprimarykey_as_string($i).'"></td>';
			} else {
				echo '<tr><td><input type="radio" name="row2edit" value="'.$model->getrowsprimarykey_as_string($i).'" disabled></td>';
			}
			
			if ($model->_edit_row_primary_key == $model->getrowsprimarykey_as_string($i) ) {
				// (*) display this row in the edit mode
				echo '<input type="hidden" name="row_being_edited" value="'.$model->getrowsprimarykey_as_string($i).'">'."\n";

				// (*) set all columns in POST if they are not set; this is required by print_input_text_or_comobox
				for($c = 0; $c < $model->getnumcolumns(); $c++) {
					if ( !isset($_POST[$model->getcolumnname($c)])) {
						$_POST[$model->getcolumnname($c)] = $model->getcolumnvalue_by_id($i, $c);
					}	
				}
				
				for($c = 0; $c < $model->getnumcolumns(); $c++) {
					echo '<td style="background-color: #BBCCBB;">';
					// (1) this is default edit mode with a text box and default value from the DB
					// echo '<input type="text" name="'.$model->getcolumnname($c).'" value="'.$model->getcolumnvalue_by_id($i, $c).'">';
					// (2) this is edit mode with a combo box
					print_input_text_or_combobox($tablename, $model->getcolumnname($c), $model->getcolumnvalue_by_id($i, $c));
					echo "</td>";
				}
				echo "</tr>\n";
								
			} else {
				// (*) display this row in the view mode (no editing of any row)
				for($c = 0; $c < $model->getnumcolumns(); $c++) {
					echo "<td>".$model->getcolumnvalue_by_id($i, $c)."</td>";
				}
				echo "</tr>\n";
			}
		}
	
	echo "</table>\n";
	echo "<br>\n";

	if ($model->_edit_row_primary_key == "") {
		echo '<input type="submit" name="edit_selected_row" value="Edit selected row">'."\n";
		echo '<input type="submit" name="delete_selected_row" value="Delete selected row" onClick="return confirmDelete()">'."\n";
	} else {
		echo '<input type="submit" name="update_selected_row" value="Save changes">'."\n";
	}
	echo "</p>\n";

	if ($model->_edit_row_primary_key == "") {
		echo "<p>\n";
		echo '<table border="1">'."\n";
		echo "<tr><th>Name</th><th>Value</th></tr>\n";
	
		for($i = 0; $i < $model->getnumcolumns(); $i++) {
			echo '<tr><td>'.$model->getcolumnname($i).'</td>';
// --- APPLICATION DEPENDENT CODE			
			if ( $tablename == "t_env_variables_values" && $model->getcolumnname($i) == "var_name" ) {
				echo '<td>Type the name of a new variable: <input type="text" name="'.$model->getcolumnname($i).'" value=""><br>';
				echo 'or choose an existing variable:';
				load_column_to_checkbox4text("var_name", "t_env_variables_values", "existing_var_name_selector", array(), "", $model->getcolumnname($i), $form_name);
				echo '</td>';
			} else if ( $tablename == "t_observations_values" && $model->getcolumnname($i) == "obs_name") {
				echo '<td>Type the name of a new observation: <input type="text" name="'.$model->getcolumnname($i).'" value=""><br>';
				echo 'or choose an existing observation:';
				load_column_to_checkbox4text("obs_name", "t_observations_values", "existing_obs_name_selector", array(), "", $model->getcolumnname($i), $form_name);
				echo '</td>';
			} else if ( $tablename == "t_abilities" && $model->getcolumnname($i) == "abil_name" ) {
				echo '<td>Type the name of a new ability: <input type="text" name="'.$model->getcolumnname($i).'" value=""><br>';
				echo 'Or use one of the suggested names for abilities:';
				load_suggested_abilities_to_checkbox("suggested_ability_selector", $model->getcolumnname($i), $form_name);
				echo '<br>If you use the suggested name you can adjust it according to your preferences.</td>';
// --- APPLICATION DEPENDENT CODE	
			} else {
				echo '<td>';
				print_input_text_or_combobox($tablename, $model->getcolumnname($i), "");
				echo '</td>';
			}
			echo '</tr>';
		}
	
		echo "</tr>\n";
		echo "</table>\n";
	
		echo "<br>\n";
		echo '<input type="submit" name="add_new_row" value="Add new row">'."\n";
		echo "</form>\n";
		echo "</p>\n";
	}
	
	if (TableModel::$_last_message != "") {
		echo '<p>Last database operation: <font color="red">'.TableModel::$_last_message.'</font></p>'."\n";
	}

include 'viewfooter.php';
?>
