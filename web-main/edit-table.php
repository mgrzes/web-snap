<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php 
	include_once("controller/EditTableController.php");

	$controller = new EditTableController();
	$controller->invoke();

?>