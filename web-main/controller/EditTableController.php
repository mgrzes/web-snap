<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once("model/TableModel.php");

class EditTableController {
	public $model;
	
	public function __construct() {  
    } 
	
	public function invoke() {
		$tablename="";
		if ( isset($_GET['tablename']) ) {
			$tablename = $_GET['tablename'];
		} else if ( isset($_POST['tablename']) ) {
			$tablename = $_POST['tablename'];
		} 
		
		// (*) display default table (useful for redirected calls to this script)
		if ($tablename == "") {
			$tablename = "t_env_variables_values";
		}
		
		// (*) tablename required
		if ( $tablename == "" ) {
			die("tablename has to be specified when executing this script!");
		}
		
		// (*) read the table from the DB
		$model = new TableModel($tablename);

		// (*) check if there is a request to add a row to the table
		if (isset($_POST['add_new_row'])) {
			$model->add_new_row();
		}
		
		// (*) delete selected row
		if (isset($_POST['delete_selected_row'])) {
			$rowid = "";
			if (isset($_POST['row2edit'])) {
				$rowid = $_POST['row2edit'];
			}
			$model->delete_selected_row($rowid);
		}

		// (*) update selected row
		if (isset($_POST['update_selected_row'])) {
			$rowid = "";
			if (isset($_POST['row_being_edited'])) {
				$rowid = $_POST['row_being_edited'];
			}
			$model->update_selected_row($rowid);
		}
		
		// (*) read the table from the DB again because the operation above could have added something to this table
		$model = new TableModel($tablename);
				
		// (*) check if there is edit request
		if (isset($_POST['edit_selected_row'])) {
			$model->_edit_row_primary_key = $_POST['row2edit'];
			$model->unset_all_columns_in_POST();
		}
		
		// (*) this is checked when the edit form is submitted due to combo box change
		if (isset($_POST['row_being_edited'])) {
			$model->_edit_row_primary_key = $_POST['row_being_edited'];
		}
		
		// (*) display the table with one row in the edit mode if requested
		include 'view/viewtable.php';
	}
}

?>
