<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once("model/IUTableModel.php");

class IUController {
	public $model;
	
	public function __construct() {  
    } 
	
	public function invoke() {
		// (*) check if there is a request to move the edited row up
		if ( isset($_POST['up_selected_row']) ) {
			IUTableModel::move_row_up($_POST['row2edit']);
		}
		// (*) check if there is a request to move the edited row down
		if ( isset($_POST['down_selected_row']) ) {
			IUTableModel::move_row_down($_POST['row2edit']);
		}
		
		// (*) check if there is a request to add a new goal to the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['add_goal']) ) {
			IUTableModel::add_varval2therow($_POST['row_being_edited'], $_POST['goal_var_name'], $_POST['goal_var_value'], "t_iu_goals", "goal");
		}
		// (*) check if there is a request to delete the goal from the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['delete_goal']) ) {
			$varname="";
			$varvalue="";
			if ( isset($_POST['goalsvarvalues']) ) {
				list($varname, $varvalue) = split('=', $_POST['goalsvarvalues']);
			}
			IUTableModel::delete_varvalfromtherow($_POST['row_being_edited'], $varname, $varvalue, "t_iu_goals", "goal");
		}

		// (*) check if there is a request to add a new state to the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['add_state']) ) {
			IUTableModel::add_varval2therow($_POST['row_being_edited'], $_POST['state_var_name'], $_POST['state_var_value'], "t_iu_task_states", "state");
		}
		// (*) check if there is a request to delete the state from the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['delete_state']) ) {
			$varname="";
			$varvalue="";
			if ( isset($_POST['statesvarvalues']) ) {
				list($varname, $varvalue) = split('=', $_POST['statesvarvalues']);
			}
			IUTableModel::delete_varvalfromtherow($_POST['row_being_edited'], $varname, $varvalue, "t_iu_task_states", "state");
		}

		// (*) check if there is a request to add a new ability to the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['add_ability']) ) {
			IUTableModel::add_value2therow($_POST['row_being_edited'], $_POST['abil_name'], "t_iu_abilities", "abil_name", "ability");
		}
		// (*) check if there is a request to delete the ability from the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['delete_ability']) ) {
			IUTableModel::delete_valuefromtherow($_POST['row_being_edited'], $_POST['ability'], "t_iu_abilities", "abil_name", "ability");
		}

		// (*) check if there is a request to add a new behaviour to the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['add_behaviour']) ) {
			IUTableModel::add_value2therow($_POST['row_being_edited'], $_POST['beh_name'], "t_iu_behaviours", "beh_name", "behaviour");
		}
		// (*) check if there is a request to delete the behaviour from the row
		if ( isset($_POST['row_being_edited']) && isset($_POST['delete_behaviour']) ) {
			IUTableModel::delete_valuefromtherow($_POST['row_being_edited'], $_POST['behaviour'], "t_iu_behaviours", "beh_name", "behaviour");
		}

		// (*) set the rows probability
		if ( isset($_POST['row_being_edited']) && isset($_POST['update_row_probability']) ) {
			IUTableModel::update_row_probability($_POST['row_being_edited'], $_POST['row_probability']);
		}

		// (*) confirm row
		if ( isset($_POST['row_being_edited']) && isset($_POST['confirm_row']) && $_POST['row_confirm_checked'] == 'checked' ) {
			IUTableModel::update_row_confirmation($_POST['row_being_edited'], true);
		}
		
		// (*) check if there is a request to add a row to the table
		if (isset($_POST['add_new_separation_row'])) {
			IUTableModel::add_new_row("false");
		}
		if (isset($_POST['add_new_data_row'])) {
			IUTableModel::add_new_row("true");
		}
		
		// (*) check if there is arequest to remove the selected row
		if (isset($_POST['delete_selected_row'])) {
			$rowid = "";
			if (isset($_POST['row2edit'])) {
				$rowid = $_POST['row2edit'];
			}
			IUTableModel::delete_selected_row($rowid);
		}

		// (*) check there are no errors in definitions of states
		if ( isset($_POST['explode_states']) ) {
			IUTableModel::explode_states();
		}

		// (*) check there are no errors in definitions of states
		if ( isset($_POST['reset_probs']) ) {
			IUTableModel::reset_probs();
		}
		
		// (*) read the table from the DB
		$model = new IUTableModel();

		// (*) check if there are no errors in definitions of states
		if ( isset($_POST['check_state_subsumption']) ) {
			$model->check_state_subsumption();
		}
		
		// (*) check if there is edit request
		if (isset($_GET['editrow'])) {
			$model->_edit_row_id = $_GET['editrow'];
		}
		if (isset($_POST['edit_selected_row'])) {
			$model->_edit_row_id = $_POST['row2edit'];
		}
		if (!isset($_GET['editrow']) && !isset($_POST['edit_selected_row']) && isset($_POST['row_being_edited'])) {
			$model->_edit_row_id = $_POST['row_being_edited'];
		}
		
		// (*) display the table with one row in the edit mode if requested
		include 'view/viewiutable.php';
		
	}
}

?>