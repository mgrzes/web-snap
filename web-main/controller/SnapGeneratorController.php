<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once("model/SnapGeneratorModel.php");

class SnapGeneratorController {
	public $model;
	
	public function __construct() {
    } 
	
	public function invoke() {
		
		include_once("model/dbparams.php");
		include_once("model/common.php");
		
		if ( isset($_GET['generate_spudd']) || isset($_GET['generate_spudd_debug']) ||
			 isset($_GET['generate_spudd_hicontrol']) || isset($_GET['generate_spudd_debug_hicontrol']) ) {
			SnapGeneratorModel::generate_theSPUDD_file();
		}
		
		//if ( isset($_POST['generate_abilities4behaviours']) ) {
		//	SnapGeneratorModel::generate_abilities4behaviours();
		//}

		//if ( isset($_POST['generate_behaviourrelevance']) ) {
		//	SnapGeneratorModel::generate_behaviourrelevance();
		//}
		
		$model = new SnapGeneratorModel();
				
		include 'view/viewsnapgenerator.php';
	}
}

?>
