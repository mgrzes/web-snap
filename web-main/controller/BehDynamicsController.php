<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once("model/BehDynamicsModel.php");

class BehDynamicsController {
	public $model;
	
	public function __construct() {  
    } 
	
	public function invoke() {
		
		// (*) read the data from the DB
		$model = new BehDynamicsModel();

		// (*) display data
		include 'view/viewbehdynamics.php';
		
	}
}

?>