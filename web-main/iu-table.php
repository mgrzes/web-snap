<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php 
	include_once("controller/IUController.php");

	$controller = new IUController();
	$controller->invoke();

?>