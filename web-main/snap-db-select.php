<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php

include_once("model/dbparams.php");
include_once("model/common.php");

global $dbname;
global $dbconfig_name;
global $dbuser;
global $dbpasswd;

if ( isset($_POST['backup_db'])) {
	$db4backup = $_POST['selected_db4bacup'];
	$str = get_currenttimestamp_as_string();
	$dir = '/tmp/';
	$filename = $db4backup."-BACKEDUP-ON-".$str.'.txt';
	$command = "pg_dump -U x721demouser ".$db4backup." > ".$dir.$filename;
	//echo $command;
	//exit(1);
	exec($command, $output = array(), $ret);
	if ( $ret != 0) {
		echo 'Your backup of '.$dbname.' failed!';
		exit(1);
	} else {
	    header("Content-type: application/force-download");
	    header("Content-Transfer-Encoding: Binary");
	    header("Content-length: ".filesize($dir.$filename));
	    header('Content-disposition: attachment; filename="'.$filename.'"');
	    readfile($dir.$filename);
	    exit(0);
	}
}

include_once('view/viewheader.php');
include_once 'view/viewtopmenu.php';
	
	if (isset($_POST['change_db'])) {
		if ( $_POST['new_selected_db4achange'] == "" ) {
			echo '<p><font color="red">You have to select the snap instance from the list. No change perfomred!</font></p>';
		} else {
			if ($dbname == $_POST['new_selected_db4achange']) {
				echo '<p><font color="red">Your current snap instance is already '.$dbname.'. No change perfomred!</font></p>';
			} else {
				$newdbname = $_POST['new_selected_db4achange'];
				$dblink = pg_connect("dbname=".$newdbname." user=".$dbuser." password=".$dbpasswd);
				if ($dblink) {
					$dbname = $newdbname;
					$_SESSION['dbname'] = $dbname;
					echo '<p><font color="red">Your successfully changed your databse to '.$dbname.'</font></p>';
				} else {
					echo '<p><font color="red">You cannot change your instgance to '.$newdbname.'. Connection to '.$newdbname.' failed!</font></p>';			}
			}
		}
	}

	if (isset($_POST['copy_dbs'])) {
		if ( $_POST['selected_dbsource'] == "" ) {
			echo '<p><font color="red">You have to select the source instance from the list. No change perfomred!</font></p>';
		} else if ( $_POST['selected_dbtarget'] == "" ) {
			echo '<p><font color="red">You have to select the target instance from the list. No change perfomred!</font></p>';
		} else {
			$sourcedbname = $_POST['selected_dbsource'];
			$targetdbname = $_POST['selected_dbtarget'];
			// (*) first erease the target databse
			$command = "psql -U x721demouser -W -d ".$targetdbname." < ../SnapGenerator/sql-files/erease-snapdb.sql";
			if ( $_DEBUG==true ) {			
				echo $command;
			}
			exec($command, $output = array(), $ret);
			if ( $ret != 0 ) {
				echo '<p><font color="red">Ereasing of '.$targetdbname.' failed!</font></p>';
			} else {
				echo "<p>1. ".$targetdbname." database earesed successfully</p>";
				// (*) if target database earased successflully, then dump the source database
				$str = get_currenttimestamp_as_string();
				$dir = '/tmp/';
				$filename = $sourcedbname."-BACKEDUP-ON-".$str.'.txt';
				$command = "pg_dump -U x721demouser ".$sourcedbname." > ".$dir.$filename."";
				if ( $_DEBUG==true ) {			
					echo $command;
				}
				exec($command, $output = array(), $ret);
				if ( $ret != 0 ) {
					echo '<p><font color="red">Dumping '.$sourcedbname.' failed!</font></p>';
				} else {
					echo "<p>2. ".$sourcedbname." database copied successfully</p>";
					$command = "psql -U x721demouser -W -d ".$targetdbname." < ".$dir.$filename."";
					if ( $_DEBUG==true ) {			
						echo $command;
					}
					//sleep(2);
					exec($command, $output = array(), $ret);
					if ( $ret != 0 ) {
						echo '<p><font color="red">Populating '.$targetdbname.' failed!</font></p>';
					} else {
						echo "<p>3. ".$targetdbname." database copulated successfully. Now, ".$targetdbname." contains the same data as ".$sourcedbname."</p><br><br>";
					}
				}
			}
		}
	}
	
	echo '<p>Your current snap instance is '.$dbname.'. You can change your snap instance using the following options below.</p><p>'."\n";

	$dblink = pg_connect("dbname=".$dbconfig_name." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbconfig_name." failed: ".pg_last_error());
	
	// This query allows me to know which databases are available to user x721demouser.
	// SELECT d.datname as "Name", r.rolname as "Owner" FROM pg_catalog.pg_database d JOIN pg_catalog.pg_roles r ON d.datdba = r.oid JOIN pg_catalog.pg_tablespace t on d.dattablespace = t.oid WHERE r.rolname='x721demouser' AND NOT d.datname='snap-config' ORDER BY 1;
	
	$query = 'SELECT d.datname as "Name" FROM pg_catalog.pg_database d JOIN pg_catalog.pg_roles r ON d.datdba = r.oid JOIN pg_catalog.pg_tablespace t on d.dattablespace = t.oid WHERE r.rolname=\''.$dbuser.'\' AND NOT d.datname=\''.$dbconfig_name.'\' ORDER BY 1'; 
	
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		// (*) choose database form
		echo "You can select the snap instance from the list below and press the button 'Use selected instance' to accept:\n";
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">'."\n";
		//displaytable($dbresult);
		echo '<select name="new_selected_db4achange">'."\n";
		echo '<option value="">(please select:)</option>'."\n";
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			if ( pg_result($dbresult, $j,0) != $dbname ) {
				echo '<option value="'.pg_result($dbresult, $j,0).'">'.pg_result($dbresult, $j,0).'</option>'."\n";
			} else {
				echo '<option value="'.pg_result($dbresult, $j,0).'" selected>'.pg_result($dbresult, $j,0).'</option>'."\n";
			}
		}
		echo "</select>\n";
		echo '<input type="submit" name="change_db" value="Use selected instance">'."\n";
		echo "</form>\n";

		// (*) backup selected database
		echo "<p>You can download the selected snap instance as the SQL file. Choose the snap instance you would like to backup and press button 'Download'.";
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">'."\n";
		//displaytable($dbresult);
		echo '<select name="selected_db4bacup">'."\n";
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			if ( pg_result($dbresult, $j,0) != $dbname ) {
				echo '<option value="'.pg_result($dbresult, $j,0).'">'.pg_result($dbresult, $j,0).'</option>'."\n";
			} else {
				echo '<option value="'.pg_result($dbresult, $j,0).'" selected>'.pg_result($dbresult, $j,0).'</option>'."\n";
			}
		}
		echo "</select>\n";
		echo '<input type="submit" name="backup_db" value="Download">'."\n";
		echo "</form>\n";
		
		echo "</p>";
		
		// (*) copy database content from the source database to the target database
		echo "<p>You can copy the contend of the source database to the target database. ";
		echo "First, select the source and the target database, and then press button 'Copy Database'. ";
		echo "Note, that the target database will be permamently erased and populated with the content of the source database. ";
		echo "This process may be also time consuming if you are copying large databases.";
		echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">'."\n";
		//displaytable($dbresult);
		echo 'Select the source database: <select name="selected_dbsource">'."\n";
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			if ( pg_result($dbresult, $j,0) != $dbname ) {
				echo '<option value="'.pg_result($dbresult, $j,0).'">'.pg_result($dbresult, $j,0).'</option>'."\n";
			} else {
				echo '<option value="'.pg_result($dbresult, $j,0).'" selected>'.pg_result($dbresult, $j,0).'</option>'."\n";
			}
		}
		echo "</select>\n";
		echo 'Select the target database: <select name="selected_dbtarget">'."\n";
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			//if ( pg_result($dbresult, $j,0) != $dbname ) {
				echo '<option value="'.pg_result($dbresult, $j,0).'">'.pg_result($dbresult, $j,0).'</option>'."\n";
			//} else {
			//	echo '<option value="'.pg_result($dbresult, $j,0).'" selected>'.pg_result($dbresult, $j,0).'</option>'."\n";
			//}
		}
		echo "</select>\n";
		echo '<input type="submit" name="copy_dbs" value="Copy Database" onClick="return confirmQuestion(\'Are you sure you wish to continue? This will remove all existing data from the target database!\')">'."\n";
		echo "</form>\n";
		
		echo "</p>";
		
	} else {
		echo(pg_last_error());
		exit(1);
	}

	echo '</p>';
	
include('view/viewfooter.php');
?>
