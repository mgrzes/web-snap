<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php 
	include_once("controller/BehDynamicsController.php");

	$controller = new BehDynamicsController();
	$controller->invoke();

?>