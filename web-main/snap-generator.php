<?php
	// Author: Marek Grzes, University of Waterloo, 2011.
?>

<?php 
	include_once("controller/SnapGeneratorController.php");

	$controller = new SnapGeneratorController();
	$controller->invoke();

?>