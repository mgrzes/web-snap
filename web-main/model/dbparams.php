<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php

	global $dbname;
	global $dbuser;
	global $dbpasswd;
	global $dbconfig_name;
	global $dbserver;

	// This database has to exist but it does not contain data of any instance.
	$dbconfig_name="snap-config";
	$dbuser="x721demouser";
	$dbpasswd="changeit";
	$dbserver="localhost";
	
	session_start();

	if (isset($_POST['use_select_db'])) {
		// selected_db is an option selected in the form below.
		$_SESSION['dbname']=$_POST['selected_db'];
	}
	
	if(isset($_SESSION['dbname']) && $_SESSION['dbname']!="" ) {
		$dbname=$_SESSION['dbname'];
	} else {
		include_once('view/viewheader.php');
		include_once("model/common.php");
		
		$dblink = pg_connect("dbname=".$dbconfig_name." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbconfig_name." failed: ".pg_last_error());
	
		// This query allows me to know which databases are available to user x721demouser.
		// SELECT d.datname as "Name", r.rolname as "Owner" FROM pg_catalog.pg_database d JOIN pg_catalog.pg_roles r ON d.datdba = r.oid JOIN pg_catalog.pg_tablespace t on d.dattablespace = t.oid WHERE r.rolname='x721demouser' AND NOT d.datname='snap-config' ORDER BY 1;
		$query = 'SELECT d.datname as "Name" FROM pg_catalog.pg_database d JOIN pg_catalog.pg_roles r ON d.datdba = r.oid JOIN pg_catalog.pg_tablespace t on d.dattablespace = t.oid WHERE r.rolname=\''.$dbuser.'\' AND NOT d.datname=\''.$dbconfig_name.'\' ORDER BY 1'; 
		
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
			echo "<p>\n";
			echo "Select the snap instance from the list below:\n";
			echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">'."\n";
			//displaytable($dbresult);
			echo '<select name="selected_db">'."\n";
			echo '<option value="" selected>(please select:)</option>'."\n";
			$rows = pg_NumRows($dbresult);
			for($j = 0; $j < $rows; $j++) {
				echo '<option value="'.pg_result($dbresult, $j,0).'">'.pg_result($dbresult, $j,0).'</option>'."\n";	
			}
			echo "</select>\n";
			echo '<input type="submit" name="use_select_db" value="Use selected instance">'."\n";
			echo "</form></p>\n";		
		} else {
			echo(pg_last_error());
			exit(1);
		}

		include('view/viewfooter.php');
		exit(1);
	}
?>
