<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php

include_once("dbparams.php");
include_once("common.php");

class IURow {
	public $_row_id;
	// data row or separator
	public $_row_type;
	// goals is an array of two element associative arrays which store the key-value pairs
	public $_row_probability;
	public $_row_confirmed;
	public $_goals;
	// goals is an array of two element associative arrays which store the key-value pairs
	public $_task_states;
	// list of abilities
	public $_abilities;
	// list of behaviurs (normally there will be only one element in this list)
	public $_behaviours;
	
	public function __construct($id, $type, $probability, $confirmed) {
		  $this->_row_id = $id;
		  $this->_row_probability = $probability;
		  if ( strcmp($confirmed, "t") == 0) {
		  	$this->_row_confirmed = true;
		  }
		  if ( strcmp($confirmed, "f") == 0) {
		  	$this->_row_confirmed = false;
		  }
		  if ( strcmp($type, "t") == 0) {
		  	$this->_row_type = 1;
		  }
		  if ( strcmp($type, "f") == 0) {
		  	$this->_row_type = 0;
		  }
		  if ( $this->_row_type == 1 ) {
		  	// (*) load goals for this row
			global $dbname;
			global $dbuser;
			global $dbpasswd;
			$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
			$dbresult = pg_exec($dblink, "SELECT var_name, var_value FROM t_iu_goals WHERE row_id='".$id."' ORDER BY 1,2");
			if ($dbresult) {
				$this->_goals = array();
				$rows = pg_NumRows($dbresult);
				for($j = 0; $j < $rows; $j++) {
					//echo $id. " = " . pg_result($dbresult, $j, 0). " = " .pg_result($dbresult, $j, 1). "\n";
					$key = pg_result($dbresult, $j,0);
					$value = pg_result($dbresult, $j,1);
					$tmparray = array();
					$tmparray[$key] = $value;
					array_push($this->_goals, $tmparray);
				}
			} else {
				echo(pg_last_error());
				exit(1);
			}
		  	// (*) load task states for this row
			$dbresult = pg_exec($dblink, "SELECT var_name, var_value FROM t_iu_task_states WHERE row_id='".$id."' ORDER BY 1,2");
			if ($dbresult) {
				$this->_task_states = array();
				$rows = pg_NumRows($dbresult);
				for($j = 0; $j < $rows; $j++) {
					//echo $id. " = " . pg_result($dbresult, $j, 0). " = " .pg_result($dbresult, $j, 1). "\n";
					$key = pg_result($dbresult, $j,0);
					$value = pg_result($dbresult, $j,1);
					if ( isset($this->_task_states[$key]) ) {
						echo "ERROR: there is a problem in the t_iu_task_states table in row ".$id." because variable [";
						echo $key."] is specified twice in this row with values [".$value."] and [".$this->_task_states[$key]."]. ";
						echo "You have to fix this problem manually in the t_iu_task_states table because this problem was caused ";
						echo "by manual manipulation of this table and has to be fixed manually. ";
						echo "If you are using web interface to create the IU table, this problem should not appear.";
						exit(1);
					}
					$this->_task_states[$key] = $value;
				}
			} else {
				echo(pg_last_error());
				exit(1);
			}
		  	// (*) load abilities for this row
			$dbresult = pg_exec($dblink, "SELECT abil_name FROM t_iu_abilities WHERE row_id='".$id."' ORDER BY 1");
			if ($dbresult) {
				$this->_abilities = array();
				$rows = pg_NumRows($dbresult);
				for($j = 0; $j < $rows; $j++) {
					//echo $id. " = " . pg_result($dbresult, $j, 0). "\n";
					array_push($this->_abilities, pg_result($dbresult, $j,0));
				}
			} else {
				echo(pg_last_error());
				exit(1);
			}
		  	// (*) load behaviours for this row
			$dbresult = pg_exec($dblink, "SELECT beh_name FROM t_iu_behaviours WHERE row_id='".$id."' ORDER BY 1");
			if ($dbresult) {
				$this->_behaviours = array();
				$rows = pg_NumRows($dbresult);
				for($j = 0; $j < $rows; $j++) {
					//echo $id. " = " . pg_result($dbresult, $j, 0). "\n";
					array_push($this->_behaviours, pg_result($dbresult, $j,0));
				}
			} else {
				echo(pg_last_error());
				exit(1);
			}
		  }
	}

	/// @return true if this row and row object2 have the same values of all keys which are not null in both rows
	public function equal_on_nonnullkeys($object2) {
		foreach ($this->_task_states as $key=>$value) {
			foreach ($object2->_task_states as $key2=>$value2) {
				if ( $key == $key2 && $value != $value2) {
					return false;
				}		
			}
		}
		return true;
	}
	
	/// @return true if this row and row object2 have the same values of all keys which are not null in both rows, and
	/// there are no keys which are null in the other object
	public function equal($object2) {
		// compare on non null keys
		foreach ($this->_task_states as $key=>$value) {
			foreach ($object2->_task_states as $key2=>$value2) {
				if ( $key == $key2 && $value != $value2) {
					return false;
				}		
			}
		}
		// check if this has some keys which are null in object2
		foreach ($this->_task_states as $key=>$value) {
			$found = false;
			foreach ($object2->_task_states as $key2=>$value2) {
				if ( $key == $key2) {
					$found = true;
				}		
			}
			if ( !$found ) return false;
		}
		// check if object2 has some keys which are null in this
		foreach ($object2->_task_states as $key2=>$value2) {
			$found = false;
			foreach ($this->_task_states as $key=>$value) {
				if ( $key == $key2) {
					$found = true;
				}		
			}
			if ( !$found ) return false;
		}
		return true;
	}
	
	/// @return true when the row has epmpty state OR empty behaviour
	public function is_row_empty() {
		if ($this->_row_type == 0) {
			// separator is never empty
			return false;
		}
		if ( count($this->_task_states) == 0 || count($this->_behaviours) == 0 ) {
			return true;
		}
		return false;
	}
}

class IUTableModel {
	protected $_rows;
	public $_edit_row_id;
	public static $_last_message;
	// This is something which when not empty will be displayed before the IU table
	public static $_front_message;
	
	public function __construct() {
		$this->_rows = array();
		// if -1 than there is not row to edit
		$this->_edit_row_id = -1;
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		// (*) read all rows from the table
		$dbresult = pg_exec($dblink, "SELECT row_id, data_row, row_probability, row_confirmed FROM t_iu_row ORDER BY row_id");
		if ($dbresult) {
			$rows = pg_NumRows($dbresult);
			for($j = 0; $j < $rows; $j++) {
				//echo pg_result($dbresult, $j, 0). " = " .pg_result($dbresult, $j, 1). "\n";
				$row = new IURow( pg_result($dbresult, $j,0), pg_result($dbresult, $j,1), pg_result($dbresult, $j,2), pg_result($dbresult, $j,3) );
				//echo $row->_row_id. " - " .$row->_row_type."\n";
				array_push($this->_rows, $row);
			}
			//exit(1);
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public function getnumrows() {
		return count($this->_rows);
	}
	
	public function getrow($id) {
		return $this->_rows[$id];
	}
	
	private static function get_max_row_id($connection) {
		$dbresult = pg_exec($connection, "SELECT MAX(row_id) FROM t_iu_row");
		if ($dbresult) {
			$rows = pg_NumRows($dbresult);
			if ($rows > 0) {
				return pg_result($dbresult, 0, 0);
			} else {
				return 0;
			}
			//exit(1);
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	/// @return - the id of the row which was added
	public static function add_new_row($rowtype) {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		$nextid = IUTableModel::get_max_row_id($dblink) + 1;
		$dbresult = pg_exec($dblink, "INSERT INTO t_iu_row(row_id, data_row) VALUES (".$nextid.",".$rowtype.")");
		if ($dbresult) {
			IUTableModel::$_last_message = "Operation add new row was successful!";
			return $nextid;
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	/// @return all row_ids from t_iu_row in the ASC order
	public static function get_all_row_ids_asc() {
		$result = array();
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		$query = "SELECT DISTINCT row_id FROM t_iu_row ORDER BY 1 ASC"; 
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
			$rows = pg_NumRows($dbresult);
			for($j = 0; $j < $rows; $j++) {
				array_push($result, pg_result($dbresult, $j,0));
			}
		} else {
			echo(pg_last_error());
			exit(1);
		}
		return $result;
	}
	
	public static function delete_selected_row($row2rem) {
		if ($row2rem == "") {
			IUTableModel::$_last_message = "Row to remove has to be specified. Operation failed!";
			return;
		}
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		// (*) first delete all data for the row to be deleted, exept the row itself
		$dbresult = pg_exec($dblink, "DELETE FROM t_iu_goals WHERE row_id=".$row2rem);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "DELETE FROM t_iu_task_states WHERE row_id=".$row2rem);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "DELETE FROM t_iu_abilities WHERE row_id=".$row2rem);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "DELETE FROM t_iu_behaviours WHERE row_id=".$row2rem);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		
		$max_row_id = IUTableModel::get_max_row_id($dblink);
		
		if ($row2rem == $max_row_id) {
			// (*) if this is the last row, then just delete this row
			$dbresult = pg_exec($dblink, "DELETE FROM t_iu_row WHERE row_id=".$row2rem);
			if ($dbresult) {
			} else {
				echo(pg_last_error());
				exit(1);
			}
			IUTableModel::$_last_message = "Row ".$row2rem. " delated successfully, no indexes updated since this was the last row!";	
		} else {
			// (*) NOW UPDATES IDS OF ALL ROWS WHICH HAVE ID > ID OF THE ROW WHICH IS GOING TO BE REMOVED
			$all_row_ids = IUTableModel::get_all_row_ids_asc();
			for ( $i=0; $i < count($all_row_ids); $i++ ) {
				if ( $all_row_ids[$i] <= $row2rem) {
					// skip all row ids which are smaller or equal to the one which is being removed
					continue;
				}
				echo "<!-- moving data of row: ".$all_row_ids[$i]." up -->\n";
				$dbresult = pg_exec($dblink, "UPDATE t_iu_goals SET row_id = row_id - 1 WHERE row_id=".$all_row_ids[$i]);
				if ($dbresult) {
				} else {
					echo(pg_last_error());
					exit(1);
				}
				$dbresult = pg_exec($dblink, "UPDATE t_iu_task_states SET row_id = row_id - 1 WHERE row_id=".$all_row_ids[$i]);
				if ($dbresult) {
				} else {
					echo(pg_last_error());
					exit(1);
				}
				$dbresult = pg_exec($dblink, "UPDATE t_iu_abilities SET row_id = row_id - 1 WHERE row_id=".$all_row_ids[$i]);
				if ($dbresult) {
				} else {
					echo(pg_last_error());
					exit(1);
				}
				$dbresult = pg_exec($dblink, "UPDATE t_iu_behaviours SET row_id = row_id - 1 WHERE row_id=".$all_row_ids[$i]);
				if ($dbresult) {
				} else {
					echo(pg_last_error());
					exit(1);
				}
			}
					
			// BEFORE I REMOVE THIS ROW I HAVE TO UPDATES IDS OF ALL ROWS WHICH HAVE ID HIGHER THAN THE ID OF THE ROW WHICH I AM REMOVING (DONE ABOVE)
			
			// (*) update data type of all rows starting from the id of the row which is being removed
			$dbresult = pg_exec($dblink, "UPDATE t_iu_row AS T1 SET data_row = (SELECT data_row FROM t_iu_row WHERE row_id = T1.row_id + 1) WHERE row_id>=".$row2rem." AND row_id < ".$max_row_id);
			if ($dbresult) {
			} else {
				echo(pg_last_error());
				exit(1);
			}
			//(*) now, actually the last row on the list is going to be removed
			$dbresult = pg_exec($dblink, "DELETE FROM t_iu_row WHERE row_id=(SELECT MAX(row_id) FROM t_iu_row)");
			if ($dbresult) {
			} else {
				echo(pg_last_error());
				exit(1);
			}
			IUTableModel::$_last_message = "Row ".$row2rem. " delated successfully, and all indexes for rows with higher id were updated!";
		}
	}
	
	public static function add_varval2therow($rowid, $varname, $varvalue, $tablename, $iucolumnname) {
		if ( $varname == "" || $varvalue == "" ) {
			IUTableModel::$_last_message = "The variable name and the value have to be specified for the new ".$iucolumnname." to be added to row ".$rowid.". Operation failed!";
			return;
		}
		
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$dbresult = pg_exec($dblink, "INSERT INTO ".$tablename."(row_id, var_name, var_value) VALUES (".$rowid.",'".$varname."','".$varvalue."')");
		if ($dbresult) {
			IUTableModel::$_last_message =  $iucolumnname." ".$varname."=".$varvalue." successfully added to row ".$rowid."!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}

	public static function getallvariablevalues($varname) {
		$result = array();
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
		
		$dbresult = pg_exec($dblink, "SELECT var_value FROM t_env_variables_values WHERE var_name='".$varname."'");
		if ($dbresult) {
			$rows = pg_NumRows($dbresult);
			for ( $i = 0; $i < $rows; $i++ ) {
				 array_push( $result, pg_result($dbresult, $i, 0) );
			}
			return $result;
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public static function delete_varvalfromtherow($rowid, $varname, $varvalue, $tablename, $iucolumnname) {
		if ( $varname == "" || $varvalue == "" ) {
			IUTableModel::$_last_message = "First, you have to select the ".$iucolumnname." which you wish to remove from row ".$rowid.". Operation failed!";
			return;
		}
		
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$dbresult = pg_exec($dblink, "DELETE FROM ".$tablename." WHERE row_id=".$rowid." AND var_name='".$varname."' AND var_value='".$varvalue."'");
		if ($dbresult) {
			IUTableModel::$_last_message = $iucolumnname." ".$varname."=".$varvalue." successfully removed from row ".$rowid."!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public static function add_value2therow($rowid, $value, $tablename, $column, $iucolumnname) {
		if ( $value == "" ) {
			IUTableModel::$_last_message = "The ".$iucolumnname." has to be specified in row ".$rowid.". Operation failed!";
			return;
		}
		
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$dbresult = pg_exec($dblink, "INSERT INTO ".$tablename."(row_id, ".$column.") VALUES (".$rowid.",'".$value."')");
		if ($dbresult) {
			IUTableModel::$_last_message =  $iucolumnname." ".$value." successfully added to row ".$rowid."!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public static function delete_valuefromtherow($rowid, $value, $tablename, $column, $iucolumnname) {
		if ( $value == "" ) {
			IUTableModel::$_last_message = "The ".$iucolumnname." which you wish to remove from row ".$rowid." has to be selected. Operation failed!";
			return;
		}
		
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$dbresult = pg_exec($dblink, "DELETE FROM ".$tablename." WHERE row_id=".$rowid." AND ".$column."='".$value."'");
		if ($dbresult) {
			IUTableModel::$_last_message = $iucolumnname." ".$value." successfully removed from row ".$rowid."!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public static function move_row_up($rowid) {
		if ( $rowid == "" ) {
			IUTableModel::$_last_message = "Row has to be selected first. The operation of moving the row up was cancelled!";
			return;
		}
		
		if ( $rowid == "1") {
			IUTableModel::$_last_message = "Row ".$rowid." is already a top row. The operation of moving the row up was cancelled!";
			return;
		}
		
		IUTableModel::swap_rows($rowid, ((int)$rowid)-1);
	}

	public static function move_row_down($rowid) {
		if ( $rowid == "" ) {
			IUTableModel::$_last_message = "Row has to be selected first. The operation of moving the row down was cancelled!";
			return;
		}

		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		$max_row_id = IUTableModel::get_max_row_id($dblink);
		
		if ( $rowid == "".$max_row_id ) {
			IUTableModel::$_last_message = "Row ".$rowid." is already a bottom row. The operation of moving the row down was cancelled!";
		}
		
		IUTableModel::swap_rows($rowid, ((int)$rowid)+1);
	}
	
	public static function swap_rows($rowid1, $rowid2) {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$tmprowid = IUTableModel::get_max_row_id($dblink) + 1;

		// (*) add temporary row at the bottom
		IUTableModel::add_new_row("true");
		
		// (*) now shift the data of these rows
		IUTableModel::move_row_data($rowid1, $tmprowid);
		IUTableModel::move_row_data($rowid2, $rowid1);
		IUTableModel::move_row_data($tmprowid, $rowid2);
		
		// (*) remove the temporary row
		IUTableModel::delete_selected_row($tmprowid);
		
		IUTableModel::$_last_message = "Rows ".$rowid1." and ".$rowid2." swapped successfully!";		
	}
	
	public static function copy_row_data($fromrow, $torow) {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");

		$query = "UPDATE t_iu_row SET data_row = (SELECT data_row FROM t_iu_row WHERE row_id=".$fromrow.") WHERE row_id=".$torow;
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
		} else {
			echo($query);
			echo(pg_last_error());
			exit(1);
		}
		$query = "INSERT INTO t_iu_goals(row_id, var_name, var_value) (SELECT ".$torow.", var_name, var_value FROM t_iu_goals WHERE row_id=".$fromrow.")";
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
		} else {
			echo($query);
			echo(pg_last_error());
			exit(1);
		}
		$query = "INSERT INTO t_iu_task_states(row_id, var_name, var_value) (SELECT ".$torow.", var_name, var_value FROM t_iu_task_states WHERE row_id=".$fromrow.")";
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
		} else {
			echo($query);
			echo(pg_last_error());
			exit(1);
		}
		$query = "INSERT INTO t_iu_abilities(row_id, abil_name) (SELECT ".$torow.", abil_name FROM t_iu_abilities WHERE row_id=".$fromrow.")";
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
		} else {
			echo($query);
			echo(pg_last_error());
			exit(1);
		}
		$query = "INSERT INTO t_iu_behaviours(row_id, beh_name) (SELECT ".$torow.", beh_name FROM t_iu_behaviours WHERE row_id=".$fromrow.")";
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
		} else {
			echo($query);
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public static function move_row_data($fromrow, $torow) {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());

		$dbresult = pg_exec($dblink, "UPDATE t_iu_row SET data_row = (SELECT data_row FROM t_iu_row WHERE row_id=".$fromrow.") WHERE row_id=".$torow);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "UPDATE t_iu_goals SET row_id = ".$torow." WHERE row_id=".$fromrow);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "UPDATE t_iu_task_states SET row_id = ".$torow." WHERE row_id=".$fromrow);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "UPDATE t_iu_abilities SET row_id = ".$torow." WHERE row_id=".$fromrow);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
		$dbresult = pg_exec($dblink, "UPDATE t_iu_behaviours SET row_id = ".$torow." WHERE row_id=".$fromrow);
		if ($dbresult) {
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public static function update_row_probability($rowid, $probability) {
		if ( intval($probability) < 0 || intval($probability) > 1 ) {
			IUTableModel::$_last_message = "The probability of a row has to be in the range of [0-1]. The value of ".$probability." was given. Operation failed!";
			return;
		}
		
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$dbresult = pg_exec($dblink, "UPDATE t_iu_row SET row_probability=".$probability." WHERE row_id=".$rowid);
		if ($dbresult) {
			IUTableModel::$_last_message = "Probability of row ".$rowid." successfully set to the value of ".$probability."!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	public static function update_row_confirmation($rowid, $status) {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$newstatus = 't';
		if ($status == false) {
			$newstatus = 'f';
		}
		
		$dbresult = pg_exec($dblink, "UPDATE t_iu_row SET row_confirmed='".$newstatus."' WHERE row_id=".$rowid);
		if ($dbresult) {
			IUTableModel::$_last_message = "Status of row ".$rowid." successfully set to the value of ".$newstatus."!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public function check_state_subsumption() {
		IUTableModel::$_front_message = array();

		array_push(IUTableModel::$_front_message, "The result of the state consistency check:");
		array_push(IUTableModel::$_front_message, "");
		array_push(IUTableModel::$_front_message, "Here, we check all pairs of rows, and the state subsumption exists when there are two rows
		which have the same behaviour but the state set of one row is a *** subset *** of the state set of the other row. If such pairs exist, it means
		that one row can be removed or the one which has bigger state set (i.e. the one which specifies less variable/value pairs)
		should be specified (by adding more variable/value pairs) so that two rows will define a disjuncive behaviour (i.e, the same behaviour will
		be applicable in two sets of states which are disjoint).
		The special case of this problem is when two rows have the same behaviour and exactly the same sets of states.
		If two rows have the same behaviour but their sets of states have non-empty difference, then this is fine from the point of view of state subsumption.");
		array_push(IUTableModel::$_front_message, "");
		$found = false;
		
		// (*) check all pairs of rows
		for ( $row1 = 0; $row1 < count($this->_rows); $row1++ ) {
			if ( $this->_rows[$row1]->_row_type == 0) {
				continue;
			}
			for ( $row2 = $row1 + 1; $row2 < count($this->_rows); $row2++ ) {
				if ( $this->_rows[$row2]->_row_type == 0) {
					continue;
				}
				$diff1 = array_diff($this->_rows[$row1]->_behaviours, $this->_rows[$row2]->_behaviours);
				$diff2 = array_diff($this->_rows[$row2]->_behaviours, $this->_rows[$row1]->_behaviours);
				if ( count($diff1) != 0 || count($diff2) != 0 ) {
					// for subsumption do not check those lines which have different behaviours
					continue;
				}
				$subsumption = true;
				$row1_data = $this->_rows[$row1];
				$row2_data = $this->_rows[$row2];
				
				// (*) the actual check of subsumption between row1 and row2
				$row1_has_one_predicate_more = false;
				foreach ($row1_data->_task_states as $key=>$value) {
					if ( !in_array($key, array_keys($row2_data->_task_states)) ) {
						$row1_has_one_predicate_more = true;
					}
					if ( in_array($key, array_keys($row2_data->_task_states)) && $row2_data->_task_states[$key] !=  $value ) {
						// if different values then no subsumption for sure
						$subsumption = false;
						break;
					}
				}
				if ( $subsumption == true && $row1_has_one_predicate_more == true) {
					foreach ($row2_data->_task_states as $key=>$value) {
						if ( !in_array($key, array_keys($row1_data->_task_states)) ) {
							// no subsumption only when row1 has a precitace which was null in row2 (this is indicated by $row1_has_one_predicate_more)
							$subsumption = false;
							break;
						}
					}
				}				
				
				if ( $subsumption == true) {
					$found = true;
					$id1 = intval($row1)+1;
					$id2 = intval($row2)+1;
					array_push(IUTableModel::$_front_message, "there is state subsumption between rows ".$id1." and ".$id2."!");
				}
			}
		}
		
		if ($found == false) {
			array_push(IUTableModel::$_front_message, "OK.");
		}
		if ( $found == true) return false;
		else return true;
	}

	/// @param $row1 and $row2 are indexes starting from 0 which are in object $this (the id in the DB is $rowi + 1)
	public function explodethese2rows($row1, $row2) {
		$row1_data = $this->_rows[$row1];
		$row2_data = $this->_rows[$row2];
		
		$newrow_db_ids = array();
		
		$values4copies_of_row2 = array();
		
		// (*) if at least one key from the row1 is null in the row 2 then explosion is required
		foreach ($row1_data->_task_states as $key=>$value) {
			if ( !in_array($key, array_keys($row2_data->_task_states)) ) {
				$all_values = IUTableModel::getallvariablevalues($key);
				//print_r($all_values);
				$tmp = $values4copies_of_row2;
				$values4copies_of_row2 = array();
				for ( $v = 0; $v < count($all_values); $v++ ) {
					if ( count($tmp) == 0) {
						$tmp2 = array();
						array_push( $values4copies_of_row2, $tmp2);
						$lastindex = count($values4copies_of_row2) - 1;
						$values4copies_of_row2[$lastindex][$key] = $all_values[$v];
					} else {
						for ( $r2 = 0; $r2 < count($tmp); $r2++ ) {
							array_push( $values4copies_of_row2, $tmp[$r2]);
							$lastindex = count($values4copies_of_row2) - 1;
							$values4copies_of_row2[$lastindex][$key] = $all_values[$v];
						}
					}
				}
			}
		}
		echo "<!-- variables for exploading the second row: \n"; print_r($values4copies_of_row2); echo "-->\n";
		
		$values4copies_of_row1 = array();

		// (*) if at least one key from the row2 is null in the row 1 then explosion is required
		foreach ($row2_data->_task_states as $key=>$value) {
			if ( !in_array($key, array_keys($row1_data->_task_states)) ) {
				$all_values = IUTableModel::getallvariablevalues($key);
				//print_r($all_values);
				$tmp = $values4copies_of_row1;
				$values4copies_of_row1 = array();
				for ( $v = 0; $v < count($all_values); $v++ ) {
					if ( count($tmp) == 0) {
						$tmp2 = array();
						array_push( $values4copies_of_row1, $tmp2);
						$lastindex = count($values4copies_of_row1) - 1;
						$values4copies_of_row1[$lastindex][$key] = $all_values[$v];
					} else {
						for ( $r1 = 0; $r1 < count($tmp); $r1++ ) {
							array_push( $values4copies_of_row1, $tmp[$r1]);
							$lastindex = count($values4copies_of_row1) - 1;
							$values4copies_of_row1[$lastindex][$key] = $all_values[$v];
						}
					}
				}
			}
		}
		echo "<!-- variables for exploading the first row: \n"; print_r($values4copies_of_row1); echo "-->\n";
		
		$row1_exploaded = false;
		for ( $r1 = 0; $r1 < count($values4copies_of_row1); $r1++ ) {
			$row1_exploaded = true;
			$newrowid = IUTableModel::add_new_row("true");
			array_push($newrow_db_ids, $newrowid);
			IUTableModel::copy_row_data($row1 + 1, $newrowid);
			foreach ( $values4copies_of_row1[$r1] as $key=>$value ) {
				IUTableModel::add_varval2therow($newrowid, $key, $value, "t_iu_task_states", "");
				echo "<!-- exploading row 1: ".$newrowid." - ".$key." - ".$value."-->\n";
			}
		}
		$row2_exploaded = false;
		for ( $r2 = 0; $r2 < count($values4copies_of_row2); $r2++ ) {
			$row2_exploaded = true;
			$newrowid = IUTableModel::add_new_row("true");
			array_push($newrow_db_ids, $newrowid);
			IUTableModel::copy_row_data($row2 + 1, $newrowid);
			foreach ( $values4copies_of_row2[$r2] as $key=>$value ) {
				IUTableModel::add_varval2therow($newrowid, $key, $value, "t_iu_task_states", "");
				echo "<!-- exploading row 2: ".$newrowid." - ".$key." - ".$value."-->\n";
			}
		}
		
		echo "<!-- rows exploaded, now those which were exploaded will be removed: ".$row1." and/or ".$row2."-->\n";
		// (*) remove expanded rows
		if ($row1 > $row2) {
			if ( $row1_exploaded == true) IUTableModel::delete_selected_row($row1 + 1);
			if ( $row2_exploaded == true) IUTableModel::delete_selected_row($row2 + 1);
		} else {
			if ( $row2_exploaded == true) IUTableModel::delete_selected_row($row2 + 1);
			if ( $row1_exploaded == true) IUTableModel::delete_selected_row($row1 + 1);
		}

		$num_removed  = 0;
		if ($row1_exploaded == true) $num_removed += 1;
		if ($row2_exploaded == true) $num_removed += 1;
		// (*) move new rows up, to the place where original rows were
		for ( $k = 0; $k < count($newrow_db_ids); $k++ ) {
			$current_db_id = $newrow_db_ids[$k] - $num_removed; // minus numer of removed rows because max two rows above were removed
			$num_positions2move_up = $current_db_id - ($row1 + 1);
			for ( $m = 0; $m < $num_positions2move_up; $m++ ) {
				IUTableModel::move_row_up($current_db_id);
				$current_db_id--;
			}
		}
	}
	
	public function explodenext2rows() {
		// (*) check all pairs of rows
		for ( $row1 = 0; $row1 < count($this->_rows); $row1++ ) {
			if ( $this->_rows[$row1]->_row_type == 0 || count($this->_rows[$row1]->_behaviours) == 0 ) {
				continue;
			}
			for ( $row2 = $row1 + 1; $row2 < count($this->_rows); $row2++ ) {
				if ( $this->_rows[$row2]->_row_type == 0 ) {
					continue;
				}
				$diff1 = array_diff($this->_rows[$row1]->_behaviours, $this->_rows[$row2]->_behaviours);
				$diff2 = array_diff($this->_rows[$row2]->_behaviours, $this->_rows[$row1]->_behaviours);
				if ( count($diff1) == 0 && count($diff2) == 0 ) {
					// for expansion do not check those rows which have the same behaviours, those rows should be checked by
					// state subsumption (state subsumption deals with rows which have the same behaviour, and state
					// explosion deals with rows which have different behaviour)
					continue;
				}
				$explosion_required = false;
				$row1_data = $this->_rows[$row1];
				$row2_data = $this->_rows[$row2];
				
				// (*) if (at least one key from the row1 is null in the row2) AND (all remaining non-null keys have equal values) then explosion is required
				foreach ($row1_data->_task_states as $key=>$value) {
					if ( !in_array($key, array_keys($row2_data->_task_states)) && $row1_data->equal_on_nonnullkeys($row2_data) ) {
						$explosion_required = true;
						break;
					}
				}
				if ( $explosion_required == false ) {
					// (*) if (at least one key from the row2 is null in the row1) AND (all remaining non-null keys have equal values) then explosion is required
					foreach ($row2_data->_task_states as $key=>$value) {
						if ( !in_array($key, array_keys($row1_data->_task_states)) && $row1_data->equal_on_nonnullkeys($row2_data) ) {
							$explosion_required = true;
							break;
						}
					}
				}
				echo "<!--";
				if ($explosion_required == false) {
					echo "explosion of these two rows not required: ";
					print_r($row1_data->_task_states);
					print_r($row2_data->_task_states);
				} else {
					echo "explosion of these two rows required: ";
					print_r($row1_data->_task_states);
					print_r($row2_data->_task_states);
				}
				echo "-->\n";
				if ( $explosion_required == true) {
					$this->explodethese2rows($row1, $row2);
					return true;
				}
			}
		}
		return false;
	}	
	
	public static function explode_states() {
		$model = new IUTableModel();
		
		// we cannot do explosion if there are still subsumptions in the table
		if ( $model->check_state_subsumption() == false) {
			IUTableModel::$_last_message = "There are still state subsumption problems in the IU table, you have to fix them before checking disjunctive behaviours!";
			return;
		}
		// check if there are empty rows in the table
		for ( $row1 = 0; $row1 < count($model->_rows); $row1++ ) {
			if ( $model->_rows[$row1]->is_row_empty() ) {
				IUTableModel::$_last_message = "There are rows in the IU table which have empty states and/or empty behaviours, they have to removed or specified before state expansion!";
				return;
			}
		}

		// -------------- BEGIN TRANSACTION ---------------
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
		if ( pg_exec($dblink, "BEGIN") == false ) {
			IUTableModel::$_last_message = "Database transaction BEGIN failed!";
		}
		// ------------------------------------------------
		
		IUTableModel::$_front_message = array();
		
		$changed = true;
		$at_least_one_change = false;
		while ($changed == true) {
			echo "<!-- ######################## START AGAIN ITERATING OVER ALL PAIRS OF ROWS IN ORDER TO SEE IF THERE IS A PAIR WHICH SHOULD BE EXPLOADED ####################### -->\n";
			$changed = $model->explodenext2rows();
			if ( $changed) {
				$at_least_one_change = true;
			}
			// read in the IU table again because now it has more rows
			$model = new IUTableModel();
			// for now I break this loop immediately so the algorithm explodes only one pair of states at a time so the user can see the result
			break;
		}
		
		// (*) set uniform probabilities and the confirmation flag, only when at least one pair of rows was expanded
		if ($at_least_one_change == true) {
			IUTableModel::reset_probs();
		}
			
		if ($at_least_one_change == true) {
			IUTableModel::$_last_message = "Two rows were exploded successfully!";
		} else {
			IUTableModel::$_last_message = "No rows expanded because no new disjunctive behaviours vere found!";
		}
		
		// -------------- COMMIT TRANSACTION ---------------
		if ( pg_exec($dblink, "COMMIT") == false ) {
			IUTableModel::$_last_message = "Database transaction COMMIT failed!";
		}
		// ------------------------------------------------
	}

	public static function reset_probs() {
		$model = new IUTableModel();
		$processed_rows = array();
		for ( $i = 0; $i < $model->getnumrows(); $i++ ) {
			$processed_rows[$i] = false;
		}
		$groups = array();
		for ( $i = 0; $i < $model->getnumrows(); $i++ ) {
			if ( $model->_rows[$i]->_row_type == 0 ) {
				continue; // this is a separator line
			}
			if ($processed_rows[$i] == false ) {
				$processed_rows[$i] = true;
				$newgroup = array();
				array_push($newgroup, $i);
				for ( $j = $i + 1; $j < $model->getnumrows(); $j++ ) {
					if ( $model->_rows[$j]->_row_type == 0 || $processed_rows[$j] == true ) {
						continue; // this is a separator line or already processed line
					}
					if ( $model->_rows[$i]->equal($model->_rows[$j]) ) {
						array_push($newgroup, $j);
						$processed_rows[$j] = true;
					}
				}
				if ( count($newgroup)>1 ) {
					array_push($groups, $newgroup);
					//echo "[".count($newgroup)."]";
					echo "<!-- rows in group: ";
					for ( $k = 0; $k < count($newgroup); $k++ ) {
						echo ($newgroup[$k]+1).",";
						IUTableModel::update_row_confirmation($newgroup[$k] + 1, false);
						IUTableModel::update_row_probability($newgroup[$k] + 1, 1.0/count($newgroup) );
					}
					echo "-->\n";
				} else {
					for ( $k = 0; $k < count($newgroup); $k++ ) {
						//echo "id:".$newgroup[$k].",";
						IUTableModel::update_row_confirmation($newgroup[$k] + 1, true);
						IUTableModel::update_row_probability($newgroup[$k] + 1, 1.0 );
					}
				}
			}
		}
		
		// print information about groups which were found
		IUTableModel::$_last_message = "The following ".count($groups)." groups of rows were identified: ";
		$str="";
		for ( $k=0; $k < count($groups); $k++ ){
			$str = $str."group ".($k+1)." consists of ";
			for ($e=0; $e<count($groups[$k]); $e++) {
				$str = $str.($groups[$k][$e]+1);
				if ( $e < count($groups[$k])-1 ) $str = $str.",";
			}
			if ($k < count($groups)-1) $str = $str."; ";
		}
		IUTableModel::$_last_message = IUTableModel::$_last_message.$str;
	}
}

?>
