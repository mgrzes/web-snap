<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php

include_once("dbparams.php");
include_once("common.php");

class VarVal {
	public $_variable;
	public $_value;
	public function __construct($var, $val) {
		$this->_variable=$var;
		$this->_value=$val;
	}
	public function toString() {
		return $this->_variable."=".$this->_value;
	}
}

class EffectPrecondition {
	// all pairs of variables values that define this precondition
	public $_effectsvarvals;
	public $_precondvarvals;
	public $_id;
	public function __construct() {
		$this->_effectsvarvals = array();
		$this->_precondvarvals = array();
	}
	public function push_effect($var, $val) {
		$tmp = new VarVal($var, $val);
		array_push($this->_effectsvarvals, $tmp);
	}
	public function push_precond($var, $val) {
		$tmp = new VarVal($var, $val);
		array_push($this->_precondvarvals, $tmp);
	}
	public function getNumEff() {
		return count($this->_effectsvarvals);
	}
	public function getEff($id) {
		return $this->_effectsvarvals[$id];
	}
	public function getNumPrec() {
		return count($this->_precondvarvals);
	}
	public function getPrec($id) {
		return $this->_precondvarvals[$id];
	}
}

class BlockedSet {
	// all pairs of variables values that define this blockade
	public $_varvals;
	public $_id;
	public function __construct() {
		$this->_varvals = array();
	}
	public function push_constraint($var, $val) {
		$tmp = new VarVal($var, $val);
		array_push($this->_varvals, $tmp);
	}
	public function getNum() {
		return count($this->_varvals);
	}
	public function get($id) {
		return $this->_varvals[$id];
	}
}

class Behaviour {
	public $_behName;
	public $_effectsPreconditionsList;
	public $_blockedList;

	public function getNumEffPrec() {
		return count($this->_effectsPreconditionsList);
	}
	
	public function getEffPrec($id) {
		return $this->_effectsPreconditionsList[$id];
	}

	public function getNumBlockedSets() {
		return count($this->_blockedList);
	}
	
	public function getBlockedSet($id) {
		return $this->_blockedList[$id];
	}
		
	public function __construct($behName) {
		  $this->_behName = $behName;
		  $this->_effectsPreconditionsList = array();
		  $this->_blockedList = array();
		  
		  	// (*) process all effect/preconditon pairs of this behaviour
			global $dbname;
			global $dbuser;
			global $dbpasswd;
			$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
			$dbresult = pg_exec($dblink, "SELECT beh_effect_id FROM t_effects_of_behaviours WHERE beh_name='".$behName."' ORDER BY 1");
			if ($dbresult) {
				$rows = pg_NumRows($dbresult);
				for($j = 0; $j < $rows; $j++) {
					//echo $id. " = " . pg_result($dbresult, $j, 0). " = " .pg_result($dbresult, $j, 1). "\n";
					$beh_effect_id = pg_result($dbresult, $j,0);
					$effectPrecond = new EffectPrecondition();
					$effectPrecond->_id = $beh_effect_id;
					// add effects of this effect id
					$query = "SELECT var_name, var_value";
					$query = $query." FROM t_effects_of_behaviours WHERE beh_effect_id='".$beh_effect_id."'";
					$dbresult2 = pg_exec($dblink, $query);
					$rows2 = pg_NumRows($dbresult2);
					for($j2 = 0; $j2 < $rows2; $j2++) {
						$effectPrecond->push_effect(pg_result($dbresult2, $j2,0), pg_result($dbresult2, $j2,1));
					}
					// add preconditions of this effect id
					$query = "SELECT var_name, var_value";
					$query = $query." FROM t_preconditions4effects_of_behaviours WHERE beh_effect_id='".$beh_effect_id."'";
					$dbresult2 = pg_exec($dblink, $query);
					$rows2 = pg_NumRows($dbresult2);
					for($j2 = 0; $j2 < $rows2; $j2++) {
						$effectPrecond->push_precond(pg_result($dbresult2, $j2,0), pg_result($dbresult2, $j2,1));
					}
					array_push($this->_effectsPreconditionsList, $effectPrecond);
				}
			} else {
				echo(pg_last_error());
				exit(1);
			}

		  	// (*) load blocked data
			$dbresult = pg_exec($dblink, "SELECT DISTINCT state_set_id FROM t_when_is_behaviour_impossible WHERE beh_name='".$behName."' ORDER BY 1");
			if ($dbresult) {
				$rows = pg_NumRows($dbresult);
				for($j = 0; $j < $rows; $j++) {
					//echo $id. " = " . pg_result($dbresult, $j, 0). " = " .pg_result($dbresult, $j, 1). "\n";
					$state_set_id = pg_result($dbresult, $j,0);
					$blockedSet = new BlockedSet();
					$blockedSet->_id = $state_set_id;
					// add the definition of this set to this blocked set
					$query = "SELECT var_name, var_value";
					$query = $query." FROM t_when_is_behaviour_impossible WHERE state_set_id='".$state_set_id."' AND beh_name='".$behName."'";
					$dbresult2 = pg_exec($dblink, $query);
					$rows2 = pg_NumRows($dbresult2);
					for($j2 = 0; $j2 < $rows2; $j2++) {
						$blockedSet->push_constraint(pg_result($dbresult2, $j2,0), pg_result($dbresult2, $j2,1));
					}
					array_push($this->_blockedList, $blockedSet);
				}
			} else {
				echo(pg_last_error());
				exit(1);
			}
	}
}

class BehDynamicsModel {
	public $_behs;
	
	public function __construct() {
		$this->_behs = array();
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		// (*) read all behaviours from the table
		$dbresult = pg_exec($dblink, "SELECT beh_name FROM t_behaviours ORDER BY 1");
		if ($dbresult) {
			$rows = pg_NumRows($dbresult);
			for($j = 0; $j < $rows; $j++) {
				//echo pg_result($dbresult, $j, 0). " = " .pg_result($dbresult, $j, 1). "\n";
				$beh = new Behaviour(pg_result($dbresult, $j,0));
				//echo $row->_row_id. " - " .$row->_row_type."\n";
				array_push($this->_behs, $beh);
			}
			//exit(1);
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public function getNumBehs() {
		return count($this->_behs);
	}
	
	public function getBeh($id) {
		return $this->_behs[$id];
	}
}

?>
