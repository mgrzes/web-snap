<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php

include_once("dbparams.php");
include_once("common.php");

class SnapGeneratorModel {
	public static $_last_message;
	
	public function __construct() {
	}

	public static function run_tranlator($spud_file_path, $spudd_file_name) {
		if ( isset($_GET['speech_actions']) && $_GET['speech_actions'] == 'yes' ) {
			$speech_config = "../SnapGenerator/speechconfig.txt";
			if ( isset($_FILES["speech_config_file"]) && $_FILES["speech_config_file"]["error"] != UPLOAD_ERR_NO_FILE) {
				// (*) get the file from the form
				if ( ($_FILES["speech_config_file"]["type"] == "text/plain") && ($_FILES["speech_config_file"]["size"] < 200000) && $_FILES["speech_config_file"]["error"] == 0) {
				    #echo "Upload: " . $_FILES["file"]["name"] . "<br />";
				    #echo "Type: " . $_FILES["file"]["type"] . "<br />";
				    #echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
				    #echo "Stored in: " . $_FILES["file"]["tmp_name"];
				    $speech_config = $_FILES["file"]["tmp_name"];
			    } else {
					include_once 'view/viewheader.php';
					include_once 'view/viewtopmenu.php';
					echo 'Invalid speech config file! Error code:'.$_FILES["speech_config_file"]["error"].".";
					include_once 'view/viewfooter.php';
					exit(1);
			    }
			}
  			// (*) return the correct speech.spudd file
  			$ext = pathinfo($spud_file_path, PATHINFO_EXTENSION);
			$newfilepath = dirname($spud_file_path)."/".basename($spud_file_path, $ext)."speech.spudd";
			$filename = basename($spud_file_path, $ext)."speech.spudd";
			$command = "java -cp ../symbolicPerseusJava Translator " .$spud_file_path. " -d " .$speech_config. " 2>&1";
			exec($command, $output, $retval);
			#error_log($command."\n", 3, "/tmp/translator.log");
			#for ( $i = 0; $i < count($output); $i++ ) {
			#	echo error_log($output[$i]."\n", 3, "/tmp/translator.log");
			#}
			if ( $retval != 0) {
				include_once 'view/viewheader.php';
				include_once 'view/viewtopmenu.php';
				echo 'Snap generator \''.$command.'\' failed on '.$dbname.' !';
				echo "<br><br>\n";
				for ( $i = 0; $i < count($output); $i++ ) {
					echo $output[$i]."<br>\n";
				}
				include_once 'view/viewfooter.php';
				exit(1);
			}
			$spud_file_path = $newfilepath;
			$spudd_file_name = $filename;
		}

		if ( isset($_GET['sensor_query_actions']) && $_GET['sensor_query_actions'] == 'yes' ) {
  			// (*) return the correct sense.spudd file
  			$ext = pathinfo($spud_file_path, PATHINFO_EXTENSION);
			$newfilepath = dirname($spud_file_path)."/".basename($spud_file_path, $ext)."sense.spudd";
			$filename = basename($spud_file_path, $ext)."sense.spudd";
			$command = "java -cp ../symbolicPerseusJava Translator " .$spud_file_path. " -q 2>&1";
			exec($command, $output, $retval);
			#error_log($command."\n", 3, "/tmp/translator.log");
			#for ( $i = 0; $i < count($output); $i++ ) {
			#	echo error_log($output[$i]."\n", 3, "/tmp/translator.log");
			#}
			if ( $retval != 0) {
				include_once 'view/viewheader.php';
				include_once 'view/viewtopmenu.php';
				echo 'Snap generator \''.$command.'\' failed on '.$dbname.' !';
				echo "<br><br>\n";
				for ( $i = 0; $i < count($output); $i++ ) {
					echo $output[$i]."<br>\n";
				}
				include_once 'view/viewfooter.php';
				exit(1);
			}
			$spud_file_path = $newfilepath;
			$spudd_file_name = $filename;
		}

		if ( isset($_GET['action_specificities']) && $_GET['action_specificities'] == 'yes' ) {
  			// (*) return the correct spec.spudd file
  			$ext = pathinfo($spud_file_path, PATHINFO_EXTENSION);
			$newfilepath = dirname($spud_file_path)."/".basename($spud_file_path, $ext)."spec.spudd";
			$filename = basename($spud_file_path, $ext)."spec.spudd";
			$command = "java -cp ../symbolicPerseusJava Translator " .$spud_file_path. " -s 2>&1";
			exec($command, $output, $retval);
			#error_log($command."\n", 3, "/tmp/translator.log");
			#for ( $i = 0; $i < count($output); $i++ ) {
			#	echo error_log($output[$i]."\n", 3, "/tmp/translator.log");
			#}
			if ( $retval != 0) {
				include_once 'view/viewheader.php';
				include_once 'view/viewtopmenu.php';
				echo 'Snap generator \''.$command.'\' failed on '.$dbname.' !';
				echo "<br><br>\n";
				for ( $i = 0; $i < count($output); $i++ ) {
					echo $output[$i]."<br>\n";
				}
				include_once 'view/viewfooter.php';
				exit(1);
			}
			$spud_file_path = $newfilepath;
			$spudd_file_name = $filename;
		}

		if ( isset($_GET['add_battery_discharge']) && $_GET['add_battery_discharge'] == 'yes' ) {
  			// (*) return the correct batt.spudd file
  			$ext = pathinfo($spud_file_path, PATHINFO_EXTENSION);
			$newfilepath = dirname($spud_file_path)."/".basename($spud_file_path, $ext)."batt.spudd";
			$filename = basename($spud_file_path, $ext)."batt.spudd";
			$rate="rate_value_not_specified";
			if ( isset($_GET['battery_discharge_rate']) && $_GET['battery_discharge_rate'] != "" ) {
				$rate = $_GET['battery_discharge_rate'];
			}
			$command = "java -cp ../symbolicPerseusJava Translator " .$spud_file_path. " -b " .$rate. " 2>&1";
			exec($command, $output, $retval);
			error_log($command."\n", 3, "/tmp/translator.log");
			#for ( $i = 0; $i < count($output); $i++ ) {
			#	echo error_log($output[$i]."\n", 3, "/tmp/translator.log");
			#}
			if ( $retval != 0) {
				include_once 'view/viewheader.php';
				include_once 'view/viewtopmenu.php';
				echo 'Snap generator \''.$command.'\' failed on '.$dbname.' !';
				echo "<br><br>\n";
				for ( $i = 0; $i < count($output); $i++ ) {
					echo $output[$i]."<br>\n";
				}
				include_once 'view/viewfooter.php';
				exit(1);
			}
			$spud_file_path = $newfilepath;
			$spudd_file_name = $filename;
		}
		
		return array($spud_file_path, $spudd_file_name);
	}
	
	public static function generate_theSPUDD_file() {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		global $dbserver;
		
		if ( isset($_GET['generate_spudd']) && $_GET['generate_spudd'] == 'Options' ) {
			include_once 'view/viewheader.php';
			include_once 'view/viewtopmenu.php';
			exec("java -jar ../SnapGenerator/snap-ubuntu.jar --help 2>&1",$output,$retval);			
			echo "<br>\n";
			for ( $i = 0; $i < count($output); $i++ ) {
				echo $output[$i]."<br>\n";
			}
			include_once 'view/viewfooter.php';
			exit(0);
		}
		
		$str = get_currenttimestamp_as_string();
		$dir = '/tmp/';
		$filename = "snap-".$dbname."-".$str.'.txt';
		$command = "java -jar ../SnapGenerator/snap-ubuntu.jar -o ".$dir.$filename;
		$command = $command." -d ".$dbname." -u ".$dbuser." -s ".$dbserver." -n";
		if ( isset($_GET['hicontrol']) && $_GET['hicontrol'] == 'yes' ) {
			$command = $command." --hicontrol";
		}
		if ( isset($_GET['call_caregiver']) && $_GET['call_caregiver'] == 'yes' ) {
			$command = $command." --call-caregiver";
		}
		//echo $command;
		//exit(1);
		exec($command." -w ".$dbpasswd." 2>&1",$output,$retval);
		if ( intval($retval) != 0 || ( isset($_GET['generate_spudd']) && $_GET['generate_spudd'] == 'Debugging' ) ) {
			include_once 'view/viewheader.php';
			include_once 'view/viewtopmenu.php';
			echo 'Snap generator \''.$command.'\' failed on '.$dbname.' !';
			echo "<br><br>\n";
			for ( $i = 0; $i < count($output); $i++ ) {
				echo $output[$i]."<br>\n";
			}
			include_once 'view/viewfooter.php';
			exit(1);
		} else {
			list($fpath, $fname) = SnapGeneratorModel::run_tranlator($dir.$filename, $filename);
		    header("Content-type: application/force-download");
		    header("Content-Transfer-Encoding: Binary");
		    header("Content-length: ".filesize($fpath));
		    header('Content-disposition: attachment; filename="'.$fname.'"');
		    readfile($dir.$filename);
		    exit(0);
		}
	}
}

?>
