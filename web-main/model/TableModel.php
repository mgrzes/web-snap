<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php

include_once("dbparams.php");
include_once("common.php");

class TableRow{
	public $_data_row;
	public function __construct() {
		$this->_data_row = array();
	}
	public function push_column_value($value) {
		array_push($this->_data_row, $value);
	}
}

class TableModel {
	protected $_rows;
	protected $_column_names;
	/// The list of column names which are in the primary key in this table
	protected $_primary_key_column_names;
	/// Values of columns which are the primary key in the edited row of this table
	public $_edit_row_primary_key;
	public static $_last_message;
	private $_table_name;
	
	public function __construct($tablename) {
		$this->_table_name = $tablename;
		$this->_rows = array();
		// if null than there is not row to edit
		$this->_edit_row_primary_key = "";
		$this->_column_names = array();
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		// (*) read all rows from the table
		$dbresult = pg_exec($dblink, "SELECT * FROM ".$tablename);
		if ($dbresult) {
			$rows = pg_NumRows($dbresult);
			$cols = pg_NumFields($dbresult);
			for($i = 0; $i < $cols; $i++) {
				array_push($this->_column_names, pg_FieldName($dbresult, $i));
			}
			for($j = 0; $j < $rows; $j++) {
				$row = new TableRow();
				for($i = 0; $i < $cols; $i++) {
					$row->push_column_value(pg_result($dbresult, $j, $i));
				}
				array_push($this->_rows, $row);
			}
		} else {
			echo(pg_last_error());
			exit(1);
		}
		// (*) read $_primary_key_column_names from the DB
		// The primary key can be read using the following query:
		// select column_name from information_schema.constraint_column_usage where table_name = 't_env_variables_values' and constraint_name = 't_env_variables_values_pkey';
		$dbresult = pg_exec($dblink, "SELECT column_name FROM information_schema.constraint_column_usage WHERE table_name='".$tablename."' and constraint_name = '".$tablename."_pkey'");
		if ($dbresult) {
			$this->_primary_key_column_names = array();
			$rows = pg_NumRows($dbresult);
			for($j = 0; $j < $rows; $j++) {
				array_push($this->_primary_key_column_names, pg_result($dbresult, $j, 0));
			}
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public function getrowsprimarykey_as_string($rowid) {
		$primary_key = "";
		for ( $c = 0; $c < count($this->_primary_key_column_names); $c++) {
			if ($primary_key != "") {
				$primary_key = $primary_key . ";";
			}
			$primary_key = $primary_key . $this->getcolumnvalue_by_name($rowid, $this->_primary_key_column_names[$c]);	
		}
		return $primary_key;
	}
	
	public function getnumrows() {
		return count($this->_rows);
	}

	public function getnumcolumns() {
		return count($this->_column_names);
	}
	
	public function getrow($id) {
		return $this->_rows[$id];
	}
	
	public function getcolumnname($id) {
		return $this->_column_names[$id];
	}

	public function getcolumnvalue_by_id($rowid, $colid) {
		// (*) get the proper row
		$row = $this->getrow($rowid);
		// (*) get the column from this row
		return $row->_data_row[$colid];
	}
	
	public function getcolumnvalue_by_name($rowid, $columnname) {
		// (*) find the column id
		$n = 0;
		for ( $i = 0; $i < $this->getnumcolumns(); $i++ ) {
			if ( $columnname == $this->getcolumnname($i)) {
				$n = $i;
				break;
			}
		}
		// (*) get the proper row
		$row = $this->getrow($rowid);
		// (*) get the column from this row
		return $row->_data_row[$n];
	}
	
	public function add_new_row() {
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
		
		$sql_query = "INSERT INTO ".$this->_table_name."(";
		for ( $col = 0; $col < $this->getnumcolumns(); $col++ ) {
			if ( $col > 0 ) {
				$sql_query = $sql_query.", ";
			}
			$sql_query = $sql_query.$this->getcolumnname($col);
		}
		$sql_query = $sql_query.") VALUES (";
		for ( $col = 0; $col < $this->getnumcolumns(); $col++ ) {
			if ( $col > 0 ) {
				$sql_query = $sql_query.", ";
			}
			if ($_POST[$this->getcolumnname($col)] != "") {
				$sql_query = $sql_query."'".$_POST[$this->getcolumnname($col)]."'";
			} else {
				$sql_query = $sql_query."null";
			}
		}
		$sql_query = $sql_query.")";
		
		$dbresult = pg_exec($dblink, $sql_query);
		
		if ($dbresult) {
			TableModel::$_last_message = "Operation add new row was successful!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	
	public function delete_selected_row($row2rem) {
		if ($row2rem == "") {
			TableModel::$_last_message = "Row to remove has to be specified. Operation failed!";
			return;
		}
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());

		$sql_query = "DELETE FROM " . $this->_table_name . " WHERE ";
		
		$primary_key = split(";",$row2rem);

		for ( $c = 0; $c < count($this->_primary_key_column_names); $c++) {
			if ($c > 0) {
				$sql_query2 = $sql_query2. " AND ";
			}
			$sql_query2 = $sql_query2 . $this->_primary_key_column_names[$c] . "='" . $primary_key[$c] . "'";	
		}
				
		$dbresult = pg_exec($dblink, $sql_query.$sql_query2);
		if ($dbresult) {
			TableModel::$_last_message = "Row ".$sql_query2." delated successfully from table " . $this->_table_name . "!";
		} else {
			echo(pg_last_error());
			exit(1);
		}
	}
	public function update_selected_row($row2update) {
		if ($row2update == "") {
			TableModel::$_last_message = "Row to update has to be specified. Operation failed!";
			return;
		}
		global $dbname;
		global $dbuser;
		global $dbpasswd;
		$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());

		$sql_query = "UPDATE " . $this->_table_name . " SET ";

		// (*) read new values form the html form
		for ( $col = 0; $col < $this->getnumcolumns(); $col++ ) {
			if ( $col > 0 ) {
				$sql_query = $sql_query.", ";
			}
			$sql_query = $sql_query.$this->getcolumnname($col) . "=";
			if ($_POST[$this->getcolumnname($col)] != "") {
				$sql_query = $sql_query."'".$_POST[$this->getcolumnname($col)]."'";
			} else {
				$sql_query = $sql_query."null";
			}
		}

		// (*) set the WHERE condition using the primary key passed from the form
		$sql_query3 = " WHERE ";
		$primary_key = split(";",$row2update);
		for ( $c = 0; $c < count($this->_primary_key_column_names); $c++) {
			if ($c > 0) {
				$sql_query3 = $sql_query3. " AND ";
			}
			$sql_query3 = $sql_query3 . $this->_primary_key_column_names[$c] . "='" . $primary_key[$c] . "'";	
		}
		
		$query = $sql_query.$sql_query3;
		$dbresult = pg_exec($dblink, $query);
		if ($dbresult) {
			TableModel::$_last_message = "Row ".$sql_query3." updated successfully in table " . $this->_table_name . "!";
		} else {
			echo(pg_last_error().$query);
			exit(1);
		}
	}
	
	public function unset_all_columns_in_POST() {
		for($i = 0; $i < $this->getnumcolumns(); $i++) {
			unset($_POST[$this->getcolumnname($i)]);
		}
	}

	// fkeys can be read in using this question:
	// select * from information_schema.constraint_column_usage where  constraint_name like 't_state_ideal_behaviours%_fkey';
}

?>
