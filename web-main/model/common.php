<?php
	// Author: Marek Grzes, University of Waterloo, 2012.
?>

<?php
include_once("dbparams.php");

/// @param $fk_table_name - table which has foreign key
/// @param $pk_table_name - table which is referenced
/// @return the number of columns which are in this foreign key, i.e., in the key where $fk_table_name defines a foreign key on attributes
/// from table $pk_table_name.
function num_columns_in_fkey($fk_table_name, $pk_table_name) {
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
	$query = "SELECT t.constraint_name, t.table_name, t.constraint_type, c.table_name, c.column_name FROM information_schema.table_constraints t, information_schema.constraint_column_usage c WHERE t.constraint_name = c.constraint_name and t.constraint_type = 'FOREIGN KEY' and t.table_name = '".$fk_table_name."' and c.table_name='".$pk_table_name."'";
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		// (*) number of rows indicates how many columns are there in this foreign key
		return pg_NumRows($dbresult);
	} else {
		echo(pg_last_error());
		exit(1);
	}
}

/// $tablename defines a foreign key which involves $columname and references table $pk_table_name. This function checks if this
/// foreign key has more columns, and then checks if any of this columns where already selected in the $_POST['column_name'], if this is the case
/// then pairs 'columns_name'=$_POST['column_name'] are added to the array and this array will be used to constraint values of $columnname.
/// @return the array of pairs which limit the possible values of $columnname
function check_other_columns_of_this_foreign_key($tablename, $columnname, $pk_table_name) {
	$res = array();
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
	$query = "SELECT t.constraint_name, t.table_name, t.constraint_type, c.table_name, c.column_name FROM information_schema.table_constraints t, information_schema.constraint_column_usage c WHERE t.constraint_name = c.constraint_name AND t.constraint_type = 'FOREIGN KEY' and t.table_name = '".$tablename."' and c.column_name<>'".$columnname."' AND c.table_name='".$pk_table_name."'";
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		for ( $i = 0; $i < $rows; $i++) {
			$pk_table_name2 = pg_result($dbresult, 0, 3);
			$pk_column_name2 = pg_result($dbresult, 0, 4);
			if ( $pk_table_name == $pk_table_name2) {
				// (*) another fkey constraint refers to the same table
				if ( isset($_POST[$pk_column_name2]) && $_POST[$pk_column_name2] != "" ) {
					array_push($res, $pk_column_name2."='".$_POST[$pk_column_name2]."'");
				}
			}
		}
	} else {
		echo(pg_last_error());
		exit(1);
	}
	return $res;
}

/// THIS GENERIC CODE FOR EDITING PG TABLES IS UNDER THE ASSUMPTION THAT ALL FOREIGN KEY
/// COLUMNS HAVE NAMES EXACTLY THE SAME AS THEIR CORRESPONDING PRIMARY KEYS! I AM NOT SURE,
/// HOW TO MATCH THIS FOREING KEY COLUMNS WITH CORRESPONDING PRIMARY KEY COLUMNS WHEN THEIR NAMES ARE DIFFERENT.
//  ---
/// This function prints HTML input for a given table and column. If the column is a foreign key, then compobox is used
/// and possible values are loaded from the corresponding table in which this column is a primary key.
/// @param $default_columnvalue - use this value as a default value when this column is not set in the form
function print_input_text_or_combobox($tablename, $columnname, $default_columnvalue) {
	// This query will return all tables (column c.table_name which are references from table t.table_name).
	// select t.constraint_name, t.table_name, t.constraint_type, c.table_name, c.column_name from information_schema.table_constraints t, information_schema.constraint_column_usage c where t.constraint_name = c.constraint_name and t.constraint_type = 'FOREIGN KEY' and t.table_name = 't_when_is_behaviour_relevant';	
    // This query will return a row if c.column_name is a foreing key in c.table_name and there is a columns of the name
    // c.column_name in table t.table_name as a primary key.
	// select t.constraint_name, t.table_name, t.constraint_type, c.table_name, c.column_name from information_schema.table_constraints t, information_schema.constraint_column_usage c where t.constraint_name = c.constraint_name and t.constraint_type = 'FOREIGN KEY' and t.table_name = 't_when_is_behaviour_relevant' and c.column_name='var_value';	
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
	// (*) check if this columns is a foreign key
	// (**)This query does not work, because if there are two columns in one foreign key, the name of the constraint does 
	// not distinguish columns properly: $query = "SELECT table_name, column_name FROM information_schema.constraint_column_usage WHERE constraint_name='".$tablename."_".$columnname."_fkey'";
	// (**) This query below works on the condition that every primary key column and corresponding foreign key columns have exactly the same name (!!!)
	$query = "SELECT t.constraint_name, t.table_name, t.constraint_type, c.table_name, c.column_name FROM information_schema.table_constraints t, information_schema.constraint_column_usage c WHERE t.constraint_name = c.constraint_name AND t.constraint_type = 'FOREIGN KEY' and t.table_name = '".$tablename."' and c.column_name='".$columnname."';";
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		$value = $default_columnvalue;
		// When addition happened, then do not retrieve the default value from the form.
		if ( !isset($_POST['add_new_row']) ) {
			if ( isset($_POST[$columnname]) ) {
				$value = $_POST[$columnname];
			}
		}
		if ($rows == 0 ) {
			// (*) we may still get here because the above query cannot detect when the foreing key refers to the primary key of the same table, so lets check that
			//     but this works only for references based on one attribute
			$query = "SELECT table_name, column_name FROM information_schema.constraint_column_usage WHERE constraint_name='".$tablename."_".$columnname."_fkey'";
			$dbresult = pg_exec($dblink, $query);
			if ($dbresult) {
				$rows = pg_NumRows($dbresult);
				if ($rows == 0 ) {
					// (*) not a foreign key
					echo '<input type="text" name="'.$columnname.'" value="'.$value.'">';
				} else {
					// (*) foreign key
					$pk_table_name = pg_result($dbresult, 0, 0);
					$pk_column_name = pg_result($dbresult, 0, 1);
					// (*) build a where condition, which will limit possible values of this combo box
					// We cannot do this check below because here we are checking keys based on one attribute only.
					//$where_condition = check_other_columns_of_this_foreign_key($tablename, $columnname, $pk_table_name);
					$where_condition = array();
					$submit = false;
					// (*) if there is only one column in this foreign key then do not use submit on change in this combo box
					if ( num_columns_in_fkey($tablename, $pk_table_name) > 1 ) {
						$submit = true;
					}
					// (*) load possible values of this column to the combo box; data is read in from the table which is fererenced in this foreign key
					load_column_to_checkbox($pk_column_name, $pk_table_name, $columnname, $where_condition, $value, $submit);
				}
			}			
		} else {
			// (*) foreign key
			$pk_table_name = pg_result($dbresult, 0, 3);
			$pk_column_name = pg_result($dbresult, 0, 4);
			// (*) build a where condition, which will limit possible values of this combo box
			$where_condition = check_other_columns_of_this_foreign_key($tablename, $columnname, $pk_table_name);
			$submit = false;
			// (*) if there is only one column in this foreign key then do not use submit on change in this combo box
			if ( num_columns_in_fkey($tablename, $pk_table_name) > 1 ) {
				$submit = true;
			}
			// (*) load possible values of this column to the combo box; data is read in from the table which is fererenced in this foreign key
			load_column_to_checkbox($pk_column_name, $pk_table_name, $columnname, $where_condition, $value, $submit);
		}
	} else {
		echo(pg_last_error());
		exit(1);
	}
}

/// Displays the entire table which is the result of the sql query. Very useful for debugging.
function displaytable($result) {
	if ($result) {
		$rows = pg_NumRows($result);
		$cols = pg_NumFields($result);
		
		echo("<table border=1>\n");
		/* Create the headers */
		echo("<tr>\n");
		for($i = 0; $i < $cols; $i++) {
			printf("<th>%s</th>\n", pg_FieldName($result, $i));
		}
		echo("</tr>");
		for($j = 0; $j < $rows; $j++) {
			echo("<tr>\n");
			for($i = 0; $i < $cols; $i++) {
				printf("<td>%s</td>\n", pg_result($result, $j, $i));
			}
			echo("</tr>");
		}
		echo("</table>");
	} else {
		echo(pg_errormessage);
	}
}

/// Loads one column of a given table to the checkbox.
/// @param $where_condition - if this param is not specified, an empty array() has to be used;
/// @param $selecte_value - if not used, then empty string should be passed ""
/// @param $submit - true or false
function load_column_to_checkbox($col_name, $tablename, $select_name, $where_condition, $selected_value, $submit) {
	$onchange="";
	if ($submit == true) {
		$onchange='onChange="submit();"';
	}
	echo '<select name="'.$select_name.'" '.$onchange.'>'."\n";
	$selected = "selected";
	if ( $selected_value != "" ) {
		$selected = "";
	}
	echo '<option value="" '.$selected.'>(please select:)</option>'."\n";
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
	$query = "SELECT DISTINCT ".$col_name." FROM ".$tablename;
	if ( count($where_condition) > 0 ) {
		$query = $query." WHERE ";
		for ( $i = 0; $i < count($where_condition); $i++ ) {
			if ($i > 0) {
				$query = $query." AND ";
			}
			$query = $query.$where_condition[$i];
		}
	}
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			if ($selected_value == pg_result($dbresult, $j,0)) {
				$selected = "selected";
			} else {
				$selected = "";
			}	
			echo '<option value="'.pg_result($dbresult, $j,0).'" '.$selected.'>'.pg_result($dbresult, $j,0).'</option>'."\n";	
		}
		echo '</select>';
	} else {
		echo(pg_last_error());
		exit(1);
	}
}

/// Loads one column of a given table to the checkbox. This is the same as load_column_to_checkbox4text but here extra java script code
/// added this select so that $dependent_textinput value is set with value selected in this combo box.
/// @param $where_condition - if this param is not specified, an empty array() has to be used;
/// @param $selecte_value - if not used, then empty string should be passed ""
/// @param $submit - true or false
function load_column_to_checkbox4text($col_name, $tablename, $select_name, $where_condition, $selected_value, $dependent_textinput, $form_name) {
	$onchange='onChange="document.'.$form_name.'.'.$dependent_textinput.'.value = document.'.$form_name.'.'.$select_name.'.options[document.'.$form_name.'.'.$select_name.'.selectedIndex].value;document.'.$form_name.'.'.$select_name.'.value=\'\';"';
	echo '<select name="'.$select_name.'" '.$onchange.'>'."\n";
	$selected = "selected";
	if ( $selected_value != "" ) {
		$selected = "";
	}
	echo '<option value="" '.$selected.'>(please select:)</option>'."\n";
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
	$query = "SELECT DISTINCT ".$col_name." FROM ".$tablename;
	if ( count($where_condition) > 0 ) {
		$query = $query." WHERE ";
		for ( $i = 0; $i < count($where_condition); $i++ ) {
			if ($i > 0) {
				$query = $query." AND ";
			}
			$query = $query.$where_condition[$i];
		}
	}
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			if ($selected_value == pg_result($dbresult, $j,0)) {
				$selected = "selected";
			} else {
				$selected = "";
			}	
			echo '<option value="'.pg_result($dbresult, $j,0).'" '.$selected.'>'.pg_result($dbresult, $j,0).'</option>'."\n";	
		}
		echo '</select>';
	} else {
		echo(pg_last_error());
		exit(1);
	}
}

/// Loads all suggested names for abilities to a checkbox.
/// @param $select_value - if not used, then empty string should be passed ""
function load_suggested_abilities_to_checkbox($select_name, $dependent_textinput, $form_name) {
	$onchange='onChange="document.'.$form_name.'.'.$dependent_textinput.'.value = document.'.$form_name.'.'.$select_name.'.options[document.'.$form_name.'.'.$select_name.'.selectedIndex].value;document.'.$form_name.'.'.$select_name.'.value=\'\';"';
	echo '<select name="'.$select_name.'" '.$onchange.'>'."\n";
	$selected = "selected";
	echo '<option value="" '.$selected.'>(please select:)</option>'."\n";
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed.");
	$query = "SELECT var_name, var_value FROM t_env_variables_values ORDER BY 1,2";
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			echo '<option value="Rn_'.pg_result($dbresult, $j,0).'_'.pg_result($dbresult, $j,1).'" '.$selected.'>'.'Rn_'.pg_result($dbresult, $j,0).'_'.pg_result($dbresult, $j,1).'</option>'."\n";	
		}
		for($j = 0; $j < $rows; $j++) {
			echo '<option value="Rl_'.pg_result($dbresult, $j,0).'_'.pg_result($dbresult, $j,1).'" '.$selected.'>'.'Rl_'.pg_result($dbresult, $j,0).'_'.pg_result($dbresult, $j,1).'</option>'."\n";	
		}
		for($j = 0; $j < $rows; $j++) {
			echo '<option value="Af_'.pg_result($dbresult, $j,0).'_'.pg_result($dbresult, $j,1).'" '.$selected.'>'.'Af_'.pg_result($dbresult, $j,0).'_'.pg_result($dbresult, $j,1).'</option>'."\n";	
		}
	} else {
		echo(pg_last_error());
		exit(1);
	}
	$query = "SELECT beh_name FROM t_behaviours ORDER BY 1";
	$dbresult = pg_exec($dblink, $query);
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			echo '<option value="Af_'.pg_result($dbresult, $j,0).'" '.$selected.'>'.'Af_'.pg_result($dbresult, $j,0).'</option>'."\n";	
		}
	} else {
		echo(pg_last_error());
		exit(1);
	}
	echo '</select>';
}

function get_currenttimestamp_as_string() {
	$str = str_replace(" ", "-", date(DATE_RFC822));
	$str = str_replace(",", "-", $str);
	$str = str_replace("--", "-", $str);
	$str = trim($str);
	return $str;
}

/// @return a vector which contains names of all tables 't_%'
function get_all_tables() {
	$res = array();
	global $dbname;
	global $dbuser;
	global $dbpasswd;
	$dblink = pg_connect("dbname=".$dbname." user=".$dbuser." password=".$dbpasswd) or die ("Connection to the database ".$dbname." failed: ".pg_last_error());
	$dbresult = pg_exec($dblink, "SELECT tablename FROM pg_tables WHERE tablename like 't_%' ORDER BY tablename");
	if ($dbresult) {
		$rows = pg_NumRows($dbresult);
		for($j = 0; $j < $rows; $j++) {
			array_push($res, pg_result($dbresult, $j,0));	
		}
	} else {
		echo(pg_last_error());
		exit(1);
	}
	return $res;
}

?> 
