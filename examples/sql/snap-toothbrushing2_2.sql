--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities (
    abil_name character varying(256) NOT NULL,
    abil_type character varying(256),
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    abil_initial_prob real NOT NULL,
    abil_prompt_cost real NOT NULL,
    CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (((abil_initial_prob >= (0)::double precision) AND (abil_initial_prob <= (1)::double precision))),
    CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK ((abil_prompt_cost >= (0)::double precision)),
    CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (((abil_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_abilities OWNER TO x721demouser;

--
-- Name: t_abilities4behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities4behaviours (
    abil_name character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_abilities4behaviours OWNER TO x721demouser;

--
-- Name: t_behaviour_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviour_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_behaviour_sensor_model OWNER TO x721demouser;

--
-- Name: t_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviours (
    beh_name character varying(256) NOT NULL,
    beh_timeout real DEFAULT 0.0,
    CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (((beh_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C5: non-negative value required" CHECK ((beh_timeout >= (0)::double precision))
);


ALTER TABLE public.t_behaviours OWNER TO x721demouser;

--
-- Name: t_default_probabilities4abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_default_probabilities4abilities (
    dementia_type character varying(256) NOT NULL,
    abil_type character varying(256) NOT NULL,
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_default_probabilities4abilities OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256),
    var_value character varying(256),
    beh_name character varying(256)
);


ALTER TABLE public.t_effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_effects_of_behaviours_beh_effect_id_seq OWNED BY t_effects_of_behaviours.beh_effect_id;


--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_env_variables_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_env_variables_values (
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    var_value_initial_prob real NOT NULL,
    CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (((var_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (((var_value)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (((var_value_initial_prob >= (0)::double precision) AND (var_value_initial_prob <= (1)::double precision)))
);


ALTER TABLE public.t_env_variables_values OWNER TO x721demouser;

--
-- Name: t_iu_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_abilities (
    row_id integer NOT NULL,
    abil_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_abilities OWNER TO x721demouser;

--
-- Name: t_iu_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_behaviours (
    row_id integer NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_behaviours OWNER TO x721demouser;

--
-- Name: t_iu_goals; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_goals (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_goals OWNER TO x721demouser;

--
-- Name: t_iu_row; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_row (
    row_id integer NOT NULL,
    data_row boolean DEFAULT true NOT NULL,
    row_probability real DEFAULT 1.0,
    row_confirmed boolean DEFAULT true NOT NULL,
    CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (((row_probability >= (0)::double precision) AND (row_probability <= (1)::double precision)))
);


ALTER TABLE public.t_iu_row OWNER TO x721demouser;

--
-- Name: t_iu_task_states; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_task_states (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_task_states OWNER TO x721demouser;

--
-- Name: t_observations_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_observations_values (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (((obs_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (((obs_value)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_observations_values OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_preconditions4effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_preconditions4effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNED BY t_preconditions4effects_of_behaviours.beh_effect_id;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards (
    state_set_id integer NOT NULL,
    reward_value real NOT NULL,
    CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK ((reward_value >= (0)::double precision))
);


ALTER TABLE public.t_rewards OWNER TO x721demouser;

--
-- Name: t_rewards_desc; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards_desc (
    state_set_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_rewards_desc OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_desc_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_desc_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_desc_state_set_id_seq OWNED BY t_rewards_desc.state_set_id;


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_state_set_id_seq OWNED BY t_rewards.state_set_id;


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_sensor_model OWNER TO x721demouser;

--
-- Name: t_types_of_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_abilities (
    abil_type character varying(256) NOT NULL,
    CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (((abil_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_abilities OWNER TO x721demouser;

--
-- Name: t_types_of_dementia; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_dementia (
    dementia_type character varying(256) NOT NULL,
    CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (((dementia_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_dementia OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_impossible (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_impossible OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_impossible_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq OWNED BY t_when_is_behaviour_impossible.state_set_id;


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Name: t_when_is_behaviour_relevant; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_relevant (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_relevant OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_relevant_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq OWNED BY t_when_is_behaviour_relevant.state_set_id;


--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_relevant_state_set_id_seq', 1, false);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_preconditions4effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_desc_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_impossible_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_relevant_state_set_id_seq'::regclass);


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) FROM stdin;
Af_tubesqueeze	affordance	0.40000001	0.2	0.94999999	0.001	0.80000001	1
Rn_tube_on_suface	recognition	0.5	0.1	0.94999999	0.001	0.94999999	1
Rn_tube_p_in_cup	recognition	0.5	0.1	0.94999999	0.001	0.94999999	1
Rn_brush_on_surface	recognition	0.5	0.1	0.94999999	0.001	0.94999999	1
Rn_brush_in_cup	recognition	0.5	0.1	0.94999999	0.001	0.94999999	1
\.


--
-- Data for Name: t_abilities4behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities4behaviours (abil_name, beh_name) FROM stdin;
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
other	1000000
nothing	1000000
take_tube_from_cup	10
take_tube_from_surface	10
take_brush_from_cup	10
take_brush_from_surface	10
put_TP_on_brush	15
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
1	tube_p	IH	take_tube_from_surface
2	tube_p	IH	take_tube_from_cup
3	brush_p	IH	take_brush_from_surface
4	brush_p	IH	take_brush_from_cup
5	brush_TP	with	put_TP_on_brush
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
tube_p	in_cup	0.2
tube_p	on_surface	0.80000001
tube_p	IH	0
brush_TP	without	1
brush_TP	with	0
brush_p	on_surface	0.30000001
brush_p	in_cup	0.60000002
brush_p	IH	0.1
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
1	Rn_brush_on_surface
2	Rn_tube_p_in_cup
13	Af_tubesqueeze
12	Rn_tube_on_suface
11	Rn_tube_on_suface
10	Rn_tube_on_suface
9	Rn_brush_on_surface
8	Rn_brush_on_surface
7	Rn_tube_p_in_cup
6	Rn_tube_p_in_cup
5	Rn_brush_in_cup
4	Rn_brush_in_cup
3	Rn_brush_in_cup
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
1	take_brush_from_surface
2	take_tube_from_cup
13	put_TP_on_brush
12	take_tube_from_surface
11	take_tube_from_surface
10	take_tube_from_surface
9	take_brush_from_surface
8	take_brush_from_surface
7	take_tube_from_cup
6	take_tube_from_cup
5	take_brush_from_cup
4	take_brush_from_cup
3	take_brush_from_cup
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
1	brush_TP	with
2	brush_TP	with
13	brush_TP	with
12	brush_TP	with
11	brush_TP	with
10	brush_TP	with
9	brush_TP	with
8	brush_TP	with
7	brush_TP	with
6	brush_TP	with
5	brush_TP	with
4	brush_TP	with
3	brush_TP	with
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
1	t	0.5	f
2	t	0.5	f
3	t	0.5	f
4	t	0.5	f
6	t	0.5	f
5	t	1	t
7	t	1	t
10	t	0.5	f
8	t	0.5	f
11	t	0.5	f
9	t	1	t
12	t	1	t
13	t	1	t
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
1	brush_p	on_surface
1	tube_p	on_surface
13	brush_p	IH
13	tube_p	IH
12	tube_p	on_surface
12	brush_p	IH
11	tube_p	on_surface
11	brush_p	in_cup
10	tube_p	on_surface
10	brush_p	on_surface
9	brush_p	on_surface
9	tube_p	IH
2	tube_p	in_cup
2	brush_p	on_surface
8	brush_p	on_surface
8	tube_p	in_cup
7	tube_p	in_cup
7	brush_p	IH
6	tube_p	in_cup
6	brush_p	in_cup
5	brush_p	in_cup
5	tube_p	IH
4	brush_p	in_cup
4	tube_p	in_cup
3	brush_p	in_cup
3	tube_p	on_surface
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
brush_position	on_surface
brush_position	in_cup
brush_position	IH
tube_position	on_surface
tube_position	in_cup
tube_position	IH
brush_TP_condition	without
brush_TP_condition	with
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
1	tube_p	on_surface
2	tube_p	in_cup
3	brush_p	on_surface
4	brush_p	in_cup
5	brush_p	IH
5	tube_p	IH
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
1	15
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	brush_TP	with
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
brush_position	on_surface	brush_p	on_surface	0.89999998
brush_position	on_surface	brush_p	in_cup	0.050000001
brush_position	on_surface	brush_p	IH	0.050000001
brush_position	in_cup	brush_p	in_cup	0.89999998
brush_position	in_cup	brush_p	on_surface	0.050000001
brush_position	in_cup	brush_p	IH	0.050000001
brush_position	IH	brush_p	IH	0.89999998
brush_position	IH	brush_p	on_surface	0.050000001
brush_position	IH	brush_p	in_cup	0.050000001
tube_position	on_surface	tube_p	on_surface	0.89999998
tube_position	on_surface	tube_p	in_cup	0.050000001
tube_position	on_surface	tube_p	IH	0.050000001
tube_position	in_cup	tube_p	in_cup	0.89999998
tube_position	in_cup	tube_p	on_surface	0.050000001
tube_position	in_cup	tube_p	IH	0.050000001
tube_position	IH	tube_p	IH	0.89999998
tube_position	IH	tube_p	on_surface	0.050000001
tube_position	IH	tube_p	in_cup	0.050000001
brush_TP_condition	without	brush_TP	with	0.050000001
brush_TP_condition	without	brush_TP	without	0.94999999
brush_TP_condition	with	brush_TP	with	0.94999999
brush_TP_condition	with	brush_TP	without	0.050000001
\.


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Data for Name: t_when_is_behaviour_relevant; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_relevant (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Name: t_abilities4behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_pkey PRIMARY KEY (abil_name, beh_name);


--
-- Name: t_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_pkey PRIMARY KEY (abil_name);


--
-- Name: t_behaviour_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, beh_name);


--
-- Name: t_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviours
    ADD CONSTRAINT t_behaviours_pkey PRIMARY KEY (beh_name);


--
-- Name: t_default_probabilities4abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_pkey PRIMARY KEY (dementia_type, abil_type);


--
-- Name: t_effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id);


--
-- Name: t_env_variables_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_env_variables_values
    ADD CONSTRAINT t_env_variables_values_pkey PRIMARY KEY (var_name, var_value);


--
-- Name: t_iu_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_pkey PRIMARY KEY (row_id, abil_name);


--
-- Name: t_iu_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_pkey PRIMARY KEY (row_id, beh_name);


--
-- Name: t_iu_goals_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_iu_row_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_row
    ADD CONSTRAINT t_iu_row_pkey PRIMARY KEY (row_id);


--
-- Name: t_iu_task_states_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_observations_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_observations_values
    ADD CONSTRAINT t_observations_values_pkey PRIMARY KEY (obs_name, obs_value);


--
-- Name: t_preconditions4effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id, var_name);


--
-- Name: t_rewards_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_pkey PRIMARY KEY (state_set_id, var_name);


--
-- Name: t_rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards
    ADD CONSTRAINT t_rewards_pkey PRIMARY KEY (state_set_id);


--
-- Name: t_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, var_name, var_value);


--
-- Name: t_types_of_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_abilities
    ADD CONSTRAINT t_types_of_abilities_pkey PRIMARY KEY (abil_type);


--
-- Name: t_types_of_dementia_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_dementia
    ADD CONSTRAINT t_types_of_dementia_pkey PRIMARY KEY (dementia_type);


--
-- Name: t_when_is_behaviour_impossible_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_when_is_behaviour_relevant_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_abilities4behaviours_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_abilities4behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_dementia_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_dementia_type_fkey FOREIGN KEY (dementia_type) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_beh_effect_id_fkey FOREIGN KEY (beh_effect_id) REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_state_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_state_set_id_fkey FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

