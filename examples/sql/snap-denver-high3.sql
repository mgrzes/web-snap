--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities (
    abil_name character varying(256) NOT NULL,
    abil_type character varying(256),
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    abil_initial_prob real NOT NULL,
    abil_prompt_cost real NOT NULL,
    CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (((abil_initial_prob >= (0)::double precision) AND (abil_initial_prob <= (1)::double precision))),
    CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK ((abil_prompt_cost >= (0)::double precision)),
    CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (((abil_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_abilities OWNER TO x721demouser;

--
-- Name: t_behaviour_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviour_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_behaviour_sensor_model OWNER TO x721demouser;

--
-- Name: t_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviours (
    beh_name character varying(256) NOT NULL,
    beh_timeout real DEFAULT 0.0,
    CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (((beh_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C5: non-negative value required" CHECK ((beh_timeout >= (0)::double precision))
);


ALTER TABLE public.t_behaviours OWNER TO x721demouser;

--
-- Name: t_default_probabilities4abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_default_probabilities4abilities (
    dementia_type character varying(256) NOT NULL,
    abil_type character varying(256) NOT NULL,
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_default_probabilities4abilities OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256),
    var_value character varying(256),
    beh_name character varying(256)
);


ALTER TABLE public.t_effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_effects_of_behaviours_beh_effect_id_seq OWNED BY t_effects_of_behaviours.beh_effect_id;


--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_env_variables_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_env_variables_values (
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    var_value_initial_prob real NOT NULL,
    CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (((var_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (((var_value)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (((var_value_initial_prob >= (0)::double precision) AND (var_value_initial_prob <= (1)::double precision)))
);


ALTER TABLE public.t_env_variables_values OWNER TO x721demouser;

--
-- Name: t_iu_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_abilities (
    row_id integer NOT NULL,
    abil_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_abilities OWNER TO x721demouser;

--
-- Name: t_iu_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_behaviours (
    row_id integer NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_behaviours OWNER TO x721demouser;

--
-- Name: t_iu_goals; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_goals (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_goals OWNER TO x721demouser;

--
-- Name: t_iu_row; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_row (
    row_id integer NOT NULL,
    data_row boolean DEFAULT true NOT NULL,
    row_probability real DEFAULT 1.0,
    row_confirmed boolean DEFAULT true NOT NULL,
    CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (((row_probability >= (0)::double precision) AND (row_probability <= (1)::double precision)))
);


ALTER TABLE public.t_iu_row OWNER TO x721demouser;

--
-- Name: t_iu_task_states; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_task_states (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_task_states OWNER TO x721demouser;

--
-- Name: t_observations_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_observations_values (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (((obs_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (((obs_value)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_observations_values OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_preconditions4effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_preconditions4effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNED BY t_preconditions4effects_of_behaviours.beh_effect_id;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards (
    state_set_id integer NOT NULL,
    reward_value real NOT NULL,
    CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK ((reward_value >= (0)::double precision))
);


ALTER TABLE public.t_rewards OWNER TO x721demouser;

--
-- Name: t_rewards_desc; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards_desc (
    state_set_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_rewards_desc OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_desc_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_desc_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_desc_state_set_id_seq OWNED BY t_rewards_desc.state_set_id;


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_state_set_id_seq OWNED BY t_rewards.state_set_id;


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_sensor_model OWNER TO x721demouser;

--
-- Name: t_types_of_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_abilities (
    abil_type character varying(256) NOT NULL,
    CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (((abil_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_abilities OWNER TO x721demouser;

--
-- Name: t_types_of_dementia; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_dementia (
    dementia_type character varying(256) NOT NULL,
    CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (((dementia_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_dementia OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_impossible (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_impossible OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_impossible_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq OWNED BY t_when_is_behaviour_impossible.state_set_id;


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_preconditions4effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_desc_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_impossible_state_set_id_seq'::regclass);


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) FROM stdin;
rl_step_t1	recall_step	0.0099999998	0.1	0.89999998	0.0099999998	0.2	1
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
other	1000000
nothing	1000000
t1	10
t2	10
t3	10
t4	10
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
1	goal_t1_complete	yes	t1
2	goal_t2_complete	yes	t2
3	goal_t3_complete	yes	t3
4	goal_t4_complete	yes	t4
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
goal_t1_complete	no	1
goal_t1_complete	yes	0
goal_t1_complete	done	0
goal_t2_complete	no	1
goal_t2_complete	yes	0
goal_t2_complete	done	0
goal_t3_complete	no	1
goal_t3_complete	yes	0
goal_t3_complete	done	0
goal_t4_complete	no	1
goal_t4_complete	yes	0
goal_t4_complete	done	0
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
90	rl_step_t1
89	rl_step_t1
88	rl_step_t1
94	rl_step_t1
93	rl_step_t1
92	rl_step_t1
102	rl_step_t1
101	rl_step_t1
100	rl_step_t1
99	rl_step_t1
98	rl_step_t1
97	rl_step_t1
68	rl_step_t1
67	rl_step_t1
66	rl_step_t1
65	rl_step_t1
64	rl_step_t1
63	rl_step_t1
62	rl_step_t1
61	rl_step_t1
60	rl_step_t1
19	rl_step_t1
18	rl_step_t1
30	rl_step_t1
29	rl_step_t1
28	rl_step_t1
27	rl_step_t1
26	rl_step_t1
25	rl_step_t1
24	rl_step_t1
23	rl_step_t1
34	rl_step_t1
47	rl_step_t1
22	rl_step_t1
21	rl_step_t1
20	rl_step_t1
108	rl_step_t1
107	rl_step_t1
106	rl_step_t1
105	rl_step_t1
33	rl_step_t1
32	rl_step_t1
31	rl_step_t1
49	rl_step_t1
48	rl_step_t1
91	rl_step_t1
96	rl_step_t1
95	rl_step_t1
104	rl_step_t1
103	rl_step_t1
75	rl_step_t1
74	rl_step_t1
73	rl_step_t1
72	rl_step_t1
71	rl_step_t1
70	rl_step_t1
69	rl_step_t1
59	rl_step_t1
58	rl_step_t1
57	rl_step_t1
56	rl_step_t1
55	rl_step_t1
54	rl_step_t1
53	rl_step_t1
52	rl_step_t1
51	rl_step_t1
50	rl_step_t1
82	rl_step_t1
81	rl_step_t1
80	rl_step_t1
79	rl_step_t1
87	rl_step_t1
86	rl_step_t1
78	rl_step_t1
77	rl_step_t1
76	rl_step_t1
85	rl_step_t1
84	rl_step_t1
83	rl_step_t1
46	rl_step_t1
45	rl_step_t1
44	rl_step_t1
36	rl_step_t1
35	rl_step_t1
43	rl_step_t1
42	rl_step_t1
41	rl_step_t1
40	rl_step_t1
39	rl_step_t1
38	rl_step_t1
37	rl_step_t1
2	rl_step_t1
1	rl_step_t1
4	rl_step_t1
3	rl_step_t1
17	rl_step_t1
16	rl_step_t1
15	rl_step_t1
14	rl_step_t1
13	rl_step_t1
12	rl_step_t1
11	rl_step_t1
10	rl_step_t1
9	rl_step_t1
8	rl_step_t1
7	rl_step_t1
6	rl_step_t1
5	rl_step_t1
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
90	t3
89	t3
88	t3
94	t3
93	t3
92	t3
102	t3
101	t3
100	t3
99	t2
98	t2
97	t2
19	t1
18	t1
30	t4
29	t1
28	t1
27	t1
26	t1
25	t1
24	t1
23	t1
34	t4
22	t3
21	t3
20	t3
49	t1
48	t1
68	t2
67	t2
66	t2
65	t2
64	t2
63	t2
62	t2
61	t2
60	t2
59	t1
58	t1
57	t1
56	t1
75	t4
74	t4
73	t4
72	t4
71	t4
70	t4
69	t4
33	t2
32	t2
31	t2
55	t1
54	t1
53	t1
52	t3
51	t3
50	t3
79	t4
91	t4
96	t4
95	t4
108	t3
107	t3
106	t3
105	t4
104	t4
103	t4
82	t4
81	t4
80	t4
78	t2
77	t2
76	t2
87	t4
86	t4
85	t3
84	t3
83	t3
36	t2
35	t2
47	t4
46	t2
45	t2
44	t2
43	t2
42	t2
41	t2
40	t2
39	t3
38	t3
37	t3
2	t4
1	t4
4	t1
3	t1
17	t4
16	t4
15	t4
14	t1
13	t1
12	t1
11	t1
10	t1
9	t1
8	t1
7	t3
6	t3
5	t3
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
101	t	0.5	f
102	t	0.33333334	f
104	t	0.33333334	f
103	t	0.33333334	f
105	t	0.25	f
106	t	1	t
108	t	0.5	f
107	t	1	t
1	t	1	t
2	t	1	t
3	t	1	t
4	t	1	t
5	t	0.5	f
8	t	0.5	f
6	t	0.5	f
72	t	0.5	f
16	t	1	t
17	t	1	t
18	t	1	t
19	t	1	t
20	t	0.5	f
23	t	0.5	f
21	t	0.5	f
26	t	0.5	f
22	t	0.33333334	f
29	t	0.33333334	f
24	t	1	t
25	t	1	t
27	t	0.5	f
30	t	0.5	f
28	t	0.5	f
75	t	0.5	f
31	t	1	t
32	t	1	t
33	t	0.5	f
34	t	0.5	f
35	t	1	t
36	t	1	t
37	t	0.5	f
40	t	0.5	f
38	t	0.5	f
43	t	0.5	f
39	t	0.33333334	f
46	t	0.33333334	f
41	t	1	t
42	t	1	t
44	t	0.5	f
47	t	0.5	f
45	t	0.5	f
48	t	0.5	f
81	t	0.33333334	f
11	t	0.5	f
77	t	1	t
7	t	0.33333334	f
14	t	0.33333334	f
84	t	1	t
9	t	1	t
86	t	0.5	f
10	t	1	t
12	t	0.5	f
15	t	0.5	f
13	t	0.5	f
51	t	0.33333334	f
56	t	0.33333334	f
65	t	0.33333334	f
52	t	0.25	f
59	t	0.25	f
68	t	0.25	f
54	t	0.5	f
63	t	0.5	f
55	t	0.5	f
89	t	1	t
90	t	0.5	f
87	t	0.33333334	f
80	t	0.5	f
60	t	0.5	f
49	t	0.5	f
61	t	0.5	f
50	t	0.33333334	f
53	t	0.33333334	f
62	t	0.33333334	f
85	t	0.5	f
88	t	1	t
91	t	0.5	f
92	t	1	t
93	t	1	t
94	t	0.5	f
95	t	0.5	f
64	t	0.5	f
57	t	0.33333334	f
66	t	0.33333334	f
69	t	0.33333334	f
58	t	0.33333334	f
67	t	0.33333334	f
70	t	1	t
71	t	1	t
73	t	1	t
74	t	1	t
76	t	1	t
78	t	0.5	f
79	t	0.5	f
82	t	0.5	f
83	t	1	t
96	t	0.33333334	f
97	t	0.5	f
98	t	0.5	f
100	t	0.5	f
99	t	0.33333334	f
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
90	goal_t1_complete	done
90	goal_t2_complete	yes
90	goal_t3_complete	no
90	goal_t4_complete	no
89	goal_t1_complete	done
89	goal_t2_complete	yes
89	goal_t3_complete	no
89	goal_t4_complete	yes
88	goal_t1_complete	done
88	goal_t2_complete	yes
88	goal_t3_complete	no
88	goal_t4_complete	done
94	goal_t1_complete	yes
94	goal_t2_complete	yes
94	goal_t3_complete	no
94	goal_t4_complete	no
93	goal_t1_complete	yes
93	goal_t2_complete	yes
93	goal_t3_complete	no
93	goal_t4_complete	yes
92	goal_t1_complete	yes
92	goal_t2_complete	yes
92	goal_t3_complete	no
92	goal_t4_complete	done
102	goal_t1_complete	done
102	goal_t2_complete	no
102	goal_t3_complete	no
102	goal_t4_complete	no
101	goal_t1_complete	done
101	goal_t2_complete	no
101	goal_t3_complete	no
101	goal_t4_complete	yes
100	goal_t1_complete	done
100	goal_t2_complete	no
100	goal_t3_complete	no
100	goal_t4_complete	done
99	goal_t1_complete	done
99	goal_t2_complete	no
99	goal_t3_complete	no
99	goal_t4_complete	no
98	goal_t1_complete	done
98	goal_t2_complete	no
98	goal_t3_complete	no
98	goal_t4_complete	yes
97	goal_t1_complete	done
97	goal_t2_complete	no
97	goal_t3_complete	no
97	goal_t4_complete	done
19	goal_t1_complete	no
19	goal_t2_complete	yes
19	goal_t3_complete	yes
19	goal_t4_complete	done
18	goal_t1_complete	no
18	goal_t2_complete	yes
18	goal_t3_complete	done
18	goal_t4_complete	done
29	goal_t1_complete	no
29	goal_t2_complete	yes
29	goal_t3_complete	no
29	goal_t4_complete	no
28	goal_t1_complete	no
28	goal_t2_complete	yes
28	goal_t3_complete	yes
28	goal_t4_complete	no
27	goal_t1_complete	no
27	goal_t2_complete	yes
27	goal_t3_complete	done
27	goal_t4_complete	no
26	goal_t1_complete	no
26	goal_t2_complete	yes
26	goal_t3_complete	no
26	goal_t4_complete	yes
25	goal_t1_complete	no
25	goal_t2_complete	yes
25	goal_t3_complete	yes
25	goal_t4_complete	yes
24	goal_t1_complete	no
24	goal_t2_complete	yes
24	goal_t3_complete	done
24	goal_t4_complete	yes
23	goal_t1_complete	no
23	goal_t2_complete	yes
23	goal_t3_complete	no
23	goal_t4_complete	done
34	goal_t1_complete	done
34	goal_t2_complete	no
34	goal_t3_complete	done
22	goal_t1_complete	no
22	goal_t2_complete	yes
22	goal_t3_complete	no
22	goal_t4_complete	no
21	goal_t1_complete	no
21	goal_t2_complete	yes
21	goal_t3_complete	no
21	goal_t4_complete	yes
20	goal_t1_complete	no
20	goal_t2_complete	yes
20	goal_t3_complete	no
20	goal_t4_complete	done
33	goal_t1_complete	done
33	goal_t2_complete	no
33	goal_t3_complete	done
33	goal_t4_complete	no
32	goal_t1_complete	done
32	goal_t2_complete	no
32	goal_t3_complete	done
32	goal_t4_complete	yes
31	goal_t1_complete	done
31	goal_t2_complete	no
31	goal_t3_complete	done
31	goal_t4_complete	done
108	goal_t1_complete	done
108	goal_t2_complete	done
108	goal_t3_complete	no
108	goal_t4_complete	no
107	goal_t1_complete	done
107	goal_t2_complete	done
107	goal_t3_complete	no
107	goal_t4_complete	yes
106	goal_t1_complete	done
49	goal_t1_complete	no
49	goal_t2_complete	no
49	goal_t3_complete	yes
49	goal_t4_complete	done
48	goal_t1_complete	no
48	goal_t2_complete	no
48	goal_t3_complete	done
48	goal_t4_complete	done
96	goal_t1_complete	no
105	goal_t4_complete	no
104	goal_t4_complete	no
104	goal_t1_complete	yes
104	goal_t2_complete	no
104	goal_t3_complete	no
103	goal_t1_complete	done
103	goal_t2_complete	no
103	goal_t3_complete	no
103	goal_t4_complete	no
82	goal_t2_complete	done
82	goal_t4_complete	no
75	goal_t4_complete	no
75	goal_t1_complete	no
75	goal_t2_complete	yes
75	goal_t3_complete	yes
74	goal_t1_complete	yes
74	goal_t2_complete	yes
74	goal_t3_complete	yes
74	goal_t4_complete	no
73	goal_t1_complete	done
73	goal_t2_complete	yes
73	goal_t3_complete	yes
73	goal_t4_complete	no
72	goal_t1_complete	no
72	goal_t2_complete	done
72	goal_t3_complete	yes
72	goal_t4_complete	no
71	goal_t1_complete	yes
71	goal_t2_complete	done
71	goal_t3_complete	yes
71	goal_t4_complete	no
70	goal_t1_complete	done
70	goal_t2_complete	done
70	goal_t3_complete	yes
70	goal_t4_complete	no
69	goal_t1_complete	no
69	goal_t2_complete	no
69	goal_t3_complete	done
69	goal_t4_complete	no
68	goal_t1_complete	no
68	goal_t2_complete	no
68	goal_t3_complete	no
68	goal_t4_complete	no
67	goal_t1_complete	no
67	goal_t2_complete	no
67	goal_t3_complete	yes
67	goal_t4_complete	no
81	goal_t1_complete	no
81	goal_t2_complete	no
81	goal_t3_complete	yes
81	goal_t4_complete	no
80	goal_t1_complete	yes
80	goal_t2_complete	no
80	goal_t3_complete	yes
80	goal_t4_complete	no
79	goal_t1_complete	done
79	goal_t2_complete	no
79	goal_t3_complete	yes
79	goal_t4_complete	no
66	goal_t1_complete	no
66	goal_t2_complete	no
66	goal_t3_complete	done
66	goal_t4_complete	no
65	goal_t1_complete	no
65	goal_t2_complete	no
65	goal_t3_complete	no
65	goal_t4_complete	yes
64	goal_t1_complete	no
64	goal_t2_complete	no
64	goal_t3_complete	yes
64	goal_t4_complete	yes
63	goal_t1_complete	no
63	goal_t2_complete	no
63	goal_t3_complete	done
63	goal_t4_complete	yes
62	goal_t1_complete	no
62	goal_t2_complete	no
62	goal_t3_complete	no
62	goal_t4_complete	done
61	goal_t1_complete	no
61	goal_t2_complete	no
61	goal_t3_complete	yes
61	goal_t4_complete	done
60	goal_t1_complete	no
60	goal_t2_complete	no
60	goal_t3_complete	done
60	goal_t4_complete	done
59	goal_t1_complete	no
59	goal_t2_complete	no
59	goal_t3_complete	no
59	goal_t4_complete	no
58	goal_t1_complete	no
58	goal_t2_complete	no
58	goal_t3_complete	yes
58	goal_t4_complete	no
57	goal_t1_complete	no
57	goal_t2_complete	no
57	goal_t3_complete	done
57	goal_t4_complete	no
56	goal_t1_complete	no
56	goal_t2_complete	no
56	goal_t3_complete	no
56	goal_t4_complete	yes
55	goal_t1_complete	no
55	goal_t2_complete	no
55	goal_t3_complete	yes
55	goal_t4_complete	yes
54	goal_t1_complete	no
54	goal_t2_complete	no
54	goal_t3_complete	done
54	goal_t4_complete	yes
53	goal_t1_complete	no
53	goal_t2_complete	no
53	goal_t3_complete	no
53	goal_t4_complete	done
52	goal_t1_complete	no
52	goal_t2_complete	no
52	goal_t3_complete	no
52	goal_t4_complete	no
51	goal_t1_complete	no
51	goal_t2_complete	no
51	goal_t3_complete	no
51	goal_t4_complete	yes
50	goal_t1_complete	no
50	goal_t2_complete	no
50	goal_t3_complete	no
50	goal_t4_complete	done
78	goal_t1_complete	done
78	goal_t2_complete	no
78	goal_t3_complete	yes
78	goal_t4_complete	no
77	goal_t2_complete	no
77	goal_t1_complete	done
77	goal_t3_complete	yes
77	goal_t4_complete	yes
76	goal_t1_complete	done
76	goal_t2_complete	no
76	goal_t3_complete	yes
76	goal_t4_complete	done
106	goal_t2_complete	done
106	goal_t3_complete	no
106	goal_t4_complete	done
91	goal_t2_complete	yes
91	goal_t3_complete	no
91	goal_t4_complete	no
91	goal_t1_complete	done
96	goal_t2_complete	yes
96	goal_t3_complete	no
96	goal_t4_complete	no
95	goal_t1_complete	yes
95	goal_t2_complete	yes
95	goal_t3_complete	no
95	goal_t4_complete	no
105	goal_t1_complete	no
105	goal_t2_complete	no
105	goal_t3_complete	no
87	goal_t1_complete	no
87	goal_t2_complete	done
87	goal_t3_complete	no
87	goal_t4_complete	no
85	goal_t1_complete	yes
85	goal_t2_complete	done
85	goal_t3_complete	no
85	goal_t4_complete	no
84	goal_t1_complete	yes
84	goal_t2_complete	done
84	goal_t3_complete	no
84	goal_t4_complete	yes
83	goal_t1_complete	yes
83	goal_t2_complete	done
83	goal_t3_complete	no
83	goal_t4_complete	done
82	goal_t1_complete	done
82	goal_t3_complete	no
86	goal_t1_complete	yes
86	goal_t2_complete	done
86	goal_t3_complete	no
86	goal_t4_complete	no
36	goal_t1_complete	yes
36	goal_t2_complete	no
36	goal_t3_complete	yes
36	goal_t4_complete	done
35	goal_t1_complete	yes
35	goal_t2_complete	no
35	goal_t3_complete	done
35	goal_t4_complete	done
47	goal_t1_complete	yes
47	goal_t2_complete	no
47	goal_t3_complete	done
47	goal_t4_complete	no
46	goal_t1_complete	yes
46	goal_t2_complete	no
46	goal_t3_complete	no
46	goal_t4_complete	no
45	goal_t1_complete	yes
45	goal_t2_complete	no
45	goal_t3_complete	yes
45	goal_t4_complete	no
44	goal_t1_complete	yes
44	goal_t2_complete	no
44	goal_t3_complete	done
44	goal_t4_complete	no
43	goal_t1_complete	yes
43	goal_t2_complete	no
43	goal_t3_complete	no
43	goal_t4_complete	yes
42	goal_t1_complete	yes
42	goal_t2_complete	no
42	goal_t3_complete	yes
42	goal_t4_complete	yes
41	goal_t1_complete	yes
41	goal_t2_complete	no
41	goal_t3_complete	done
41	goal_t4_complete	yes
40	goal_t1_complete	yes
40	goal_t2_complete	no
40	goal_t3_complete	no
40	goal_t4_complete	done
39	goal_t1_complete	yes
39	goal_t2_complete	no
39	goal_t3_complete	no
39	goal_t4_complete	no
38	goal_t1_complete	yes
38	goal_t2_complete	no
38	goal_t3_complete	no
38	goal_t4_complete	yes
37	goal_t1_complete	yes
37	goal_t2_complete	no
37	goal_t3_complete	no
37	goal_t4_complete	done
2	goal_t4_complete	no
2	goal_t1_complete	yes
2	goal_t2_complete	done
2	goal_t3_complete	done
1	goal_t1_complete	done
1	goal_t2_complete	done
1	goal_t3_complete	done
1	goal_t4_complete	no
4	goal_t1_complete	no
4	goal_t2_complete	done
4	goal_t3_complete	yes
4	goal_t4_complete	done
3	goal_t1_complete	no
3	goal_t2_complete	done
3	goal_t3_complete	done
3	goal_t4_complete	done
17	goal_t1_complete	yes
17	goal_t2_complete	yes
17	goal_t3_complete	done
17	goal_t4_complete	no
16	goal_t1_complete	done
16	goal_t4_complete	no
16	goal_t2_complete	yes
16	goal_t3_complete	done
15	goal_t1_complete	no
15	goal_t2_complete	done
15	goal_t3_complete	done
15	goal_t4_complete	no
14	goal_t1_complete	no
14	goal_t2_complete	done
14	goal_t3_complete	no
14	goal_t4_complete	no
13	goal_t1_complete	no
13	goal_t2_complete	done
13	goal_t3_complete	yes
13	goal_t4_complete	no
12	goal_t1_complete	no
12	goal_t2_complete	done
12	goal_t3_complete	done
12	goal_t4_complete	no
30	goal_t1_complete	no
30	goal_t2_complete	yes
30	goal_t3_complete	done
30	goal_t4_complete	no
34	goal_t4_complete	no
11	goal_t1_complete	no
11	goal_t2_complete	done
11	goal_t3_complete	no
11	goal_t4_complete	yes
10	goal_t1_complete	no
10	goal_t2_complete	done
10	goal_t3_complete	yes
10	goal_t4_complete	yes
9	goal_t1_complete	no
9	goal_t2_complete	done
9	goal_t3_complete	done
9	goal_t4_complete	yes
8	goal_t1_complete	no
8	goal_t2_complete	done
8	goal_t3_complete	no
8	goal_t4_complete	done
7	goal_t1_complete	no
7	goal_t2_complete	done
7	goal_t3_complete	no
7	goal_t4_complete	no
6	goal_t1_complete	no
6	goal_t2_complete	done
6	goal_t3_complete	no
6	goal_t4_complete	yes
5	goal_t1_complete	no
5	goal_t2_complete	done
5	goal_t3_complete	no
5	goal_t4_complete	done
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
t1_complete	no
t1_complete	yes
t2_complete	no
t2_complete	yes
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
1	goal_t1_complete	no
2	goal_t2_complete	no
3	goal_t3_complete	no
4	goal_t4_complete	no
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
1	20
2	10
3	10
4	10
5	30
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	goal_t2_complete	yes
1	goal_t1_complete	yes
2	goal_t2_complete	no
2	goal_t1_complete	yes
3	goal_t2_complete	yes
3	goal_t1_complete	no
5	goal_t1_complete	yes
5	goal_t2_complete	yes
5	goal_t3_complete	yes
4	goal_t3_complete	yes
4	goal_t1_complete	no
3	goal_t3_complete	no
4	goal_t2_complete	no
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
t2_complete	no	goal_t2_complete	no	0.99900001
t2_complete	yes	goal_t2_complete	no	0.001
t2_complete	no	goal_t2_complete	yes	0.001
t2_complete	yes	goal_t2_complete	yes	0.99900001
t2_complete	no	goal_t2_complete	done	0.001
t2_complete	yes	goal_t2_complete	done	0.99900001
t1_complete	no	goal_t1_complete	no	0.99900001
t1_complete	yes	goal_t1_complete	no	0.001
t1_complete	no	goal_t1_complete	yes	0.001
t1_complete	no	goal_t1_complete	done	0.001
t1_complete	yes	goal_t1_complete	done	0.99900001
t1_complete	yes	goal_t1_complete	yes	0.99900001
\.


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Name: t_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_pkey PRIMARY KEY (abil_name);


--
-- Name: t_behaviour_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, beh_name);


--
-- Name: t_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviours
    ADD CONSTRAINT t_behaviours_pkey PRIMARY KEY (beh_name);


--
-- Name: t_default_probabilities4abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_pkey PRIMARY KEY (dementia_type, abil_type);


--
-- Name: t_effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id);


--
-- Name: t_env_variables_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_env_variables_values
    ADD CONSTRAINT t_env_variables_values_pkey PRIMARY KEY (var_name, var_value);


--
-- Name: t_iu_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_pkey PRIMARY KEY (row_id, abil_name);


--
-- Name: t_iu_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_pkey PRIMARY KEY (row_id, beh_name);


--
-- Name: t_iu_goals_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_iu_row_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_row
    ADD CONSTRAINT t_iu_row_pkey PRIMARY KEY (row_id);


--
-- Name: t_iu_task_states_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_observations_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_observations_values
    ADD CONSTRAINT t_observations_values_pkey PRIMARY KEY (obs_name, obs_value);


--
-- Name: t_preconditions4effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id, var_name);


--
-- Name: t_rewards_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_pkey PRIMARY KEY (state_set_id, var_name);


--
-- Name: t_rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards
    ADD CONSTRAINT t_rewards_pkey PRIMARY KEY (state_set_id);


--
-- Name: t_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, var_name, var_value);


--
-- Name: t_types_of_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_abilities
    ADD CONSTRAINT t_types_of_abilities_pkey PRIMARY KEY (abil_type);


--
-- Name: t_types_of_dementia_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_dementia
    ADD CONSTRAINT t_types_of_dementia_pkey PRIMARY KEY (dementia_type);


--
-- Name: t_when_is_behaviour_impossible_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_dementia_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_dementia_type_fkey FOREIGN KEY (dementia_type) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_beh_effect_id_fkey FOREIGN KEY (beh_effect_id) REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_state_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_state_set_id_fkey FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

