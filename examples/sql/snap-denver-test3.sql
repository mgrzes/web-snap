--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities (
    abil_name character varying(256) NOT NULL,
    abil_type character varying(256),
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    abil_initial_prob real NOT NULL,
    abil_prompt_cost real NOT NULL,
    CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (((abil_initial_prob >= (0)::double precision) AND (abil_initial_prob <= (1)::double precision))),
    CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK ((abil_prompt_cost >= (0)::double precision)),
    CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (((abil_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_abilities OWNER TO x721demouser;

--
-- Name: t_abilities4behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities4behaviours (
    abil_name character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_abilities4behaviours OWNER TO x721demouser;

--
-- Name: t_behaviour_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviour_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_behaviour_sensor_model OWNER TO x721demouser;

--
-- Name: t_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviours (
    beh_name character varying(256) NOT NULL,
    beh_timeout real DEFAULT 0.0,
    CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (((beh_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C5: non-negative value required" CHECK ((beh_timeout >= (0)::double precision))
);


ALTER TABLE public.t_behaviours OWNER TO x721demouser;

--
-- Name: t_default_probabilities4abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_default_probabilities4abilities (
    dementia_type character varying(256) NOT NULL,
    abil_type character varying(256) NOT NULL,
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_default_probabilities4abilities OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256),
    var_value character varying(256),
    beh_name character varying(256)
);


ALTER TABLE public.t_effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_effects_of_behaviours_beh_effect_id_seq OWNED BY t_effects_of_behaviours.beh_effect_id;


--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_env_variables_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_env_variables_values (
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    var_value_initial_prob real NOT NULL,
    CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (((var_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (((var_value)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (((var_value_initial_prob >= (0)::double precision) AND (var_value_initial_prob <= (1)::double precision)))
);


ALTER TABLE public.t_env_variables_values OWNER TO x721demouser;

--
-- Name: t_iu_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_abilities (
    row_id integer NOT NULL,
    abil_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_abilities OWNER TO x721demouser;

--
-- Name: t_iu_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_behaviours (
    row_id integer NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_behaviours OWNER TO x721demouser;

--
-- Name: t_iu_goals; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_goals (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_goals OWNER TO x721demouser;

--
-- Name: t_iu_row; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_row (
    row_id integer NOT NULL,
    data_row boolean DEFAULT true NOT NULL,
    row_probability real DEFAULT 1.0,
    row_confirmed boolean DEFAULT true NOT NULL,
    CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (((row_probability >= (0)::double precision) AND (row_probability <= (1)::double precision)))
);


ALTER TABLE public.t_iu_row OWNER TO x721demouser;

--
-- Name: t_iu_task_states; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_task_states (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_task_states OWNER TO x721demouser;

--
-- Name: t_observations_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_observations_values (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (((obs_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (((obs_value)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_observations_values OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_preconditions4effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_preconditions4effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNED BY t_preconditions4effects_of_behaviours.beh_effect_id;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards (
    state_set_id integer NOT NULL,
    reward_value real NOT NULL,
    CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK ((reward_value >= (0)::double precision))
);


ALTER TABLE public.t_rewards OWNER TO x721demouser;

--
-- Name: t_rewards_desc; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards_desc (
    state_set_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_rewards_desc OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_desc_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_desc_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_desc_state_set_id_seq OWNED BY t_rewards_desc.state_set_id;


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_state_set_id_seq OWNED BY t_rewards.state_set_id;


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_sensor_model OWNER TO x721demouser;

--
-- Name: t_types_of_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_abilities (
    abil_type character varying(256) NOT NULL,
    CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (((abil_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_abilities OWNER TO x721demouser;

--
-- Name: t_types_of_dementia; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_dementia (
    dementia_type character varying(256) NOT NULL,
    CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (((dementia_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_dementia OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_impossible (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_impossible OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_impossible_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq OWNED BY t_when_is_behaviour_impossible.state_set_id;


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Name: t_when_is_behaviour_relevant; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_relevant (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_relevant OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_relevant_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq OWNED BY t_when_is_behaviour_relevant.state_set_id;


--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_relevant_state_set_id_seq', 1, false);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_preconditions4effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_desc_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_impossible_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_relevant_state_set_id_seq'::regclass);


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) FROM stdin;
Rn_whitebin_position_in_slot_bin	recognition	0.5	0.5	0.94999999	0.050000001	0.5	1
Af_alter_whitebin_lid_open	affordance	0.5	0.5	0.94999999	0.050000001	0.5	0.5
Rn_slot_whitebins_empty	recognition	0.5	0.5	0.94999999	0.050000001	0.5	1
Rn_whitebin_position_in_assembly_area	recognition	0.5	0.5	0.94999999	0.050000001	0.5	1
Rn_whitebin_position_in_hand	recognition	0.5	0.5	0.94999999	0.050000001	0.5	1
\.


--
-- Data for Name: t_abilities4behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities4behaviours (abil_name, beh_name) FROM stdin;
Rn_whitebin_position_in_slot_bin	move_whitebin_from_slot
Af_alter_whitebin_lid_open	alter_whitebin_lid_open
Rn_slot_whitebins_empty	fill_slot_whitebins
Rn_whitebin_position_in_assembly_area	alter_whitebin_lid_open
Rn_whitebin_position_in_hand	move_whitebin_to_brownarea
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
other	1000000
nothing	1000000
move_whitebin_from_slot	3
move_whitebin_to_brownarea	3
alter_whitebin_lid_open	3
move_whitebins_to_slot	3
fill_slot_whitebins	3
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
1	whitebin_position	in_hand	move_whitebin_from_slot
2	whitebin_position	in_assembly_area	move_whitebin_to_brownarea
3	whitebin_lid	open	alter_whitebin_lid_open
4	slot_whitebins	full	fill_slot_whitebins
5	whitebin_position	in_slot_bin	fill_slot_whitebins
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
whitebin_position	in_assembly_area	0
whitebin_position	in_hand	0
whitebin_lid	open	0
whitebin_lid	closed	1
slot_whitebins	full	0.80000001
slot_whitebins	empty	0.2
whitebin_position	other	0.2
whitebin_position	in_slot_bin	0.80000001
whitebin_position	in_completed_area	0
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
2	Rn_whitebin_position_in_slot_bin
1	Rn_slot_whitebins_empty
4	Af_alter_whitebin_lid_open
4	Rn_whitebin_position_in_assembly_area
3	Rn_whitebin_position_in_hand
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
3	move_whitebin_to_brownarea
4	alter_whitebin_lid_open
2	move_whitebin_from_slot
1	fill_slot_whitebins
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
4	whitebin_position	in_assembly_area
4	whitebin_lid	open
3	whitebin_position	in_assembly_area
3	whitebin_lid	open
2	whitebin_position	in_assembly_area
2	whitebin_lid	open
1	whitebin_lid	open
1	whitebin_position	in_assembly_area
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
1	t	1	t
2	t	1	t
3	t	1	t
4	t	1	t
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
3	whitebin_position	in_hand
4	whitebin_lid	closed
4	whitebin_position	in_assembly_area
3	slot_whitebins	full
2	whitebin_lid	closed
4	slot_whitebins	full
2	slot_whitebins	full
2	whitebin_position	in_slot_bin
1	slot_whitebins	empty
3	whitebin_lid	closed
1	whitebin_position	other
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
whitebin_position_sensor	in_slot_bin
whitebin_position_sensor	in_assembly_area
whitebin_position_sensor	in_hand
slot_whitebins_sensor	full
slot_whitebins_sensor	empty
whitebin_lid_sensor	open
whitebin_lid_sensor	closed
whitebin_position_sensor	other
whitebin_position_sensor	in_completed_area
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
2	whitebin_position	in_hand
1	whitebin_position	in_slot_bin
3	whitebin_lid	closed
4	slot_whitebins	empty
2	slot_whitebins	full
3	slot_whitebins	full
1	whitebin_lid	closed
2	whitebin_lid	closed
1	slot_whitebins	full
5	whitebin_position	other
4	whitebin_position	other
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
1	15
3	5
2	15
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	whitebin_lid	open
2	whitebin_position	in_assembly_area
3	slot_whitebins	full
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
slot_whitebins_sensor	empty	slot_whitebins	empty	0.99000001
whitebin_position_sensor	in_completed_area	whitebin_position	in_completed_area	0.99000001
whitebin_position_sensor	in_completed_area	whitebin_position	in_assembly_area	0.001
whitebin_position_sensor	in_completed_area	whitebin_position	in_slot_bin	0.001
whitebin_position_sensor	in_completed_area	whitebin_position	in_hand	0.001
whitebin_position_sensor	in_slot_bin	whitebin_position	in_slot_bin	0.99000001
whitebin_position_sensor	in_slot_bin	whitebin_position	in_assembly_area	0.001
whitebin_position_sensor	in_slot_bin	whitebin_position	in_hand	0.001
whitebin_position_sensor	in_slot_bin	whitebin_position	in_completed_area	0.001
whitebin_position_sensor	in_assembly_area	whitebin_position	in_assembly_area	0.99000001
whitebin_position_sensor	in_hand	whitebin_position	in_hand	0.99000001
whitebin_position_sensor	other	whitebin_position	other	0.99000001
whitebin_lid_sensor	open	whitebin_lid	open	0.99000001
whitebin_lid_sensor	closed	whitebin_lid	closed	0.99000001
slot_whitebins_sensor	full	slot_whitebins	full	0.99000001
whitebin_position_sensor	in_assembly_area	whitebin_position	in_hand	0.001
whitebin_position_sensor	in_assembly_area	whitebin_position	in_slot_bin	0.001
whitebin_position_sensor	in_hand	whitebin_position	in_assembly_area	0.001
whitebin_position_sensor	in_hand	whitebin_position	in_slot_bin	0.001
whitebin_position_sensor	other	whitebin_position	in_assembly_area	0.001
whitebin_position_sensor	other	whitebin_position	in_hand	0.001
whitebin_position_sensor	other	whitebin_position	in_slot_bin	0.001
whitebin_position_sensor	in_hand	whitebin_position	in_completed_area	0.001
whitebin_position_sensor	in_completed_area	whitebin_position	other	0.001
whitebin_position_sensor	in_slot_bin	whitebin_position	other	0.001
whitebin_lid_sensor	open	whitebin_lid	closed	0.001
whitebin_position_sensor	in_assembly_area	whitebin_position	other	0.001
whitebin_lid_sensor	closed	whitebin_lid	open	0.001
whitebin_position_sensor	in_assembly_area	whitebin_position	in_completed_area	0.001
whitebin_position_sensor	other	whitebin_position	in_completed_area	0.001
slot_whitebins_sensor	full	slot_whitebins	empty	0.001
slot_whitebins_sensor	empty	slot_whitebins	full	0.001
whitebin_position_sensor	in_hand	whitebin_position	other	0.001
\.


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Data for Name: t_when_is_behaviour_relevant; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_relevant (state_set_id, beh_name, var_name, var_value) FROM stdin;
3	move_whitebin_to_brownarea	whitebin_position	in_hand
4	alter_whitebin_lid_open	whitebin_lid	closed
4	alter_whitebin_lid_open	whitebin_position	in_assembly_area
3	move_whitebin_to_brownarea	slot_whitebins	full
2	move_whitebin_from_slot	whitebin_lid	closed
4	alter_whitebin_lid_open	slot_whitebins	full
2	move_whitebin_from_slot	slot_whitebins	full
2	move_whitebin_from_slot	whitebin_position	in_slot_bin
1	fill_slot_whitebins	slot_whitebins	empty
3	move_whitebin_to_brownarea	whitebin_lid	closed
\.


--
-- Name: t_abilities4behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_pkey PRIMARY KEY (abil_name, beh_name);


--
-- Name: t_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_pkey PRIMARY KEY (abil_name);


--
-- Name: t_behaviour_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, beh_name);


--
-- Name: t_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviours
    ADD CONSTRAINT t_behaviours_pkey PRIMARY KEY (beh_name);


--
-- Name: t_default_probabilities4abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_pkey PRIMARY KEY (dementia_type, abil_type);


--
-- Name: t_effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id);


--
-- Name: t_env_variables_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_env_variables_values
    ADD CONSTRAINT t_env_variables_values_pkey PRIMARY KEY (var_name, var_value);


--
-- Name: t_iu_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_pkey PRIMARY KEY (row_id, abil_name);


--
-- Name: t_iu_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_pkey PRIMARY KEY (row_id, beh_name);


--
-- Name: t_iu_goals_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_iu_row_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_row
    ADD CONSTRAINT t_iu_row_pkey PRIMARY KEY (row_id);


--
-- Name: t_iu_task_states_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_observations_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_observations_values
    ADD CONSTRAINT t_observations_values_pkey PRIMARY KEY (obs_name, obs_value);


--
-- Name: t_preconditions4effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id, var_name);


--
-- Name: t_rewards_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_pkey PRIMARY KEY (state_set_id, var_name);


--
-- Name: t_rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards
    ADD CONSTRAINT t_rewards_pkey PRIMARY KEY (state_set_id);


--
-- Name: t_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, var_name, var_value);


--
-- Name: t_types_of_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_abilities
    ADD CONSTRAINT t_types_of_abilities_pkey PRIMARY KEY (abil_type);


--
-- Name: t_types_of_dementia_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_dementia
    ADD CONSTRAINT t_types_of_dementia_pkey PRIMARY KEY (dementia_type);


--
-- Name: t_when_is_behaviour_impossible_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_when_is_behaviour_relevant_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_abilities4behaviours_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_abilities4behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_dementia_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_dementia_type_fkey FOREIGN KEY (dementia_type) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_beh_effect_id_fkey FOREIGN KEY (beh_effect_id) REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_state_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_state_set_id_fkey FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

