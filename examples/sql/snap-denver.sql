--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities (
    abil_name character varying(256) NOT NULL,
    abil_type character varying(256),
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    abil_initial_prob real NOT NULL,
    abil_prompt_cost real NOT NULL,
    CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (((abil_initial_prob >= (0)::double precision) AND (abil_initial_prob <= (1)::double precision))),
    CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK ((abil_prompt_cost >= (0)::double precision)),
    CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (((abil_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_abilities OWNER TO x721demouser;

--
-- Name: t_abilities4behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities4behaviours (
    abil_name character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_abilities4behaviours OWNER TO x721demouser;

--
-- Name: t_behaviour_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviour_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_behaviour_sensor_model OWNER TO x721demouser;

--
-- Name: t_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviours (
    beh_name character varying(256) NOT NULL,
    beh_timeout real DEFAULT 0.0,
    CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (((beh_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C5: non-negative value required" CHECK ((beh_timeout >= (0)::double precision))
);


ALTER TABLE public.t_behaviours OWNER TO x721demouser;

--
-- Name: t_default_probabilities4abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_default_probabilities4abilities (
    dementia_type character varying(256) NOT NULL,
    abil_type character varying(256) NOT NULL,
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_default_probabilities4abilities OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256),
    var_value character varying(256),
    beh_name character varying(256)
);


ALTER TABLE public.t_effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_effects_of_behaviours_beh_effect_id_seq OWNED BY t_effects_of_behaviours.beh_effect_id;


--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_env_variables_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_env_variables_values (
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    var_value_initial_prob real NOT NULL,
    CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (((var_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (((var_value)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (((var_value_initial_prob >= (0)::double precision) AND (var_value_initial_prob <= (1)::double precision)))
);


ALTER TABLE public.t_env_variables_values OWNER TO x721demouser;

--
-- Name: t_iu_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_abilities (
    row_id integer NOT NULL,
    abil_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_abilities OWNER TO x721demouser;

--
-- Name: t_iu_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_behaviours (
    row_id integer NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_behaviours OWNER TO x721demouser;

--
-- Name: t_iu_goals; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_goals (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_goals OWNER TO x721demouser;

--
-- Name: t_iu_row; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_row (
    row_id integer NOT NULL,
    data_row boolean DEFAULT true NOT NULL,
    row_probability real DEFAULT 1.0,
    row_confirmed boolean DEFAULT true NOT NULL,
    CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (((row_probability >= (0)::double precision) AND (row_probability <= (1)::double precision)))
);


ALTER TABLE public.t_iu_row OWNER TO x721demouser;

--
-- Name: t_iu_task_states; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_task_states (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_task_states OWNER TO x721demouser;

--
-- Name: t_observations_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_observations_values (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (((obs_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (((obs_value)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_observations_values OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_preconditions4effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_preconditions4effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNED BY t_preconditions4effects_of_behaviours.beh_effect_id;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards (
    state_set_id integer NOT NULL,
    reward_value real NOT NULL,
    CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK ((reward_value >= (0)::double precision))
);


ALTER TABLE public.t_rewards OWNER TO x721demouser;

--
-- Name: t_rewards_desc; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards_desc (
    state_set_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_rewards_desc OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_desc_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_desc_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_desc_state_set_id_seq OWNED BY t_rewards_desc.state_set_id;


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_state_set_id_seq OWNED BY t_rewards.state_set_id;


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_sensor_model OWNER TO x721demouser;

--
-- Name: t_types_of_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_abilities (
    abil_type character varying(256) NOT NULL,
    CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (((abil_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_abilities OWNER TO x721demouser;

--
-- Name: t_types_of_dementia; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_dementia (
    dementia_type character varying(256) NOT NULL,
    CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (((dementia_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_dementia OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_impossible (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_impossible OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_impossible_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq OWNED BY t_when_is_behaviour_impossible.state_set_id;


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Name: t_when_is_behaviour_relevant; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_relevant (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_relevant OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_relevant_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq OWNED BY t_when_is_behaviour_relevant.state_set_id;


--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_relevant_state_set_id_seq', 1, false);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_preconditions4effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_desc_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_impossible_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_relevant_state_set_id_seq'::regclass);


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) FROM stdin;
Rn_slot_blue_empty	recognition	0.30000001	0.69999999	0.99000001	0.0099999998	0.30000001	1
Rn_slot_orange_empty	recognition	0.30000001	0.69999999	0.99000001	0.0099999998	0.30000001	1
Rn_slot_purple_empty	recognition	0.30000001	0.69999999	0.99000001	0.0099999998	0.30000001	1
Rn_slot_whitebins_empty	recognition	0.30000001	0.69999999	0.99000001	0.0099999998	0.30000001	1
Rn_slot_yellow_empty	recognition	0.30000001	0.75	0.99000001	0.0099999998	0.30000001	1
\.


--
-- Data for Name: t_abilities4behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities4behaviours (abil_name, beh_name) FROM stdin;
Rn_slot_blue_empty	fill_slot_blue
Rn_slot_purple_empty	fill_slot_purple
Rn_slot_orange_empty	fill_slot_orange
Rn_slot_whitebins_empty	fill_slot_whitebins
Rn_slot_yellow_empty	fill_slot_yellow
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
other	1000000
nothing	1000000
fill_slot_blue	10
fill_slot_yellow	10
fill_slot_whitebins	10
fill_slot_orange	10
fill_slot_purple	10
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
3	slot_purple	full	fill_slot_purple
4	slot_orange	full	fill_slot_orange
5	slot_whitebins	full	fill_slot_whitebins
2	slot_yellow	full	fill_slot_yellow
1	slot_blue	full	fill_slot_blue
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
slot_whitebins	full	0
slot_purple	full	0
slot_yellow	full	0
slot_blue	full	0
slot_orange	full	0
slot_purple	empty	1
slot_blue	empty	1
slot_yellow	empty	1
slot_whitebins	empty	1
slot_orange	empty	1
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
11	Rn_slot_orange_empty
10	Rn_slot_orange_empty
62	Rn_slot_blue_empty
61	Rn_slot_blue_empty
7	Rn_slot_purple_empty
6	Rn_slot_orange_empty
5	Rn_slot_orange_empty
9	Rn_slot_yellow_empty
8	Rn_slot_yellow_empty
60	Rn_slot_orange_empty
59	Rn_slot_orange_empty
58	Rn_slot_orange_empty
57	Rn_slot_orange_empty
4	Rn_slot_blue_empty
3	Rn_slot_blue_empty
24	Rn_slot_blue_empty
23	Rn_slot_blue_empty
2	Rn_slot_yellow_empty
1	Rn_slot_yellow_empty
46	Rn_slot_purple_empty
45	Rn_slot_purple_empty
44	Rn_slot_purple_empty
43	Rn_slot_purple_empty
36	Rn_slot_yellow_empty
35	Rn_slot_yellow_empty
34	Rn_slot_yellow_empty
33	Rn_slot_yellow_empty
32	Rn_slot_yellow_empty
31	Rn_slot_yellow_empty
26	Rn_slot_purple_empty
22	Rn_slot_yellow_empty
21	Rn_slot_yellow_empty
20	Rn_slot_yellow_empty
19	Rn_slot_yellow_empty
28	Rn_slot_yellow_empty
27	Rn_slot_yellow_empty
66	Rn_slot_purple_empty
65	Rn_slot_purple_empty
64	Rn_slot_purple_empty
63	Rn_slot_purple_empty
74	Rn_slot_purple_empty
73	Rn_slot_purple_empty
80	Rn_slot_whitebins_empty
79	Rn_slot_whitebins_empty
78	Rn_slot_purple_empty
77	Rn_slot_purple_empty
13	Rn_slot_whitebins_empty
12	Rn_slot_whitebins_empty
48	Rn_slot_whitebins_empty
47	Rn_slot_whitebins_empty
56	Rn_slot_whitebins_empty
55	Rn_slot_whitebins_empty
54	Rn_slot_whitebins_empty
53	Rn_slot_whitebins_empty
52	Rn_slot_whitebins_empty
51	Rn_slot_whitebins_empty
50	Rn_slot_blue_empty
49	Rn_slot_blue_empty
25	Rn_slot_purple_empty
18	Rn_slot_whitebins_empty
17	Rn_slot_whitebins_empty
72	Rn_slot_blue_empty
71	Rn_slot_blue_empty
16	Rn_slot_whitebins_empty
15	Rn_slot_whitebins_empty
30	Rn_slot_blue_empty
29	Rn_slot_blue_empty
70	Rn_slot_orange_empty
69	Rn_slot_orange_empty
68	Rn_slot_orange_empty
67	Rn_slot_orange_empty
42	Rn_slot_blue_empty
41	Rn_slot_blue_empty
76	Rn_slot_blue_empty
75	Rn_slot_blue_empty
40	Rn_slot_orange_empty
39	Rn_slot_orange_empty
38	Rn_slot_orange_empty
37	Rn_slot_orange_empty
14	Rn_slot_purple_empty
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
62	fill_slot_blue
61	fill_slot_blue
9	fill_slot_yellow
8	fill_slot_yellow
7	fill_slot_purple
6	fill_slot_orange
5	fill_slot_orange
66	fill_slot_purple
60	fill_slot_orange
59	fill_slot_orange
58	fill_slot_orange
57	fill_slot_orange
73	fill_slot_purple
24	fill_slot_blue
23	fill_slot_blue
4	fill_slot_blue
3	fill_slot_blue
42	fill_slot_blue
41	fill_slot_blue
36	fill_slot_yellow
35	fill_slot_yellow
34	fill_slot_yellow
33	fill_slot_yellow
32	fill_slot_yellow
31	fill_slot_yellow
22	fill_slot_yellow
21	fill_slot_yellow
20	fill_slot_yellow
19	fill_slot_yellow
28	fill_slot_yellow
27	fill_slot_yellow
2	fill_slot_yellow
1	fill_slot_yellow
65	fill_slot_purple
64	fill_slot_purple
56	fill_slot_whitebins
55	fill_slot_whitebins
54	fill_slot_whitebins
53	fill_slot_whitebins
52	fill_slot_whitebins
51	fill_slot_whitebins
48	fill_slot_whitebins
47	fill_slot_whitebins
14	fill_slot_purple
13	fill_slot_whitebins
12	fill_slot_whitebins
50	fill_slot_blue
49	fill_slot_blue
46	fill_slot_purple
45	fill_slot_purple
44	fill_slot_purple
43	fill_slot_purple
29	fill_slot_blue
11	fill_slot_orange
10	fill_slot_orange
26	fill_slot_purple
25	fill_slot_purple
18	fill_slot_whitebins
17	fill_slot_whitebins
16	fill_slot_whitebins
15	fill_slot_whitebins
63	fill_slot_purple
72	fill_slot_blue
71	fill_slot_blue
70	fill_slot_orange
69	fill_slot_orange
68	fill_slot_orange
67	fill_slot_orange
80	fill_slot_whitebins
79	fill_slot_whitebins
40	fill_slot_orange
39	fill_slot_orange
38	fill_slot_orange
37	fill_slot_orange
30	fill_slot_blue
74	fill_slot_purple
78	fill_slot_purple
77	fill_slot_purple
76	fill_slot_blue
75	fill_slot_blue
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
14	slot_whitebins	full
13	slot_blue	full
13	slot_orange	full
13	slot_purple	full
13	slot_whitebins	full
13	slot_yellow	full
12	slot_blue	full
12	slot_orange	full
12	slot_purple	full
12	slot_whitebins	full
12	slot_yellow	full
11	slot_blue	full
11	slot_orange	full
11	slot_purple	full
11	slot_whitebins	full
11	slot_yellow	full
10	slot_blue	full
10	slot_orange	full
10	slot_purple	full
10	slot_whitebins	full
10	slot_yellow	full
35	slot_purple	full
35	slot_whitebins	full
35	slot_yellow	full
34	slot_blue	full
34	slot_orange	full
34	slot_purple	full
34	slot_whitebins	full
34	slot_yellow	full
33	slot_blue	full
33	slot_orange	full
33	slot_purple	full
33	slot_whitebins	full
33	slot_yellow	full
32	slot_blue	full
32	slot_orange	full
32	slot_purple	full
32	slot_whitebins	full
32	slot_yellow	full
31	slot_blue	full
31	slot_orange	full
31	slot_purple	full
31	slot_whitebins	full
31	slot_yellow	full
30	slot_blue	full
30	slot_orange	full
30	slot_purple	full
30	slot_whitebins	full
30	slot_yellow	full
29	slot_blue	full
29	slot_orange	full
29	slot_purple	full
29	slot_whitebins	full
29	slot_yellow	full
62	slot_blue	full
62	slot_orange	full
62	slot_purple	full
62	slot_whitebins	full
62	slot_yellow	full
61	slot_blue	full
61	slot_orange	full
61	slot_purple	full
61	slot_whitebins	full
61	slot_yellow	full
9	slot_blue	full
9	slot_orange	full
9	slot_purple	full
9	slot_whitebins	full
9	slot_yellow	full
8	slot_blue	full
8	slot_orange	full
8	slot_purple	full
14	slot_yellow	full
14	slot_blue	full
14	slot_orange	full
14	slot_purple	full
8	slot_whitebins	full
8	slot_yellow	full
7	slot_blue	full
7	slot_orange	full
7	slot_purple	full
7	slot_whitebins	full
7	slot_yellow	full
6	slot_blue	full
6	slot_orange	full
6	slot_purple	full
6	slot_whitebins	full
6	slot_yellow	full
5	slot_blue	full
5	slot_orange	full
5	slot_purple	full
5	slot_whitebins	full
5	slot_yellow	full
4	slot_blue	full
4	slot_orange	full
4	slot_purple	full
4	slot_whitebins	full
4	slot_yellow	full
3	slot_blue	full
3	slot_orange	full
3	slot_purple	full
3	slot_whitebins	full
3	slot_yellow	full
2	slot_blue	full
2	slot_orange	full
2	slot_purple	full
2	slot_whitebins	full
2	slot_yellow	full
1	slot_blue	full
1	slot_orange	full
1	slot_purple	full
1	slot_whitebins	full
1	slot_yellow	full
66	slot_blue	full
66	slot_orange	full
66	slot_purple	full
66	slot_whitebins	full
66	slot_yellow	full
65	slot_blue	full
65	slot_orange	full
65	slot_purple	full
65	slot_whitebins	full
65	slot_yellow	full
64	slot_blue	full
64	slot_orange	full
64	slot_purple	full
64	slot_whitebins	full
64	slot_yellow	full
63	slot_blue	full
63	slot_orange	full
63	slot_purple	full
63	slot_whitebins	full
63	slot_yellow	full
60	slot_blue	full
60	slot_orange	full
60	slot_purple	full
60	slot_whitebins	full
60	slot_yellow	full
59	slot_blue	full
59	slot_orange	full
59	slot_purple	full
59	slot_whitebins	full
59	slot_yellow	full
48	slot_blue	full
48	slot_orange	full
48	slot_purple	full
48	slot_whitebins	full
48	slot_yellow	full
47	slot_blue	full
47	slot_orange	full
47	slot_purple	full
47	slot_whitebins	full
47	slot_yellow	full
58	slot_blue	full
58	slot_orange	full
58	slot_purple	full
58	slot_whitebins	full
58	slot_yellow	full
57	slot_blue	full
57	slot_orange	full
57	slot_purple	full
57	slot_whitebins	full
57	slot_yellow	full
56	slot_blue	full
56	slot_orange	full
56	slot_purple	full
56	slot_whitebins	full
56	slot_yellow	full
55	slot_blue	full
55	slot_orange	full
55	slot_purple	full
55	slot_whitebins	full
55	slot_yellow	full
54	slot_blue	full
54	slot_orange	full
54	slot_purple	full
54	slot_whitebins	full
54	slot_yellow	full
53	slot_blue	full
53	slot_orange	full
53	slot_purple	full
53	slot_whitebins	full
53	slot_yellow	full
52	slot_blue	full
52	slot_orange	full
52	slot_purple	full
52	slot_whitebins	full
52	slot_yellow	full
51	slot_blue	full
51	slot_orange	full
51	slot_purple	full
51	slot_whitebins	full
51	slot_yellow	full
50	slot_blue	full
50	slot_orange	full
50	slot_purple	full
50	slot_whitebins	full
50	slot_yellow	full
49	slot_blue	full
49	slot_orange	full
49	slot_purple	full
49	slot_whitebins	full
49	slot_yellow	full
72	slot_blue	full
72	slot_orange	full
72	slot_purple	full
72	slot_whitebins	full
72	slot_yellow	full
71	slot_blue	full
71	slot_orange	full
71	slot_purple	full
71	slot_whitebins	full
71	slot_yellow	full
26	slot_blue	full
26	slot_orange	full
26	slot_purple	full
26	slot_whitebins	full
26	slot_yellow	full
25	slot_blue	full
25	slot_orange	full
25	slot_purple	full
25	slot_whitebins	full
25	slot_yellow	full
24	slot_blue	full
24	slot_orange	full
24	slot_purple	full
24	slot_whitebins	full
24	slot_yellow	full
23	slot_blue	full
23	slot_orange	full
23	slot_purple	full
23	slot_whitebins	full
23	slot_yellow	full
22	slot_blue	full
22	slot_orange	full
22	slot_purple	full
22	slot_whitebins	full
22	slot_yellow	full
21	slot_blue	full
21	slot_orange	full
21	slot_purple	full
21	slot_whitebins	full
21	slot_yellow	full
20	slot_blue	full
20	slot_orange	full
20	slot_purple	full
20	slot_whitebins	full
20	slot_yellow	full
19	slot_blue	full
19	slot_orange	full
19	slot_purple	full
19	slot_whitebins	full
19	slot_yellow	full
18	slot_blue	full
18	slot_orange	full
18	slot_purple	full
18	slot_whitebins	full
18	slot_yellow	full
17	slot_blue	full
17	slot_orange	full
17	slot_purple	full
17	slot_whitebins	full
17	slot_yellow	full
16	slot_blue	full
16	slot_orange	full
16	slot_purple	full
16	slot_whitebins	full
16	slot_yellow	full
15	slot_blue	full
15	slot_orange	full
15	slot_purple	full
15	slot_whitebins	full
15	slot_yellow	full
74	slot_blue	full
74	slot_orange	full
74	slot_purple	full
74	slot_whitebins	full
74	slot_yellow	full
73	slot_blue	full
73	slot_orange	full
73	slot_purple	full
73	slot_whitebins	full
73	slot_yellow	full
70	slot_blue	full
70	slot_orange	full
70	slot_purple	full
70	slot_whitebins	full
70	slot_yellow	full
80	slot_blue	full
80	slot_orange	full
80	slot_purple	full
80	slot_whitebins	full
80	slot_yellow	full
79	slot_blue	full
79	slot_orange	full
79	slot_purple	full
79	slot_whitebins	full
79	slot_yellow	full
78	slot_blue	full
78	slot_orange	full
78	slot_purple	full
78	slot_whitebins	full
78	slot_yellow	full
77	slot_blue	full
77	slot_orange	full
77	slot_purple	full
77	slot_whitebins	full
77	slot_yellow	full
69	slot_blue	full
69	slot_orange	full
69	slot_purple	full
69	slot_whitebins	full
69	slot_yellow	full
68	slot_blue	full
68	slot_orange	full
68	slot_purple	full
68	slot_whitebins	full
68	slot_yellow	full
67	slot_blue	full
67	slot_orange	full
67	slot_purple	full
67	slot_whitebins	full
67	slot_yellow	full
76	slot_blue	full
76	slot_orange	full
76	slot_purple	full
76	slot_whitebins	full
76	slot_yellow	full
75	slot_blue	full
75	slot_orange	full
75	slot_purple	full
75	slot_whitebins	full
75	slot_yellow	full
46	slot_blue	full
46	slot_orange	full
46	slot_purple	full
46	slot_whitebins	full
46	slot_yellow	full
45	slot_blue	full
45	slot_orange	full
45	slot_purple	full
45	slot_whitebins	full
45	slot_yellow	full
44	slot_blue	full
44	slot_orange	full
44	slot_purple	full
44	slot_whitebins	full
44	slot_yellow	full
43	slot_blue	full
43	slot_orange	full
43	slot_purple	full
43	slot_whitebins	full
43	slot_yellow	full
42	slot_blue	full
42	slot_orange	full
42	slot_purple	full
42	slot_whitebins	full
42	slot_yellow	full
41	slot_blue	full
41	slot_orange	full
41	slot_purple	full
41	slot_whitebins	full
41	slot_yellow	full
40	slot_blue	full
40	slot_orange	full
40	slot_purple	full
40	slot_whitebins	full
40	slot_yellow	full
39	slot_blue	full
39	slot_orange	full
39	slot_purple	full
39	slot_whitebins	full
39	slot_yellow	full
38	slot_blue	full
38	slot_orange	full
38	slot_purple	full
38	slot_whitebins	full
38	slot_yellow	full
37	slot_blue	full
37	slot_orange	full
37	slot_purple	full
37	slot_whitebins	full
37	slot_yellow	full
36	slot_blue	full
36	slot_orange	full
36	slot_purple	full
36	slot_whitebins	full
36	slot_yellow	full
35	slot_blue	full
35	slot_orange	full
28	slot_blue	full
28	slot_orange	full
28	slot_purple	full
28	slot_whitebins	full
28	slot_yellow	full
27	slot_blue	full
27	slot_orange	full
27	slot_purple	full
27	slot_whitebins	full
27	slot_yellow	full
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
6	t	0.25	f
8	t	0.25	f
10	t	0.25	f
12	t	0.25	f
14	t	0.25	f
9	t	0.33333334	f
11	t	0.33333334	f
13	t	0.33333334	f
15	t	0.25	f
25	t	0.25	f
16	t	0.33333334	f
26	t	0.33333334	f
17	t	0.33333334	f
24	t	0.33333334	f
18	t	0.5	f
22	t	0.5	f
27	t	0.25	f
37	t	0.25	f
41	t	0.25	f
55	t	0.5	f
56	t	1	t
80	t	0.25	f
43	t	0.25	f
28	t	0.33333334	f
38	t	0.33333334	f
44	t	0.33333334	f
29	t	0.33333334	f
31	t	0.33333334	f
45	t	0.33333334	f
30	t	0.5	f
35	t	0.5	f
32	t	0.5	f
46	t	0.5	f
33	t	0.33333334	f
42	t	0.33333334	f
34	t	0.5	f
36	t	1	t
47	t	0.25	f
61	t	0.25	f
62	t	0.33333334	f
64	t	0.33333334	f
57	t	0.25	f
48	t	0.33333334	f
58	t	0.33333334	f
49	t	0.33333334	f
51	t	0.33333334	f
50	t	0.5	f
52	t	0.5	f
59	t	0.33333334	f
54	t	0.5	f
66	t	0.5	f
60	t	0.5	f
67	t	0.33333334	f
63	t	0.25	f
65	t	0.33333334	f
19	t	0.25	f
23	t	0.25	f
20	t	0.33333334	f
21	t	0.33333334	f
39	t	0.33333334	f
40	t	0.5	f
71	t	0.33333334	f
73	t	0.33333334	f
68	t	0.5	f
74	t	0.5	f
69	t	0.5	f
72	t	0.5	f
70	t	1	t
75	t	0.5	f
1	t	0.2	f
3	t	0.2	f
5	t	0.2	f
7	t	0.2	f
2	t	0.25	f
4	t	0.25	f
53	t	0.33333334	f
76	t	1	t
79	t	0.2	f
77	t	0.5	f
78	t	1	t
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
11	slot_blue	full
11	slot_orange	empty
11	slot_purple	full
11	slot_whitebins	empty
11	slot_yellow	empty
10	slot_blue	full
10	slot_orange	empty
10	slot_purple	empty
10	slot_whitebins	empty
10	slot_yellow	empty
40	slot_orange	empty
40	slot_purple	full
40	slot_whitebins	full
40	slot_yellow	empty
39	slot_blue	empty
39	slot_orange	empty
39	slot_purple	full
39	slot_whitebins	full
39	slot_yellow	empty
38	slot_blue	full
38	slot_orange	empty
38	slot_purple	empty
38	slot_whitebins	full
38	slot_yellow	empty
37	slot_blue	empty
37	slot_orange	empty
37	slot_purple	empty
37	slot_whitebins	full
37	slot_yellow	empty
36	slot_blue	full
36	slot_orange	full
36	slot_purple	full
36	slot_whitebins	full
36	slot_yellow	empty
35	slot_blue	empty
35	slot_orange	full
35	slot_purple	full
35	slot_whitebins	full
35	slot_yellow	empty
34	slot_blue	full
34	slot_orange	empty
34	slot_purple	full
34	slot_whitebins	full
34	slot_yellow	empty
33	slot_blue	empty
33	slot_orange	empty
33	slot_purple	full
33	slot_whitebins	full
33	slot_yellow	empty
32	slot_blue	full
32	slot_orange	full
32	slot_purple	empty
32	slot_whitebins	full
32	slot_yellow	empty
31	slot_blue	empty
31	slot_orange	full
31	slot_purple	empty
31	slot_whitebins	full
31	slot_yellow	empty
30	slot_blue	empty
30	slot_orange	full
30	slot_purple	full
30	slot_whitebins	full
30	slot_yellow	empty
29	slot_blue	empty
29	slot_orange	full
29	slot_purple	empty
29	slot_whitebins	full
29	slot_yellow	empty
62	slot_blue	empty
62	slot_orange	empty
62	slot_purple	full
62	slot_whitebins	empty
62	slot_yellow	full
61	slot_blue	empty
61	slot_orange	empty
61	slot_purple	empty
61	slot_whitebins	empty
61	slot_yellow	full
7	slot_blue	empty
7	slot_orange	empty
7	slot_purple	empty
7	slot_whitebins	empty
7	slot_yellow	empty
6	slot_blue	empty
6	slot_orange	empty
6	slot_purple	full
6	slot_whitebins	empty
6	slot_yellow	empty
5	slot_blue	empty
5	slot_orange	empty
5	slot_purple	empty
5	slot_whitebins	empty
5	slot_yellow	empty
26	slot_blue	full
26	slot_orange	full
26	slot_purple	empty
26	slot_whitebins	empty
26	slot_yellow	empty
25	slot_blue	empty
9	slot_blue	full
9	slot_orange	empty
9	slot_purple	full
9	slot_whitebins	empty
9	slot_yellow	empty
8	slot_blue	full
8	slot_orange	empty
8	slot_purple	empty
8	slot_whitebins	empty
8	slot_yellow	empty
25	slot_orange	full
25	slot_purple	empty
25	slot_whitebins	empty
25	slot_yellow	empty
4	slot_blue	empty
4	slot_orange	empty
4	slot_purple	full
4	slot_whitebins	empty
4	slot_yellow	empty
3	slot_blue	empty
3	slot_orange	empty
3	slot_purple	empty
3	slot_whitebins	empty
3	slot_yellow	empty
24	slot_blue	empty
24	slot_orange	full
24	slot_purple	full
24	slot_whitebins	empty
24	slot_yellow	empty
23	slot_blue	empty
23	slot_orange	full
23	slot_purple	empty
23	slot_whitebins	empty
23	slot_yellow	empty
2	slot_blue	empty
2	slot_orange	empty
2	slot_purple	full
2	slot_whitebins	empty
2	slot_yellow	empty
1	slot_blue	empty
1	slot_orange	empty
1	slot_purple	empty
1	slot_whitebins	empty
1	slot_yellow	empty
13	slot_blue	full
13	slot_orange	empty
13	slot_purple	full
13	slot_whitebins	empty
13	slot_yellow	empty
12	slot_blue	full
12	slot_orange	empty
12	slot_purple	empty
12	slot_whitebins	empty
12	slot_yellow	empty
20	slot_yellow	empty
19	slot_blue	empty
19	slot_orange	full
19	slot_purple	empty
19	slot_whitebins	empty
19	slot_yellow	empty
66	slot_blue	full
66	slot_orange	full
66	slot_purple	empty
66	slot_whitebins	empty
66	slot_yellow	full
65	slot_blue	empty
65	slot_orange	full
65	slot_purple	empty
65	slot_whitebins	empty
65	slot_yellow	full
64	slot_blue	full
64	slot_orange	empty
64	slot_purple	empty
64	slot_whitebins	empty
64	slot_yellow	full
63	slot_blue	empty
63	slot_orange	empty
63	slot_purple	empty
63	slot_whitebins	empty
63	slot_yellow	full
60	slot_blue	full
60	slot_orange	empty
60	slot_purple	full
60	slot_whitebins	empty
60	slot_yellow	full
59	slot_blue	empty
59	slot_orange	empty
59	slot_purple	full
59	slot_whitebins	empty
59	slot_yellow	full
58	slot_blue	full
58	slot_orange	empty
58	slot_purple	empty
58	slot_whitebins	empty
58	slot_yellow	full
57	slot_blue	empty
57	slot_orange	empty
57	slot_purple	empty
57	slot_whitebins	empty
57	slot_yellow	full
56	slot_blue	full
56	slot_orange	full
56	slot_purple	full
56	slot_whitebins	empty
56	slot_yellow	full
55	slot_blue	empty
55	slot_orange	full
55	slot_purple	full
55	slot_whitebins	empty
55	slot_yellow	full
54	slot_blue	full
48	slot_blue	full
48	slot_orange	empty
48	slot_purple	empty
48	slot_whitebins	empty
48	slot_yellow	full
47	slot_blue	empty
47	slot_orange	empty
47	slot_purple	empty
47	slot_whitebins	empty
47	slot_yellow	full
54	slot_orange	empty
54	slot_purple	full
54	slot_whitebins	empty
54	slot_yellow	full
53	slot_blue	empty
53	slot_orange	empty
53	slot_purple	full
53	slot_whitebins	empty
53	slot_yellow	full
52	slot_blue	full
52	slot_orange	full
52	slot_purple	empty
52	slot_whitebins	empty
52	slot_yellow	full
51	slot_blue	empty
51	slot_orange	full
51	slot_purple	empty
51	slot_whitebins	empty
51	slot_yellow	full
50	slot_blue	empty
50	slot_orange	full
50	slot_purple	full
50	slot_whitebins	empty
50	slot_yellow	full
49	slot_blue	empty
49	slot_orange	full
49	slot_purple	empty
49	slot_whitebins	empty
49	slot_yellow	full
72	slot_blue	empty
72	slot_orange	empty
72	slot_purple	full
72	slot_whitebins	full
72	slot_yellow	full
71	slot_blue	empty
71	slot_orange	empty
71	slot_purple	empty
71	slot_whitebins	full
71	slot_yellow	full
22	slot_blue	full
22	slot_orange	full
22	slot_purple	full
22	slot_whitebins	empty
22	slot_yellow	empty
21	slot_blue	empty
21	slot_orange	full
21	slot_purple	full
21	slot_whitebins	empty
21	slot_yellow	empty
20	slot_blue	full
20	slot_orange	full
20	slot_purple	empty
20	slot_whitebins	empty
18	slot_blue	full
18	slot_orange	full
18	slot_purple	full
18	slot_whitebins	empty
18	slot_yellow	empty
17	slot_blue	empty
17	slot_orange	full
17	slot_purple	full
17	slot_whitebins	empty
17	slot_yellow	empty
16	slot_blue	full
16	slot_orange	full
16	slot_purple	empty
16	slot_whitebins	empty
16	slot_yellow	empty
15	slot_blue	empty
15	slot_orange	full
15	slot_purple	empty
15	slot_whitebins	empty
15	slot_yellow	empty
74	slot_blue	full
74	slot_orange	empty
74	slot_purple	empty
74	slot_whitebins	full
74	slot_yellow	full
73	slot_blue	empty
73	slot_orange	empty
73	slot_purple	empty
73	slot_whitebins	full
73	slot_yellow	full
70	slot_blue	full
70	slot_orange	empty
70	slot_purple	full
70	slot_whitebins	full
70	slot_yellow	full
80	slot_blue	empty
80	slot_orange	empty
80	slot_purple	full
80	slot_whitebins	empty
80	slot_yellow	empty
79	slot_blue	empty
79	slot_orange	empty
79	slot_purple	empty
79	slot_whitebins	empty
79	slot_yellow	empty
78	slot_blue	full
78	slot_orange	full
78	slot_purple	empty
78	slot_whitebins	full
78	slot_yellow	full
77	slot_purple	empty
77	slot_whitebins	full
77	slot_yellow	full
69	slot_blue	empty
69	slot_orange	empty
69	slot_purple	full
69	slot_whitebins	full
69	slot_yellow	full
68	slot_blue	full
68	slot_orange	empty
68	slot_purple	empty
68	slot_whitebins	full
68	slot_yellow	full
67	slot_blue	empty
67	slot_orange	empty
67	slot_purple	empty
67	slot_whitebins	full
67	slot_yellow	full
76	slot_blue	empty
76	slot_orange	full
76	slot_purple	full
76	slot_whitebins	full
76	slot_yellow	full
75	slot_blue	empty
75	slot_orange	full
75	slot_purple	empty
75	slot_whitebins	full
75	slot_yellow	full
77	slot_blue	empty
77	slot_orange	full
14	slot_blue	full
14	slot_orange	empty
14	slot_purple	empty
14	slot_whitebins	empty
14	slot_yellow	empty
46	slot_blue	full
46	slot_orange	full
46	slot_purple	empty
46	slot_whitebins	full
46	slot_yellow	empty
45	slot_blue	empty
45	slot_orange	full
45	slot_purple	empty
45	slot_whitebins	full
45	slot_yellow	empty
44	slot_blue	full
44	slot_orange	empty
44	slot_purple	empty
44	slot_whitebins	full
44	slot_yellow	empty
43	slot_blue	empty
43	slot_orange	empty
43	slot_purple	empty
43	slot_whitebins	full
43	slot_yellow	empty
42	slot_blue	empty
42	slot_orange	empty
42	slot_purple	full
42	slot_whitebins	full
42	slot_yellow	empty
41	slot_blue	empty
41	slot_orange	empty
41	slot_purple	empty
41	slot_whitebins	full
41	slot_yellow	empty
40	slot_blue	full
28	slot_blue	full
28	slot_orange	empty
28	slot_purple	empty
28	slot_whitebins	full
28	slot_yellow	empty
27	slot_blue	empty
27	slot_orange	empty
27	slot_purple	empty
27	slot_whitebins	full
27	slot_yellow	empty
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
slot_blue_sensor	empty
slot_yellow_sensor	empty
slot_blue_sensor	full
slot_yellow_sensor	full
slot_orange_sensor	full
slot_orange_sensor	empty
slot_purple_sensor	empty
slot_purple_sensor	full
slot_whitebins_sensor	full
slot_whitebins_sensor	empty
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
3	slot_purple	empty
4	slot_orange	empty
5	slot_whitebins	empty
2	slot_yellow	empty
1	slot_blue	empty
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
1	15
2	15
3	15
4	15
5	15
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	slot_yellow	full
2	slot_blue	full
3	slot_purple	full
4	slot_orange	full
5	slot_whitebins	full
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
slot_blue_sensor	empty	slot_blue	empty	0.99000001
slot_blue_sensor	empty	slot_blue	full	0.0099999998
slot_blue_sensor	full	slot_blue	full	0.99000001
slot_blue_sensor	full	slot_blue	empty	0.0099999998
slot_yellow_sensor	empty	slot_yellow	full	0.0099999998
slot_yellow_sensor	full	slot_yellow	empty	0.0099999998
slot_yellow_sensor	full	slot_yellow	full	0.99000001
slot_yellow_sensor	empty	slot_yellow	empty	0.99000001
slot_orange_sensor	empty	slot_orange	empty	0.99000001
slot_orange_sensor	full	slot_orange	full	0.99000001
slot_orange_sensor	empty	slot_orange	full	0.0099999998
slot_orange_sensor	full	slot_orange	empty	0.0099999998
slot_purple_sensor	full	slot_purple	empty	0.0099999998
slot_purple_sensor	empty	slot_purple	full	0.0099999998
slot_purple_sensor	empty	slot_purple	empty	0.99000001
slot_purple_sensor	full	slot_purple	full	0.99000001
slot_whitebins_sensor	full	slot_whitebins	full	0.99000001
slot_whitebins_sensor	empty	slot_whitebins	empty	0.99000001
slot_whitebins_sensor	full	slot_whitebins	empty	0.0099999998
slot_whitebins_sensor	empty	slot_whitebins	full	0.0099999998
\.


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Data for Name: t_when_is_behaviour_relevant; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_relevant (state_set_id, beh_name, var_name, var_value) FROM stdin;
2	fill_slot_blue	slot_blue	empty
3	fill_slot_purple	slot_purple	empty
4	fill_slot_orange	slot_orange	empty
5	fill_slot_whitebins	slot_whitebins	empty
1	fill_slot_yellow	slot_yellow	empty
\.


--
-- Name: t_abilities4behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_pkey PRIMARY KEY (abil_name, beh_name);


--
-- Name: t_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_pkey PRIMARY KEY (abil_name);


--
-- Name: t_behaviour_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, beh_name);


--
-- Name: t_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviours
    ADD CONSTRAINT t_behaviours_pkey PRIMARY KEY (beh_name);


--
-- Name: t_default_probabilities4abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_pkey PRIMARY KEY (dementia_type, abil_type);


--
-- Name: t_effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id);


--
-- Name: t_env_variables_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_env_variables_values
    ADD CONSTRAINT t_env_variables_values_pkey PRIMARY KEY (var_name, var_value);


--
-- Name: t_iu_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_pkey PRIMARY KEY (row_id, abil_name);


--
-- Name: t_iu_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_pkey PRIMARY KEY (row_id, beh_name);


--
-- Name: t_iu_goals_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_iu_row_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_row
    ADD CONSTRAINT t_iu_row_pkey PRIMARY KEY (row_id);


--
-- Name: t_iu_task_states_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_observations_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_observations_values
    ADD CONSTRAINT t_observations_values_pkey PRIMARY KEY (obs_name, obs_value);


--
-- Name: t_preconditions4effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id, var_name);


--
-- Name: t_rewards_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_pkey PRIMARY KEY (state_set_id, var_name);


--
-- Name: t_rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards
    ADD CONSTRAINT t_rewards_pkey PRIMARY KEY (state_set_id);


--
-- Name: t_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, var_name, var_value);


--
-- Name: t_types_of_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_abilities
    ADD CONSTRAINT t_types_of_abilities_pkey PRIMARY KEY (abil_type);


--
-- Name: t_types_of_dementia_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_dementia
    ADD CONSTRAINT t_types_of_dementia_pkey PRIMARY KEY (dementia_type);


--
-- Name: t_when_is_behaviour_impossible_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_when_is_behaviour_relevant_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_abilities4behaviours_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_abilities4behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_dementia_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_dementia_type_fkey FOREIGN KEY (dementia_type) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_beh_effect_id_fkey FOREIGN KEY (beh_effect_id) REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_state_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_state_set_id_fkey FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

