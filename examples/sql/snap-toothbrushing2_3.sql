--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities (
    abil_name character varying(256) NOT NULL,
    abil_type character varying(256),
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    abil_initial_prob real NOT NULL,
    abil_prompt_cost real NOT NULL,
    CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (((abil_initial_prob >= (0)::double precision) AND (abil_initial_prob <= (1)::double precision))),
    CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK ((abil_prompt_cost >= (0)::double precision)),
    CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (((abil_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_abilities OWNER TO x721demouser;

--
-- Name: t_abilities4behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities4behaviours (
    abil_name character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_abilities4behaviours OWNER TO x721demouser;

--
-- Name: t_behaviour_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviour_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_behaviour_sensor_model OWNER TO x721demouser;

--
-- Name: t_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviours (
    beh_name character varying(256) NOT NULL,
    beh_timeout real DEFAULT 0.0,
    CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (((beh_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C5: non-negative value required" CHECK ((beh_timeout >= (0)::double precision))
);


ALTER TABLE public.t_behaviours OWNER TO x721demouser;

--
-- Name: t_default_probabilities4abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_default_probabilities4abilities (
    dementia_type character varying(256) NOT NULL,
    abil_type character varying(256) NOT NULL,
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_default_probabilities4abilities OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256),
    var_value character varying(256),
    beh_name character varying(256)
);


ALTER TABLE public.t_effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_effects_of_behaviours_beh_effect_id_seq OWNED BY t_effects_of_behaviours.beh_effect_id;


--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_env_variables_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_env_variables_values (
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    var_value_initial_prob real NOT NULL,
    CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (((var_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (((var_value)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (((var_value_initial_prob >= (0)::double precision) AND (var_value_initial_prob <= (1)::double precision)))
);


ALTER TABLE public.t_env_variables_values OWNER TO x721demouser;

--
-- Name: t_iu_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_abilities (
    row_id integer NOT NULL,
    abil_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_abilities OWNER TO x721demouser;

--
-- Name: t_iu_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_behaviours (
    row_id integer NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_behaviours OWNER TO x721demouser;

--
-- Name: t_iu_goals; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_goals (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_goals OWNER TO x721demouser;

--
-- Name: t_iu_row; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_row (
    row_id integer NOT NULL,
    data_row boolean DEFAULT true NOT NULL,
    row_probability real DEFAULT 1.0,
    row_confirmed boolean DEFAULT true NOT NULL,
    CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (((row_probability >= (0)::double precision) AND (row_probability <= (1)::double precision)))
);


ALTER TABLE public.t_iu_row OWNER TO x721demouser;

--
-- Name: t_iu_task_states; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_task_states (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_task_states OWNER TO x721demouser;

--
-- Name: t_observations_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_observations_values (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (((obs_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (((obs_value)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_observations_values OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_preconditions4effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_preconditions4effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNED BY t_preconditions4effects_of_behaviours.beh_effect_id;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards (
    state_set_id integer NOT NULL,
    reward_value real NOT NULL,
    CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK ((reward_value >= (0)::double precision))
);


ALTER TABLE public.t_rewards OWNER TO x721demouser;

--
-- Name: t_rewards_desc; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards_desc (
    state_set_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_rewards_desc OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_desc_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_desc_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_desc_state_set_id_seq OWNED BY t_rewards_desc.state_set_id;


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_state_set_id_seq OWNED BY t_rewards.state_set_id;


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_sensor_model OWNER TO x721demouser;

--
-- Name: t_types_of_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_abilities (
    abil_type character varying(256) NOT NULL,
    CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (((abil_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_abilities OWNER TO x721demouser;

--
-- Name: t_types_of_dementia; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_dementia (
    dementia_type character varying(256) NOT NULL,
    CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (((dementia_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_dementia OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_impossible (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_impossible OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_impossible_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq OWNED BY t_when_is_behaviour_impossible.state_set_id;


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Name: t_when_is_behaviour_relevant; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_relevant (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_relevant OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_relevant_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_relevant_state_set_id_seq OWNED BY t_when_is_behaviour_relevant.state_set_id;


--
-- Name: t_when_is_behaviour_relevant_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_relevant_state_set_id_seq', 1, false);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_preconditions4effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_desc_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_impossible_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_relevant_state_set_id_seq'::regclass);


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost) FROM stdin;
Af_brushing	affordance	0.40000001	0.2	0.94999999	0.001	0.80000001	1
Af_spit	affordance	0.40000001	0.2	0.94999999	0.001	0.80000001	1
Af_give_brush_to_surface	affordance	0.40000001	0.2	0.94999999	0.001	0.80000001	1
Af_put_brush_in_mouth	affordance	0.40000001	0.2	0.94999999	0.001	0.80000001	1
\.


--
-- Data for Name: t_abilities4behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities4behaviours (abil_name, beh_name) FROM stdin;
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
other	1000000
nothing	1000000
put_brush_in_mouth	5
brush_teeth	120
give_brush_to_surface	10
spit	15
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
2	teeth	clean	brush_teeth
3	brush_p	on_surface	give_brush_to_surface
4	mouth_w	empty	spit
1	brush_m	in	put_brush_in_mouth
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
brush_p	IH	1
brush_p	on_surface	0
teeth	dirty	1
teeth	clean	0
mouth_w	empty	1
mouth_w	full	0
brush_m	in	0
brush_m	out	1
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
3	Af_brushing
2	Af_brushing
13	Af_put_brush_in_mouth
12	Af_put_brush_in_mouth
23	Af_spit
22	Af_spit
17	Af_put_brush_in_mouth
16	Af_put_brush_in_mouth
32	Af_give_brush_to_surface
31	Af_give_brush_to_surface
30	Af_spit
29	Af_spit
28	Af_spit
25	Af_brushing
24	Af_brushing
6	Af_put_brush_in_mouth
5	Af_put_brush_in_mouth
10	Af_put_brush_in_mouth
9	Af_put_brush_in_mouth
15	Af_give_brush_to_surface
14	Af_give_brush_to_surface
8	Af_give_brush_to_surface
7	Af_give_brush_to_surface
27	Af_give_brush_to_surface
26	Af_give_brush_to_surface
21	Af_brushing
20	Af_brushing
19	Af_brushing
18	Af_brushing
1	Af_spit
4	Af_spit
11	Af_spit
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
3	brush_teeth
2	brush_teeth
15	give_brush_to_surface
14	give_brush_to_surface
6	put_brush_in_mouth
5	put_brush_in_mouth
10	put_brush_in_mouth
9	put_brush_in_mouth
32	give_brush_to_surface
31	give_brush_to_surface
30	spit
29	spit
28	spit
27	give_brush_to_surface
26	give_brush_to_surface
8	give_brush_to_surface
7	give_brush_to_surface
25	brush_teeth
24	brush_teeth
21	brush_teeth
20	brush_teeth
19	brush_teeth
18	brush_teeth
13	put_brush_in_mouth
12	put_brush_in_mouth
17	put_brush_in_mouth
16	put_brush_in_mouth
23	spit
22	spit
1	spit
4	spit
11	spit
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
3	brush_p	on_surface
3	teeth	clean
3	mouth_w	empty
2	mouth_w	empty
2	brush_p	on_surface
2	teeth	clean
6	brush_p	on_surface
6	mouth_w	empty
6	teeth	clean
5	brush_p	on_surface
5	mouth_w	empty
5	teeth	clean
10	brush_p	on_surface
10	mouth_w	empty
10	teeth	clean
9	brush_p	on_surface
9	mouth_w	empty
9	teeth	clean
8	mouth_w	empty
8	teeth	clean
8	brush_p	on_surface
7	mouth_w	empty
7	teeth	clean
7	brush_p	on_surface
23	teeth	clean
23	mouth_w	empty
23	brush_p	on_surface
22	teeth	clean
22	mouth_w	empty
22	brush_p	on_surface
21	brush_p	on_surface
21	teeth	clean
21	mouth_w	empty
20	brush_p	on_surface
13	brush_p	on_surface
13	mouth_w	empty
13	teeth	clean
12	brush_p	on_surface
12	mouth_w	empty
12	teeth	clean
1	teeth	clean
1	mouth_w	empty
1	brush_p	on_surface
4	teeth	clean
4	mouth_w	empty
4	brush_p	on_surface
11	teeth	clean
11	mouth_w	empty
11	brush_p	on_surface
20	teeth	clean
20	mouth_w	empty
19	brush_p	on_surface
19	teeth	clean
19	mouth_w	empty
18	brush_p	on_surface
18	teeth	clean
18	mouth_w	empty
17	brush_p	on_surface
17	mouth_w	empty
17	teeth	clean
16	teeth	clean
16	brush_p	on_surface
16	mouth_w	empty
15	brush_p	on_surface
15	mouth_w	empty
15	teeth	clean
14	brush_p	on_surface
14	mouth_w	empty
14	teeth	clean
32	brush_p	on_surface
32	mouth_w	empty
32	teeth	clean
31	brush_p	on_surface
31	mouth_w	empty
31	teeth	clean
30	teeth	clean
30	mouth_w	empty
30	brush_p	on_surface
29	teeth	clean
29	mouth_w	empty
29	brush_p	on_surface
28	teeth	clean
28	mouth_w	empty
28	brush_p	on_surface
27	brush_p	on_surface
27	mouth_w	empty
27	teeth	clean
26	brush_p	on_surface
26	mouth_w	empty
26	teeth	clean
25	brush_p	on_surface
25	teeth	clean
25	mouth_w	empty
24	brush_p	on_surface
24	teeth	clean
24	mouth_w	empty
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
2	t	0.5	f
3	t	1	t
11	t	0.5	f
29	t	0.33333334	f
12	t	0.33333334	f
18	t	0.33333334	f
13	t	0.5	f
19	t	0.5	f
14	t	0.25	f
16	t	0.25	f
15	t	0.33333334	f
17	t	0.33333334	f
28	t	0.33333334	f
27	t	0.5	f
4	t	0.5	f
22	t	0.33333334	f
20	t	0.25	f
30	t	0.25	f
21	t	0.33333334	f
5	t	0.5	f
6	t	1	t
23	t	0.5	f
31	t	0.5	f
7	t	0.33333334	f
9	t	0.33333334	f
8	t	0.5	f
10	t	0.5	f
24	t	0.33333334	f
26	t	0.33333334	f
25	t	0.5	f
32	t	1	t
1	t	1	t
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
1	mouth_w	full
1	brush_m	out
1	brush_p	on_surface
1	teeth	dirty
4	mouth_w	full
4	brush_m	in
4	brush_p	on_surface
4	teeth	dirty
3	brush_m	in
3	brush_p	on_surface
3	teeth	dirty
3	mouth_w	empty
2	brush_m	in
2	brush_p	on_surface
2	teeth	dirty
2	mouth_w	full
6	brush_m	out
6	brush_p	IH
6	mouth_w	empty
6	teeth	dirty
5	brush_m	out
5	brush_p	IH
5	mouth_w	full
5	teeth	dirty
11	brush_m	out
11	brush_p	IH
11	mouth_w	full
11	teeth	dirty
10	brush_m	out
10	brush_p	IH
10	mouth_w	empty
10	teeth	clean
9	brush_m	out
9	brush_p	IH
9	mouth_w	full
9	teeth	clean
8	brush_m	out
8	brush_p	IH
8	mouth_w	empty
8	teeth	clean
7	brush_m	out
7	brush_p	IH
7	mouth_w	full
7	teeth	clean
23	brush_m	out
23	brush_p	on_surface
23	mouth_w	full
23	teeth	clean
22	brush_m	in
13	brush_m	in
13	brush_p	IH
13	mouth_w	empty
13	teeth	dirty
12	brush_m	in
12	brush_p	IH
12	mouth_w	full
12	teeth	dirty
22	brush_p	IH
22	mouth_w	full
22	teeth	dirty
21	brush_m	in
21	brush_p	IH
21	mouth_w	empty
21	teeth	clean
20	brush_m	in
20	brush_p	IH
20	mouth_w	full
20	teeth	clean
19	brush_m	in
19	brush_p	IH
19	mouth_w	empty
19	teeth	dirty
18	brush_m	in
18	brush_p	IH
18	mouth_w	full
18	teeth	dirty
17	brush_m	in
17	brush_p	IH
17	mouth_w	empty
17	teeth	clean
16	brush_m	in
16	brush_p	IH
16	mouth_w	full
16	teeth	clean
15	brush_m	in
15	brush_p	IH
15	mouth_w	empty
15	teeth	clean
14	brush_m	in
14	brush_p	IH
14	mouth_w	full
14	teeth	clean
32	brush_m	out
32	brush_p	on_surface
32	mouth_w	empty
32	teeth	clean
31	brush_m	out
31	brush_p	on_surface
31	mouth_w	full
31	teeth	clean
30	brush_m	in
30	brush_p	IH
30	mouth_w	full
30	teeth	clean
29	brush_m	out
29	brush_p	IH
29	mouth_w	full
29	teeth	clean
28	brush_m	in
28	brush_p	on_surface
28	mouth_w	full
28	teeth	clean
27	brush_m	in
27	brush_p	on_surface
27	mouth_w	empty
27	teeth	clean
26	brush_m	in
26	brush_p	on_surface
26	mouth_w	full
26	teeth	clean
25	brush_m	in
25	brush_p	on_surface
25	mouth_w	empty
25	teeth	clean
24	brush_m	in
24	brush_p	on_surface
24	mouth_w	full
24	teeth	clean
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
brush_position	IH
brush_position	on_surface
teeth_condition	clean
teeth_condition	dirty
mouth_water	empty
mouth_water	full
brush_mouth	in
brush_mouth	out
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
1	brush_p	IH
3	teeth	clean
4	mouth_w	full
2	brush_m	in
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
1	15
2	5
3	5
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	teeth	clean
2	mouth_w	empty
3	brush_p	on_surface
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
brush_mouth	in	brush_m	in	0.94999999
brush_mouth	out	brush_m	out	0.94999999
brush_mouth	out	brush_m	in	0.050000001
brush_mouth	in	brush_m	out	0.050000001
brush_position	IH	brush_p	IH	0.94999999
brush_position	on_surface	brush_p	on_surface	0.94999999
brush_position	IH	brush_p	on_surface	0.050000001
brush_position	on_surface	brush_p	IH	0.050000001
teeth_condition	clean	teeth	dirty	0.050000001
teeth_condition	dirty	teeth	clean	0.050000001
teeth_condition	dirty	teeth	dirty	0.94999999
teeth_condition	clean	teeth	clean	0.94999999
mouth_water	empty	mouth_w	empty	0.94999999
mouth_water	empty	mouth_w	full	0.050000001
mouth_water	full	mouth_w	empty	0.050000001
mouth_water	full	mouth_w	full	0.94999999
\.


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Data for Name: t_when_is_behaviour_relevant; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_relevant (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Name: t_abilities4behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_pkey PRIMARY KEY (abil_name, beh_name);


--
-- Name: t_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_pkey PRIMARY KEY (abil_name);


--
-- Name: t_behaviour_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, beh_name);


--
-- Name: t_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviours
    ADD CONSTRAINT t_behaviours_pkey PRIMARY KEY (beh_name);


--
-- Name: t_default_probabilities4abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_pkey PRIMARY KEY (dementia_type, abil_type);


--
-- Name: t_effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id);


--
-- Name: t_env_variables_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_env_variables_values
    ADD CONSTRAINT t_env_variables_values_pkey PRIMARY KEY (var_name, var_value);


--
-- Name: t_iu_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_pkey PRIMARY KEY (row_id, abil_name);


--
-- Name: t_iu_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_pkey PRIMARY KEY (row_id, beh_name);


--
-- Name: t_iu_goals_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_iu_row_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_row
    ADD CONSTRAINT t_iu_row_pkey PRIMARY KEY (row_id);


--
-- Name: t_iu_task_states_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_observations_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_observations_values
    ADD CONSTRAINT t_observations_values_pkey PRIMARY KEY (obs_name, obs_value);


--
-- Name: t_preconditions4effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id, var_name);


--
-- Name: t_rewards_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_pkey PRIMARY KEY (state_set_id, var_name);


--
-- Name: t_rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards
    ADD CONSTRAINT t_rewards_pkey PRIMARY KEY (state_set_id);


--
-- Name: t_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, var_name, var_value);


--
-- Name: t_types_of_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_abilities
    ADD CONSTRAINT t_types_of_abilities_pkey PRIMARY KEY (abil_type);


--
-- Name: t_types_of_dementia_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_dementia
    ADD CONSTRAINT t_types_of_dementia_pkey PRIMARY KEY (dementia_type);


--
-- Name: t_when_is_behaviour_impossible_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_when_is_behaviour_relevant_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_abilities4behaviours_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_abilities4behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities4behaviours
    ADD CONSTRAINT t_abilities4behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_dementia_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_dementia_type_fkey FOREIGN KEY (dementia_type) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_beh_effect_id_fkey FOREIGN KEY (beh_effect_id) REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_state_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_state_set_id_fkey FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_relevant_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_relevant
    ADD CONSTRAINT t_when_is_behaviour_relevant_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

