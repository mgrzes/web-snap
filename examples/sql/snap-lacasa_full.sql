--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: t_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_abilities (
    abil_name character varying(256) NOT NULL,
    abil_type character varying(256),
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    abil_initial_prob real NOT NULL,
    abil_prompt_cost real NOT NULL,
    parent_abil_name character varying(256),
    CONSTRAINT "C10: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C11: the probability has to be in range [0-1]" CHECK (((abil_initial_prob >= (0)::double precision) AND (abil_initial_prob <= (1)::double precision))),
    CONSTRAINT "C25: non-negative value required (costs are minimised)" CHECK ((abil_prompt_cost >= (0)::double precision)),
    CONSTRAINT "C30: ability cannot depend on itself" CHECK (((parent_abil_name)::text <> (abil_name)::text)),
    CONSTRAINT "C6: only alphanumeric characters and the underscore are allowed" CHECK (((abil_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C7: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C8: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C9: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_abilities OWNER TO x721demouser;

--
-- Name: t_behaviour_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviour_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    beh_name character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_behaviour_sensor_model OWNER TO x721demouser;

--
-- Name: t_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_behaviours (
    beh_name character varying(256) NOT NULL,
    beh_timeout real DEFAULT 0.0,
    CONSTRAINT "C4: only alphanumeric characters and the underscore are allowed" CHECK (((beh_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C5: non-negative value required" CHECK ((beh_timeout >= (0)::double precision))
);


ALTER TABLE public.t_behaviours OWNER TO x721demouser;

--
-- Name: t_default_probabilities4abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_default_probabilities4abilities (
    dementia_type character varying(256) NOT NULL,
    abil_type character varying(256) NOT NULL,
    gain_prob real NOT NULL,
    lose_prob real NOT NULL,
    gain_prompt_prob real NOT NULL,
    lose_prompt_prob real NOT NULL,
    CONSTRAINT "C17: the probability has to be in range [0-1]" CHECK (((gain_prob >= (0)::double precision) AND (gain_prob <= (1)::double precision))),
    CONSTRAINT "C18: the probability has to be in range [0-1]" CHECK (((lose_prob >= (0)::double precision) AND (lose_prob <= (1)::double precision))),
    CONSTRAINT "C19: the probability has to be in range [0-1]" CHECK (((gain_prompt_prob >= (0)::double precision) AND (gain_prompt_prob <= (1)::double precision))),
    CONSTRAINT "C20: the probability has to be in range [0-1]" CHECK (((lose_prompt_prob >= (0)::double precision) AND (lose_prompt_prob <= (1)::double precision)))
);


ALTER TABLE public.t_default_probabilities4abilities OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256),
    var_value character varying(256),
    beh_name character varying(256)
);


ALTER TABLE public.t_effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_effects_of_behaviours_beh_effect_id_seq OWNED BY t_effects_of_behaviours.beh_effect_id;


--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_env_variables_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_env_variables_values (
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    var_value_initial_prob real NOT NULL,
    CONSTRAINT "C1: only alphanumeric characters and the underscore are allowed" CHECK (((var_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C2: only alphanumeric characters and the underscore are allowed" CHECK (((var_value)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C3: the probability has to be in range [0-1]" CHECK (((var_value_initial_prob >= (0)::double precision) AND (var_value_initial_prob <= (1)::double precision)))
);


ALTER TABLE public.t_env_variables_values OWNER TO x721demouser;

--
-- Name: t_iu_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_abilities (
    row_id integer NOT NULL,
    abil_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_abilities OWNER TO x721demouser;

--
-- Name: t_iu_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_behaviours (
    row_id integer NOT NULL,
    beh_name character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_behaviours OWNER TO x721demouser;

--
-- Name: t_iu_goals; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_goals (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_goals OWNER TO x721demouser;

--
-- Name: t_iu_row; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_row (
    row_id integer NOT NULL,
    data_row boolean DEFAULT true NOT NULL,
    row_probability real DEFAULT 1.0,
    row_confirmed boolean DEFAULT true NOT NULL,
    CONSTRAINT "C14: the probability in the range [0-1] required" CHECK (((row_probability >= (0)::double precision) AND (row_probability <= (1)::double precision)))
);


ALTER TABLE public.t_iu_row OWNER TO x721demouser;

--
-- Name: t_iu_task_states; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_iu_task_states (
    row_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL
);


ALTER TABLE public.t_iu_task_states OWNER TO x721demouser;

--
-- Name: t_observations_values; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_observations_values (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    CONSTRAINT "C15: only alphanumeric characters and underscore are allowed" CHECK (((obs_name)::text ~* '^[a-z_0-9]+$'::text)),
    CONSTRAINT "C16: only alphanumeric characters and underscore are allowed" CHECK (((obs_value)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_observations_values OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_preconditions4effects_of_behaviours (
    beh_effect_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_preconditions4effects_of_behaviours OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNER TO x721demouser;

--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_preconditions4effects_of_behaviours_beh_effect_id_seq OWNED BY t_preconditions4effects_of_behaviours.beh_effect_id;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards (
    state_set_id integer NOT NULL,
    reward_value real NOT NULL,
    CONSTRAINT "C26: non-negative value required (rewards are maximised)" CHECK ((reward_value >= (0)::double precision))
);


ALTER TABLE public.t_rewards OWNER TO x721demouser;

--
-- Name: t_rewards_desc; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_rewards_desc (
    state_set_id integer NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_rewards_desc OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_desc_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_desc_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_desc_state_set_id_seq OWNED BY t_rewards_desc.state_set_id;


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_rewards_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_rewards_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_rewards_state_set_id_seq OWNED BY t_rewards.state_set_id;


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_sensor_model; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_sensor_model (
    obs_name character varying(256) NOT NULL,
    obs_value character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256) NOT NULL,
    probability real NOT NULL,
    CONSTRAINT "C12: the probability has to be in range [0-1]" CHECK (((probability >= (0)::double precision) AND (probability <= (1)::double precision)))
);


ALTER TABLE public.t_sensor_model OWNER TO x721demouser;

--
-- Name: t_types_of_abilities; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_abilities (
    abil_type character varying(256) NOT NULL,
    CONSTRAINT "C23: only alphanumeric characters and underscore are allowed" CHECK (((abil_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_abilities OWNER TO x721demouser;

--
-- Name: t_types_of_dementia; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_types_of_dementia (
    dementia_type character varying(256) NOT NULL,
    CONSTRAINT "C24: only alphanumeric characters and underscore are allowed" CHECK (((dementia_type)::text ~* '^[a-z_0-9]+$'::text))
);


ALTER TABLE public.t_types_of_dementia OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible; Type: TABLE; Schema: public; Owner: x721demouser; Tablespace: 
--

CREATE TABLE t_when_is_behaviour_impossible (
    state_set_id integer NOT NULL,
    beh_name character varying(256) NOT NULL,
    var_name character varying(256) NOT NULL,
    var_value character varying(256)
);


ALTER TABLE public.t_when_is_behaviour_impossible OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE; Schema: public; Owner: x721demouser
--

CREATE SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.t_when_is_behaviour_impossible_state_set_id_seq OWNER TO x721demouser;

--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x721demouser
--

ALTER SEQUENCE t_when_is_behaviour_impossible_state_set_id_seq OWNED BY t_when_is_behaviour_impossible.state_set_id;


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: beh_effect_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours ALTER COLUMN beh_effect_id SET DEFAULT nextval('t_preconditions4effects_of_behaviours_beh_effect_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc ALTER COLUMN state_set_id SET DEFAULT nextval('t_rewards_desc_state_set_id_seq'::regclass);


--
-- Name: state_set_id; Type: DEFAULT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible ALTER COLUMN state_set_id SET DEFAULT nextval('t_when_is_behaviour_impossible_state_set_id_seq'::regclass);


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost, parent_abil_name) FROM stdin;
know_path_to_known	recall_step	0.1	0.5	0.89999998	0.1	0.5	2	rn_known_location
rn_known_location	recognition	0.30000001	0.2	0.95999998	0.15000001	0.5	2	\N
know_path_home	recall_step	0.1	0.69999999	0.30000001	0.5	0.5	4	\N
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
wander	1000
go_home	1000
go_to_known	1000
other	1000
nothing	1000
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
1	at_home	no	wander
2	at_home	yes	go_home
3	in_known_location	yes	go_to_known
4	known_location_close	yes	wander
5	known_location_close	yes	go_to_known
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
at_home	yes	1
at_home	no	0
in_known_location	yes	1
in_known_location	no	0
known_location_close	no	0
known_location_close	yes	1
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
1	rn_known_location
2	know_path_home
3	know_path_to_known
4	know_path_home
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
1	go_to_known
2	go_home
3	go_to_known
4	go_home
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
2	at_home	yes
3	at_home	yes
1	at_home	yes
4	at_home	yes
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
1	t	1	t
2	t	1	t
3	t	1	t
4	t	1	t
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
1	at_home	no
1	in_known_location	no
1	known_location_close	yes
2	at_home	no
2	in_known_location	no
2	known_location_close	no
3	at_home	no
3	in_known_location	no
3	known_location_close	no
4	at_home	no
4	in_known_location	yes
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
location	home
location	close_to_home
location	far_from_home
at_known_location	yes
at_known_location	no
known_loc	close
known_loc	far
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
3	known_location_close	yes
4	known_location_close	no
5	known_location_close	no
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
2	7
1	20
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	at_home	yes
2	in_known_location	yes
2	at_home	no
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
location	home	at_home	yes	0.94999999
location	home	at_home	no	0.050000001
location	close_to_home	at_home	yes	0.039999999
location	far_from_home	at_home	yes	0.0099999998
location	far_from_home	at_home	no	0.40000001
location	close_to_home	at_home	no	0.55000001
at_known_location	yes	in_known_location	yes	0.99000001
at_known_location	no	in_known_location	yes	0.0099999998
at_known_location	no	in_known_location	no	1
at_known_location	yes	in_known_location	no	0
known_loc	close	known_location_close	yes	0.89999998
known_loc	far	known_location_close	yes	0.1
known_loc	far	known_location_close	no	0.89999998
known_loc	close	known_location_close	no	0.1
\.


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- Name: t_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_pkey PRIMARY KEY (abil_name);


--
-- Name: t_behaviour_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, beh_name);


--
-- Name: t_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_behaviours
    ADD CONSTRAINT t_behaviours_pkey PRIMARY KEY (beh_name);


--
-- Name: t_default_probabilities4abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_pkey PRIMARY KEY (dementia_type, abil_type);


--
-- Name: t_effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id);


--
-- Name: t_env_variables_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_env_variables_values
    ADD CONSTRAINT t_env_variables_values_pkey PRIMARY KEY (var_name, var_value);


--
-- Name: t_iu_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_pkey PRIMARY KEY (row_id, abil_name);


--
-- Name: t_iu_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_pkey PRIMARY KEY (row_id, beh_name);


--
-- Name: t_iu_goals_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_iu_row_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_row
    ADD CONSTRAINT t_iu_row_pkey PRIMARY KEY (row_id);


--
-- Name: t_iu_task_states_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_pkey PRIMARY KEY (row_id, var_name, var_value);


--
-- Name: t_observations_values_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_observations_values
    ADD CONSTRAINT t_observations_values_pkey PRIMARY KEY (obs_name, obs_value);


--
-- Name: t_preconditions4effects_of_behaviours_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_pkey PRIMARY KEY (beh_effect_id, var_name);


--
-- Name: t_rewards_desc_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_pkey PRIMARY KEY (state_set_id, var_name);


--
-- Name: t_rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_rewards
    ADD CONSTRAINT t_rewards_pkey PRIMARY KEY (state_set_id);


--
-- Name: t_sensor_model_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_pkey PRIMARY KEY (obs_name, obs_value, var_name, var_value);


--
-- Name: t_types_of_abilities_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_abilities
    ADD CONSTRAINT t_types_of_abilities_pkey PRIMARY KEY (abil_type);


--
-- Name: t_types_of_dementia_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_types_of_dementia
    ADD CONSTRAINT t_types_of_dementia_pkey PRIMARY KEY (dementia_type);


--
-- Name: t_when_is_behaviour_impossible_pkey; Type: CONSTRAINT; Schema: public; Owner: x721demouser; Tablespace: 
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_pkey PRIMARY KEY (state_set_id, beh_name, var_name);


--
-- Name: t_abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_abilities_parent_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_abilities
    ADD CONSTRAINT t_abilities_parent_abil_name_fkey FOREIGN KEY (parent_abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_behaviour_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_behaviour_sensor_model
    ADD CONSTRAINT t_behaviour_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_abil_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_abil_type_fkey FOREIGN KEY (abil_type) REFERENCES t_types_of_abilities(abil_type) ON UPDATE CASCADE;


--
-- Name: t_default_probabilities4abilities_dementia_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_default_probabilities4abilities
    ADD CONSTRAINT t_default_probabilities4abilities_dementia_type_fkey FOREIGN KEY (dementia_type) REFERENCES t_types_of_dementia(dementia_type) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_effects_of_behaviours
    ADD CONSTRAINT t_effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_abil_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_abil_name_fkey FOREIGN KEY (abil_name) REFERENCES t_abilities(abil_name) ON UPDATE CASCADE;


--
-- Name: t_iu_abilities_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_abilities
    ADD CONSTRAINT t_iu_abilities_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_iu_behaviours_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_behaviours
    ADD CONSTRAINT t_iu_behaviours_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_goals_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_goals
    ADD CONSTRAINT t_iu_goals_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_row_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_row_id_fkey FOREIGN KEY (row_id) REFERENCES t_iu_row(row_id) ON UPDATE CASCADE;


--
-- Name: t_iu_task_states_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_iu_task_states
    ADD CONSTRAINT t_iu_task_states_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_beh_effect_id_fkey FOREIGN KEY (beh_effect_id) REFERENCES t_effects_of_behaviours(beh_effect_id) ON UPDATE CASCADE;


--
-- Name: t_preconditions4effects_of_behaviours_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_preconditions4effects_of_behaviours
    ADD CONSTRAINT t_preconditions4effects_of_behaviours_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_state_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_state_set_id_fkey FOREIGN KEY (state_set_id) REFERENCES t_rewards(state_set_id) ON UPDATE CASCADE;


--
-- Name: t_rewards_desc_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_rewards_desc
    ADD CONSTRAINT t_rewards_desc_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_obs_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_obs_name_fkey FOREIGN KEY (obs_name, obs_value) REFERENCES t_observations_values(obs_name, obs_value) ON UPDATE CASCADE;


--
-- Name: t_sensor_model_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_sensor_model
    ADD CONSTRAINT t_sensor_model_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_beh_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_beh_name_fkey FOREIGN KEY (beh_name) REFERENCES t_behaviours(beh_name) ON UPDATE CASCADE;


--
-- Name: t_when_is_behaviour_impossible_var_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x721demouser
--

ALTER TABLE ONLY t_when_is_behaviour_impossible
    ADD CONSTRAINT t_when_is_behaviour_impossible_var_name_fkey FOREIGN KEY (var_name, var_value) REFERENCES t_env_variables_values(var_name, var_value) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

