--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: t_effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_preconditions4effects_of_behaviours_beh_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_preconditions4effects_of_behaviours_beh_effect_id_seq', 1, false);


--
-- Name: t_rewards_desc_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_desc_state_set_id_seq', 1, false);


--
-- Name: t_rewards_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_rewards_state_set_id_seq', 1, false);


--
-- Name: t_when_is_behaviour_impossible_state_set_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x721demouser
--

SELECT pg_catalog.setval('t_when_is_behaviour_impossible_state_set_id_seq', 1, false);


--
-- Data for Name: t_types_of_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_abilities (abil_type) FROM stdin;
recognition
affordance
recall_step
\.


--
-- Data for Name: t_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_abilities (abil_name, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob, abil_initial_prob, abil_prompt_cost, parent_abil_name) FROM stdin;
Rn_tap_off	recognition	0.5	0.1	0.94999999	0.050000001	0.94999999	1	\N
Rn_tap_on	recognition	0.5	0.1	0.94999999	0.050000001	0.94999999	1	\N
Af_alter_hands_c_to_clean	affordance	0.40000001	0.2	0.94999999	0.050000001	0.80000001	0.5	\N
Af_alter_hands_c_to_soapy	affordance	0.40000001	0.2	0.94999999	0.050000001	0.80000001	0.5	\N
Af_alter_hands_w_to_dry	affordance	0.40000001	0.2	0.94999999	0.050000001	0.80000001	0.5	\N
Af_hw_yes	affordance	0.40000001	0.2	0.94999999	0.050000001	0.80000001	0.55000001	\N
\.


--
-- Data for Name: t_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviours (beh_name, beh_timeout) FROM stdin;
other	1000000
nothing	1000000
turn_on_tap	10
turn_off_tap	10
rinse_hands	10
lather_hands	10
dry_hands	10
\.


--
-- Data for Name: t_observations_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_observations_values (obs_name, obs_value) FROM stdin;
tap_sensor	on
tap_sensor	off
hands_c_sensor	dirty
hands_c_sensor	soapy
hands_c_sensor	clean
hands_w_sensor	dry
hands_w_sensor	wet
\.


--
-- Data for Name: t_behaviour_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_behaviour_sensor_model (obs_name, obs_value, beh_name, probability) FROM stdin;
\.


--
-- Data for Name: t_types_of_dementia; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_types_of_dementia (dementia_type) FROM stdin;
light
medium
severe
\.


--
-- Data for Name: t_default_probabilities4abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_default_probabilities4abilities (dementia_type, abil_type, gain_prob, lose_prob, gain_prompt_prob, lose_prompt_prob) FROM stdin;
light	recognition	0.80000001	0.2	0.99000001	0.0099999998
light	affordance	0.80000001	0.2	0.99000001	0.0099999998
light	recall_step	0.80000001	0.2	0.99000001	0.0099999998
medium	recognition	0.69999999	0.30000001	0.99000001	0.0099999998
medium	affordance	0.69999999	0.30000001	0.99000001	0.0099999998
medium	recall_step	0.60000002	0.40000001	0.99000001	0.0099999998
severe	recognition	0.60000002	0.40000001	0.69999999	0.30000001
severe	affordance	0.60000002	0.40000001	0.69999999	0.30000001
severe	recall_step	0.5	0.5	0.69999999	0.30000001
\.


--
-- Data for Name: t_env_variables_values; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_env_variables_values (var_name, var_value, var_value_initial_prob) FROM stdin;
tap	on	0
tap	off	1
hands_w	dry	1
hands_w	wet	0
hands_c	soapy	0
hands_c	clean	0
hands_c	dirty	1
hw	yes	0
hw	no	1
\.


--
-- Data for Name: t_effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_effects_of_behaviours (beh_effect_id, var_name, var_value, beh_name) FROM stdin;
1	tap	on	turn_on_tap
2	hands_c	soapy	lather_hands
3	hands_c	clean	rinse_hands
4	hands_w	wet	rinse_hands
5	tap	off	turn_off_tap
6	hands_w	dry	dry_hands
7	hw	yes	finish_handwashing
\.


--
-- Data for Name: t_iu_row; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_row (row_id, data_row, row_probability, row_confirmed) FROM stdin;
1	t	1	t
4	t	1	t
5	t	1	t
6	t	1	t
9	t	1	t
10	t	1	t
2	t	0.34999999	t
3	t	0.64999998	t
7	t	0.2	t
8	t	0.80000001	t
\.


--
-- Data for Name: t_iu_abilities; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_abilities (row_id, abil_name) FROM stdin;
1	Af_alter_hands_c_to_soapy
2	Af_alter_hands_c_to_soapy
5	Af_alter_hands_c_to_clean
4	Rn_tap_off
3	Rn_tap_off
10	Af_hw_yes
9	Rn_tap_on
8	Rn_tap_on
7	Af_alter_hands_w_to_dry
6	Af_alter_hands_w_to_dry
\.


--
-- Data for Name: t_iu_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_behaviours (row_id, beh_name) FROM stdin;
1	lather_hands
2	lather_hands
5	rinse_hands
4	turn_on_tap
3	turn_on_tap
10	finish_handwashing
9	turn_off_tap
8	turn_off_tap
7	dry_hands
6	dry_hands
\.


--
-- Data for Name: t_iu_goals; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_goals (row_id, var_name, var_value) FROM stdin;
1	hands_c	clean
1	hw	yes
2	hands_c	clean
2	hw	yes
5	hw	yes
5	hands_c	clean
4	hw	yes
4	hands_c	clean
3	hands_c	clean
3	hw	yes
10	hands_c	clean
10	hw	yes
9	hands_c	clean
9	hw	yes
8	hands_c	clean
8	hw	yes
7	hw	yes
7	hands_c	clean
6	hw	yes
6	hands_c	clean
\.


--
-- Data for Name: t_iu_task_states; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_iu_task_states (row_id, var_name, var_value) FROM stdin;
1	hands_c	dirty
1	tap	on
2	hands_c	dirty
2	tap	off
5	hands_c	soapy
5	tap	on
4	tap	off
4	hands_c	soapy
3	hands_c	dirty
3	tap	off
10	hands_w	dry
10	hands_c	clean
10	tap	off
9	tap	on
9	hands_c	clean
9	hands_w	dry
8	tap	on
8	hands_c	clean
8	hands_w	wet
7	hands_c	clean
7	hands_w	wet
7	tap	on
6	hands_c	clean
6	hands_w	wet
6	tap	off
\.


--
-- Data for Name: t_preconditions4effects_of_behaviours; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_preconditions4effects_of_behaviours (beh_effect_id, var_name, var_value) FROM stdin;
1	tap	off
2	hands_c	dirty
3	hands_c	soapy
4	hands_c	soapy
3	tap	on
4	tap	on
5	hands_c	clean
5	tap	on
6	hands_w	wet
6	hands_c	clean
7	hands_c	clean
7	hands_w	dry
7	tap	off
\.


--
-- Data for Name: t_rewards; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards (state_set_id, reward_value) FROM stdin;
1	15
2	3
\.


--
-- Data for Name: t_rewards_desc; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_rewards_desc (state_set_id, var_name, var_value) FROM stdin;
1	hw	yes
2	hands_c	clean
\.


--
-- Data for Name: t_sensor_model; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_sensor_model (obs_name, obs_value, var_name, var_value, probability) FROM stdin;
tap_sensor	on	tap	on	0.94999999
tap_sensor	on	tap	off	0.050000001
tap_sensor	off	tap	off	0.94999999
tap_sensor	off	tap	on	0.050000001
hw_sensor	no	hw	no	0.94999999
hw_sensor	no	hw	yes	0.050000001
hw_sensor	yes	hw	yes	0.94999999
hw_sensor	yes	hw	no	0.050000001
hands_w_sensor	wet	hands_w	wet	0.94999999
hands_w_sensor	wet	hands_w	dry	0.050000001
hands_w_sensor	dry	hands_w	dry	0.94999999
hands_w_sensor	dry	hands_w	wet	0.050000001
hands_c_sensor	clean	hands_c	clean	0.89999998
hands_c_sensor	clean	hands_c	dirty	0.050000001
hands_c_sensor	clean	hands_c	soapy	0.050000001
hands_c_sensor	dirty	hands_c	soapy	0.050000001
hands_c_sensor	dirty	hands_c	clean	0.050000001
hands_c_sensor	dirty	hands_c	dirty	0.89999998
hands_c_sensor	soapy	hands_c	soapy	0.89999998
hands_c_sensor	soapy	hands_c	clean	0.050000001
hands_c_sensor	soapy	hands_c	dirty	0.050000001
\.


--
-- Data for Name: t_when_is_behaviour_impossible; Type: TABLE DATA; Schema: public; Owner: x721demouser
--

COPY t_when_is_behaviour_impossible (state_set_id, beh_name, var_name, var_value) FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

